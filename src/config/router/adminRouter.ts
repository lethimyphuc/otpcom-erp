export type RouteData = {
  Link: string
  Name: string
  Icon: string
  isDynamic?: boolean
  childrens: RouteData[]
}

export const adminRouter: RouteData[] = [
  {
    Link: '/accessories',
    Name: 'Nhóm phụ kiện',
    Icon: 'fas fa-tachometer-alt-fast',
    childrens: [],
  },
  {
    Link: '/account',
    Name: 'Thông tin tài khoản',
    Icon: '',
    childrens: [
      {
        Link: '/account/address',
        Name: 'Sổ địa chỉ',
        Icon: '',
        childrens: [],
      },
      {
        Link: '/account/change-password',
        Name: 'Thay đổi mật khẩu',
        Icon: '',
        childrens: [],
      },
      {
        Link: '/account/orders',
        Name: 'Lịch sử đơn hàng',
        Icon: '',
        childrens: [],
      },
    ],
  },
  {
    Link: '/bill-of-lading',
    Name: 'Tra vận đơn',
    Icon: 'fas fa-tachometer-alt-fast',
    childrens: [],
  },
  {
    Link: '/brand',
    Name: 'Thương hiệu',
    Icon: 'fas fa-tachometer-alt-fast',
    childrens: [],
  },
  {
    Link: '/car-company',
    Name: 'Hãng xe',
    Icon: 'fas fa-tachometer-alt-fast',
    childrens: [
      {
        Link: '/car-company/kia',
        Name: 'KIA',
        Icon: '/icons/kia.svg',
        childrens: [],
      },
      {
        Link: '/car-company/toyota',
        Name: 'Toyota',
        Icon: '/icons/toyota.svg',
        childrens: [],
      },
      {
        Link: '/car-company/hyundai',
        Name: 'Hyundai Accent 2021',
        Icon: '/icons/hyundai.svg',
        childrens: [],
      },
      {
        Link: '/car-company/general-motors',
        Name: 'General Motors',
        Icon: '/icons/gm.svg',
        childrens: [
          {
            Link: '/car-company/kia',
            Name: 'KIA',
            Icon: '/icons/kia.svg',
            childrens: [
              {
                Link: '/car-company/hyundai',
                Name: 'Hyundai Accent 2021',
                Icon: '/icons/hyundai.svg',
                childrens: [
                  {
                    Link: '/car-company/kia',
                    Name: 'KIA',
                    Icon: '/icons/kia.svg',
                    childrens: [],
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
  },
  {
    Link: '/category',
    Name: 'Hãng xe',
    Icon: 'fas fa-tachometer-alt-fast',
    childrens: [
      // {
      //   Link: '/category',
      //   Name: 'Hãng xe',
      //   isDynamic: true,
      //   Icon: 'fas fa-tachometer-alt-fast',
      //   childrens: [],
      // },
    ],
  },
  {
    Link: '/diary',
    Name: 'Nhật ký',
    Icon: 'fas fa-tachometer-alt-fast',
    childrens: [
      // {
      //   Link: '/diary/[]',
      //   Name: 'Nhật ký',
      //   isDynamic: true,
      //   Icon: 'fas fa-tachometer-alt-fast',
      //   childrens: [],
      // },
    ],
  },
  {
    Link: '/forgot-password',
    Name: 'Quên mật khẩu',
    Icon: 'fas fa-tachometer-alt-fast',
    childrens: [],
  },
  {
    Link: '/group-accessory',
    Name: 'Nhóm phụ tùng',
    Icon: 'fas fa-tachometer-alt-fast',
    childrens: [],
  },
  {
    Link: '/login',
    Name: 'Đăng nhập',
    Icon: 'fas fa-tachometer-alt-fast',
    childrens: [],
  },
  {
    Link: '/product',
    Name: 'Sản phẩm',
    Icon: 'fas fa-tachometer-alt-fast',
    childrens: [],
  },
  {
    Link: '/product-detail',
    isDynamic: true,
    Name: 'Sản phẩm',
    Icon: 'fas fa-tachometer-alt-fast',
    childrens: [],
  },
  {
    Link: '/promotion',
    Name: 'Khuyến mãi',
    Icon: 'fas fa-tachometer-alt-fast',
    childrens: [],
  },
  {
    Link: '/resetpassword',
    Name: 'Đặt lại mật khẩu',
    Icon: 'fas fa-tachometer-alt-fast',
    childrens: [],
  },
  {
    Link: '/shopping-cart',
    Name: 'Giỏ hàng',
    Icon: 'fas fa-tachometer-alt-fast',
    childrens: [],
  },
]
