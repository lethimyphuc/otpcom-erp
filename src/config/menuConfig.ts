export const listMenu = [
  // {
  //   Link: "/",
  //   Name: "",
  //   Id: 1,
  // },
  {
    Link: "/customers/member-ship",
    Name: " Thành viên VIP",
    Id: 2,
  },
  {
    Link: "/customers",
    Name: "Khách hàng",
    Id: 3,
  },
  {
    Link: "/customers/customers-care",
    Name: "Chăm sóc khách hàng",
    Id: 4,
  },
  {
    Link: "/staff",
    Name: "Nhân viên",
    Id: 5,
  },
  {
    Link: "/staff/permission",
    Name: "Phân quyền",
    Id: 6,
  },
  {
    Name: "Lịch sử",
    Link: "/staff/operation-history",
    Id: 7,
  },
  {
    Name: "Cửa hàng",
    Link: "/general-setting/store",
    Id: 8,
  },
  {
    Name: "Quầy",
    Link: "/general-setting/stalls",
    Id: 9,
  },
  {
    Name: "Nhà cung cấp",
    Link: "/general-setting/supplier",
    Id: 10,
  },
  {
    Name: "Khuyến mãi",
    Link: "/general-setting/promotion",
    Id: 11,
  },
  {
    Name: "Chiết khấu",
    Link: "/general-setting/discount",
    Id: 12,
  },
  {
    Name: "Phí thu đổi",
    Link: "/general-setting/exchange-fee",
    Id: 13,
  },
  {
    Name: "Tạo tem",
    Link: "/general-setting/stamp",
    Id: 14,
  },
  {
    Name: "Nhóm loại hàng",
    Link: "/general-setting-jewelry/commodity-group",
    Id: 15,
  },
  {
    Name: "Loại nữ trang",
    Link: "/general-setting-jewelry/jewelry-shape",
    Id: 16,
  },
  {
    Name: "Kiểu dáng nữ trang",
    Link: "/general-setting-jewelry/jewelry-design",
    Id: 17,
  },
  {
    Name: "Loại vàng",
    Link: "/general-setting-jewelry/kind-of-gold",
    Id: 18,
  },
  {
    Name: "Hình dáng viên",
    Link: "/general-setting-diamond/pellet-shape",
    Id: 19,
  },
  {
    Name: "Loại kiểm định",
    Link: "/general-setting-diamond/type-accreditation",
    Id: 20,
  },
  {
    Name: "Màu sắc",
    Link: "/general-setting-diamond/colors",
    Id: 21,
  },
  {
    Name: "Độ sạch",
    Link: "/general-setting-diamond/configure-cleanliness",
    Id: 22,
  },
  {
    Name: "Giác cắt",
    Link: "/general-setting-diamond/sectional-configuration",
    Id: 23,
  },
  {
    Name: "Độ bóng",
    Link: "/general-setting-diamond/gloss-configuration",
    Id: 24,
  },
  {
    Name: "Đối xứng",
    Link: "/general-setting-diamond/symmetrical-configuration",
    Id: 25,
  },
  {
    Name: "Phát quang",
    Link: "/general-setting-diamond/luminescence-configuration",
    Id: 26,
  },
  {
    Name: "Danh sách cầm đồ",
    Link: "/pawn/list",
    Id: 27,
  },
  {
    Name: "Cấu hình lãi suất",
    Link: "/pawn/interest-rate-setting",
    Id: 28,
  },
  {
    Name: "Lập biên nhận",
    Link: "/pawn/create-receipt",
    Id: 29,
  },
  {
    Name: "Danh sách phiên gia hạn",
    Link: "/pawn/extension-session",
    Id: 30,
  },
  {
    Name: "Danh sách thanh lý",
    Link: "/pawn/pawn-liquidation",
    Id: 31,
  },
  {
    Name: "Danh sách nhận đồ",
    Link: "/pawn/pick-up-session",
    Id: 32,
  },
  {
    Name: "Đơn hàng",
    Link: "/order/order-list",
    Id: 33,
  },
  {
    Name: "Đơn đặt hàng",
    Link: "/order/custom-order-list",
    Id: 34,
  },
  {
    Name: "Ban hàng",
    Link: "/order/sales",
    Id: 35,
  },
  {
    Name: "Thu đổi hàng",
    Link: "/order/exchange-management",
    Id: 36,
  },
  {
    Name: "Đặt hàng",
    Link: "/order/reserve",
    Id: 37,
  },
  {
    Name: "Lưu thông tin khách",
    Link: "/order/save-customer-info",
    Id: 38,
  },
  {
    Name: "Danh sách thông tin khách",
    Link: "/order/customer-info-list",
    Id: 39,
  },
  // {
  //   Name: "Thanh toán và hoàn tiền",
  //   Link: "/order/payment-refund",
  //   Id: 40,
  // },
  {
    Name: "Nhập nữ trang",
    Link: "/products/jewerlry",
    Id: 41,
  },
  {
    Name: "Nhập hột xoàn",
    Link: "/products/diamond",
    Id: 42,
  },
  {
    Name: "Danh sách hột xoàn",
    Link: "/products/diamond-list",
    Id: 43,
  },
  {
    Name: "Danh sách nữ trang",
    Link: "/products/jewerlry-list",
    Id: 44,
  },
  {
    Name: "Danh sách đợi duyệt hột xoàn",
    Link: "/products/pending-diamond",
    Id: 45,
  },
  {
    Name: "Danh sách đợi duyệt nữ trang",
    Link: "/products/pending-jewerlry",
    Id: 46,
  },
  {
    Name: "Loại giao dịch",
    Link: "/invoice/setting",
    Id: 47,
  },
  {
    Name: "Tạo phiếu",
    Link: "/invoice/create",
    Id: 48,
  },
  {
    Name: "Danh sách thu chi",
    Link: "/invoice/list",
    Id: 49,
  },
  {
    Name: "Tạo phiên xuất kho",
    Link: "/warehouse/create-delivery",
    Id: 50,
  },
  {
    Name: "Danh sách phiên xuất kho",
    Link: "/warehouse/delivery-list",
    Id: 51,
  },
  {
    Name: "Tạo phiên chuyển kho",
    Link: "/warehouse/create-transaction",
    Id: 52,
  },
  {
    Name: "Danh sách phiên chuyển kho",
    Link: "/warehouse/transaction-list",
    Id: 53,
  },
  {
    Name: "Tạo phiên nhập kho",
    Link: "/warehouse/create-reception",
    Id: 54,
  },
  {
    Name: "Danh sách nhập kho",
    Link: "/warehouse/receipt-list",
    Id: 55,
  },
  {
    Name: "Danh sách kho",
    Link: "/warehouse/list",
    Id: 56,
  },
  {
    Name: "Báo cáo - thống kê",
    Link: "/statistic-report/turnover",
    Id: 57,
  },
  {
    Name: "Kiểm kê sản phẩm",
    Link: "/inventory/product-inventory",
    Id: 58,
  },
  {
    Name: "Kiểm kê cầm đồ",
    Link: "/inventory/pawn-inventory",
    Id: 59,
  },
  {
    Name: "Hàng ngoài chờ xử lý",
    Link: "/products/external-goods",
    Id: 60,
  },
  {
    Name: "Chăm sóc khách hàng",
    Link: "/customers/customers-care",
    Id: 61,
  },
  {
    Name: "Bảo hành",
    Link: "/insurance",
    Id: 62,
  },
  {
    Name: "Tạo phiếu bảo hành",
    Link: "/insurance/create-insurance",
    Id: 63,
  },
  {
    Name: "",
    Link: "/statistic-report/business-index",
    Id: 64,
  },
  {
    Name: "",
    Link: "/statistic-report/customer-report",
    Id: 65,
  },
  {
    Name: "Kiểm kê chi tiết cầm đồ",
    Link: "/inventory/pawn-detail-inventory",
    Id: 66,
  },
  {
    Name: "Kiểm kê chi tiết sản phẩm",
    Link: "/inventory/product-detail-inventory",
    Id: 67,
  },
  {
    Name: "Báo cáo chăm sóc khách hàng",
    Link: " /statistic-report/customer-care",
    Id: 68,
  },
  {
    Name: "Báo cáo chỉ số kinh doanh",
    Link: "/statistic-report/business-index",
    Id: 69,
  },
  {
    Name: "Báo cáo sản phẩm và cầm đồ",
    Link: "/statistic-report/sale-and-pawn",
    Id: 70,
  },
  {
    Name: "Báo cáo hao hụt",
    Link: "/statistic-report/loss-report",
    Id: 71,
  },
  {
    Name: "Báo cáo tồn kho",
    Link: "/statistic-report/existing-import",
    Id: 72,
  },
  {
    Name: "Báo cáo nữ trang",
    Link: "/statistic-report/report-jewelry",
    Id: 73,
  },
  {
    Name: "Báo cáo hột xoàn",
    Link: "/statistic-report/report-diamond",
    Id: 74,
  },
  {
    Name: "Báo cáo tài sản",
    Link: "/statistic-report/property-report",
    Id: 75,
  },
  {
    Name: "Báo cáo nhân viên",
    Link: "/statistic-report/report-staff",
    Id: 76,
  },
  {
    Name: "Dòng tiền cầm đồ",
    Link: "/statistic-report/report-pawn-money",
    Id: 77,
  },
  {
    Name: "Doanh thu khách hàng",
    Link: "/statistic-report/customer-revenue",
    Id: 78,
  },
  {
    Name: "Đơn hàng đang giao",
    Link: "/statistic-report/order-pending",
    Id: 79,
  },
  {
    Name: "Đơn hàng đợi xuất kho",
    Link: "/statistic-report/order-pending-export",
    Id: 80,
  },
  {
    Name: "Mã vận đơn",
    Link: "/order/shipper-list",
    Id: 81,
  },
];
