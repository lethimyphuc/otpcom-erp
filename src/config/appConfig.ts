export const config = {
  PRODUCTION: '' || process.env.PRODUCTION,
  DEVELOPMENT: '' || process.env.DEVELOPMENT,
  API_URL: '' || process.env.NEXT_PUBLIC_API_URL,
  ENV: process.env.NODE_ENV
}

export const regex = {
  email: /[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/,
  number: /^[0-9]+$/,
  numberAndWord: /^[a-zA-Z 0-9_.+-]+$/,
  numbersWithCommas: /^\d+(\,\d+)*$/g,
  PHONE: /((^(\+84|84|0|0084){1})(3|5|7|8|9))+([0-9]{8})$/,
  PASSWORD: /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
  BASE64: /^([0-9a-zA-Z+/]{4})*(([0-9a-zA-Z+/]{2}==)|([0-9a-zA-Z+/]{3}=))?$/,
  BASE64_READER: /^data:image\/\w+;base64,/,
  EMAIL: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
  HTTP: /^([A-Za-z\-]+):\s*(.*)$/,
  HTML: /<[a-z][\s\S]*>/i
}

export const defaultPagination = {
  current: 1,
  pageSize: 20,
  total: 0
}

export const defaultPaginationSmall = {
  current: 1,
  pageSize: 5,
  total: 0
}

export const defaultSorter = {
  field: 'Id',
  order: 'descend'
} as any

export enum ETypeGender {
  male = 1,
  Female = 2,
  unGender = 3
}

export const TypeProductTypes = [
  {
    id: ETypeGender.male,
    name: 'Nữ'
  },
  {
    id: ETypeGender.Female,
    name: 'Nam'
  },
  {
    id: ETypeGender.unGender,
    name: 'Khác'
  }
]

export enum ETypeGuaranteeOption {
  unGuarantee = 0,
  month1 = 1,
  month2 = 2,
  month3 = 3,
  month6 = 4,
  month12 = 5,
  month24 = 6,
  month36 = 7
}

export const GuaranteeOption = [
  {
    id: ETypeGuaranteeOption.unGuarantee,
    name: 'Không bảo hành'
  },
  {
    id: ETypeGuaranteeOption.month1,
    name: 'Bảo hành 1 tháng'
  },
  {
    id: ETypeGuaranteeOption.month2,
    name: 'Bảo hành 2 tháng'
  },
  {
    id: ETypeGuaranteeOption.month3,
    name: 'Bảo hành 3 tháng'
  },
  {
    id: ETypeGuaranteeOption.month6,
    name: 'Bảo hành 6 tháng'
  },
  {
    id: ETypeGuaranteeOption.month12,
    name: 'Bảo hành 12 tháng'
  },
  {
    id: ETypeGuaranteeOption.month24,
    name: 'Bảo hành 24 tháng'
  },
  {
    id: ETypeGuaranteeOption.month36,
    name: 'Bảo hành 36 tháng'
  }
]

export enum ETypeProductOption {
  simple = 'simple',
  configurable = 'configurable'
}

export const TypeProductOption = [
  {
    id: ETypeProductOption.simple,
    name: 'Đơn giản'
  }
  // {
  //   id: ETypeProductOption.configurable,
  //   name: "Biến thể",
  // },
]

export enum ETypeTrackOption {
  simple = 'custom'
}

export const TrackOption = [
  {
    id: ETypeTrackOption.simple,
    name: 'Custom'
  }
]

export enum ETypeTransferOption {
  pending = 1,
  approved = 2,
  done = 3,
  unapproved = 0
}

export const TransferOption = [
  {
    id: ETypeTransferOption.pending,
    name: 'pending'
  },
  {
    id: ETypeTransferOption.approved,
    name: 'approved'
  },
  {
    id: ETypeTransferOption.done,
    name: 'done'
  },
  {
    id: ETypeTransferOption.unapproved,
    name: 'unapproved'
  }
]

export const stockSource = [
  {
    stock: 10,
    source: 'KH-2',
    store: 4
  },
  {
    stock: 11,
    source: 'C-TB',
    store: 5
  },
  {
    stock: 12,
    source: 'a-ch',
    store: 6
  }
]

export const listStatusOrder = [
  {
    status: 'pending',
    name: 'Chưa giải quyết'
  },
  {
    status: 'processing',
    name: 'Xử lý'
  },
  {
    status: 'complete',
    name: 'Hoàn thành'
  },
  {
    status: 'success',
    name: 'Thành công'
  },
  {
    status: 'fraud',
    name: 'Gian lận'
  },
  {
    status: 'holded',
    name: 'Tạm giữ'
  },
  {
    status: 'closed',
    name: 'Đóng lại'
  }
]
