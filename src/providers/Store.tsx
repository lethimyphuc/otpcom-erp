import React, { createContext, useReducer } from "react";

const Reducer = (state: any, action: any) => {
  console.log("action: ", action);
  switch (action?.type) {
    case "SELECT_ITEM_CART":
      return {
        ...state,
        itemCart: action?.payload,
      };
    default:
      return state;
  }
};

const initialState = {
  itemCart: [] as any,
} as any;

export const Context = createContext(initialState);

const Store = ({ children }: { children: any }) => {
  const [state, dispatch] = useReducer(Reducer, initialState);
  return (
    <Context.Provider value={[state, dispatch]}>{children}</Context.Provider>
  );
};

export default Store;
