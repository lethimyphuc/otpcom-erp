import { useEffect, useState } from "react";

type TSession = {
  token: any;
  userInfo: any;
  menu: any;
  store: any;
};

export const useSession = () => {
  const [session, setSession] = useState<TSession>({
    token: null,
    userInfo: null,
    menu: null,
    store: null,
  });

  useEffect(() => {
    try {
      const token = localStorage.getItem("token");

      const rawUserInfo = localStorage.getItem("userInfo");

      const userInfo = JSON.parse(rawUserInfo as string);

      const menu = JSON.parse(localStorage.getItem("menu") as string);

      const store = JSON.parse(localStorage.getItem("store") as string);

      setSession({ userInfo, menu, store, token });
    } catch (error) {
      console.log(error);
    }
  }, []);

  // Biến đếm để theo dõi sự thay đổi của localStorage.getItem("store")
  const [storeChangeCount, setStoreChangeCount] = useState(0);

  // Kiểm tra sự thay đổi của localStorage.getItem("store") bằng cách so sánh giá trị hiện tại với giá trị trước đó
  useEffect(() => {
    const previousStoreValue = localStorage.getItem("store");

    const checkStoreChange = () => {
      const currentStoreValue = localStorage.getItem("store");

      if (currentStoreValue !== previousStoreValue) {
        setStoreChangeCount((prevCount) => prevCount + 1);
      }
    };

    const storeChangeInterval = setInterval(checkStoreChange, 1000); // Kiểm tra sự thay đổi mỗi giây

    return () => {
      clearInterval(storeChangeInterval); // Hủy kiểm tra sự thay đổi khi component bị hủy
    };
  }, []);

  // Cập nhật session khi có sự thay đổi của localStorage.getItem("store")
  useEffect(() => {
    try {
      const store = JSON.parse(localStorage.getItem("store") as string);

      setSession((prevSession) => ({
        ...prevSession,
        store: store,
      }));
    } catch (error) {
      console.log(error);
    }
  }, [storeChangeCount]);

  return session;
};
