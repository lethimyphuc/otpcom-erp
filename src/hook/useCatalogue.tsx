import { useQuery } from "react-query";
// import { catalogue } from "../api/catalogue";
import { toast } from "../components";
import { useState } from "react";
import { TablePaginationConfig } from "antd";
import { defaultPagination } from "~src/config/appConfig";
import { user } from "~src/api/user";
import axios from "axios";
// import {} from "~src/api";

// thời gian sẽ api loại 1 lần // 5 là số phút + 6 * 10000 là 1 phút  =  5 phút
const staleTime = 5 * 6 * 10000;
// bắt đầu thực thi từ thời gian
const initialDataUpdatedAt = new Date().getTime();
// api error

type TProps = {
  userEnabled?: boolean;
};

export const useCatalogue = ({ userEnabled = false }: TProps) => {
  const [pagination, setPagination] =
    useState<TablePaginationConfig>(defaultPagination);
  const onError = toast.error;

  const { data: userData } = useQuery(
    ["userData"],
    async () =>
      await user
        .getListUsers({ currentPage: 1, pageSize: 9999 })
        .then((res) => res),
    {
      staleTime,
      initialDataUpdatedAt,
      onError,
      enabled: userEnabled,
    }
  );

  //HANDLE PROVINCE

  // const Province = useQuery(
  //   ["ProvinceData"],
  //   async () =>
  //     await axios.get("https://provinces.open-api.vn/api/").then((res) => {
  //       res.data.map((item: any) => (item.id = item?.code));
  //       return res.data;
  //     }),
  //   {
  //     staleTime,
  //     initialDataUpdatedAt,
  //     onError,
  //   }
  // );

  return {
    userData: userData?.items,
  } as const;
};
