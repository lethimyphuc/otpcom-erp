import { useQuery } from "react-query";
import { GET_CATEGORIE_DETAIL } from "~src/api/categories";
import {graphQLClient, graphQLClientGET} from "~src/api/graphql";

export const useCategorieDetailContent = ({
  key,
  uid,
}: {
  key: string;
  uid: string;
}) => {
  return useQuery({
    queryKey: [key],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_CATEGORIE_DETAIL, {
        eq: uid,
      })) as TCategories;
      return data.categories?.items[0];
    },
    enabled: !!uid,
  });
};
