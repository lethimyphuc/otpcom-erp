import { useQuery } from "react-query";
import { GET_CATEGORIES } from "~src/api/categories";
import {graphQLClient, graphQLClientGET} from "~src/api/graphql";

export const useCategoriesContent = ({
  key,
  uid,
}: {
  key: string;
  uid: string;
}) => {
  return useQuery({
    queryKey: [key],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_CATEGORIES, {
        eq: uid,
      })) as TCategories;
      return data.categories?.items[0]?.children;
    },
    enabled: !!uid,
  });
};
