import { useQuery } from "react-query";
import { GET_CATEGORIES, GET_CATEGORIES_BY_SLUG } from "~src/api/categories";
import {graphQLClient, graphQLClientGET} from "~src/api/graphql";

export const useCategoriesContentBySlug = ({
  key,
  slug,
}: {
  key: string;
  slug: string;
}) => {
  return useQuery({
    queryKey: [key],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_CATEGORIES_BY_SLUG, {
        eq: slug,
      })) as TCategories;
      return data.categories?.items[0]?.children ?? null;
    },
    enabled: !!slug,
  });
};
