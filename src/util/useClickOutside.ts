import { useEffect } from "react";

export default function useClickOutside(
  ref: any,
  handler: (bool: boolean) => void
) {
  useEffect(() => {
    const listener = (event: any) => {
      const headerEl = ref[0]?.current;
      const el = ref[1]?.current;

      if (!headerEl || headerEl.contains(event.target)) {
        return;
      }
      if (!el || el.contains(event.target)) {
        return;
      }

      handler(event);
    };

    document.addEventListener("mousedown", listener);
    document.addEventListener("touchstart", listener);

    return () => {
      document.removeEventListener("mousedown", listener);
      document.removeEventListener("touchstart", listener);
    };
  }, [ref, handler]);
}
