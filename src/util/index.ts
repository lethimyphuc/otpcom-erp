import { SorterResult } from 'antd/lib/table/interface'
import { UploadFile } from 'antd/lib/upload/interface'
import jwt_decode from 'jwt-decode'
import moment from 'moment'
import { GroupBase, StylesConfig, ThemeConfig } from 'react-select'
import { toast } from '~src/components'
// @ts-ignore
// import VNnum2words from "vn-num2words";
import { getText } from 'number-to-text-vietnamese'
import axios from 'axios'
// import { baseFile } from "~/api";

class Format {
  // format date
  getVNDate = (date: Date, format: string = 'DD/MM/YYYY h:mm:ss A') => moment(date).format(format)

  getShortVNDate = (date: Date) => moment(date).format('DD/MM/YYYY')

  getBase64 = (img: any, callback: any) => {
    if (img) {
      const reader = new FileReader()
      reader.addEventListener('load', () => callback(reader.result))
      reader.readAsDataURL(img)
    }
  }

  // lấy thông tin từ token
  getJWTDecode = (
    token: string
  ): {
    exp: number
    'http://schemas.microsoft.com/ws/2008/06/identity/claims/userdata': string
    iat: number
    nbf: number
  } => jwt_decode(token)

  addUrlForImage = (imageName?: string) =>
    !imageName ? imageName : process.env.NEXT_PUBLIC_IMAGE_URL + imageName

  getNameImage = (url?: string): string | undefined => url?.split('/Temp/')[1]

  // kiểm tra có phải là số hay không
  isNumber = (val: string) => {
    if (val.match(/^-?[0-9]\d*([,.]\d+)?$/)) return true
    return false
  }

  // format ngày giờ

  converseDateTime = (val: number | any) => {
    return val == 0 ? '' : moment(val * 1000.0).format('DD/MM/YYYY HH:mm:ss')
  }

  converseDate = (val: number | any) => {
    return moment(val * 1000.0).format('DD/MM/YYYY')
  }

  converseDay = (val: number | any) => {
    return moment(val * 1000.0).format('DD')
  }

  converseYear = (val: number | any) => {
    return moment(val * 1000.0).format('YYYY')
  }

  // chuyển ngày giờ về dạng dãy số
  converseDateNumber = (val: string | any) => {
    const newD = Math.floor(new Date(val).getTime() / 1000.0)
    return newD
  }

  //
  converseStringToNumber = (val: number | undefined) => {
    return val == undefined ? 0 : val
  }

  // Format chuỗi string về DD/MM/YYYY
  formatDate = (inputDate: string) => {
    const dateTimeParts = inputDate?.split(' ')
    if (dateTimeParts?.length >= 1) {
      const datePart = dateTimeParts[0]
      const dateParts = datePart?.split('-')

      if (dateParts.length === 3) {
        const [year, month, day] = dateParts
        return `${day}-${month}-${year}`
      }
    }

    return inputDate
  }

  // format tiền việt nam
  getVND = (price: number, suffix: string = '') =>
    (price?.toString() || '0').replace(/\B(?=(\d{3})+(?!\d))/g, ',') + ' ' + suffix

  // format phần trăm
  getPercent = (price: number, suffix: string = ' %') => (price?.toString() || '0') + suffix

  // format sorter table antd
  getCurrentSorter = <T>(sorter: SorterResult<T>) => {
    return sorter.field + ' ' + (sorter.order === 'ascend' ? 'asc' : 'desc')
  }
  customThemes: ThemeConfig | undefined
  customStyles: StylesConfig<unknown, boolean, GroupBase<unknown>> | undefined
  previewImage: ((file: UploadFile<any>) => void) | undefined

  converseMoneyToString = (value: number) => {
    return (
      getText(value || 0)
        .charAt(0)
        .toUpperCase() + getText(value || 0).slice(1)
    )
  }

  beforeUpload = (
    file: any,
    fileType: string | string[],
    mb: number = 2,
    messsageFileType: string = 'Đây không là hình ảnh',
    messsageFileMB: string = 'Hình ảnh phải nhỏ hơn 2mb'
  ): boolean => {
    let isJpgOrPng = false
    if (typeof fileType === 'string') {
      isJpgOrPng = file.type === fileType
    } else {
      isJpgOrPng = fileType.some((i) => i === file.type)
    }
    if (!isJpgOrPng) {
      toast.error({ title: 'Lỗi upload file', message: messsageFileType })
    }
    const isLt2M = file.size / 1024 / 1024 < mb
    if (!isLt2M) {
      toast.error({ title: 'Lỗi upload file', message: messsageFileMB })
    }
    return isJpgOrPng && isLt2M
  }

  searchContent = (fields: [], value: string, conditionType?: string) => {
    let result = ''
    fields.map((item, index) => {
      const itemFields = `searchCriteria[filterGroups][0][filters][${index}][field]=${item}`
      const itemValue = `searchCriteria[filterGroups][0][filters][${index}][value]=%\\${value}`
      const itemConditionType = `searchCriteria[filterGroups][0][filters][${index}][conditionType]=${
        conditionType || 'like'
      }`
      result += `&${itemFields}&${itemValue}%&${itemConditionType}`
    })

    return result
  }

  converStringToString = (str: string) => {
    str = str?.toLowerCase()
    str = str?.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a')
    str = str?.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e')
    str = str?.replace(/ì|í|ị|ỉ|ĩ/g, 'i')
    str = str?.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o')
    str = str?.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u')
    str = str?.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y')
    str = str?.replace(/đ/g, 'd')
    str = str?.replace(
      /!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g,
      '-'
    )
    /* tìm và thay thế các kí tự đặc biệt trong chuỗi sang kí tự - */
    str = str?.replace(/-+-/g, '-') //thay thế 2- thành 1-
    str = str?.replace(/^\-+|\-+$/g, '')
    //cắt bỏ ký tự - ở đầu và cuối chuỗi
    return str
  }

  converseRemoveVietnameseAccents = (str: string) => {
    str = str?.replaceAll(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a')
    str = str?.replaceAll(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e')
    str = str?.replaceAll(/ì|í|ị|ỉ|ĩ/g, 'i')
    str = str?.replaceAll(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o')
    str = str?.replaceAll(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u')
    str = str?.replaceAll(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y')
    str = str?.replaceAll(/đ/g, 'd')

    return str
  }

  parseJwt(token: string) {
    const base64Url = token.split('.')[1]
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/')
    const buf = Buffer.from(base64, 'base64').toString('utf-8')
    const payload = JSON.parse(buf)
    return {
      userInfo: JSON.parse(
        payload['http://schemas.microsoft.com/ws/2008/06/identity/claims/userdata']
      ),
      exp: payload.exp,
    }
  }

  removeCommas(str: any) {
    return typeof str == 'string' ? str.replace(/,/g, '') : str
  }
  sumArray(arr: any) {
    return arr.reduce((item: any, a: any) => item + a, 0)
  }
  calculateTimeDifference(timestamp1: number, timestamp2: number) {
    // Tính thời gian chênh lệch giữa hai timestamp
    const timeDiff = Math.abs(timestamp1 - timestamp2)

    // Chuyển đổi thời gian chênh lệch thành giờ, phút và giây
    const seconds = Math.floor(timeDiff) % 60
    const minutes = Math.floor(timeDiff / 60) % 60
    const hours = Math.floor(timeDiff / (60 * 60)) % 24
    const days = Math.floor(timeDiff / (60 * 60 * 24))

    // Trả về kết quả dưới dạng một đối tượng chứa các giá trị
    const total = `${!!days ? days + ' ngày' : ''} ${!!hours ? hours + ' giờ' : ''} ${
      !!minutes ? minutes + ' phút' : ''
    } ${!!seconds ? seconds + ' giây' : ''} `
    return total
  }
}

export const _format = new Format()

export function removeEmptyValueFromObject<T>(obj: T extends object ? object : any): T {
  return Object.fromEntries(
    Object.entries(obj).filter(([_, v]) => v !== null && v !== undefined && v !== '')
  ) as T
}
