import { gql } from 'graphql-request'
import { graphQLClient } from '../graphql'

export const GET_LIST_CATALOG = gql`
  query catalogues(
    $filter: CatalogueFilterInput
    $sort: CatalogueSortInput
    $currentPage: Int
    $pageSize: Int
  ) {
    catalogues(filter: $filter, sort: $sort, currentPage: $currentPage, pageSize: $pageSize) {
      items {
        catalogue_image
        category_id
        created_at
        id
        meta_description
        meta_keyword
        meta_title
        name
        points
        status
        updated_at
        url_key
      }
      page_info {
        current_page
        page_size
        total_pages
      }
      total_count
    }
  }
`
export const GET_CATALOG_URL_KEY = gql`
  query catalogueUrlkey($url_key: String!) {
    catalogueUrlkey(url_key: $url_key) {
      catalogue_image
      category_id
      created_at
      id
      meta_description
      meta_keyword
      meta_title
      name

      products {
        items {
          __typename
          sku
          uid
          id
          name
          url_key
          stock_status
          image {
            url
          }
          price_range {
            maximum_price {
              regular_price {
                ...MoneyFields
              }
              discount {
                amount_off
                percent_off
              }
              final_price {
                ...MoneyFields
              }
            }
            minimum_price {
              regular_price {
                ...MoneyFields
              }
              discount {
                amount_off
                percent_off
              }
              final_price {
                ...MoneyFields
              }
            }
          }
        }
      }
      points
      status
      updated_at
      url_key
    }
  }

  fragment MoneyFields on Money {
    currency
    value
  }
`
export const UPDATE_CATALOG = gql`
  mutation updateCatalogue($input: CatalogueCreateInput!) {
    updateCatalogue(input: $input) {
      id
      name
      catalogue_image
      status
      points
      category_id
      url_key
      meta_keyword
      meta_description
      meta_title
      created_at
      updated_at
    }
  }
`
export const GET_MENU_CATALOG = `
query CatalogueCategories($filter: CatalogueCategoryFilterInput) {
    catalogueCategories(filter: $filter) {
        ...CatalogueCategoryField
        breadcrumbs {
            ...CatalogueBreadcrumbsFields
        }
        catalogues {
            ...CatalogueFields
        }
        children {
            ...CatalogueCategoryField
            breadcrumbs {
                ...CatalogueBreadcrumbsFields
            }
            children {
                ...CatalogueCategoryField
                breadcrumbs {
                    ...CatalogueBreadcrumbsFields
                }
            }
        }
    }
}

fragment CatalogueCategoryField on CatalogueCategory {
    id
    path
    meta_description
    meta_keywords
    meta_title
    name
    url_key
    image
    image_icon_menu
    image_banner
    image_car
    tags {
        id
        name
        display_name
    }
}

fragment CatalogueBreadcrumbsFields on Breadcrumb {
    category_id
    category_level
    category_name
    category_uid
    category_url_key
    category_url_path
}

fragment CatalogueFields on CatalogueField {
    catalogue_image
    category_id
    created_at
    id
    meta_description
    meta_keyword
    meta_title
    name
    points
    status
    updated_at
    url_key
}
`

export const getCatalogDetail = async () => {
  return await graphQLClient.request(GET_CATALOG_URL_KEY, {
    url_key: 'test',
  })
}

export const getMenuCatalog = async (params: IOptionsCatalogProps) => {
  const { filter } = params

  try {
    return await graphQLClient
      .request(GET_MENU_CATALOG, {
        filter,
      })
      .then((res: any) => {
        return res?.catalogueCategories as ICatalogMenu[]
      })
  } catch (err) {
    console.log('Error when getting categories: ', err)
    return null
  }
}
