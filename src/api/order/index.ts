import { gql } from "graphql-request";

export const SET_SHIPPING_ADDRESS_NO_SELECT_CUSTOMER_ADDRESS = gql`
  mutation setShippingAddressNoSelectCustomerAddress(
    $input: SetShippingAddressesOnCartInput
  ) {
    setShippingAddressesOnCart(input: $input) {
      cart {
        billing_address {
          city
          company
          country {
            ...CartAddressCountryFields
          }
          firstname
          lastname
          postcode
          region {
            ...CartAddressRegionFields
          }
          street
          telephone
          uid
          vat_id
        }
        shipping_addresses {
          cart_items_v2 {
            errors {
              code
              message
            }
          }
          city
          company
          country {
            ...CartAddressCountryFields
          }
          customer_notes
          firstname
          lastname
          pickup_location_code
          postcode
          region {
            ...CartAddressRegionFields
          }
          selected_shipping_method {
            amount {
              ...MoneyFields
            }
            base_amount {
              ...MoneyFields
            }
            carrier_code
            carrier_title
            method_code
            method_title
            price_excl_tax {
              ...MoneyFields
            }
            price_incl_tax {
              ...MoneyFields
            }
          }
          street
          telephone
          uid
          vat_id
        }
      }
    }
  }

  fragment CartAddressCountryFields on CartAddressCountry {
    code
    label
  }

  fragment CartAddressRegionFields on CartAddressRegion {
    code
    label
    region_id
  }

  fragment MoneyFields on Money {
    currency
    value
  }
`;

export const SET_INFO_ORDER = gql`
  mutation SetGuestShipping(
    $cartId: String!
    $email: String!
    $address: CartAddressInput!
  ) {
    setGuestEmailOnCart(input: { cart_id: $cartId, email: $email }) {
      cart {
        id
      }
    }
    setShippingAddressesOnCart(
      input: { cart_id: $cartId, shipping_addresses: [{ address: $address }] }
    ) {
      cart {
        id
        ...ShippingInformationFragment
        ...ShippingMethodsCheckoutFragment
        ...PriceSummaryFragment
        ...AvailablePaymentMethodsFragment
      }
    }
  }
  fragment ShippingInformationFragment on Cart {
    id
    email
    shipping_addresses {
      city
      country {
        code
        label
      }
      firstname
      lastname
      postcode
      region {
        code
        label
        region_id
      }
      street
      telephone
    }
  }
  fragment ShippingMethodsCheckoutFragment on Cart {
    id
    ...AvailableShippingMethodsCheckoutFragment
    ...SelectedShippingMethodCheckoutFragment
    shipping_addresses {
      country {
        code
      }
      postcode
      region {
        code
      }
      street
    }
  }
  fragment AvailableShippingMethodsCheckoutFragment on Cart {
    id
    shipping_addresses {
      available_shipping_methods {
        amount {
          currency
          value
        }
        available
        carrier_code
        carrier_title
        method_code
        method_title
      }
      street
    }
  }
  fragment SelectedShippingMethodCheckoutFragment on Cart {
    id
    shipping_addresses {
      selected_shipping_method {
        amount {
          currency
          value
        }
        carrier_code
        method_code
        method_title
      }
      street
    }
  }
  fragment PriceSummaryFragment on Cart {
    id
    items {
      uid
      quantity
    }
    ...ShippingSummaryFragment
    prices {
      ...TaxSummaryFragment
      ...DiscountSummaryFragment
      ...GrandTotalFragment
      subtotal_excluding_tax {
        currency
        value
      }
      subtotal_including_tax {
        currency
        value
      }
    }
  }
  fragment DiscountSummaryFragment on CartPrices {
    discounts {
      amount {
        currency
        value
      }
      label
    }
  }

  fragment GrandTotalFragment on CartPrices {
    grand_total {
      currency
      value
    }
  }
  fragment ShippingSummaryFragment on Cart {
    id
    shipping_addresses {
      selected_shipping_method {
        amount {
          currency
          value
        }
      }
      street
    }
  }
  fragment TaxSummaryFragment on CartPrices {
    applied_taxes {
      amount {
        currency
        value
      }
    }
  }
  fragment AvailablePaymentMethodsFragment on Cart {
    id
    available_payment_methods {
      code
      title
    }
  }
`;

export const SET_PAYMENT_METHOD_ON_CART = gql`
  mutation setPaymentMethodOnCart($input: SetPaymentMethodOnCartInput) {
    setPaymentMethodOnCart(input: $input) {
      cart {
        selected_payment_method {
          code
          purchase_order_number
          title
        }
      }
    }
  }
`;

export const SET_GUEST_EMAIL_ON_CART = gql`
  mutation setGuestEmailOnCart($input: SetGuestEmailOnCartInput) {
    setGuestEmailOnCart(input: $input) {
      cart {
        email
      }
    }
  }
`;

export const SET_SHIPPING_METHOD_ON_CART = gql`
  mutation setShippingMethodsOnCart($input: SetShippingMethodsOnCartInput) {
    setShippingMethodsOnCart(input: $input) {
      cart {
        shipping_addresses {
          selected_shipping_method {
            carrier_code
            carrier_title
            method_code
            method_title
            amount {
              value
              currency
            }
          }
        }
      }
    }
  }
`;

export const SET_BILLING_ADDRESS_ON_CART = gql`
  mutation setBillingAddressOnCart($input: SetBillingAddressOnCartInput) {
    setBillingAddressOnCart(input: $input) {
      cart {
        billing_address {
          firstname
          lastname
          company
          street
          city
          region {
            code
            label
          }
          postcode
          telephone
          country {
            code
            label
          }
        }
      }
    }
  }
`;

export const FRAGMENT_CART_FIELD = gql`
  fragment CartFields on Cart {
    id
    email
  }
`
export const QUERY_PLACE_ORDER = gql`
    setShippingAddressesOnCart(input: $inputSetShippingAddresses) {
      cart {
        ...CartFields
      }
    }
    setBillingAddressOnCart(input: $inputSetBillingAddress) {
      cart {
        ...CartFields
      }
    }
    setShippingMethodsOnCart(input: $inputSetShippingMethods) {
      cart {
        ...CartFields
      }
    }
    setPaymentMethodOnCart(input: $inputSetPaymentMethod) {
      cart {
        selected_payment_method {
          code
          title
        }
        ...CartFields
        applied_coupons{
           code
        }
        total_quantity
        prices{
            grand_total {
              currency
              value
            }   
        }
      }
    }
    placeOrder(input: $inputPlaceOrder) {
      order {
        order_number
      }
    }
`
export const PLACE_ORDER_GUEST = gql`
  mutation PlaceOrderGuest(
    $inputSetGuestEmailOnCart: SetGuestEmailOnCartInput
    $inputSetShippingAddresses: SetShippingAddressesOnCartInput
    $inputSetBillingAddress: SetBillingAddressOnCartInput
    $inputSetShippingMethods: SetShippingMethodsOnCartInput
    $inputSetPaymentMethod: SetPaymentMethodOnCartInput
    $inputPlaceOrder: PlaceOrderInput
  ) {
    setGuestEmailOnCart(input: $inputSetGuestEmailOnCart) {
      cart{
        ...CartFields
      }
    }
    ${QUERY_PLACE_ORDER}
  }
  
  ${FRAGMENT_CART_FIELD}
`
export const PLACE_ORDER_CUSTOMER = gql`
  mutation PlaceOrderCustomer(
    $inputSetShippingAddresses: SetShippingAddressesOnCartInput
    $inputSetBillingAddress: SetBillingAddressOnCartInput
    $inputSetShippingMethods: SetShippingMethodsOnCartInput
    $inputSetPaymentMethod: SetPaymentMethodOnCartInput
    $inputPlaceOrder: PlaceOrderInput
  ){
    ${QUERY_PLACE_ORDER}
  }
  ${FRAGMENT_CART_FIELD}
`;

// export const CRETAE_REPLACE_ORDER = gql`
//   mutation FlowPlaceOrder(
//     $inputSetShippingAddresses: SetShippingAddressesOnCartInput
//     $inputSetBillingAddress: SetBillingAddressOnCartInput
//     $inputSetShippingMethods: SetShippingMethodsOnCartInput
//     $inputSetPaymentMethod: SetPaymentMethodOnCartInput
//     $inputPlaceOrder: PlaceOrderInput
//   ) {
//     setShippingAddressesOnCart(input: $inputSetShippingAddresses) {
//       cart {
//         ...CartFields
//       }
//     }
//     setBillingAddressOnCart(input: $inputSetBillingAddress) {
//       cart {
//         ...CartFields
//       }
//     }
//     setShippingMethodsOnCart(input: $inputSetShippingMethods) {
//       cart {
//         ...CartFields
//       }
//     }
//     setPaymentMethodOnCart(input: $inputSetPaymentMethod) {
//       cart {
//         selected_payment_method {
//           code
//           title
//         }
//         ...CartFields
//       }
//     }
//     placeOrder(input: $inputPlaceOrder) {
//       order {
//         order_number
//       }
//     }
//   }

//   fragment CartFields on Cart {
//     id
//     email
//   }
// `;
