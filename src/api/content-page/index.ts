import { gql } from 'graphql-request'

export const GET_CONTENT_HOME_PAGE = gql`
  query getContentPage($identifiers: [String]) {
    snowdogMenus(identifiers: $identifiers) {
      items {
        identifier
        title
        nodes {
          items {
            ...SnowdogMenuNodeInterfaceFields
            ... on SnowdogMenuCustomUrlNodeInterface {
              target
            }
            ... on SnowdogMenuNodeImageFieldInterface {
              image
              image_alt_text
            }
            ... on SnowdogMenuNodeContentFieldInterface {
              content
            }
          }
        }
      }
    }
  }

  fragment SnowdogMenuNodeInterfaceFields on SnowdogMenuNodeInterface {
    additional_data
    classes
    creation_time
    level
    menu_id
    node_id
    node_template
    parent_id
    position
    submenu_template
    title
    type
    update_time
  }
`

export const GET_BANNER_PAGE = gql`
  query getBanner($identifier: String) {
    managebanner(identifier: $identifier) {
      banner_id
      title
      identifier
      image
      creation_time
      update_time
    }
  }
`
