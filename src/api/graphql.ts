import { GraphQLClient, RequestMiddleware, ResponseMiddleware } from 'graphql-request'
import { toast } from 'react-toastify'

const requestMiddleware: RequestMiddleware = async (request) => {
  return request
}

const responseMiddleware: ResponseMiddleware = (response: any) => {
  if (!(response instanceof Error) && response.errors) {
    // const traceId = response.headers.get(`x-b3-trace-id`) || `unknown`
    // console.error(
    //   `[${traceId}] Request error:
    //     status ${response.status}
    //     details: ${response.errors.map((_: any) => _.message).join(`, `)}`
    // )
    const errorMessage = response?.errors[0]?.message
    const errorPath = response?.errors[0]?.path?.[0]
    if (
      response.errors[0]?.extensions?.category === 'graphql-authorization' &&
      errorPath !== 'generateCustomerToken' &&
      errorPath !== 'cart'
    ) {
      localStorage.removeItem('token')
      localStorage.removeItem('cartId')
      localStorage.removeItem('loginSocial')
      window.location.replace(`${window.location.origin}/login`)
    }
    if (response.errors[0]?.extensions?.category === 'graphql-input') return
    if (errorMessage && errorPath !== 'cart' && errorPath !== 'addProductsToCart') {
      toast.error(errorMessage)
      // throw Error(errorMessage)
    }
  }
  return response
}

const graphQLClient = new GraphQLClient(`${process.env.NEXT_PUBLIC_API_URL}`, {
  requestMiddleware,
  responseMiddleware,
  errorPolicy: 'all',
})

if (typeof window !== 'undefined') {
  graphQLClient.setHeaders({
    Authorization:
      localStorage && localStorage.getItem('token')
        ? 'Bearer ' + localStorage.getItem('token')
        : '',
    'Content-Type': 'application/json',
    accept: 'application/json',
    store:
      localStorage && localStorage.getItem('language')
        ? localStorage.getItem('language') || 'vi'
        : '',
  })
}

const graphQLClientGET = new GraphQLClient(`${process.env.NEXT_PUBLIC_API_URL}`, {
  requestMiddleware,
  responseMiddleware,
  errorPolicy: 'all',
  method: 'GET',
})

if (typeof window !== 'undefined') {
  graphQLClient.setHeaders({
    Authorization:
      localStorage && localStorage.getItem('token')
        ? 'Bearer ' + localStorage.getItem('token')
        : '',
    'Content-Type': 'application/json',
    accept: 'application/json',
    store:
      localStorage && localStorage.getItem('language')
        ? localStorage.getItem('language') || 'vi'
        : '',
  })
}

export const setTokenGraphQL = (token: string) => {
  graphQLClient.setHeaders({
    Authorization: `Bearer ${token}`,
    'Content-Type': 'application/json',
    accept: 'application/json',
  })
}

export { graphQLClient, graphQLClientGET }
