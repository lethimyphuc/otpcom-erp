import { gql } from "graphql-request";

export const APPLY_COUPON_TO_CART = gql`
mutation applyCouponToCart($input: ApplyCouponToCartInput) {
  applyCouponToCart(input: $input) {
    cart {
      applied_coupons {
        code
      }
      email
      id
      is_virtual
      items {
        quantity
        uid
        product {
          sku
          name
        }
        prices {
          price {
            ...MoneyFields
          }
        }
      }
      prices {
        grand_total {
          ...MoneyFields
        }
        discounts {
          amount {
            ...MoneyFields
          }
          label
        }
      }
      total_quantity
    }
  }
}

fragment MoneyFields on Money {
  currency
  value
}
`;

export const REMOVE_COUPON_TO_CART = gql`
mutation removeCouponToCart($input: RemoveCouponFromCartInput) {
  removeCouponFromCart(input: $input) {
    cart {
      prices {
        grand_total {
          ...MoneyFields
        }
        discounts {
          amount {
            ...MoneyFields
          }
          label
        }
      }
      total_quantity
    }
  }
}

fragment MoneyFields on Money {
  currency
  value
}
`
