import { gql } from 'graphql-request'

export const CREATE_EMPTY_CART = gql`
  mutation createEmptyCart($input: createEmptyCartInput) {
    createEmptyCart(input: $input)
  }
`

export const GET_CART = gql`
  query getCart($cart_id: String!) {
    cart(cart_id: $cart_id) {
      total_quantity
      items {
        uid
        quantity
        prices {
          discounts {
            amount {
              value
              currency
            }
            ...DiscountFields
          }
          price {
            value
            currency
          }
          price_including_tax {
            value
            currency
          }
          row_total {
            value
            currency
          }
          row_total_including_tax {
            value
            currency
          }
          total_item_discount {
            value
            currency
          }
        }
        product {
          image {
            url
          }
          price_range {
            maximum_price {
              final_price {
                value
                currency
              }
              regular_price {
                value
                currency
              }
            }
            minimum_price {
              final_price {
                value
                currency
              }
              regular_price {
                value
                currency
              }
            }
          }
          ...ProductInterfaceFields
        }
      }
      applied_coupons {
        code
      }
      available_payment_methods {
        code
        is_deferred
        title
      }
      shipping_addresses {
        available_shipping_methods {
          amount {
            ...MoneyFields
          }
          method_title
          carrier_code
          carrier_title
          available
          method_code
        }
      }
      billing_address {
        city
        company
        country {
          ...CartAddressCountryFields
        }
        customer_notes
        firstname
        lastname
        postcode
        region {
          ...CartAddressRegionFields
        }
        street
        telephone
        uid
        vat_id
      }
      gift_message {
        from
        message
        to
      }
      prices {
        discounts {
          amount {
            value
            currency
          }
          label
        }
        applied_taxes {
          amount {
            ...MoneyFields
          }
          label
        }
        grand_total {
          ...MoneyFields
        }
        subtotal_excluding_tax {
          ...MoneyFields
        }
        subtotal_including_tax {
          ...MoneyFields
        }
        subtotal_with_discount_excluding_tax {
          ...MoneyFields
        }
      }
      selected_payment_method {
        code
        purchase_order_number
        title
      }
      shipping_addresses {
        available_shipping_methods {
          amount {
            currency
            value
          }
        }
        city
        company
        country {
          code
          label
        }
        customer_notes
        firstname
        items_weight
        lastname
        pickup_location_code
        postcode
        region {
          code
          label
          region_id
        }
        selected_shipping_method {
          amount {
            currency
            value
          }
          carrier_code
          carrier_title
          method_code
        }
        street
        telephone
        uid
        vat_id
      }
    }
  }

  fragment CartAddressCountryFields on CartAddressCountry {
    code
    label
  }

  fragment CartAddressRegionFields on CartAddressRegion {
    code
    label
    region_id
  }

  fragment MoneyFields on Money {
    currency
    value
  }

  fragment DiscountFields on Discount {
    label
  }

  fragment ProductInterfaceFields on ProductInterface {
    name
    sku
  }
`

export const SHIPPING_FEE = gql`
  query ShippingFee(
    $quoteId: String
    $sku: String
    $toCity: String!
    $toDistrict: String!
    $shippingType: String!
  ) {
    ShippingFee(
      quoteId: $quoteId
      sku: $sku
      toCity: $toCity
      toDistrict: $toDistrict
      shippingType: $shippingType
    ) {
      items {
        carrierId
        carrierName
        codFee
        declaredFee
        estimatedDeliveryTime
        logo
        serviceDescription
        serviceId
        serviceName
        serviceTypeName
        shipFee
      }
    }
  }
`

// cart_items_v2 {
//   ...CartItemInterfaceFields
// }

// fragment CartItemInterfaceFields on CartItemInterface {
//   errors {
//     code
//   }
//   id
//   prices {
//     discounts {
//       amount {
//         currency
//         value
//       }
//       ...DiscountFields
//     }
//   }
//   product {
//     ...ProductInterfaceFields
//   }
//   quantity
//   uid
// }

export const GET_BANK_ACCOUNT = gql`
  query getBank($filter: BankFilterInput, $pageSize: Int, $currentPage: Int) {
    Banks(filter: $filter, pageSize: $pageSize, currentPage: $currentPage) {
      total_count
      page_info {
        page_size
        current_page
        total_pages
      }
      items {
        bank_id
        bank
        account_holder
        account_number
        payment_method
        sort_order
        logo
        qr_code
      }
    }
  }
`
