import { _format } from "~src/util";
import BaseAPI from "../methods";

type TFilterParams = {
  Type: number;
  Roles: any;
  Branchs: any;
};

type TUserInfo = {};

const { put, get, post, gDelete } = new BaseAPI<
  TSourceType & TFilterParams & TUserInfo
>("rest/V1/users");

const fieldSearch: any = ["username", "email", "lastname", "firstname"];

export const user = {
  getListUsers: (params: {
    currentPage?: any;
    pageSize?: any;
    SearchContent?: string;
  }) =>
    get<any>(
      `${
        !!params?.currentPage
          ? `?searchCriteria[currentPage]=${
              !!params?.SearchContent ? 1 : params?.currentPage
            }`
          : ``
      }${
        !!params?.pageSize
          ? `&searchCriteria[pageSize]=${params?.pageSize}`
          : ``
      }${
        !!params?.SearchContent
          ? `&${_format.searchContent(fieldSearch, params?.SearchContent)}`
          : ""
      }`,
      {
        params,
      }
    ),
  lockUser: (id: number) => post(`/lock/${id}`),
  unLockUser: (id: number) => post(`/unlock/${id}`),
  createUser: (params: any) => post(``, { ...params }),
  updateUser: (params: any) => put(``, { ...params }),
  getDetail: (id: any) => get(`/${id}`),
  // deleteUser: (id: number) => gDelete(`${id}`),
};
