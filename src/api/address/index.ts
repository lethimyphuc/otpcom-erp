import { gql } from "graphql-request";

export const GET_CITY = gql`
query getCity($locationType: String!) {
  Locations(locationType: $locationType) {
    items {
      id
      name
      parentId
    }
  }
}
`

export const GET_DISTRICT = gql`
query getDistrict($parentId: Int, $locationType: String!) {
  Locations(parentId: $parentId, locationType: $locationType) {
    items {
      id
      name
      parentId
    }
  }
}
`

export const GET_WARD = gql`
query getWard($parentId: Int, $locationType: String!) {
  Locations(parentId: $parentId, locationType: $locationType) {
    items {
      id
      name
      parentId
    }
  }
}
`