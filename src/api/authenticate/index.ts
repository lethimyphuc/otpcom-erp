import FormData from 'form-data'
import { gql } from 'graphql-request'
import { TLogin } from '~src/types/authenticate'
import instance from '../instance'
import BaseAPI from '../methods'

const { put, get, post } = new BaseAPI('rest/V1/integration')

export const authenticate = {
  login: (params: TLogin) => {
    let frmData = new FormData()
    frmData.append('username', params.username)
    frmData.append('password', params.password)
    return post<any>('/admin/token', frmData)
  },

  getUserInfo: (token: string) => {
    instance.defaults.headers.common['Authorization'] = 'Bearer ' + token
    return get<any>('/currentUserData')
  },

  register: (data: any) => post('/register', data),

  logout: (userId: string) => get(`/admin/revoke/${userId}`),

  changePassword: (
    userId: string,
    data: {
      oldPassword: string
      newPassword: string
      confirmNewPassword: string
    }
  ) => put(`/changePassword/${userId}`, data),

  forgotPassword: async (params: { username: string }) =>
    await put(`/forgot-password/${params.username}`, undefined, { params }),

  updateInfo: (info: any) => put('/change-information', info),
}

export const REQUEST_PASSWORD_SEND_EMAIL = gql`
  mutation requestPasswordResetEmail($email: String!) {
    requestPasswordResetEmail(email: $email)
  }
`

export const RESET_PASSWORD = gql`
  mutation resetPassword($email: String!, $resetPasswordToken: String!, $newPassword: String!) {
    resetPassword(email: $email, resetPasswordToken: $resetPasswordToken, newPassword: $newPassword)
  }
`

export const LOGIN = gql`
  mutation login($email: String!, $password: String!) {
    generateCustomerToken(email: $email, password: $password) {
      token
    }
  }
`

export const LOGIN_SOCIAL_GOOGLE = gql`
  mutation loginSocial(
    $state: String!
    $code: String!
    $scope: String!
    $authuser: String!
    $prompt: String!
    $handle: String!
  ) {
    generateCustomerTokenSocialLogin(
      state: $state
      code: $code
      scope: $scope
      authuser: $authuser
      prompt: $prompt
      handle: $handle
    ) {
      token
    }
  }
`

export const SIGNUP = gql`
  mutation signup($input: CustomerCreateInput!) {
    createCustomerV2(input: $input) {
      customer {
        created_at
        email
        firstname
        lastname
        phone_number
      }
    }
  }
`

export const SOCIAL_LOGIN = gql`
  {
    socialLoginUrl {
      key
      label
      login_url
    }
  }
`

export const GET_DETAIL_ORDER = gql`
  query orderDetailCustomer($orderId: String!) {
    orderDetailCustomer(orderId: $orderId) {
      order_date
      id
      number
      nhanhvn_status
      state
      comments {
        ...SalesCommentItemFields
      }
      invoices {
        id
        number
        comments {
          timestamp
          ...SalesCommentItemFields
        }
        items {
          id
          product_sale_price {
            ...MoneyFields
          }
          discounts {
            amount {
              ...MoneyFields
            }
            ...DiscountFields
          }
          product_name
          product_sku
          quantity_invoiced
        }
      }
      items {
        id
        product_image
        product_name
        quantity_ordered
        product_sale_price {
          ...MoneyFields
        }
      }
      carrier
      status
      total {
        grand_total {
          ...MoneyFields
        }
        base_grand_total {
          ...MoneyFields
        }
        total_shipping {
          ...MoneyFields
        }
        discounts {
          amount {
            ...MoneyFields
          }
          ...DiscountFields
        }
        subtotal {
          ...MoneyFields
        }
      }
      shipping_method
      shipping_address {
        city
        company
        firstname
        lastname
        telephone
        street
        district
        ward
        customer_note
      }
    }
  }

  fragment MoneyFields on Money {
    currency
    value
  }

  fragment SalesCommentItemFields on SalesCommentItem {
    message
  }

  fragment DiscountFields on Discount {
    label
  }
`

export const INFO_CUSTOMER = gql`
  query inforCustomer {
    customer {
      addresses {
        city
        company
        country_code
        country_id
        custom_attributes {
          ...CustomerAddressAttributeFields
        }
        default_billing
        default_shipping
        extension_attributes {
          ...CustomerAddressAttributeFields
        }
        fax
        firstname
        id
        lastname
        middlename
        postcode
        prefix
        region {
          region
          region_code
          region_id
        }
        ward
        district
        region_id
        street
        suffix
        telephone
        vat_id
      }
      phone_number
      profile_picture
      allow_remote_shopping_assistance
      created_at
      date_of_birth
      default_billing
      default_shipping
      date_of_birth
      email
      firstname
      gender
      is_subscribed
      lastname
      middlename
      reviews {
        items {
          average_rating
          created_at
          nickname
          product {
            sku
            name
          }
        }
      }
      orders {
        total_count
        page_info {
          current_page
          page_size
          total_pages
        }
        items {
          order_date
          id
          number
          comments {
            message
          }
          invoices {
            id
            number
            comments {
              message
              timestamp
            }
            items {
              id
              product_sale_price {
                ...MoneyFields
              }
              discounts {
                amount {
                  ...MoneyFields
                }
                label
              }
              product_name
              product_sku
              quantity_invoiced
            }
          }
          items {
            id
            product_image
            product_name
            quantity_ordered
            product_sale_price {
              ...MoneyFields
            }
          }
          carrier
          status
          total {
            grand_total {
              ...MoneyFields
            }
            base_grand_total {
              ...MoneyFields
            }
            total_shipping {
              ...MoneyFields
            }
            discounts {
              amount {
                ...MoneyFields
              }
              label
            }
            subtotal {
              ...MoneyFields
            }
          }
          shipping_method
        }
      }
    }
  }

  fragment CustomerAddressAttributeFields on CustomerAddressAttribute {
    attribute_code
    value
  }

  fragment MoneyFields on Money {
    value
    currency
  }
`

export const CUSTOMER_ADDRESSES = gql`
  query customerAddress {
      city
      company
      country_code
      country_id
      default_billing
      default_shipping
      fax
      firstname
      id
      lastname
      middlename
      postcode
      prefix
      region {
        region
        region_code
        region_id
      }
      region_id
      street
      suffix
      telephone
      vat_id
    }
  }
`

export const CUSTOMER_CART = gql`
  query customerCart {
    customerCart {
      total_quantity
      id
      items {
        uid
        quantity
        prices {
          discounts {
            amount {
              value
              currency
            }
            ...DiscountFields
          }
          price {
            value
            currency
          }
          price_including_tax {
            value
            currency
          }
          row_total {
            value
            currency
          }
          row_total_including_tax {
            value
            currency
          }
          total_item_discount {
            value
            currency
          }
        }
        product {
          image {
            url
          }
          price_range {
            maximum_price {
              final_price {
                value
                currency
              }
              regular_price {
                value
                currency
              }
            }
            minimum_price {
              final_price {
                value
                currency
              }
              regular_price {
                value
                currency
              }
            }
          }
          ...ProductInterfaceFields
        }
      }
      applied_coupons {
        code
      }
      available_payment_methods {
        code
        is_deferred
        title
      }
      shipping_addresses {
        available_shipping_methods {
          amount {
            ...MoneyFields
          }
          method_title
          carrier_code
          carrier_title
          available
          method_code
        }
      }
      billing_address {
        city
        company
        country {
          ...CartAddressCountryFields
        }
        customer_notes
        firstname
        lastname
        postcode
        region {
          ...CartAddressRegionFields
        }
        street
        telephone
        uid
        vat_id
      }
      gift_message {
        from
        message
        to
      }
      prices {
        discounts {
          amount {
            value
            currency
          }
          label
        }
        applied_taxes {
          amount {
            ...MoneyFields
          }
          label
        }
        grand_total {
          ...MoneyFields
        }
        subtotal_excluding_tax {
          ...MoneyFields
        }
        subtotal_including_tax {
          ...MoneyFields
        }
        subtotal_with_discount_excluding_tax {
          ...MoneyFields
        }
      }
      selected_payment_method {
        code
        purchase_order_number
        title
      }
      shipping_addresses {
        available_shipping_methods {
          amount {
            currency
            value
          }
        }
        city
        company
        country {
          code
          label
        }
        customer_notes
        firstname
        items_weight
        lastname
        pickup_location_code
        postcode
        region {
          code
          label
          region_id
        }
        selected_shipping_method {
          amount {
            currency
            value
          }
          carrier_code
          carrier_title
          method_code
        }
        street
        telephone
        uid
        vat_id
      }
    }
  }

  fragment CartAddressCountryFields on CartAddressCountry {
    code
    label
  }

  fragment CartAddressRegionFields on CartAddressRegion {
    code
    label
    region_id
  }

  fragment MoneyFields on Money {
    currency
    value
  }

  fragment DiscountFields on Discount {
    label
  }

  fragment ProductInterfaceFields on ProductInterface {
    name
    sku
  }
`

export const MERGE_CART = gql`
  mutation mergeCart($source_cart_id: String!, $destination_cart_id: String) {
    mergeCarts(source_cart_id: $source_cart_id, destination_cart_id: $destination_cart_id) {
      total_quantity
      items {
        uid
        quantity
        prices {
          discounts {
            amount {
              value
              currency
            }
            ...DiscountFields
          }
          price {
            value
            currency
          }
          price_including_tax {
            value
            currency
          }
          row_total {
            value
            currency
          }
          row_total_including_tax {
            value
            currency
          }
          total_item_discount {
            value
            currency
          }
        }
        product {
          image {
            url
          }
          price_range {
            maximum_price {
              final_price {
                value
                currency
              }
              regular_price {
                value
                currency
              }
            }
            minimum_price {
              final_price {
                value
                currency
              }
              regular_price {
                value
                currency
              }
            }
          }
          ...ProductInterfaceFields
        }
      }
      applied_coupons {
        code
      }
      available_payment_methods {
        code
        is_deferred
        title
      }
      shipping_addresses {
        available_shipping_methods {
          amount {
            ...MoneyFields
          }
          method_title
          carrier_code
          carrier_title
          available
          method_code
        }
      }
      billing_address {
        city
        company
        country {
          ...CartAddressCountryFields
        }
        customer_notes
        firstname
        lastname
        postcode
        region {
          ...CartAddressRegionFields
        }
        street
        telephone
        uid
        vat_id
      }
      gift_message {
        from
        message
        to
      }
      prices {
        discounts {
          amount {
            value
            currency
          }
          label
        }
        applied_taxes {
          amount {
            ...MoneyFields
          }
          label
        }
        grand_total {
          ...MoneyFields
        }
        subtotal_excluding_tax {
          ...MoneyFields
        }
        subtotal_including_tax {
          ...MoneyFields
        }
        subtotal_with_discount_excluding_tax {
          ...MoneyFields
        }
      }
      selected_payment_method {
        code
        purchase_order_number
        title
      }
      shipping_addresses {
        available_shipping_methods {
          amount {
            currency
            value
          }
        }
        city
        company
        country {
          code
          label
        }
        customer_notes
        firstname
        items_weight
        lastname
        pickup_location_code
        postcode
        region {
          code
          label
          region_id
        }
        selected_shipping_method {
          amount {
            currency
            value
          }
          carrier_code
          carrier_title
          method_code
        }
        street
        telephone
        uid
        vat_id
      }
    }
  }
  fragment CartAddressCountryFields on CartAddressCountry {
    code
    label
  }

  fragment CartAddressRegionFields on CartAddressRegion {
    code
    label
    region_id
  }

  fragment MoneyFields on Money {
    currency
    value
  }

  fragment DiscountFields on Discount {
    label
  }

  fragment ProductInterfaceFields on ProductInterface {
    name
    sku
  }
`

export const CHANGE_CUSTOMER_PASSWORD = gql`
  mutation changeCustomerPassword($currentPassword: String!, $newPassword: String!) {
    changeCustomerPassword(currentPassword: $currentPassword, newPassword: $newPassword) {
      id
      email
    }
  }
`

export const UPDATE_CUSTOMER = gql`
  mutation updateCustomerV2($input: CustomerUpdateInput!) {
    updateCustomerV2(input: $input) {
      customer {
        allow_remote_shopping_assistance
        created_at
        date_of_birth
        default_billing
        default_shipping
        email
        profile_picture
        firstname
        gender
        is_subscribed
        lastname
        middlename
      }
    }
  }
`

export const UPDATE_CUSTOMER_EMAIL = gql`
  mutation updateCustomerEmail($email: String!, $password: String!) {
    updateCustomerEmail(email: $email, password: $password) {
      customer {
        email
      }
    }
  }
`

export const UPDATE_CUSTOMER_ADDRESS = gql`
  mutation updateCustomAddress($id: Int!, $input: CustomerAddressInput) {
    updateCustomerAddress(id: $id, input: $input) {
      city
      company
      country_code
      country_id
      custom_attributes {
        ...CustomerAddressAttributeFields
      }
      customer_id
      default_billing
      default_shipping
      extension_attributes {
        ...CustomerAddressAttributeFields
      }
      fax
      firstname
      id
      lastname
      middlename
      postcode
      prefix
      region {
        region
        region_code
        region_id
      }
      region_id
      street
      suffix
      telephone
      vat_id
    }
  }

  fragment CustomerAddressAttributeFields on CustomerAddressAttribute {
    attribute_code
    value
  }
`

export const DELETE_CUSTOMER_ADDRESS = gql`
  mutation deleteCustomerAddress($id: Int!) {
    deleteCustomerAddress(id: $id)
  }
`

export const CREATE_CUSTOMER_ADDRESS = gql`
  mutation createCustomerAddress($input: CustomerAddressInput!) {
    createCustomerAddress(input: $input) {
      city
      company
      country_code
      default_billing
      default_shipping
      district
      extension_attributes {
        attribute_code
        value
      }
      fax
      firstname
      id
      lastname
      middlename
      postcode
      prefix
      region {
        region
        region_code
        region_id
      }
      region_id
      street
      suffix
      telephone
      vat_id
      ward
    }
  }
`

export const SNOW_DOG_MENU_NODES = gql`
  query getMenu($identifier: String!) {
    snowdogMenuNodes(identifier: $identifier) {
      items {
        additional_data
        categories_uids
        classes
        creation_time
        level
        menu_id
        node_id
        node_template
        parent_id
        position
        submenu_template
        title
        type
        update_time
        url_key
        brand {
          entity_id
          name
          logo
          title
          url_key
          query_ids
          children {
            entity_id
            name
            title
            logo
            url_key
            query_ids
          }
        }
        ... on SnowdogMenuCustomUrlNodeInterface {
          target
        }
        ... on SnowdogMenuNodeImageFieldInterface {
          image
          image_alt_text
        }
        ... on SnowdogMenuCustomUrlNode {
          image
          image_alt_text
          content
        }
      }
    }
  }
`
export const GET_MENU_FOOTER = gql`
  query getMenu($identifier: String!) {
    snowdogMenuNodes(identifier: $identifier) {
      items {
        additional_data
        categories_uids
        classes
        creation_time
        level
        menu_id
        node_id
        node_template
        parent_id
        position
        submenu_template
        title
        type
        update_time
        url_key
        ... on SnowdogMenuCustomUrlNodeInterface {
          target
        }
        ... on SnowdogMenuNodeImageFieldInterface {
          image
          image_alt_text
        }
        ... on SnowdogMenuCustomUrlNode {
          image
          image_alt_text
          content
        }
      }
    }
  }
`

export const GET_ORDERS_LIST = gql`
query inforCustomer($sort: CustomerOrderSortInput, $orderCurrentPage: Int, $orderPageSize: Int, $filter: CustomerOrdersFilterInput) {
  customer {
    orders(
      filter: $filter
      sort: $sort
      currentPage: $orderCurrentPage
      pageSize: $orderPageSize
    ) {
      total_count
      page_info {
        current_page
        page_size
        total_pages
      }
      items {
        order_date
        id
        number
        comments {
          ...SalesCommentItemFields
        }
        invoices {
          id
          number
          comments {
            timestamp
            ...SalesCommentItemFields
          }
          items {
            id
            product_sale_price {
              ...MoneyFields
            }
            discounts {
              amount {
                ...MoneyFields
              }
              ...DiscountFields
            }
            product_name
            product_sku
            quantity_invoiced
          }
        }
        items {
          id
          product_image
          product_name
          quantity_ordered
          product_sale_price {
            ...MoneyFields
          }
        }
        carrier
        status
        total {
          grand_total {
            ...MoneyFields
          }
          base_grand_total {
            ...MoneyFields
          }
          total_shipping {
            ...MoneyFields
          }
          discounts {
            amount {
              ...MoneyFields
            }
            ...DiscountFields
          }
          subtotal {
            ...MoneyFields
          }
        }
        shipping_method
      }
    }
  }
}

fragment MoneyFields on Money {
  value
  currency
}

fragment SalesCommentItemFields on SalesCommentItem {
  message
}

fragment DiscountFields on Discount {
  label
}
`