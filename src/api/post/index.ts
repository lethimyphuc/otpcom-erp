import { gql } from 'graphql-request'

export const GET_POST_DETAIL = gql`
  query getPostDetail($postId: String!) {
    mpBlogPostGetById(postId: $postId) {
      album_images {
        ...AlbumImageFields
      }
      categories {
        items {
          ...CategoryFields
        }
        ...CategoriesOutputNoPageFields
      }
      posts {
        items {
          album_images {
            ...AlbumImageFields
          }
          categories {
            items {
              ...CategoryFields
            }
            ...CategoriesOutputNoPageFields
          }
          ...PostFields
        }
        total_count
      }
      publish_date
      short_description
      store_ids
      tags {
        items {
          name
          updated_at
          url_key
          tag_id
        }
        total_count
      }
      updated_at
      url_key
      view_traffic
      ...PostFields
    }
  }

  fragment PostFields on Post {
    allow_comment
    author_id
    author_name
    author_url
    author_url_key
    created_at
    enabled
    image
    import_source
    in_rss
    layout
    meta_description
    meta_keywords
    meta_robots
    meta_title
    name
    post_content
    post_id
  }

  fragment AlbumImageFields on AlbumImage {
    alt
    image
  }

  fragment CategoriesOutputNoPageFields on CategoriesOutputNoPage {
    total_count
  }

  fragment CategoryFields on Category {
    name
    category_id
  }
`

export const GET_LIST_POST = gql`
  query getListBlogPosts(
    $postId: Int
    $pageSize: Int
    $currentPage: Int
    $action: String!
    $sortBy: String
    $categoryId: Int
  ) {
    mpBlogPosts(
      postId: $postId
      pageSize: $pageSize
      currentPage: $currentPage
      action: $action
      sortBy: $sortBy
      categoryId: $categoryId
    ) {
      items {
        categories {
          items {
            category_id
            name
            url_key
            path
          }
        }
        posts {
          items {
            ...PostFields
          }
        }
        ...PostFields
      }
      pageInfo {
        currentPage
        endPage
        hasNextPage
        hasPreviousPage
        pageSize
        startPage
      }
      total_count
    }
  }

  fragment PostFields on Post {
    created_at
    enabled
    image
    name
    post_content
    author_name
    post_id
    publish_date
    short_description
    store_ids
    updated_at
    album_images {
      alt
      image
    }
  }
`

export const GET_LIST_CATEGORY_POST = gql`
  query getListCategoryPosts(
    $action: String!
    $filter: CategoriesFilterInput
    $pageSize: Int
    $currentPage: Int
    $postId: Int
  ) {
    mpBlogCategories(
      action: $action
      filter: $filter
      pageSize: $pageSize
      currentPage: $currentPage
      postId: $postId
    ) {
      items {
        category_id
        children_count
        created_at
        description
        enabled
        import_source
        level
        meta_description
        meta_keywords
        meta_robots
        meta_title
        name
        parent_id
        path
        position
        store_ids
        updated_at
        url_key
      }
      pageInfo {
        currentPage
        endPage
        hasNextPage
        hasPreviousPage
        pageSize
        startPage
      }
      total_count
    }
  }
`
export const GET_LIST_POLICY = gql`
  query getListBlogPosts(
    $postId: Int
    $pageSize: Int
    $currentPage: Int
    $action: String!
    $sortBy: String
    $categoryId: Int
  ) {
    mpBlogPosts(
      postId: $postId
      pageSize: $pageSize
      currentPage: $currentPage
      action: $action
      sortBy: $sortBy
      categoryId: $categoryId
    ) {
      items {
        categories {
          items {
            category_id
            name
            url_key
            path
          }
        }
        ...PostFields
      }
      pageInfo {
        currentPage
        endPage
        hasNextPage
        hasPreviousPage
        pageSize
        startPage
      }
      total_count
    }
  }

  fragment PostFields on Post {
    created_at
    enabled
    image
    name
    post_content
    author_name
    post_id
    publish_date
    url_key
    short_description
    store_ids
    updated_at
    album_images {
      alt
      image
    }
  }
`
