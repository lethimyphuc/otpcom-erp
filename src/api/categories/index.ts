import { gql } from 'graphql-request'

const categoryFragment = `
  fragment CategoryTreeFields on CategoryTree {
    uid
    tags {
      id
      name
    }
    name
    level
    url_key
    url_path
    meta_description
    meta_keywords
    image
    image_car
    id
    image_banner
    image_catalogue
    product_count
  }
`

export const GET_CATEGORIES = gql`
  query getCategories($eq: String) {
    categories(filters: { category_uid: { eq: $eq } }) {
      items {
        children {
          ...CategoryTreeFields
        }
      }
    }
  }

  ${categoryFragment}
`

export const GET_CATEGORIES_BY_URL_KEY = gql`
  query getCategories($eq: String) {
    categories(filters: { url_key: { eq: $eq } }) {
      items {
        children {
          ...CategoryTreeFields
        }
      }
    }
  }

  ${categoryFragment}
`

export const GET_CATEGORIES_BY_SLUG = gql`
  query getCategories($eq: String) {
    categories(filters: { url_key: { eq: $eq } }) {
      items {
        children {
          ...CategoryTreeFields
        }
      }
    }
  }

  ${categoryFragment}
`

export const GET_CATEGORIE_DETAIL = gql`
  query getCategories($eq: String) {
    categories(filters: { category_uid: { eq: $eq } }) {
      items {
        uid
        tags {
          id
          name
          display_name
        }
        name
        level
        url_key
        meta_description
        meta_keywords
        image
        image_car
        image_banner
        product_count
      }
    }
  }
`
export const GET_CATEGORIE_DETAIL_BY_URL_KEY = gql`
  query getCategories($eq: String) {
    categories(filters: { url_key: { eq: $eq } }) {
      items {
        uid
        tags {
          id
          name
          display_name
        }
        name
        level
        url_key
        meta_description
        meta_keywords
        image
        image_car
        image_banner
        product_count
      }
    }
  }
`
export const GET_BRANDS = gql`
  query getBrands($pageSize: Int, $currentPage: Int) {
    BrandClassifications(pageSize: $pageSize, currentPage: $currentPage) {
      items {
        entity_id
        image
        logo
        name
        query_ids
        url_key
        items {
          entity_id
          image
          logo
          name
          query_ids
          url_key
        }
      }
      page_info {
        current_page
        page_size
        total_pages
      }
      total_count
    }
  }
`
