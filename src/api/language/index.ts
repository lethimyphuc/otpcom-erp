import { gql } from "graphql-request";

export const GET_AVAILABLESTORES = gql`
  {
    availableStores(useCurrentGroup: true) {
      store_code
      store_name
      copyright
      welcome
      website_name
      timezone
      locale
      base_media_url
      base_url
      base_media_url
      base_static_url
      secure_base_url
      secure_base_media_url
      secure_base_static_url
    }
  }
`;
