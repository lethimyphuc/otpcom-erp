import { gql } from 'graphql-request'

// color
//      brand
//      package_size
//      country_of_manufacture
//      unit

export const GET_PRODUCTS = gql`
  query getProducts(
    $filter: ProductAttributeFilterInput
    $search: String
    $pageSize: Int
    $currentPage: Int
    $sort: ProductAttributeSortInput
  ) {
    products(
      filter: $filter
      search: $search
      pageSize: $pageSize
      currentPage: $currentPage
      sort: $sort
    ) {
      items {
        related_products {
          name
          sort_name
          sku
          uid
          color
          unit
          height
          width
          length
          country_of_manufacture
          description {
            html
          }
          rating_summary
          review_count
          reviews {
            items {
              comments {
                ...ReviewOptionsFields
              }
              created_at
              customer_avatar
              images {
                ...ReviewOptionsFields
              }
              review_id
              link_video
              nickname
              ratings_breakdown {
                name
                value
              }
              statistics {
                comment
                dislike
                images
                like
                video
              }
              average_rating
              created_at
              nickname
              text
              product {
                image {
                  url
                }
              }
            }
            page_info {
              current_page
              page_size
              total_pages
            }
          }
          media_gallery {
            url
          }
          daily_sale {
            end_date
            entity_id
            product_id
            sale_price
            sale_qty
            sold_qty
            start_date
          }
          stock_status
          image {
            url
          }
          price_tiers {
            discount {
              percent_off
            }
            final_price {
              value
              currency
            }
          }
          price_range {
            maximum_price {
              final_price {
                ...MoneyFields
              }
              regular_price {
                ...MoneyFields
              }
            }
            minimum_price {
              final_price {
                ...MoneyFields
              }
              regular_price {
                ...MoneyFields
              }
            }
          }
          reviews {
            items {
              comments {
                ...ReviewOptionsFields
              }
              created_at
              customer_avatar
              images {
                ...ReviewOptionsFields
              }
              review_id
              link_video
              nickname
              ratings_breakdown {
                name
                value
              }
              statistics {
                comment
                dislike
                images
                like
                video
              }
            }
          }
        }
        ...ProductItemField
      }
      page_info {
        current_page
        page_size
        total_pages
      }
    }
  }

  fragment ProductItemField on ProductInterface {
    name
    sku
    catalogues
    uid
    color
    package_size
    country_of_manufacture
    description {
      html
    }
    sort_name
    rating_summary
    review_count
    unit
    height
    width
    length
    brand_id
    brand {
      entity_id
      name
      image
      logo
    }
    country
    short_description {
      html
    }
    reviews {
      items {
        comments {
          ...ReviewOptionsFields
        }
        created_at
        customer_avatar
        images {
          ...ReviewOptionsFields
        }
        review_id
        link_video
        nickname
        ratings_breakdown {
          name
          value
        }
        statistics {
          comment
          dislike
          images
          like
          video
        }
        average_rating
        created_at
        nickname
        text
        product {
          image {
            url
          }
        }
      }
      page_info {
        current_page
        page_size
        total_pages
      }
    }
    media_gallery {
      url
    }
    daily_sale {
      end_date
      entity_id
      product_id
      sale_price
      sale_qty
      sold_qty
      start_date
    }
    stock_status
    image {
      url
    }
    price_tiers {
      discount {
        percent_off
      }
      final_price {
        value
        currency
      }
    }
    price_range {
      maximum_price {
        final_price {
          ...MoneyFields
        }
        regular_price {
          ...MoneyFields
        }
      }
      minimum_price {
        final_price {
          ...MoneyFields
        }
        regular_price {
          ...MoneyFields
        }
      }
    }
    reviews {
      items {
        comments {
          ...ReviewOptionsFields
        }
        created_at
        customer_avatar
        images {
          ...ReviewOptionsFields
        }
        review_id
        link_video
        nickname
        ratings_breakdown {
          name
          value
        }
        statistics {
          comment
          dislike
          images
          like
          video
        }
      }
    }
  }

  fragment MoneyFields on Money {
    value
    currency
  }
  fragment ReviewOptionsFields on ReviewOptions {
    content
    created_at
    customer_avatar
    customer_id
    post_by
    review_id
    status
    type
  }
`

export const GET_DAILYSALES = gql`
  query getDailySale($filter: DailySaleFilterInput, $pageSize: Int, $currentPage: Int) {
    DailySales(filter: $filter, pageSize: $pageSize, currentPage: $currentPage) {
      items {
        end_date
        entity_id
        items {
          entity_id
          product {
            name
            sku
            uid
            daily_sale {
              end_date
              entity_id
              product_id
              sale_price
              sale_qty
              sold_qty
              start_date
              currency
            }
            stock_status
            image {
              url
            }
            price_tiers {
              discount {
                percent_off
              }
              final_price {
                value
                currency
              }
            }
            price_range {
              maximum_price {
                final_price {
                  ...MoneyFields
                }
                regular_price {
                  ...MoneyFields
                }
              }
              minimum_price {
                final_price {
                  ...MoneyFields
                }
                regular_price {
                  ...MoneyFields
                }
              }
            }
          }
          product_id
          sale_price
          sale_qty
          sold_qty
        }
        priority
        show_in_home
        start_date
        status
        title
      }
      page_info {
        current_page
        page_size
        total_pages
      }
      total_count
    }
  }

  fragment MoneyFields on Money {
    value
    currency
  }
`

export const GET_PRODUCT_DAILY_SALE = gql`
  query getProductDailySale(
    $filter: ProductAttributeFilterInput
    $search: String
    $pageSize: Int
    $currentPage: Int
    $sort: ProductDailySaleSort
  ) {
    ProductDailySales(
      filter: $filter
      search: $search
      pageSize: $pageSize
      currentPage: $currentPage
      sort: $sort
    ) {
      items {
        entity_id
        product_id
        sale_price
        sale_qty
        saleable_qty
        sold_qty
        start_date
        end_date
        product {
          name
          sku
          uid
          image {
            url
          }
          daily_sale {
            sale_price
            end_date
            start_date
            sold_qty
            saleable_qty
            sale_qty
          }
          stock_status
          price_tiers {
            discount {
              percent_off
            }
            final_price {
              value
              currency
            }
          }
          price_range {
            maximum_price {
              final_price {
                ...MoneyFields
              }
              regular_price {
                ...MoneyFields
              }
            }
            minimum_price {
              final_price {
                ...MoneyFields
              }
              regular_price {
                ...MoneyFields
              }
            }
          }
        }
      }
      page_info {
        current_page
        page_size
        total_pages
      }
      total_count
    }
  }

  fragment MoneyFields on Money {
    value
    currency
  }
`

export const GET_REVIEW_RATING_PRODUCT = gql`
  {
    productReviewRatingsMetadata {
      items {
        id
        name
        values {
          value_id
          value
        }
      }
    }
  }
`

export const ADD_PRODUCT_TO_CART = gql`
  mutation addProductToCart($cartId: String!, $cartItems: [CartItemInput!]!) {
    addProductsToCart(cartId: $cartId, cartItems: $cartItems) {
      cart {
        email
        id
        is_virtual
        items {
          quantity
          uid
          product {
            sku
            name
          }
          prices {
            price {
              ...MoneyFields
            }
          }
        }
        prices {
          grand_total {
            ...MoneyFields
          }
        }
        total_quantity
      }
      user_errors {
        code
        message
      }
    }
  }

  fragment MoneyFields on Money {
    currency
    value
  }
`

export const UPDATE_PRODUCT_TO_CART = gql`
  mutation updateCartItem($input: UpdateCartItemsInput) {
    updateCartItems(input: $input) {
      cart {
        email
        id
        is_virtual
        items {
          quantity
          uid
          product {
            sku
            name
            image {
              url
            }
            price_range {
              maximum_price {
                final_price {
                  value
                  currency
                }
                regular_price {
                  value
                  currency
                }
              }
              minimum_price {
                final_price {
                  value
                  currency
                }
                regular_price {
                  value
                  currency
                }
              }
            }
          }
          prices {
            price {
              ...MoneyFields
            }
          }
        }
        prices {
          discounts {
            amount {
              value
              currency
            }
            label
          }
          applied_taxes {
            amount {
              ...MoneyFields
            }
            label
          }
          grand_total {
            ...MoneyFields
          }
          subtotal_excluding_tax {
            ...MoneyFields
          }
          subtotal_including_tax {
            ...MoneyFields
          }
          subtotal_with_discount_excluding_tax {
            ...MoneyFields
          }
        }
        total_quantity
      }
    }
  }

  fragment MoneyFields on Money {
    currency
    value
  }
`

export const REMOVE_PRODUCT_TO_CART = gql`
  mutation removeItemFromCart($input: RemoveItemFromCartInput) {
    removeItemFromCart(input: $input) {
      cart {
        email
        id
        is_virtual
        items {
          quantity
          uid
          product {
            sku
            name
            image {
              url
            }
          }
          prices {
            price {
              ...MoneyFields
            }
          }
        }
        prices {
          discounts {
            amount {
              value
              currency
            }
            label
          }
          applied_taxes {
            amount {
              ...MoneyFields
            }
            label
          }
          grand_total {
            ...MoneyFields
          }
          subtotal_excluding_tax {
            ...MoneyFields
          }
          subtotal_including_tax {
            ...MoneyFields
          }
          subtotal_with_discount_excluding_tax {
            ...MoneyFields
          }
        }
        total_quantity
      }
    }
  }

  fragment MoneyFields on Money {
    currency
    value
  }
`

export const CREATE_PRODUCT_REVIEW = gql`
  mutation createProductReview($input: CreateProductReviewInput!) {
    createProductReview(input: $input) {
      review {
        average_rating
        created_at
        nickname
        ratings_breakdown {
          name
          value
        }
        product {
          reviews {
            items {
              average_rating
              nickname
              text
              created_at
              product {
                image {
                  url
                }
              }
            }
            page_info {
              current_page
              page_size
              total_pages
            }
          }
        }
        summary
        text
      }
    }
  }
`

export const CREATE_COMMENT = gql`
  mutation commentReviewProduct($input: CommentReviewProductInput!) {
    commentReviewProduct(input: $input) {
      content
      created_at
      customer_avatar
      customer_id
      post_by
      review_id
      status
      type
    }
  }
`

export const GET_SHIPPING = gql`
  query ShippingFee($sku: String, $toCity: String!, $toDistrict: String!, $shippingType: String!) {
    ShippingFee(sku: $sku, toCity: $toCity, toDistrict: $toDistrict, shippingType: $shippingType) {
      items {
        carrierId
        carrierName
        logo
        serviceId
        serviceTypeName
        serviceName
        serviceDescription
        estimatedDeliveryTime
        shipFee
        codFee
        declaredFee
      }
    }
  }
`
