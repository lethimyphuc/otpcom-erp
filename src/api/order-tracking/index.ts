import { gql } from "graphql-request";

export const ORDER_TRACKING = gql`
  query getOrderTracking($increment_id: String!, $email: String!) {
    OrderTracking(increment_id: $increment_id, email: $email) {
      customer_name
      order_increment
      phone_number
      address
      city
      district
      ward
      carrier_name
      carrier_service
      carrier_tracking_code
      logo
      url
    }
  }
`;
