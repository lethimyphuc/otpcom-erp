/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useMemo, useState } from 'react'
import clsx from 'clsx'
import styles from './Diary.module.scss'
import { useRouter } from 'next/router'
import { graphQLClient } from '~src/api/graphql'
import { GET_LIST_CATEGORY_POST, GET_LIST_POST } from '~src/api/post'
import { useQuery } from 'react-query'
import Link from 'next/link'
import { PaginationCustom } from '~src/components/common/PaginationCustom'
import { FormSelect } from '~src/components/global/FormControls/FormSelect'
import { format } from 'date-fns'
import { AnimatePresence, motion } from 'framer-motion'
import { useMediaQuery } from 'react-responsive'

export const DiaryBlog = ({ formControl }: { formControl: any }) => {
  const router = useRouter()

  const isScreenDesktopMedium = useMediaQuery({ query: '(max-width: 1280px)' })
  const isScreenMobile = useMediaQuery({ query: '(max-width: 640px)' })

  const numOfLazyItem = useMemo<number>(() => {
    return (isScreenMobile && 1) || (isScreenDesktopMedium && 2) || 3
  }, [isScreenDesktopMedium, isScreenMobile])

  const { data: dataBlogPosts, isLoading: isLoadingDataBlogPosts } = useQuery({
    queryKey: ['getBlogPosts', router.query],
    queryFn: async () => {
      const DATA_FILTER: any = {
        sortBy: !!router.query?.sortBy ? `${router.query?.sortBy}` : 'Popular',
        action: 'get_post_by_categoryId',
        pageSize: !!router?.query?.pageSize ? parseInt(`${router.query.pageSize}`) : 12,
        currentPage: !!router?.query?.currentPage ? parseInt(`${router.query.currentPage}`) : 1,
        categoryId: 2,
      }
      if (!!router.query?.categoryId) {
        DATA_FILTER.categoryId = parseInt(`${router.query?.categoryId}`)
      }
      const data = (await graphQLClient.request(GET_LIST_POST, DATA_FILTER)) as {
        mpBlogPosts: {
          items: TPost[]
          page_info: TPageInfo
          total_count: number
        }
      }
      return data?.mpBlogPosts
    },
  })

  return (
    <div className="max-w-[1230px] m-auto px-[15px]">
      <div className="list-search md:mt-20 mt-5">
        <div className="title-search">
          <div className="icon-center">
            <img src="/icons/search.svg" alt="" />
          </div>
          <div className="content-text">
            <span className="text">tất cả bài viết</span>
          </div>
        </div>
      </div>
      <div>
        <div className={`${clsx(styles.tabSection)} flex`}>
          <div className={clsx(styles.tabSelect)}>
            <div>
              <h3 className="sm:text-[20px] text-[16px] ">Nhật ký</h3>
            </div>
            {/* {dataCategoryPosts?.map((category: TCategoryPost, index: number) => {
              return (
                <>
                  <Link
                    href={{
                      query: {
                        ...router.query,
                        categoryId: category?.category_id,
                      },
                    }}
                  >
                    <div>
                      <p
                        className={clsx(
                          category?.category_id === parseInt(`${router.query?.categoryId}`)
                            ? styles.selected
                            : ''
                        )}
                      >
                        {category?.name}
                      </p>
                      {index < dataCategoryPosts?.length - 1 ? (
                        <img src={'/icons/vertical-line.svg'} alt="" />
                      ) : (
                        <></>
                      )}
                    </div>
                  </Link>
                </>
              )
            })} */}
            {/* <p>Blog chia sẻ</p>
          <img src={"/icons/vertical-line.svg"} alt="" />
          <p>Kiến thức phụ tùng</p>
          <img src={"/icons/vertical-line.svg"} alt="" />
          <p>Chính sách mua hàng</p> */}
          </div>
          <div className={clsx(styles.sort)}>
            <p className="hidden xxs:block">Sắp xếp theo:</p>
            <div className={clsx(styles.diarySort)}>
              <div className="sort-select">
                <FormSelect
                  label=""
                  control={formControl}
                  name={'sortBy'}
                  optionList={[
                    { label: 'Mới nhất', value: 'Popular' },
                    { label: 'Cũ nhất', value: 'Latest' },
                  ]}
                  placeholder=""
                  showSearch={false}
                  onChange={(value) =>
                    router.push({
                      query: {
                        ...router.query,
                        sortBy: value,
                      },
                    })
                  }
                />
              </div>
            </div>
          </div>
        </div>
        <div className={clsx(styles.blogList)}>
          {Array.from({ length: numOfLazyItem }, (v, k) => (
            <AnimatePresence mode="popLayout" key={k}>
              {!dataBlogPosts && isLoadingDataBlogPosts && (
                <motion.div
                  exit={{ opacity: 0 }}
                  transition={{ duration: 0.3, ease: 'easeInOut' }}
                  className={clsx('relative')}
                >
                  <LazyBlogItem key={k} />
                </motion.div>
              )}
            </AnimatePresence>
          ))}
          {(dataBlogPosts?.items?.length as number) > 0 ? (
            <>
              {dataBlogPosts?.items?.map((item: TPost, index: number) => (
                <motion.div
                  initial={{ opacity: 0 }}
                  animate={{ opacity: 1 }}
                  transition={{ duration: 0.5, ease: 'easeInOut' }}
                  className={clsx(styles.blogItem)}
                  key={index}
                >
                  <div className={clsx(styles.blogImage)}>
                    <img src={!!item?.image ? `${item?.image}` : '/image/Rectangle-2.png'} alt="" />
                    <Link
                      href={{
                        pathname: `/diary/${item?.post_id}` || '/',
                      }}
                    >
                      <div className={clsx(styles.viewBlog)}>
                        <span>Xem chi tiết</span>
                        <img style={{ width: 32, height: 32 }} src="/icons/red-arrow.svg" alt="" />
                      </div>
                    </Link>
                  </div>
                  <div className={clsx(styles.blogTitle)}>
                    <span className="date">
                      {!!item?.publish_date
                        ? format(new Date(item?.publish_date), 'dd/MM/yyyy')
                        : 'Chưa có'}
                    </span>
                    <Link
                      href={{
                        pathname: `/diary/${item?.post_id}` || '/',
                      }}
                    >
                      <a>{item.name}</a>
                    </Link>
                  </div>
                </motion.div>
              ))}
            </>
          ) : (
            <>
              <div className="w-full text-center">
                <p>Không có sản phẩm</p>
              </div>
            </>
          )}
        </div>
        {(dataBlogPosts?.page_info?.total_pages as number) > 1 && (
          <div className={clsx(styles.paginationBar)}>
            <PaginationCustom
              current={!!router.query?.currentPage ? parseInt(`${router.query?.currentPage}`) : 1}
              pageSize={!!router?.query?.pageSize ? parseInt(`${router.query?.pageSize}`) : 20}
              total={
                !!dataBlogPosts?.page_info?.total_pages && !!dataBlogPosts?.page_info?.page_size
                  ? parseInt(`${dataBlogPosts?.page_info?.total_pages}`) *
                    parseInt(`${dataBlogPosts?.page_info?.page_size}`)
                  : !!router?.query?.pageSize
                  ? parseInt(`${router.query?.pageSize}`)
                  : 20
              }
              onChange={(page: number) =>
                router.push({
                  query: {
                    ...router.query,
                    currentPage: page,
                  },
                })
              }
              totalPage={
                !!dataBlogPosts?.page_info?.total_pages
                  ? parseInt(`${dataBlogPosts?.page_info?.total_pages}`)
                  : 1
              }
            />
          </div>
        )}
      </div>
    </div>
  )
}

const LazyBlogItem = () => {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 0.3, ease: 'easeInOut' }}
      className="fx-skeleton w-full h-full"
    >
      <div className="overlay z-[2]" />
      <div className={clsx(styles.blogItem)}>
        <div className={clsx(styles.blogImage)}>
          <div className={clsx(styles.img, 'skeholder w-full h-full rounded-[4px]')} />
        </div>
        <div className={clsx(styles.blogTitle, 'skeholder w-fit rounded-[4px]')}>
          ________________
        </div>
        <div className={clsx(styles.blogTitle, 'py-1 skeholder w-fit rounded-[4px]')}>
          _______________________________________
        </div>
      </div>
    </motion.div>
  )
}
