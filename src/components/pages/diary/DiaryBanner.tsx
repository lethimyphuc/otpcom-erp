'use client'
/* eslint-disable @next/next/no-img-element */
import React from 'react'
import { BreadcrumbBanner } from '~src/components/global/Layout/BreadcrumbBanner'
import clsx from 'clsx'
import styles from './Diary.module.scss'
import { motion } from 'framer-motion'
import { BreadcrumbHead } from '~src/components/global/Layout/BreadcrumHead'

export const DiaryBanner = ({ dataCms }: { dataCms: any }) => {
  return (
    <div className="banner-promotion">
      <BreadcrumbHead />
      <div className="img">
        <img src={dataCms?.image} alt="" className=" w-full" />
      </div>
      {/* <div className="img-position">
        <div className="img">
          <img src="/image/21.png" alt="" />
        </div>
      </div> */}
      {/* <div className="content-inner !translate-x-[-50%] [&_ol]:justify-start containers">
        <div className="breadcrum">
          <BreadcrumbBanner breadcrumbHead={false} />
        </div>
      </div> */}
    </div>
  )

  // return (
  //   <div className={clsx(styles.titleSection)}>
  //     <div className="w-full h-full">
  //       {!bannerImage ? (
  //         <div className="z-[0] absolute inset-0 h-full w-full">
  //           <LazyBanner />
  //         </div>
  //       ) : (
  //         <motion.img
  //           initial={{ opacity: 0, filter: 'blur(8px)' }}
  //           animate={{ opacity: 1, filter: 'blur(0)' }}
  //           src={bannerImage}
  //           alt=""
  //         />
  //       )}
  //     </div>
  //     <div className={clsx(styles.breadcrumb)}>
  //       <BreadcrumbBanner breadcrumbHead={false} />
  //     </div>
  //     <div className={clsx(styles.title)}>
  //       <h1>nhật ký</h1>
  //     </div>
  //     <img className={clsx(styles.gear)} src="/image/212.png" alt="" />
  //   </div>
  // )
}

const LazyBanner = () => {
  return (
    <div className="fx-skeleton w-full h-full">
      <div className="overlay" />
      <div className="skeholder w-full h-full" />
      <div
        style={{
          background: 'linear-gradient(to top,rgba(255,255,255,1) 30%, rgba(255,255,255,0))',
        }}
        className="absolute bottom-0 left-0 right-0 h-32"
      />
    </div>
  )
}
