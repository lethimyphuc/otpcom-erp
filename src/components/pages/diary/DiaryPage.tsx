/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useMemo, useState } from 'react'
import clsx from 'clsx'
import styles from './Diary.module.scss'
import { PaginationCustom } from '~src/components/common/PaginationCustom'
import Fancybox from './components/Fancybox'
import { BreadcrumbBanner } from '~src/components/global/Layout/BreadcrumbBanner'
import Link from 'next/link'
import { FormSelect } from '~src/components/global/FormControls/FormSelect'
import { useForm } from 'react-hook-form'
import { useRouter } from 'next/router'
import { useQuery } from 'react-query'
import {graphQLClient, graphQLClientGET} from '~src/api/graphql'
import { GET_LIST_CATEGORY_POST, GET_LIST_POST } from '~src/api/post'
import { format } from 'date-fns'
import { DiaryBanner } from './DiaryBanner'
import { GET_BANNER_PAGE } from '~src/api'
import { DiaryBlog } from './DiaryBlog'
import { BannerBrand } from '../brand/BannerBrand'
import { removeEmptyValueFromObject } from '~src/util'
import icon from '~src/assets/image/icon.png'

export interface IDiaryPageProps {}

export default function DiaryPage(props: IDiaryPageProps) {
  const { control, setValue, handleSubmit } = useForm({
    defaultValues: {
      sortBy: '',
    },
  })
  const [itemToShow, setItemToShow] = useState(6)
  const router = useRouter()

  useEffect(() => {
    setValue('sortBy', !!router.query?.sortBy ? `${router.query?.sortBy}` : 'Popular')
  }, [])

  const { data: dataContentDiary, isLoading: isLoadingContentDiary } = useQuery({
    queryKey: ['getContentDiaryPage'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_BANNER_PAGE, {
        identifier: 'diary-banner',
      })) as {
        managebanner: any
      }
      return data?.managebanner
    },
    cacheTime: Infinity,
  })

  useEffect(() => {
    if (!!router.query && !!Object.keys(router.query).length) {
      router.replace({
        query: {
          ...router.query,
          currentPage: !!router.query?.currentPage ? router.query.currentPage : '1',
          pageSize: !!router.query?.pageSize ? router.query.pageSize : '12',
          sortBy: !!router.query?.sortBy ? router.query?.sortBy : 'Popular',
        },
      })
    } else {
      router.replace({
        query: {
          currentPage: '1',
          pageSize: '12',
          sortBy: 'Popular',
        },
      })
    }
  }, [])

  const { data: dataBlogImagePosts, isLoading: isLoadingDataBlogImagePosts } = useQuery({
    queryKey: ['getBlogImagePosts', router.query],
    queryFn: async () => {
      const DATA_FILTER: any = {
        sortBy: !!router.query?.sortBy ? `${router.query?.sortBy}` : 'Popular',
        action: 'get_post_by_categoryId',
        pageSize: !!router?.query?.pageSize ? parseInt(`${router.query.pageSize}`) : 12,
        currentPage: !!router?.query?.currentPage ? parseInt(`${router.query.currentPage}`) : 1,
        categoryId: 3,
      }
      if (!!router.query?.categoryId) {
        DATA_FILTER.categoryId = parseInt(`${router.query?.categoryId}`)
      }
      const data = (await graphQLClient.request(GET_LIST_POST, DATA_FILTER)) as {
        mpBlogPosts: {
          items: TPost[]
          page_info: TPageInfo
          total_count: number
        }
      }
      return data?.mpBlogPosts
    },
  })
  function handleShowMore() {
    if (itemToShow == 6) {
      setItemToShow((prev) => prev + (dataBlogImagePosts?.items?.length || 6))
    } else {
      setItemToShow(6)
    }
  }
  return (
    <div className={clsx(styles.diaryContainer)}>
      <div className="banner-page">
        <DiaryBanner dataCms={dataContentDiary} />
      </div>

      <DiaryBlog formControl={control} />
      <div className="max-w-[100%] m-auto px-[15px]">
        <div className="list-search md:mt-20 mt-5">
          <div className="title-search">
            <div className="icon-center">
              <img src="/icons/search.svg" alt="" />
            </div>
            <div className="content-text">
              <span className="text">hình ảnh và video</span>
            </div>
          </div>
        </div>
        <div className="relative w-full">
          <div className={clsx(styles.galleryContainer)}>
            <Fancybox
              options={{
                Carousel: {
                  infinite: false,
                },
              }}
            >
              {dataBlogImagePosts?.items?.slice(0, itemToShow).map((item: any, index: number) => (
                <div className={clsx(styles.imageItem)} key={index}>
                  <a data-fancybox={item.post_id} href={item.image}>
                    <img className={clsx(styles.imageAlbum)} alt="" src={item.image} />
                    <div className={clsx(styles.viewImage)}>
                      <img src="/image/zoom-in.png" alt="" />
                    </div>
                  </a>
                  <div className="hidden">
                    {item.album_images.map((itm: any, index: number) => (
                      <a href={itm.image} key={index} data-fancybox={item.post_id}>
                        <img className={clsx(styles.imageAlbum)} alt="" src={itm.image} />
                      </a>
                    ))}
                  </div>
                </div>
              ))}
            </Fancybox>
          </div>
          <button
            className="w-full flex justify-center items-center text-black70 z-10 relative mt-12 hover:scale-105 transition-all duration-200 text-[#239f40]"
            onClick={handleShowMore}
          >
            {(dataBlogImagePosts?.items?.length || 6) < itemToShow ? (
              <>
                Thu gọn
                <img
                  src="/icons/arrow-up-bold.svg"
                  alt="arrow"
                  className="max-w-[12px] ml-2 opacity-70"
                />
              </>
            ) : (
              <>
                Xem thêm
                <img
                  src="/icons/arrow-down.svg"
                  alt="arrow"
                  className="max-w-[12px] ml-2 opacity-70"
                />
              </>
            )}
          </button>
          {(dataBlogImagePosts?.items?.length || 6) > itemToShow && (
            <div className={clsx(styles.bgLinear)}></div>
          )}
        </div>
      </div>
    </div>
  )
}
