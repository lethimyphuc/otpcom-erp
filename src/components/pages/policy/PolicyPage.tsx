/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from 'react'
import { useQuery } from 'react-query'
import { BreadcrumbHead } from '~src/components/global/Layout/BreadcrumHead'
import { useRouter } from 'next/router'
import {graphQLClient, graphQLClientGET} from '~src/api/graphql'
import { GET_LIST_POLICY } from '~src/api/post'
import parse from 'html-react-parser'
import { Empty, Skeleton } from 'antd'

type Props = {}

const PolicyPage = (props: Props) => {
  const router = useRouter()
  const [selectPolies, setSelectPolies] = useState<TPost>()

  const { data: dataPolies, isLoading: isLoadingdataPolies } = useQuery({
    queryKey: ['getDataPolies'],
    queryFn: async () => {
      const DATA_FILTER: any = {
        action: 'get_post_by_categoryId',
        categoryId: 4,
      }
      if (!!router.query?.categoryId) {
        DATA_FILTER.categoryId = parseInt(`${router.query?.categoryId}`)
      }
      const data = (await graphQLClientGET.request(GET_LIST_POLICY, DATA_FILTER)) as {
        mpBlogPosts: {
          items: TPost[]
          page_info: TPageInfo
          total_count: number
        }
      }
      return data?.mpBlogPosts
    },
  })
  useEffect(() => {
    if (router.query?.slug) {
      const data = dataPolies?.items?.find((item) => item.url_key.toString() == router.query?.slug)
      setSelectPolies(data)
    }
    // if (!router.query?.policyId && !!dataPolies?.items?.length) {
    //   router.push({
    //     pathname: router.pathname,
    //     query: { ...router.query, policyId: dataPolies?.items[0]?.post_id },
    //   })
    // }
  }, [dataPolies?.items, dataPolies?.items.length, router])
  if (!!isLoadingdataPolies) {
    return (
      <>
        <div className="w-full grid mb-28 md:mb-14">
          <Skeleton.Input style={{ width: '100%', height: 40 }} active />
        </div>
        <div className="containers !mt-20 grid grid-flow-row md:grid-flow-col gap-8 md:gap-4 grid-cols-10 !mb-32">
          <div className="col-span-full md:col-span-7 gap-4 grid">
            <div className="w-full grid gap-4">
              <Skeleton.Input style={{ width: '100%', height: 60 }} active />
              <Skeleton.Input style={{ width: '100%', height: 400 }} active />
            </div>
            <div className="grid gap-3 w-full mt-4 md:mt-0">
              <Skeleton.Input style={{ width: '100%', height: 40 }} active />
              <Skeleton.Input style={{ width: '100%', height: 40 }} active />
              <Skeleton.Input style={{ width: '100%', height: 40 }} active />
              <Skeleton.Input style={{ width: '100%', height: 40 }} active />
              <Skeleton.Input style={{ width: '100%', height: 40 }} active />
              <Skeleton.Input style={{ width: '100%', height: 40 }} active />
              <Skeleton.Input style={{ width: '100%', height: 40 }} active />
              <Skeleton.Input style={{ width: '100%', height: 40 }} active />
              <Skeleton.Input style={{ width: '100%', height: 40 }} active />
            </div>
          </div>
          <div className="col-span-full md:col-span-3">
            <div className="w-full">
              <Skeleton.Input
                style={{ width: '100%', height: 400 }}
                active
                className="!w-full rounded-3xl"
              />
            </div>
          </div>
        </div>
      </>
    )
  }

  if (!dataPolies) {
    return (
      <div className="py-96">
        <Empty description="Không tìm thấy bài viết hoặc đã bị xoá" />
      </div>
    )
  }

  return (
    <>
      <BreadcrumbHead
        breadcrumbs={[
          // { breadcrumbName: 'Chính sách', path: '/policy' },
          {
            breadcrumbName: selectPolies?.name || 'Chính sách',
            path: `/policy/${selectPolies?.url_key}`,
          },
        ]}
      />
      <div className=" containers !mt-20 grid grid-flow-row md:grid-flow-col lg:gap-12 md:gap-8 grid-cols-10 !mb-40">
        <div className="col-span-full md:col-span-7">
          <div className="font-bold lg:text-[40px] md:text-[32px] text-[24px] mb-8">
            {selectPolies?.name}
          </div>
          {!!selectPolies?.image && (
            <div className="w-full mb-10">
              <img
                className="w-full"
                src={!!selectPolies?.image ? `${selectPolies?.image}` : ''}
                alt=""
              />
            </div>
          )}

          <div className=" main-content-policy">
            {selectPolies?.post_content ? parse(selectPolies?.post_content) : ''}
          </div>
        </div>
        <div className="col-span-full md:col-span-3 mt-20 md:mt-0">
          <ul className="p-8 md:p-10 bg-[#f5f5f5] flex flex-col gap-4 rounded-[2.4rem]">
            <li>
              <h3 className="text-[20px] font-bold mb-2">Chính sách OTPCOM</h3>
            </li>
            {dataPolies?.items.map((item) => {
              return (
                <li
                  key={item.post_id}
                  className={`cursor-pointer hover:text-green duration-200 whitespace-nowrap ${
                    selectPolies?.post_id === item.post_id ? 'text-green font-bold' : 'text-black'
                  }`}
                  onClick={() => {
                    setSelectPolies(item)
                    router.push({
                      pathname: '/policy/' + item.url_key,
                    })
                  }}
                >
                  <div className="flex justify-between">
                    <p>{item.name}</p>
                  </div>
                </li>
              )
            })}
          </ul>
        </div>
      </div>
    </>
  )
}

export default PolicyPage
