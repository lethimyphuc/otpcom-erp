/* eslint-disable @next/next/no-img-element */
import { Empty, Skeleton } from 'antd'
import clsx from 'clsx'
import { format } from 'date-fns'
import parse from 'html-react-parser'
import { FacebookIcon, FacebookShareButton, TwitterIcon, TwitterShareButton } from 'next-share'
import Link from 'next/link'
import { NextRouter } from 'next/router'
import { useQuery } from 'react-query'
import { graphQLClientGET } from '~src/api/graphql'
import { GET_LIST_CATEGORY_POST, GET_POST_DETAIL } from '~src/api/post'
import { BreadcrumbHead } from '~src/components/global/Layout/BreadcrumHead'
import styles from './DiaryDetail.module.scss'
export interface IDiaryDetailPageProps {
  router: NextRouter
}

export default function DiaryDetailPage(props: IDiaryDetailPageProps) {
  const { router } = props

  const { data: dataBlogPostDetail, isLoading: isLoadingDataBlogPostDetail } = useQuery({
    queryKey: ['getBlogPostDetail', router.query?.slug],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_POST_DETAIL, {
        postId: !!router.query?.slug ? parseInt(`${router.query?.slug}`) : '',
        action: 'get_post_list',
      })) as any
      return data?.mpBlogPostGetById
    },
  })

  const { data: dataCategoryPosts, isLoading: isLoadingDataCategoryPosts } = useQuery({
    queryKey: ['getCategoryPosts', router.query],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_LIST_CATEGORY_POST, {
        sortBy: !!router.query?.sortBy ? `${router.query?.sortBy}` : 'asc',
        action: 'get_category_list',
        pageSize: 10,
        currentPage: 1,
      })) as {
        mpBlogCategories: {
          items: TCategoryPost[]
          page_info: TPageInfo
          total_count: number
        }
      }

      return data?.mpBlogCategories?.items?.filter(
        (category: TCategoryPost) => category?.level === 1
      )
    },
  })

  if (!!isLoadingDataBlogPostDetail) {
    return (
      <>
        <div className="w-full grid mb-28 sm:mb-14">
          <Skeleton.Input style={{ width: '100%', height: 40 }} active />
        </div>
        <div className="px-[10%] mt-[10px] mb-[60px]">
          <div className="sm:flex gap-3 items-start mb-3 w-full">
            <div className="w-full sm:w-[40%] grid">
              <Skeleton.Input style={{ width: '100%', height: 400 }} active />
            </div>
            <div className="grid gap-3 w-full sm:w-[60%] mt-4 sm:mt-0">
              <Skeleton.Input style={{ width: '100%', height: 40 }} active />
              <Skeleton.Input style={{ width: '100%', height: 40 }} active />
              <Skeleton.Input style={{ width: '100%', height: 40 }} active />
              <Skeleton.Input style={{ width: '100%', height: 40 }} active />
              <Skeleton.Input style={{ width: '100%', height: 40 }} active />
              <Skeleton.Input style={{ width: '100%', height: 40 }} active />
              <Skeleton.Input style={{ width: '100%', height: 40 }} active />
              <Skeleton.Input style={{ width: '100%', height: 40 }} active />
              <Skeleton.Input style={{ width: '100%', height: 40 }} active />
            </div>
          </div>
          <Skeleton paragraph={{ rows: 10 }} active />
        </div>
      </>
    )
  }

  if (!dataBlogPostDetail) {
    return (
      <div className="py-96">
        <Empty description="Không tìm thấy bài viết hoặc đã bị xoá" />
      </div>
    )
  }

  return (
    <div className={clsx(styles.diaryDetailContainer)}>
      <BreadcrumbHead
        breadcrumbs={[
          { breadcrumbName: 'Nhật ký', path: '/diary' },
          {
            breadcrumbName: dataBlogPostDetail?.name || 'Chi tiết nhật ký',
            path: `/diary/${dataBlogPostDetail?.post_id || router.query?.slug}`,
          },
        ]}
      />
      <div className={clsx(styles.diaryDetail)}>
        <div className={clsx(styles.leftSection)}>
          {dataBlogPostDetail?.categories?.items?.map((category: any) => {
            return <span key={`${category?.category_id}`}>{category?.name}</span>
          })}

          <h1>{!!dataBlogPostDetail ? dataBlogPostDetail?.name : ''}</h1>
          <div className={clsx(styles.intro)}>
            <div className={clsx(styles.description)}>
              <img src={'/icons/calendar.svg'} alt="" />
              <p>
                {!!dataBlogPostDetail && !!dataBlogPostDetail?.publish_date
                  ? format(new Date(dataBlogPostDetail?.publish_date), 'dd/MM/yyyy')
                  : ''}
              </p>
              <p>|</p>
              <img src={'/icons/user-icon.svg'} alt="" />
              <p className="whitespace-nowrap">
                Đăng bởi {!!dataBlogPostDetail ? dataBlogPostDetail?.author_name : ''}
              </p>
            </div>
            <div className={clsx(styles.socialIcon, '!hidden xs:!flex')}>
              <TwitterShareButton url={window.location.href} title={dataBlogPostDetail?.name}>
                <TwitterIcon size={26} round />
              </TwitterShareButton>
              <FacebookShareButton
                url={window.location.href}
                quote={dataBlogPostDetail?.name}
                hashtag={'#otpcom'}
                title={dataBlogPostDetail?.name}
              >
                <FacebookIcon size={26} round />
              </FacebookShareButton>
            </div>
          </div>
          <div className={clsx(styles.detailImage)}>
            <img
              src={
                !!dataBlogPostDetail?.image
                  ? `${dataBlogPostDetail?.image}`
                  : '/image/rectangle-79.3.png'
              }
              alt=""
            />
          </div>
          <div className={clsx(styles.diaryContent)}>
            {!!dataBlogPostDetail?.post_content ? parse(dataBlogPostDetail?.post_content) : ''}
          </div>
          <div className={clsx(styles.recentDiary)}>
            <div className={clsx(styles.recentItem)}>
              <p>Tin trước đó</p>
              {!!dataBlogPostDetail &&
              !!dataBlogPostDetail?.posts?.items?.length &&
              !!dataBlogPostDetail?.posts?.items[0] ? (
                <Link
                  href={{
                    pathname: `/diary/${dataBlogPostDetail?.posts?.items[0]?.post_id}`,
                  }}
                >
                  <div className={clsx(styles.recentTitle)}>
                    <i className="far fa-angle-left"></i>
                    <p>
                      {!!dataBlogPostDetail &&
                      !!dataBlogPostDetail?.posts?.items?.length &&
                      !!dataBlogPostDetail?.posts?.items[0]
                        ? dataBlogPostDetail?.posts?.items[0]?.name
                        : 'Không có'}
                    </p>
                  </div>
                </Link>
              ) : (
                <div className={clsx(styles.recentTitle, 'cursor-default')}>
                  <i className="far fa-angle-left"></i>
                  <p>
                    {!!dataBlogPostDetail &&
                    !!dataBlogPostDetail?.posts?.items?.length &&
                    !!dataBlogPostDetail?.posts?.items[0]
                      ? dataBlogPostDetail?.posts?.items[0]?.name
                      : 'Không có'}
                  </p>
                </div>
              )}
            </div>
            <div className={clsx(styles.recentItem, styles.recentItemActive)}>
              <p>Tin tiếp theo</p>
              {!!dataBlogPostDetail &&
              !!dataBlogPostDetail?.posts?.items?.length &&
              !!dataBlogPostDetail?.posts?.items[1] ? (
                <Link
                  href={{
                    pathname: `/diary/${dataBlogPostDetail?.posts?.items[1]?.post_id}`,
                  }}
                >
                  <div className={clsx(styles.recentTitle)}>
                    <p>
                      {!!dataBlogPostDetail &&
                      !!dataBlogPostDetail?.posts?.items?.length &&
                      !!dataBlogPostDetail?.posts?.items[1]
                        ? dataBlogPostDetail?.posts?.items[1]?.name
                        : 'Không có'}
                    </p>
                    <i className="far fa-angle-right"></i>
                  </div>
                </Link>
              ) : (
                <div className={clsx(styles.recentTitle, 'cursor-default')}>
                  <p className="w-full">Không có</p>
                  <img className={clsx(styles.next)} src={'/icons/next.svg'} alt="" />
                </div>
              )}
            </div>
          </div>
        </div>
        <div className={clsx(styles.rightSection, 'mt-8 lg:mt-0')}>
          <div className={clsx(styles.categories)}>
            <h3>Danh mục bài viết</h3>
            <ul>
              {dataCategoryPosts?.map((category: TCategoryPost) => {
                return (
                  <Link
                    href={{
                      pathname: '/diary',
                      query: {
                        categoryId: category?.category_id,
                      },
                    }}
                    key={`${category?.category_id}`}
                  >
                    <li>
                      <a>
                        <p>{category?.name}</p>
                        <img className={clsx(styles.arrow)} src={'/icons/arrow-right.svg'} alt="" />
                        <img
                          className={clsx(styles.greenArrow)}
                          src={'/icons/arrow-right-green.svg'}
                          alt=""
                        />
                      </a>
                    </li>
                  </Link>
                )
              })}
            </ul>
          </div>
          <div className={clsx(styles.relatedDiary)}>
            <h3>Tin tức liên quan</h3>
            <ul>
              {!!dataBlogPostDetail?.posts?.items?.length ? (
                <>
                  {dataBlogPostDetail?.posts?.items?.map((item: TPost, index: number) => {
                    return (
                      <li key={index}>
                        <div className={clsx(styles.relatedDiaryImage, '!h-auto')}>
                          <img
                            className={clsx(styles.imageItem, '!h-full')}
                            src={!!item?.image ? `${item?.image}` : '/image/related-diary.png'}
                            alt=""
                          />
                          <span>
                            <img src="/icons/cross-arrow.svg" alt="" />
                          </span>
                        </div>
                        <div className={clsx(styles.relatedDiaryTitle)}>
                          <p>{!!item?.name ? item?.name : ''}</p>
                          <Link
                            href={{
                              pathname: `/diary/${item?.post_id}`,
                            }}
                          >
                            <a>
                              <p>Xem thêm</p>
                              <img
                                className={clsx(styles.arrow)}
                                src={'/icons/arrow-right.svg'}
                                alt=""
                              />
                              <img
                                className={clsx(styles.greenArrow)}
                                src={'/icons/arrow-green-next.svg'}
                                alt=""
                              />
                            </a>
                          </Link>
                        </div>
                      </li>
                    )
                  })}
                </>
              ) : (
                <p>Không có</p>
              )}
            </ul>
          </div>
        </div>
      </div>
    </div>
  )
}
