import { DefaultOptionType } from 'antd/lib/select'
import { NextRouter } from 'next/router'
import { Dispatch, SetStateAction, useEffect, useMemo } from 'react'
import { useForm } from 'react-hook-form'
import { useQuery } from 'react-query'
import { GET_BRANDS } from '~src/api/categories'
import { graphQLClientGET } from '~src/api/graphql'
import { ButtonMain } from '~src/components/global/Button/ButtonMain/ButtonMain'
import { FormSelect } from '~src/components/global/FormControls/FormSelect'

type ProductFilterProps = {
  router: NextRouter
  filterData: any
  banner?: any
  title: any
  onChangeImageCar: Dispatch<SetStateAction<string | undefined>>
}
export const FormFilterBrand = (props: ProductFilterProps) => {
  const { router, banner, title, filterData, onChangeImageCar } = props
  const { control, handleSubmit, setValue, watch, getValues } = useForm<BoxSearchFormBrand>({
    defaultValues: {
      type: null,
      brand: null,
      sort: null,
    },
  })

  const slugs = useMemo(() => router.query.slug!, [router])

  const carTypeSlug = useMemo<string | null>(() => {
    return (slugs || [])[0] ?? null
  }, [slugs])
  const carBrandSlug = useMemo<string | null>(() => {
    return (slugs || [])[1] ?? null
  }, [slugs])
  // Danh sách loại Thương hiệu
  const {
    data: dataCategoriesByType,
    isLoading: isLoadingDataCategoriesByType,
  }: {
    data?: any[]
    isLoading?: boolean
  } = useQuery({
    queryKey: ['getCategoriesByType'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_BRANDS, {
        pageSize: 20,
        currentPage: 1,
      })) as any
      return data?.BrandClassifications?.items
    },
  })
  // Danh sách Thương hiệu
  const dataCategoriesByBrand = useMemo<any>(() => {
    return dataCategoriesByType?.find((v) => v.query_ids === watch('type'))?.items ?? null
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dataCategoriesByType, watch('type')])

  const handleSelectCategoryType = (
    value: any,
    option?: DefaultOptionType | DefaultOptionType[] | undefined
  ) => {
    setValue('type', value)
    setValue('brand', null)
  }
  //get query_ids of category by name
  const carTypePickedDataBySlug = useMemo<any>(() => {
    setValue(
      'type',
      dataCategoriesByType?.find((v) => v.url_key === '/brand/' + carTypeSlug)?.query_ids
    )
    return (dataCategoriesByType || []).find((v) => v.url_key === '/brand/' + carTypeSlug) ?? null
  }, [setValue, dataCategoriesByType, carTypeSlug])

  const carBrandPickedDataBySlug = useMemo<any>(() => {
    setValue(
      'brand',
      dataCategoriesByBrand?.find((v: any) => v.url_key === '/brand/' + carBrandSlug)?.query_ids
    )
    return (
      (dataCategoriesByBrand || []).find(
        (v: any) => v.url_key == '/brand/' + carTypeSlug + '/' + carBrandSlug
      ) ?? null
    )
  }, [setValue, dataCategoriesByBrand, carBrandSlug, carTypeSlug])

  //get banner image of category
  useEffect(() => {
    if (!isLoadingDataCategoriesByType) {
      if (carBrandPickedDataBySlug?.image_banner) {
        banner(carBrandPickedDataBySlug?.image_banner)
      } else if (carTypePickedDataBySlug?.image_banner) {
        banner(carTypePickedDataBySlug?.image_banner)
      } else {
        banner('/image/bg-product.png')
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [carTypePickedDataBySlug, isLoadingDataCategoriesByType])
  useEffect(() => {
    if (dataCategoriesByType?.find?.((item) => item.query_ids === watch('type'))?.image) {
      return onChangeImageCar?.(
        dataCategoriesByType?.find?.((item) => item.query_ids === watch('type'))?.image
      )
    }
    if (dataCategoriesByBrand?.find?.((item: any) => item.query_ids === watch('brand'))?.image) {
      return onChangeImageCar?.(
        dataCategoriesByBrand?.find?.((item: any) => item.query_ids === watch('brand'))?.image
      )
    }
    onChangeImageCar?.(undefined)
  }, [dataCategoriesByBrand, dataCategoriesByType, filterData, onChangeImageCar, watch])

  //get title of category
  const setTitle = useMemo<any>(() => {
    if (carBrandPickedDataBySlug?.name) {
      return carBrandPickedDataBySlug?.name
    } else if (carTypePickedDataBySlug?.name) {
      return carTypePickedDataBySlug?.name
    } else {
      return 'thương hiệu'
    }
  }, [carBrandPickedDataBySlug, carTypePickedDataBySlug])
  useEffect(() => {
    title(setTitle)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setTitle])

  // // get Name of category
  const carTypePickedDataByUID = useMemo<any>(() => {
    return (dataCategoriesByType || []).find((v) => v.query_ids === watch('type')) ?? null
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dataCategoriesByType, watch('type')])

  const carBrandPickedDataByUID = useMemo<TChildrenCategories | null>(() => {
    return (dataCategoriesByBrand || []).find((v: any) => v.query_ids === watch('brand')) ?? null
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dataCategoriesByBrand, watch('brand')])

  useEffect(() => {
    carTypePickedDataBySlug && setValue('type', carTypePickedDataBySlug?.query_ids)
    props.filterData(watch())
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [carTypePickedDataBySlug])

  useEffect(() => {
    carBrandPickedDataBySlug && setValue('brand', carBrandPickedDataBySlug?.query_ids)
    props.filterData(watch())
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [carBrandPickedDataBySlug])

  const onSubmit = (data: BoxSearchFormBrand) => {
    const carTypePath = `/${carTypePickedDataByUID?.url_key ?? ''}`
    const carBrandPath = `/${carBrandPickedDataByUID?.url_key ?? ''}`
    let finalQuery

    if (!!carBrandPickedDataByUID) {
      finalQuery = `${carBrandPath}`
    } else if (!!carTypePickedDataByUID) {
      finalQuery = `${carTypePath}`
    } else {
      finalQuery = ''
    }

    props.filterData(data)
    router.replace(
      {
        pathname: finalQuery == '' ? '/brand' : finalQuery.slice(1),
      },
      undefined,
      { scroll: false }
    )
  }

  return (
    <div className="form-filter-product">
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="box">
          <div className="items">
            <div className="itemAccesories">
              <div className="item-label">Sắp xếp theo:</div>
              <FormSelect
                control={control}
                name={'sort'}
                optionList={[
                  { label: 'Tên A -> Z', value: '{"name":"ASC"}' },
                  { label: 'Tên Z -> A', value: '{"name":"DESC"}' },
                  { label: 'Giá tăng dần', value: '{"price":"ASC"}' },
                  { label: 'Giá giảm dần', value: '{"price":"DESC"}' },
                ]}
                label=""
                placeholder="Sắp xếp theo"
                allowClear
              />
            </div>
            <div className="itemAccesories">
              <div className="item-label">Lọc theo:</div>
              <FormSelect
                control={control}
                name={'type'}
                optionList={dataCategoriesByType?.map((company: any) => {
                  return {
                    label: company?.name,
                    value: company?.query_ids,
                  }
                })}
                onChange={handleSelectCategoryType}
                label=""
                placeholder="Loại thương hiệu"
                allowClear
              />
            </div>
            <div className="itemAccesories">
              <FormSelect
                control={control}
                name={'brand'}
                optionList={dataCategoriesByBrand?.map((type: any) => {
                  return {
                    label: type?.name,
                    value: type?.query_ids,
                  }
                })}
                // onChange={handleSelectCategoryType}
                label=""
                placeholder="Thương hiệu"
                allowClear
              />
            </div>
          </div>
          <div className="button-search h-[45px]">
            <ButtonMain background="green" type="button" onClick={handleSubmit(onSubmit)}>
              <div className="flex items-center gap-[0.4rem]">
                <div className="icon">
                  <img src="/icons/search-icon.svg" alt="" />
                </div>
                <div className="text">Tìm kiếm</div>
              </div>
            </ButtonMain>
          </div>
        </div>
      </form>
    </div>
  )
}
