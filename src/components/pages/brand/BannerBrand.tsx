/* eslint-disable @next/next/no-img-element */
import { Route } from 'antd/lib/breadcrumb/Breadcrumb'
import { motion } from 'framer-motion'
import { BreadcrumbHead } from '~src/components/global/Layout/BreadcrumHead'

export const BannerBrand = ({
  bannerImage,
  breadcrumbs,
  imageCar,
}: {
  customBreadscumb?: { label: string; href: string }[]
  bannerImage?: any
  breadcrumbs?: Route[]
  imageCar?: string
}) => {
  return (
    <div className="banner-promotion">
      <BreadcrumbHead breadcrumbs={breadcrumbs} />
      <div className="img 2xl:min-h-[40rem] min-h-[20rem]">
        <img
          className="w-full max-h-[600px] object-cover"
          src={bannerImage || ''}
          alt=""
          srcSet={bannerImage || ''}
        />
      </div>
      <div className="img-position">
        <div className="img">
          <img src="/image/21.png" alt="" />
        </div>
      </div>
      {imageCar && (
        <motion.div
          key={imageCar}
          initial={{ opacity: 0, x: '100%', top: '10% !important' }}
          animate={{ opacity: 1, x: '50%' }}
          transition={{ duration: 0.7 }}
          className="absolute sm:top-[35%] top-[40%] w-[45%] right-1/2 translate-x-1/2 -translate-y-1/5 sm:-translate-y-1/4"
        >
          <div className="img">
            <img src={imageCar} alt="" className="w-[930px] " />
          </div>
        </motion.div>
      )}
    </div>
  )
}
