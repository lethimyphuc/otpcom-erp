import { Route } from 'antd/lib/breadcrumb/Breadcrumb'
import { useRouter } from 'next/router'
import { useMemo, useState } from 'react'
import { useQuery } from 'react-query'
import { GET_BANNER_PAGE } from '~src/api'
import { graphQLClientGET } from '~src/api/graphql'
import { GET_PRODUCTS } from '~src/api/product'
import { PaginationCustom } from '~src/components/common/PaginationCustom'
import { TitleProduct } from '~src/components/global/TitlePage/TitleProduct'
import { BannerBrand } from '../brand/BannerBrand'
import { ProductList } from '../product/ProductList'
import { FormFilterAccessories } from './FormFilterAccessories'

export const AccessoriesPage = () => {
  const router = useRouter()
  const [filter, setFilter] = useState<any>({})

  const [bannerImage, setBannerImage] = useState<string | undefined>(undefined)
  const [titleName, setTitleName] = useState<string | undefined>(undefined)
  const [imageCar, setImageCar] = useState<string | undefined>(undefined)

  const { data: dataProducts, isLoading: isLoadingDataProducts } = useQuery({
    queryKey: ['getProducts', filter, router],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_PRODUCTS, {
        filter: {
          category_uid: {
            eq: filter.accessories ? filter.accessories : null,
          },
          tags: {
            eq: filter.tag ? filter.tag : null,
          },
        },
        sort: JSON.parse(filter.sort ?? '{}'),
        search: !!router.query?.search ? `${router.query?.search}`.trim() : '',
        pageSize: !!router?.query?.pageSize ? parseInt(`${router.query.pageSize}`) : 20,
        currentPage: !!router?.query?.currentPage ? parseInt(`${router.query.currentPage}`) : 1,
      })) as {
        products: {
          items: TProduct[]
          page_info: {
            current_page: number
            page_size: number
            total_pages: number
          }
        }
      }
      return data?.products
    },
  })

  const { data: contentAccessoriesPage, isLoading: isloadingCategoryPage } = useQuery<any>({
    queryKey: ['getContentAccessoriesPage'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_BANNER_PAGE, {
        identifier: 'accessories-default-banner',
      })) as {
        managebanner: any
      }
      return data?.managebanner
    },
    cacheTime: Infinity,
  })
  const { data: dataCategoriesByType }: { data: any } = useQuery({
    queryKey: ['getCategoriesByType', 'accessories'],
    enabled: false,
  })

  const breadcrumb: Route[] = useMemo(() => {
    let breadcrumb: Route[] = []
    let currentType = dataCategoriesByType?.find((item: any) => item?.uid === filter?.accessories)

    if (filter?.accessories && dataCategoriesByType?.length) {
      breadcrumb.push({
        breadcrumbName: currentType?.name,
        path: currentType?.url_key,
      })
    }
    if (filter?.tag && currentType?.tags?.length) {
      let currentBrand = currentType?.tags?.find((item: any) => item?.id == filter?.tag)
      breadcrumb.push({
        breadcrumbName: currentBrand?.name,
        path: currentBrand?.url_key,
      })
    }

    return breadcrumb
  }, [dataCategoriesByType, filter])

  return (
    <div className="car-company branch-page">
      <div className="banner-page">
        <BannerBrand
          breadcrumbs={[...breadcrumb]}
          bannerImage={
            !isloadingCategoryPage &&
            (bannerImage
              ? bannerImage
              : contentAccessoriesPage.image
              ? contentAccessoriesPage.image
              : '/image/bg-product.png')
          }
          imageCar={imageCar}
        />
      </div>
      <div className="containers">
        <div className="title-promotion">
          <TitleProduct title={titleName} />
        </div>
        <div className="content-inner-promotion">
          <div className="filter-promotion">
            <FormFilterAccessories
              filterData={setFilter}
              router={router}
              banner={setBannerImage}
              title={setTitleName}
              onChangeImageCar={setImageCar}
            />
          </div>
          <div className="wrapper-product mb-12">
            <ProductList data={dataProducts?.items} loading={isLoadingDataProducts} />
          </div>
          {(dataProducts?.page_info?.total_pages as any) > 1 && (
            <div className="pagination-promotion">
              <PaginationCustom
                current={!!router.query?.currentPage ? parseInt(`${router.query?.currentPage}`) : 1}
                pageSize={!!router?.query?.pageSize ? parseInt(`${router.query?.pageSize}`) : 20}
                total={
                  !!dataProducts?.page_info?.total_pages && !!dataProducts?.page_info?.page_size
                    ? parseInt(`${dataProducts?.page_info?.total_pages}`) *
                      parseInt(`${dataProducts?.page_info?.page_size}`)
                    : !!router?.query?.pageSize
                    ? parseInt(`${router.query?.pageSize}`)
                    : 20
                }
                onChange={(page: number) =>
                  router.push(
                    {
                      query: {
                        ...router.query,
                        currentPage: page,
                      },
                    },
                    undefined,
                    { scroll: false }
                  )
                }
                totalPage={
                  !!dataProducts?.page_info?.total_pages
                    ? parseInt(`${dataProducts?.page_info?.total_pages}`)
                    : 1
                }
              />
            </div>
          )}
        </div>
      </div>
    </div>
  )
}
