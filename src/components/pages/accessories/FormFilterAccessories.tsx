/* eslint-disable react-hooks/exhaustive-deps */
/**
 * /* eslint-disable react-hooks/exhaustive-deps
 *
 * @format
 */

/* eslint-disable @next/next/no-img-element */
import React, { Dispatch, SetStateAction, useEffect, useMemo } from 'react'
import { useForm } from 'react-hook-form'
import { useQuery } from 'react-query'
import { graphQLClient, graphQLClientGET } from '~src/api/graphql'
import { GET_CATEGORIES } from '~src/api/categories'
import { DefaultOptionType } from 'antd/lib/select'
import { NextRouter } from 'next/router'
import { FormSelect } from '~src/components/global/FormControls/FormSelect'
import { ButtonMain } from '~src/components/global/Button/ButtonMain/ButtonMain'

type ProductFilterProps = {
  router: NextRouter
  filterData: any
  banner: any
  title: (childData: string) => void
  onChangeImageCar: Dispatch<SetStateAction<string | undefined>>
}
export const FormFilterAccessories = (props: ProductFilterProps) => {
  const { router, banner, title, filterData, onChangeImageCar } = props
  const { control, handleSubmit, setValue, watch } = useForm<BoxSearchFormAccessories>({
    defaultValues: {
      accessories: null,
      tag: null,
      sort: null,
    },
  })
  const slugs = useMemo(() => router.query.slug!, [router])

  const carAccessoriesSlug = useMemo<string | null>(() => {
    return (slugs || [])[0] ?? null
  }, [slugs])
  const carTagSlug = useMemo<string | null>(() => {
    return (slugs || [])[1] ?? null
  }, [slugs])
  // Danh sách loại Thương hiệu
  const {
    data: dataCategoriesByAccessories,
    isLoading: isLoadingDataCategoriesByAccessories,
  }: {
    data?: any[]
    isLoading?: boolean
  } = useQuery({
    queryKey: ['getCategoriesByType', 'accessories'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_CATEGORIES, {
        eq: 'NTU=',
      })) as TCategories
      return data.categories?.items[0]?.children
    },
  })
  // Danh sách tag
  const dataCategoriesByBrand = useMemo<any>(() => {
    return dataCategoriesByAccessories?.find((v) => v.uid == watch('accessories'))?.tags ?? null
  }, [slugs, dataCategoriesByAccessories, watch('accessories')])

  const handleSelectCategoryType = (
    value: any,
    option?: DefaultOptionType | DefaultOptionType[] | undefined
  ) => {
    setValue('accessories', value)
    setValue('tag', null)
  }
  //get query_ids of category by name
  const carTypePickedDataBySlug = useMemo<any>(() => {
    setValue(
      'accessories',
      dataCategoriesByAccessories?.find((v) => v?.url_key === carAccessoriesSlug)?.uid
    )
    return dataCategoriesByAccessories?.find((v) => v?.url_key === carAccessoriesSlug) ?? null
  }, [dataCategoriesByAccessories, carAccessoriesSlug])

  const carBrandPickedDataBySlug = useMemo<any>(() => {
    setValue('tag', dataCategoriesByBrand?.find((v: any) => v?.id == carTagSlug)?.id)
    return dataCategoriesByBrand?.find((v: any) => v?.id == carTagSlug) ?? null
  }, [dataCategoriesByBrand, carTagSlug])

  // set image banner
  useEffect(() => {
    if (!isLoadingDataCategoriesByAccessories) {
      if (carTypePickedDataBySlug?.image_banner) {
        banner(carTypePickedDataBySlug?.image_banner)
      } else {
        banner(null)
      }
    }
  }, [carTypePickedDataBySlug, isLoadingDataCategoriesByAccessories])
  // set title
  const setTitle = useMemo<any>(() => {
    if (carBrandPickedDataBySlug?.name) {
      return carBrandPickedDataBySlug?.name
    } else if (carTypePickedDataBySlug?.name) {
      return carTypePickedDataBySlug?.name
    } else {
      return 'phụ tùng'
    }
  }, [carBrandPickedDataBySlug, carTypePickedDataBySlug])
  useEffect(() => {
    title(setTitle)
  }, [setTitle])

  // // get Name of category
  const carAccessoriesPickedDataByUID = useMemo<any>(() => {
    return (dataCategoriesByAccessories || []).find((v) => v.uid === watch('accessories')) ?? null
  }, [dataCategoriesByAccessories, watch('accessories')])

  const carTagPickedDataByUID = useMemo<TChildrenCategories | null>(() => {
    return (dataCategoriesByBrand || []).find((v: any) => v.id === watch('tag')) ?? null
  }, [dataCategoriesByBrand, watch('tag')])
  console.log('dataCategoriesByAccessories', dataCategoriesByAccessories)
  useEffect(() => {
    if (
      dataCategoriesByAccessories?.find?.((item) => item.uid === watch('accessories'))?.image_car
    ) {
      return onChangeImageCar?.(
        dataCategoriesByAccessories?.find?.((item) => item.uid === watch('accessories'))?.image_car
      )
    }
    // if (dataCategoriesByBrand?.find?.((item: any) => item.id === watch('tag'))?.logo) {
    //   return onChangeImageCar?.(
    //     dataCategoriesByBrand?.find?.((item: any) => item.id === watch('tag'))?.logo
    //   )
    // }
    onChangeImageCar?.(undefined)
  }, [dataCategoriesByBrand, filterData, onChangeImageCar, watch])

  useEffect(() => {
    carTypePickedDataBySlug && setValue('accessories', carTypePickedDataBySlug?.uid)
    props.filterData(watch())
    setValue('tag', null)
  }, [carTypePickedDataBySlug])

  useEffect(() => {
    carBrandPickedDataBySlug && setValue('tag', carBrandPickedDataBySlug?.id)
    props.filterData(watch())
  }, [carBrandPickedDataBySlug])

  const onSubmit = (data: BoxSearchFormAccessories) => {
    const carAccessoriesPath = `/${carAccessoriesPickedDataByUID?.url_key ?? ''}`
    const carTagPath = `/${carTagPickedDataByUID?.id ?? ''}`
    let finalQuery

    if (!!carTagPickedDataByUID) {
      finalQuery = `${carAccessoriesPath}${carTagPath}`
    } else if (!!carAccessoriesPickedDataByUID) {
      finalQuery = `${carAccessoriesPath}`
    } else {
      finalQuery = ''
    }

    props.filterData(data)
    router.replace({
      pathname: finalQuery == '' ? '/accessories' : '/accessories' + finalQuery,
    })
  }

  return (
    <div className="form-filter-product">
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="box">
          <div className="items">
            <div className="itemAccesories">
              <div className="item-label">Sắp xếp theo:</div>
              <FormSelect
                control={control}
                name={'sort'}
                optionList={[
                  { label: 'Tên A -> Z', value: '{"name":"ASC"}' },
                  { label: 'Tên Z -> A', value: '{"name":"DESC"}' },
                  { label: 'Giá tăng dần', value: '{"price":"ASC"}' },
                  { label: 'Giá giảm dần', value: '{"price":"DESC"}' },
                ]}
                label=""
                placeholder="Sắp xếp theo"
                allowClear
              />
            </div>
            <div className="itemAccesories">
              <div className="item-label">Lọc theo:</div>
              <FormSelect
                control={control}
                name={'accessories'}
                optionList={dataCategoriesByAccessories?.map((company: any) => {
                  return {
                    label: company?.name,
                    value: company?.uid,
                  }
                })}
                onChange={handleSelectCategoryType}
                label=""
                placeholder="Loại phụ tùng"
                allowClear
              />
            </div>
            <div className="itemAccesories">
              <FormSelect
                control={control}
                name={'tag'}
                optionList={dataCategoriesByBrand?.map((type: any) => {
                  return {
                    label: type?.name,
                    value: type?.id,
                  }
                })}
                // onChange={handleSelectCategoryType}
                label=""
                placeholder="Tag"
                allowClear
              />
            </div>
          </div>
          <div className="button-search h-[45px]">
            <ButtonMain background="green" type="button" onClick={handleSubmit(onSubmit)}>
              <div className="flex items-center gap-[0.4rem]">
                <div className="icon">
                  <img src="/icons/search-icon.svg" alt="" />
                </div>
                <div className="text">Tìm kiếm</div>
              </div>
            </ButtonMain>
          </div>
        </div>
      </form>
    </div>
  )
}
