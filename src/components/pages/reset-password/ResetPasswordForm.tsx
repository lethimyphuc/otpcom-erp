import React, { useEffect, useMemo } from 'react'
import clsx from 'clsx'
import { useForm } from 'react-hook-form'
import { ButtonMain } from '~src/components/global/Button/ButtonMain/ButtonMain'
import { InputField } from './InputField'
import { useMutation } from 'react-query'
import { graphQLClient } from '~src/api/graphql'
import { RESET_PASSWORD } from '~src/api'
import { toast } from 'react-toastify'
import { useRouter } from 'next/router'
import { TAuthenticate } from '~src/types/authenticate'

export const ResetPasswordForm = () => {
  const router = useRouter()
  const { ptoken: ptokenParam } = router.query

  const {
    control,
    getValues,
    handleSubmit,
    formState: { errors }
  } = useForm<TAuthenticate>({
    defaultValues: {
      email: '',
      password: '',
      repassword: ''
    }
  })

  const formCustomValidate = {
    emailValidate: (email: string) => {
      return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        email
      )
    },
    passWordValidate: (password: string | any) => {
      return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/.test(password)
    },
    rePasswordValidate: (rePassword: string | any) => {
      const password = getValues('password')
      return password === rePassword
    }
  }

  const { isLoading: isCallResetPassword, mutate: mutateResetPassword } = useMutation({
    mutationFn: async (payload: {
      email: string
      resetPasswordToken: string
      password: string
    }) => {
      await graphQLClient
        .request(RESET_PASSWORD, {
          email: payload.email,
          resetPasswordToken: payload.resetPasswordToken,
          newPassword: payload.password
        })
        .then((response: any) => {
          return response
        })
        .catch((err) => {
          toast.error(err?.response.errors[0]?.message)
        })
    },
    onSuccess: () => {
      toast('Đặt lại mật khẩu thành công!', { type: 'success' })
      setTimeout(() => {
        router.push('/login')
      }, 200)
    }
  })

  const onSubmit = (data: TAuthenticate) => {
    const payload = {
      email: `${data?.email ?? ''}`.trim(),
      resetPasswordToken: (ptokenParam as string) ?? '',
      password: `${data?.password ?? ''}`.trim()
    }
    mutateResetPassword(payload)
  }

  return (
    <>
      <div className="flex justify-start text-center lg:text-left mb-6 mt-12 xl:mt-16 w-[324px] md:w-[384px] xl:w-[484px] mx-auto border-none rounded-[40px] py-2">
        <div className="leading-[24px] py-3 text-[#00] z-10 w-full h-[40px] rounded-[40px] text-[16px] m-auto lg:m-0">
          Đặt lại mật khẩu mới
        </div>
      </div>

      <form
        className="form-login w-[32.4rem] mt-12 md:w-[38.4rem] lg:w-[32.0rem] xl:w-[48.4rem] m-auto"
        onSubmit={handleSubmit(onSubmit)}
      >
        {/* Email input */}
        <div>
          <InputField
            name="email"
            isRequired
            type="text"
            label="Địa chỉ Email"
            placeholder="Nhập email của bạn"
            customValidate={{
              validateFn: formCustomValidate.emailValidate,
              validateMess: 'Email không hợp lệ!'
            }}
            formControl={control}
            errorState={errors?.email}
          />
        </div>

        {/* Password input */}
        <div className="mt-6">
          <InputField
            name="password"
            isRequired
            type="password"
            label="Mật khẩu mới"
            placeholder="Nhập mật khẩu mới"
            customValidate={{
              validateFn: formCustomValidate.passWordValidate,
              validateMess:
                'Mật khẩu tối thiếu 8 ký tự, bao gồm ký tự chữ hoa, chữ thường và chữ số!'
            }}
            formControl={control}
            errorState={errors?.password}
          />
        </div>

        {/* Repassword input */}
        <div className="mt-6">
          <InputField
            name="repassword"
            isRequired
            type="password"
            label="Nhập lại mật khẩu mới"
            placeholder="Nhập lại mật khẩu mới"
            customValidate={{
              validateFn: formCustomValidate.rePasswordValidate,
              validateMess: 'Mật khẩu nhập lại không chính xác!'
            }}
            formControl={control}
            errorState={errors?.repassword}
          />
        </div>

        {/* Send button */}
        <div className={clsx('mt-10 flex justify-center relative h-[4.8rem]')}>
          <ButtonMain
            type="submit"
            background="green"
            className="md:w-[200px] xl:w-[237px] h-[40px] border rounded-[40px] text-center leading-6 m-auto lg:m-0 font-bold text-[#FFFFFF] bg-[#239F40] !text-[1.6rem] hover:bg-[#007134] !top-0"
            loading={isCallResetPassword}
          >
            Xác nhận
          </ButtonMain>
        </div>
      </form>
    </>
  )
}
