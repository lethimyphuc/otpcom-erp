import clsx from "clsx";
import React from "react";
import { Control, Controller, FieldError } from "react-hook-form";
import ErrorMessage from "~src/components/global/ErrorMessage";

interface InputFieldProps {
  name: string;
  isRequired: boolean;
  type: React.HTMLInputTypeAttribute;
  label: string;
  placeholder: string;
  customValidate: {
    validateFn: (v: any) => boolean;
    validateMess: string;
  };
  formControl: Control<any>;
  errorState: FieldError | any;
}

export const InputField = ({
  name,
  isRequired,
  label,
  type,
  placeholder,
  customValidate,
  formControl,
  errorState,
}: InputFieldProps) => {
  return (
    <div>
      <div className="relative">
        <p className="text-[14px] leading-4 font-bold">{label}</p>
        <Controller
          name={name}
          rules={{
            required: isRequired,
            validate: customValidate.validateFn,
          }}
          control={formControl}
          render={({ field }) => (
            <input
              {...field}
              className="peer focus:outline-none focus:placeholder:text-[#000] focus:placeholder:text-[14px] w-full border-0 border-b-[1px] border-[#CCCCCC] p-[8px] mt-4 placeholder:text-[#cccccc] text-[14px]"
              type={type}
              placeholder={placeholder}
            />
          )}
        />
        <div className="peer-focus:w-full absolute bottom-0 left-0 w-0 h-[2px] bg-[#239f40] transition-all duration-500" />
      </div>
      <div className="h-6">
        <div className={clsx(errorState ? "overflow-visible" : "hidden")}>
          <ErrorMessage>
            {errorState?.type === "validate"
              ? customValidate.validateMess
              : "Vui lòng không để trống!"}
          </ErrorMessage>
        </div>
      </div>
    </div>
  );
};
