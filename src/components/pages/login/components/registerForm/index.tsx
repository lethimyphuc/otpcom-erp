import { yupResolver } from '@hookform/resolvers/yup'
import clsx from 'clsx'
import { Controller, useForm } from 'react-hook-form'
import { useMutation } from 'react-query'
import { toast } from 'react-toastify'
import * as Yup from 'yup'
import { SIGNUP } from '~src/api'
import { graphQLClient } from '~src/api/graphql'
import { ButtonMain } from '~src/components/global/Button/ButtonMain/ButtonMain'
import { regex } from '~src/config/appConfig'
import { RegisterForm as IRegisterForm, TAuthenticate } from '~src/types/authenticate'
import styles from '../../Login.module.scss'
import { TextField } from '~src/components/global'

type RegisterFormProps = {
  onSuccess?: () => void
}

export const RegisterForm = ({ onSuccess }: RegisterFormProps) => {
  const { control, handleSubmit, reset } = useForm<IRegisterForm>({
    mode: 'all',
    resolver: yupResolver(schema),
  })

  const { isLoading, mutate } = useMutation({
    mutationFn: async (data: TAuthenticate) => {
      return await graphQLClient
        .request(SIGNUP, {
          input: {
            email: data?.email,
            password: data?.password,
            lastname: data?.lastname,
            firstname: data?.firstname,
            phone_number: data?.phone_number,
          } as TAuthenticate,
        })
        .then((res: any) => {
          if (res?.createCustomerV2 !== null) {
            toast('Đăng ký tài khoản thành công', { type: 'success' })
            onSuccess?.()
            reset()
          } else {
          }
        })
    },
    // onSuccess: (response: any) => {
    //   if (!!response?.createCustomerV2) {
    //     onSuccess?.()
    //     reset()
    //   }
    //   // toast('Đăng ký tài khoản thành công', { type: 'success' })
    // },
  })

  const onSubmitHandler = handleSubmit((value) => {
    mutate(value)
  })

  return (
    <form
      className="form-login w-[32.4rem] mt-12 md:w-[38.4rem] lg:w-[32.0rem] xl:w-[48.4rem] m-auto"
      onSubmit={onSubmitHandler}
    >
      <div className="flex flex-col sm:flex-row gap-6 mb-8">
        <div className="flex-1">
          <TextField
            required
            label="Họ"
            control={control}
            name="lastname"
            placeholder="Nhập họ của bạn"
          />
        </div>
        <div className="flex-1">
          <TextField
            required
            label="Tên"
            control={control}
            name="firstname"
            placeholder="Nhập tên của bạn"
          />
        </div>
      </div>
      <div className="flex flex-col sm:flex-row gap-6 mb-8">
        <div className="flex-1">
          <TextField
            required
            control={control}
            name="email"
            type="email"
            label="Email"
            placeholder="Nhập email của bạn"
          />
        </div>
        <div className="flex-1">
          <TextField
            required
            control={control}
            name="phone_number"
            label="Số điện thoại"
            type="number"
            placeholder="Nhập số điện thoại của bạn"
          />
        </div>
      </div>
      <div className="mt-8">
        <TextField
          required
          control={control}
          type="password"
          name="password"
          placeholder="Nhập mật khẩu"
          label="Mật khẩu"
        />
      </div>
      <div className="mt-8">
        <TextField
          required
          control={control}
          type="password"
          name="repassword"
          label="Nhập lại mật khẩu"
          placeholder="Nhập lại mật khẩu"
        />
      </div>
      <div className="flex gap-2 justify-start mb-3 items-center mt-8">
        <Controller
          name="term"
          control={control}
          render={({ field: { onChange, value }, fieldState: { error } }) => (
            <input
              required
              checked={!!value}
              className="cursor-pointer"
              onChange={() => onChange(!value)}
              type="checkbox"
              id="term"
            />
          )}
        />
        <label htmlFor="term" className="leading-6 xl:text-[1.4rem] text-[1.2rem]">
          Tôi đồng ý với{' '}
          <span className="text-[#C82127] font-bold">Điều khoản sử dụng dịch vụ</span>
        </label>
      </div>
      <div className={clsx('flex justify-center mt-10 relative h-[4.8rem]')}>
        <ButtonMain
          type="submit"
          loading={isLoading}
          background="green"
          className={clsx(
            styles.loginTab,
            'text-[#FFFFFF] bg-[#239F40] !w-[12.7rem] !text-[1.6rem] hover:bg-[#007134] !top-0'
          )}
        >
          {!isLoading ? ' Đăng ký ' : ''}
        </ButtonMain>
      </div>
    </form>
  )
}

const schema: Yup.ObjectSchema<IRegisterForm> = Yup.object().shape({
  firstname: Yup.string().required('Vui lòng nhập trường này'),
  lastname: Yup.string().required('Vui lòng nhập trường này'),
  email: Yup.string()
    .matches(regex.EMAIL, 'Vui lòng nhập đúng định dạng email')
    .required('Vui lòng nhập trường này'),
  password: Yup.string()
    .min(8, 'Mật khẩu phải có ít nhất 8 ký tự')
    .required('Vui lòng nhập trường này'),
  repassword: Yup.string()
    .oneOf([Yup.ref('password')], 'Vui lòng nhập trùng với mật khẩu')
    // .test('passwords-match', 'Vui lòng nhập trùng với mật khẩu', function (value) {
    //   return this.parent.password === value
    // })
    .required('Vui lòng nhập trường này'),
  phone_number: Yup.string()
    .matches(regex.PHONE, 'Vui lòng nhập đúng định dạng số điện thoại')
    .required('Vui lòng nhập trường này'),
  term: Yup.boolean()
    .required('Vui lòng nhập trường này')
    .oneOf([true], 'Vui lòng nhập trường này'),
})
