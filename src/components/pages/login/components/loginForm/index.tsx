import { yupResolver } from '@hookform/resolvers/yup'
import clsx from 'clsx'
import { useForm } from 'react-hook-form'
import { useMutation } from 'react-query'
import * as Yup from 'yup'
import { LOGIN, MERGE_CART } from '~src/api'
import { CREATE_EMPTY_CART } from '~src/api/cart'
import { graphQLClient, graphQLClientGET } from '~src/api/graphql'
import { ButtonMain } from '~src/components/global/Button/ButtonMain/ButtonMain'
import { LoginForm as ILoginForm } from '~src/types/authenticate'
import styles from '../../Login.module.scss'
import { TextField } from '~src/components/global'
import Link from 'next/link'
import { useCookies } from 'react-cookie'
import { useRouter } from 'next/router'

export const LoginForm = () => {
  const [cookies, setCookie] = useCookies(['token'])
  const router = useRouter()
  const { control, handleSubmit } = useForm<ILoginForm>({
    mode: 'all',
    resolver: yupResolver(schema),
  })

  const { status, isLoading, mutate } = useMutation({
    mutationFn: async (data: ILoginForm) => {
      const response: any = await graphQLClient
        .request(LOGIN, {
          email: data?.email,
          password: data?.password,
        })
        .then(async (res: any) => {
          if (res?.generateCustomerToken !== null) {
            const token = res?.generateCustomerToken?.token
            console.log('token', res?.generateCustomerToken?.token)
            localStorage.setItem('token', token)
            setCookie('token', token, { path: '/' })

            graphQLClient.setHeaders({
              Authorization: 'Bearer ' + token,
              'Content-Type': 'application/json',
              accept: 'application/json',
            })
            graphQLClientGET.setHeaders({
              Authorization: 'Bearer ' + token,
              'Content-Type': 'application/json',
              accept: 'application/json',
            })

            const cartEmptyResponse: any = await graphQLClient.request(CREATE_EMPTY_CART, {
              input: {},
            })
            const destination_cart_id = cartEmptyResponse?.createEmptyCart
            const source_cart_id = data?.cartId

            if (destination_cart_id && source_cart_id) {
              try {
                await graphQLClient.request(MERGE_CART, {
                  source_cart_id,
                  destination_cart_id,
                })
              } catch (error) {
                console.log('Merge cart error: ', error)
              }
            }

            localStorage.setItem('cartId', destination_cart_id)
            // window.location.replace(`${window.location?.origin}/home`)
            router.replace('/home')
            return res
          }
        })

      return response
    },
    // onSettled: (error: any) => {
    //   console.log('error', error?.response.errors[0]?.message)
    //   // toast.error(error?.response.errors[0]?.message)
    // },
    // onError: (error: any) => {
    //   console.log('error', error?.response.errors[0]?.message)
    //   // toast.error(error?.response.errors[0]?.message)
    // },
  })

  const onSubmitHandler = handleSubmit((value) => {
    mutate({ ...value, cartId: localStorage.getItem('cartId') })
  }, console.log)

  return (
    <form
      className="form-login w-[32.4rem] mt-12 md:w-[38.4rem] lg:w-[32.0rem] xl:w-[48.4rem] m-auto"
      onSubmit={onSubmitHandler}
    >
      <div className="">
        <TextField
          required
          control={control}
          name="email"
          label="Email/số điện thoại"
          placeholder="Nhập email/số điện thoại của bạn"
        />
      </div>
      <div className="mt-6">
        <TextField
          required
          control={control}
          type="password"
          name="password"
          placeholder="Nhập mật khẩu"
          label="Mật khẩu"
          autoComplete="on"
        />
      </div>

      <div className="flex justify-end">
        <Link href={'/forgot-password'}>
          <a className="font-bold mt-2 cursor-pointer text-[1.4rem]">Quên mật khẩu?</a>
        </Link>
      </div>

      <div className={clsx('flex justify-center mt-10 relative h-[4.8rem]')}>
        <ButtonMain
          type="submit"
          loading={isLoading}
          background="green"
          className={clsx(
            styles.loginTab,
            'text-[#FFFFFF] bg-[#239F40] !w-[12.7rem] !text-[1.6rem] hover:bg-[#007134] !top-0'
          )}
        >
          {!isLoading ? 'Đăng nhập' : ''}
        </ButtonMain>
      </div>
    </form>
  )
}

const schema: Yup.ObjectSchema<ILoginForm> = Yup.object().shape({
  cartId: Yup.string(),
  email: Yup.string().required('Vui lòng nhập trường này'),
  password: Yup.string()
    .min(8, 'Mật khẩu phải có ít nhất 8 ký tự')
    .required('Vui lòng nhập trường này'),
})
