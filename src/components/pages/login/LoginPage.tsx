/* eslint-disable @next/next/no-img-element */
import clsx from 'clsx'
import { useState, useEffect } from 'react'
import { useQuery } from 'react-query'
import { INFO_CUSTOMER, SOCIAL_LOGIN } from '~src/api'
import { graphQLClient } from '~src/api/graphql'
import { TSocialLoginUrl } from '~src/types/authenticate'
import styles from './Login.module.scss'
import { LoginForm, RegisterForm } from './components'
import { useRouter } from 'next/router'

const tabs = [
  {
    id: 1,
    name: 'Đăng nhập',
    active: 'text-[#FFFFFF] bg-[#239F40] z-10',
    inactive: 'border-none text-[#239F40]',
  },
  {
    id: 2,
    name: 'Đăng ký',
    active: 'text-[#FFFFFF] bg-[#239F40] z-10',
    inactive: 'border-none text-[#239F40]',
  },
]

export default function LoginPage(prop: any) {
  const [tabIdSelected, setTabIdSelected] = useState(prop.tab)
  const router = useRouter()

  const {
    data: dataCategoriesByCompany,
  }: {
    data?: TSocialLoginUrl
    isLoading?: boolean
  } = useQuery({
    queryKey: ['getSocialLoginUrl'],
    queryFn: async () => {
      try {
        const data = (await graphQLClient.request(SOCIAL_LOGIN)) as {
          socialLoginUrl: TSocialLoginUrl[]
        }
        return data?.socialLoginUrl[0]
      } catch (error) {
        throw new Error('Lỗi')
      }
    },
  })
  // const { data } = useQuery<any>({
  //   queryKey: ["checkLogin"],
  //   queryFn: async () => {
  //     const data = (await graphQLClient.request(INFO_CUSTOMER)) as {
  //       customer: any;
  //     };
  //     return data;
  //   },
  //   cacheTime: Infinity,
  // });
  useEffect(() => {
    if (localStorage.getItem('token')) {
      router.push(`/home`)
    }
  }, [router])

  const handleChangeTab = (value: number) => {
    setTabIdSelected(value)
  }

  return (
    <div className={clsx(styles.loginContainer)}>
      <div className="hidden relative lg:block">
        <img className={clsx(styles.subtract)} src="/image/Subtract.png" alt="" />
        <img className={clsx(styles.car)} src="/image/red-car.png" alt="" />
        <img className={clsx(styles.gear)} src="/image/gear.png" alt="" />
      </div>
      <div className={clsx(styles.loginForm)}>
        <img className={clsx(styles.logo)} src="/image/logo/logo-header.png" alt="" />
        <img className={clsx(styles.verticalLogo)} src={'/icons/svg/vertical-logo.svg'} alt="" />
        <img className={clsx(styles.stripes)} src={'/icons/svg/stripes.svg'} alt="" />
        <div className={clsx(styles.loginPageTab)}>
          {tabs.map((item) => (
            <button
              key={item.id}
              className={clsx(
                styles.loginTab,
                `${item.id === tabIdSelected ? item.active : item.inactive}`
              )}
              onClick={() => handleChangeTab(item.id)}
            >
              {item.name}
            </button>
          ))}
        </div>

        {tabIdSelected === 1 ? (
          <LoginForm />
        ) : (
          <RegisterForm onSuccess={() => setTabIdSelected(1)} />
        )}

        <div className="flex items-center justify-between gap-1 mx-auto mt-12 w-[32.4rem] md:w-[38.4rem] lg:w-[32.0rem] xl:w-[48.4rem]">
          <span className="border-b border-solid border-gray20 flex-1"></span>
          <span className="text-[1.4rem] text-center leading-6 text-[#2C0709] mx-[16px]">
            Hoặc đăng nhập với
          </span>
          <span className="border-b border-solid border-gray20 flex-1"></span>
        </div>

        <div className="mt-6 flex justify-center">
          <a href={dataCategoriesByCompany?.login_url} className="cursor-pointer">
            <img className="w-[3.2rem]" src={'/icons/svg/google.svg'} alt="" />
          </a>
        </div>

        {tabIdSelected === 1 && (
          <div className="flex justify-center mt-8 text-[1.4rem] ">
            <p className="text-[#382E2E] leading-6">Bạn mới biết đến OTPCom?</p>
            <p
              className="font-bold text-[#CB2127] ml-3 leading-6 cursor-pointer"
              onClick={() => handleChangeTab(2)}
            >
              Đăng ký
            </p>
          </div>
        )}
        {tabIdSelected === 2 && (
          <div className="flex justify-center mt-8 text-[1.4rem]">
            <p className="text-[#382E2E] leading-6">Bạn đã có tài khoản?</p>
            <p
              className="font-bold text-[#CB2127] ml-3 leading-6 cursor-pointer"
              onClick={() => handleChangeTab(1)}
            >
              Đăng nhập
            </p>
          </div>
        )}
      </div>
    </div>
  )
}
