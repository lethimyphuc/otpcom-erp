import { NextRouter } from 'next/router'
import { useEffect } from 'react'
import { useQuery } from 'react-query'
import { graphQLClientGET } from '~src/api/graphql'
import { GET_PRODUCTS } from '~src/api/product'
import { PaginationCustom } from '~src/components/common/PaginationCustom'
import { TitleProduct } from '~src/components/global/TitlePage/TitleProduct'
import { BannerBrand } from '../brand/BannerBrand'
import { ProductList } from './ProductList'

type ProductPageContentProps = {
  router: NextRouter
}
const ProductPageContent = (props: ProductPageContentProps) => {
  const { router } = props
  const { data: dataProducts, isLoading: isLoadingDataProducts } = useQuery({
    queryKey: ['getProducts', router.query],
    queryFn: async () => {
      const convertFilterIn = [
        router.query?.company,
        router.query?.type,
        router.query?.end,
        router.query?.groupAccessary,
      ].filter(Boolean)

      let dataSort: Record<string, any> = {}
      if (router.query?.sort === 'ascName') {
        dataSort.name === 'ASC'
      }
      if (router.query?.sort === 'descName') {
        dataSort.name === 'DESC'
      }
      if (router.query?.sort === 'ascPrice') {
        dataSort.price === 'ASC'
      }
      if (router.query?.sort === 'descPrice') {
        dataSort.price === 'DESC'
      }
      if (router.query?.sort === 'ascPosition') {
        dataSort.position === 'ASC'
      }
      if (router.query?.sort === 'descPosition') {
        dataSort.position === 'DESC'
      }
      if (router.query?.sort === 'ascRelevance') {
        dataSort.relevance === 'ASC'
      }
      if (router.query?.sort === 'descRelevance') {
        dataSort.relevance === 'DESC'
      }
      const data = (await graphQLClientGET.request(GET_PRODUCTS, {
        filter: {
          category_uid: {
            in: convertFilterIn,
          },
        },
        sort: {
          ...dataSort,
        },
        search: !!router.query?.search ? `${router.query?.search}`.trim() : '',
        pageSize: !!router?.query?.pageSize ? parseInt(`${router.query.pageSize}`) : 20,
        currentPage: !!router?.query?.currentPage ? parseInt(`${router.query.currentPage}`) : 1,
      })) as {
        products: {
          items: TProduct[]
          page_info: {
            current_page: number
            page_size: number
            total_pages: number
          }
        }
      }
      return data?.products
    },
  })

  useEffect(() => {
    if (!!router.query && !!Object.keys(router.query).length) {
      router.replace({
        query: {
          ...router.query,
          currentPage: !!router.query?.currentPage ? router.query.currentPage : '1',
          pageSize: !!router.query?.pageSize ? router.query.pageSize : '20',
          sort: !!router.query?.sort ? router.query?.sort : 'ascName',
        },
      })
    } else {
      router.replace({
        query: {
          currentPage: '1',
          pageSize: '20',
          sort: 'ascName',
        },
      })
    }
  }, [])

  return (
    <div className="car-company">
      <div className="banner-page">
        <BannerBrand />
      </div>

      <div className="containers">
        <div className="wrapper-product">
          <div className="title-promotion">
            <TitleProduct />
            <div className="flex justify-center gap-3 items-center">
              <p className="text-[#a3a0a0]">Tìm kiếm:</p>
              <p>{router.query?.search}</p>
            </div>
          </div>

          {/* <FormFilterProduct router={router} /> */}
          <ProductList data={dataProducts?.items} loading={isLoadingDataProducts} />
          <div className="content-inner-promotion">
            {(dataProducts?.page_info?.total_pages as number) > 1 && (
              <div className="pagination-promotion">
                <PaginationCustom
                  current={
                    !!router.query?.currentPage ? parseInt(`${router.query?.currentPage}`) : 1
                  }
                  pageSize={!!router?.query?.pageSize ? parseInt(`${router.query?.pageSize}`) : 20}
                  total={
                    !!dataProducts?.page_info?.total_pages && !!dataProducts?.page_info?.page_size
                      ? parseInt(`${dataProducts?.page_info?.total_pages}`) *
                        parseInt(`${dataProducts?.page_info?.page_size}`)
                      : !!router?.query?.pageSize
                      ? parseInt(`${router.query?.pageSize}`)
                      : 20
                  }
                  onChange={(page: number) =>
                    router.push({
                      query: {
                        ...router.query,
                        currentPage: page,
                      },
                    })
                  }
                  totalPage={
                    !!dataProducts?.page_info?.total_pages
                      ? parseInt(`${dataProducts?.page_info?.total_pages}`)
                      : 1
                  }
                />
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  )
}

export default ProductPageContent
