'use client'

import clsx from 'clsx'
import { motion, AnimatePresence } from 'framer-motion'
/* eslint-disable @next/next/no-img-element */
import { useRouter } from 'next/router'
import React, { useMemo } from 'react'
import { useMediaQuery } from 'react-responsive'
import ProductItem from '~src/components/global/Product/ProductItem'
import ProductSkeleton from '~src/components/global/Product/ProductSkeleton'
import { _format } from '~src/util'

type I = {
  data: any
  loading: boolean
}
export const ProductList: React.FC<I> = ({ data, loading }) => {
  const router = useRouter()

  const isScreenDesktopMedium = useMediaQuery({ query: '(max-width: 1200px)' })
  const isScreenTablet = useMediaQuery({ query: '(max-width: 900px)' })

  const numOfLazyItem = useMemo<number>(() => {
    return (isScreenTablet && 2) || (isScreenDesktopMedium && 3) || 4
  }, [isScreenDesktopMedium, isScreenTablet])

  if (data?.length == 0 && !loading) {
    return (
      <div className="w-full !flex !justify-center !text-center">
        <p className="text-center">Không có sản phẩm</p>
      </div>
    )
  }

  if (loading) {
    return (
      <AnimatePresence mode="popLayout">
        <div className="product-list">
          {Array.from({ length: numOfLazyItem }, (v, k) => (
            <motion.div
              key={k}
              exit={{ opacity: 0 }}
              transition={{ duration: 0.5, ease: 'easeInOut' }}
              className={clsx('relative z-[1] product-item')}
            >
              <ProductSkeleton key={k} />
            </motion.div>
          ))}
        </div>
      </AnimatePresence>
    )
  }

  return (
    <div className="product-list">
      {data?.map((item: TProduct & { sale_price: number }, index: number) => (
        <ProductItem product={item} key={`${item?.sku}`} router={router} />
      ))}
    </div>
  )
}
