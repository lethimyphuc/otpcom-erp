import React from 'react'
import clsx from 'clsx'
import { Controller, useForm } from 'react-hook-form'
import ErrorMessage from '~src/components/global/ErrorMessage'
import { ButtonMain } from '~src/components/global/Button/ButtonMain/ButtonMain'
import Link from 'next/link'
import { useMutation } from 'react-query'
import { graphQLClient } from '~src/api/graphql'
import { toast } from 'react-toastify'
import { REQUEST_PASSWORD_SEND_EMAIL } from '~src/api'
import { TAuthenticate } from '~src/types/authenticate'

export const ForgotPasswordForm = () => {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<TAuthenticate>({
    defaultValues: {
      email: '',
    },
  })

  const emailValidate = (email: string) => {
    return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
      email
    )
  }

  const { isLoading: isCallRequestPassword, mutate: mutateRequestPassword } = useMutation({
    mutationFn: async (payload: { email: string }) => {
      await graphQLClient
        .request(REQUEST_PASSWORD_SEND_EMAIL, {
          email: payload.email,
        })
        .then((response: any) => {
          return response
        })
        .catch((err) => {
          toast.error(err?.response.errors[0]?.message)
        })
    },
    onSuccess: () => {
      toast('Gửi yêu cầu cấp lại mật khẩu thành công!', { type: 'success' })
    },
  })

  const onSubmit = (data: TAuthenticate) => {
    const payload = {
      email: `${data?.email ?? ''}`.trim(),
    }
    mutateRequestPassword(payload)
  }

  return (
    <>
      <div className="flex justify-start text-center lg:text-left mb-6 mt-12 xl:mt-16 w-[324px] md:w-[384px] xl:w-[484px] mx-auto border-none rounded-[40px] py-2">
        <div className="uppercase font-bold leading-[24px] py-3 text-[#00] z-10 w-full h-[40px] rounded-[40px] text-[16px] m-auto lg:m-0 ">
          Quên mật khẩu
        </div>
      </div>

      <form
        className="form-login mt-12 w-[324px] md:w-[384px] xl:w-[484px] mx-auto"
        onSubmit={handleSubmit(onSubmit)}
      >
        {/* Email input */}
        <div>
          <div className="relative">
            <p className="text-[14px] leading-4 font-bold">Địa chỉ Email</p>
            <Controller
              name="email"
              rules={{
                required: true,
                validate: emailValidate,
              }}
              control={control}
              render={({ field }) => (
                <input
                  {...field}
                  className="peer focus:outline-none focus:placeholder:text-[#000] focus:placeholder:text-[14px] w-full border-0 border-b-[1px] border-[#CCCCCC] p-[8px] mt-4 placeholder:text-[#cccccc] text-[14px]"
                  type="text"
                  placeholder="Nhập email của bạn"
                />
              )}
            />
            <div className="peer-focus:w-full absolute bottom-0 left-0 w-0 h-[2px] bg-[#239f40] transition-all duration-500" />
          </div>
          <div className="h-6">
            <div className={clsx(errors.email ? 'overflow-visible' : 'hidden')}>
              <ErrorMessage>
                {errors.email?.type === 'validate'
                  ? 'Email không hợp lệ!'
                  : 'Vui lòng không để trống!'}
              </ErrorMessage>
            </div>
          </div>
        </div>

        {/* Backto login */}
        <div className="flex justify-end">
          <Link href={'/login'}>
            <p className="font-bold mt-2 cursor-pointer text-[1.4rem]">Quay lại trang đăng nhập</p>
          </Link>
        </div>

        {/* Send button */}
        <div className={clsx('mt-10 flex justify-center relative h-[4.8rem]')}>
          <ButtonMain
            type="submit"
            background="green"
            className="h-[40px] border rounded-[40px] text-center leading-6 m-auto lg:m-0 font-bold text-[#FFFFFF] bg-[#239F40] !text-[1.6rem] hover:bg-[#007134] !top-0 w-[250px] "
            loading={isCallRequestPassword}
          >
            {!isCallRequestPassword ? 'Yêu cầu cấp lại mật khẩu' : ''}
          </ButtonMain>
        </div>
      </form>
    </>
  )
}
