/* eslint-disable @next/next/no-img-element */
import React from "react";
import { ForgotPasswordForm } from "./ForgotPasswordForm";

const ForgotPasswordSection = () => {
  return (
    <section className="translate-y-[-3rem] w-[100%] lg:h-[750px] xl:h-[850px] flex justify-between">
      <div className="hidden relative lg:block">
        <img
          className="lg:w-[960px] xl:w-[760px] h-[950px]"
          src="/image/Subtract.png"
          alt=""
        />
        <img
          className="absolute top-1/2 left-[15%] lg:w-[540px] xl:w-[640px]"
          src="/image/red-car.png"
          alt=""
        />
        <img
          className="absolute top-1/3 right-[-15%]"
          src="/image/gear.png"
          alt=""
        />
      </div>

      {/* Forgot password form */}
      <div className="w-full lg:w-[85%] mt-20 lg:ml-8 xl:ml-0">
        <img
          className="w-[136px] h-[52px] m-auto"
          src="/image/logo/logo-header.png"
          alt=""
        />
        <img
          className="absolute w-[60px] right-28 top-80 hidden 2xl:block"
          src={"/icons/svg/vertical-logo.svg"}
          alt=""
        />
        <img
          className="absolute w-[56px] right-0 top-60 hidden xl:block"
          src={"/icons/svg/stripes.svg"}
          alt=""
        />

        <ForgotPasswordForm />
      </div>
    </section>
  );
};

export default ForgotPasswordSection;
