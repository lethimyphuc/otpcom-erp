/* eslint-disable react-hooks/exhaustive-deps */
/**
 * /* eslint-disable react-hooks/exhaustive-deps
 *
 * @format
 */

import { NextRouter, useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { useQuery } from 'react-query'
import { GET_BANNER_PAGE } from '~src/api'
import { graphQLClient,graphQLClientGET } from '~src/api/graphql'
import { GET_PRODUCT_DAILY_SALE } from '~src/api/product'
import { PaginationCustom } from '~src/components/common/PaginationCustom'
import { ProductFilter } from '~src/components/common/ProductFilter'
import { removeEmptyValueFromObject } from '~src/util'
import { BannerBrand } from '../brand/BannerBrand'
import { ProductList } from '../product/ProductList'
import { HeroTitle } from '../dynamic-page/HeroTittle'

type PromotionPageProps = {
  router: NextRouter
}

export const PromotionPage = (props: PromotionPageProps) => {
  const router = useRouter()
  const [filter, setFilter] = useState<ProductFilterReq>({})
  const [bannerImage, setBannerImage] = useState<string | undefined>(undefined)

  const {
    data: dataDailySales,
    isLoading: isLoadingDataDailySales,
    isFetching: isfetchingDataDailySales,
  } = useQuery({
    queryKey: ['getDailySales', { filter, query: router?.query?.currentPage }],
    queryFn: async () => {
      const convertFilterIn = [filter.groupAccessary, filter.company, filter.type, filter.end]
        .filter(Boolean)
        .pop()
      const data = (await graphQLClientGET.request(GET_PRODUCT_DAILY_SALE, {
        filter: {
          category_uid: {
            eq: convertFilterIn ?? 'Ng==',
          },
          tags: {
            eq: filter.tag ? filter.tag : null,
          },
        },
        pageSize: !!router?.query?.pageSize ? parseInt(`${router.query.pageSize}`) : 20,
        currentPage: !!router?.query?.currentPage ? parseInt(`${router.query.currentPage}`) : 1,
        sort: JSON.parse(filter.sort ?? '{}'),
      })) as {
        ProductDailySales: {
          items: TDailySales[]
          page_info: TPageInfo
          total_count: number
        }
      }

      return data
    },
  })

  const { data: dataContentPromotion, isLoading: isLoadingContentPromotion } = useQuery({
    queryKey: ['getContentPromotionPage'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_BANNER_PAGE, {
        identifier: 'promotion-banner',
      })) as {
        managebanner: any
      }
      return data?.managebanner
    },
    cacheTime: Infinity,
  })

  const convertListProduct = (dataSale: any) => {
    return dataSale?.items?.map((item: TItemDailySale) => {
      return { ...item, ...item?.product }
    })
  }

  useEffect(() => {
    const query = router.query || {}
    const data: ProductFilterReq = removeEmptyValueFromObject({
      company: query?.company as string,
      end: query?.end as string,
      groupAccessary: query?.groupAccessary as string,
      sort: query?.sort as string,
      tag: query?.tag as string,
      type: query?.type as string,
    })
    setFilter(data)
  }, [router.query])

  const handleFilter = (data: ProductFilterReq) => {
    router.push({ query: data }, undefined, { scroll: false })
  }
  return (
    <div className="promotion-page">
      <div className="banner-page">
        <BannerBrand
          bannerImage={
            !isLoadingContentPromotion &&
            (bannerImage
              ? bannerImage
              : dataContentPromotion?.image
              ? dataContentPromotion.image
              : '/image/bg-product.png')
          }
          breadcrumbs={[{ breadcrumbName: 'Khuyến mãi', path: '' }]}
        />
      </div>
      <div className="containers">
        <div className="title-promotion">
          <HeroTitle data={filter} />
        </div>
        <div className="content-inner-promotion">
          <div className="filter-promotion">
            <ProductFilter onChange={handleFilter} value={filter} onChangeBanner={setBannerImage} />
          </div>
          <div className="wrapper-product">
            <div className="list-product">
              <ProductList
                data={
                  !!dataDailySales?.ProductDailySales?.items?.length &&
                  !!convertListProduct(dataDailySales?.ProductDailySales)
                    ? convertListProduct(dataDailySales?.ProductDailySales)
                    : []
                }
                loading={isLoadingDataDailySales || isfetchingDataDailySales}
              />
            </div>
          </div>
          <div className="pagination-promotion">
            {(dataDailySales?.ProductDailySales?.page_info?.total_pages as any) > 1 && (
              <div className="pagination-promotion">
                <PaginationCustom
                  current={
                    !!router.query?.currentPage ? parseInt(`${router.query?.currentPage}`) : 1
                  }
                  pageSize={!!router?.query?.pageSize ? parseInt(`${router.query?.pageSize}`) : 20}
                  total={
                    !!dataDailySales?.ProductDailySales?.page_info?.total_pages &&
                    !!dataDailySales?.ProductDailySales?.page_info?.page_size
                      ? parseInt(`${dataDailySales?.ProductDailySales?.page_info?.total_pages}`) *
                        parseInt(`${dataDailySales?.ProductDailySales?.page_info?.page_size}`)
                      : !!router?.query?.pageSize
                      ? parseInt(`${router.query?.pageSize}`)
                      : 20
                  }
                  onChange={(page: number) =>
                    router.push({
                      query: {
                        ...router.query,
                        currentPage: page,
                      },
                    })
                  }
                  totalPage={
                    !!dataDailySales?.ProductDailySales?.page_info?.total_pages
                      ? parseInt(`${dataDailySales?.ProductDailySales?.page_info?.total_pages}`)
                      : 1
                  }
                />
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  )
}
