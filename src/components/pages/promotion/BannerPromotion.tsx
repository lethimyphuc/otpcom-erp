"use client";
/* eslint-disable @next/next/no-img-element */
import React from "react";
import { BreadcrumbBanner } from "~src/components/global/Layout/BreadcrumbBanner";
import { motion } from "framer-motion";

export const BannerPromotion = ({ dataCms }: { dataCms: any }) => {
  const dataBanner = dataCms?.items?.find(
    (item: any) => item?.identifier === "promotion-banner"
  );

  const bannerImage = dataBanner?.nodes?.items?.find(
    (item: any) => item?.classes === "image-promotion-banner"
  )?.image;

  return (
    <div className="banner-promotion">
      <div className="img relative">
        {!bannerImage ? (
          <div className="z-[0] absolute inset-0 h-full w-full">
            <LazyBanner />
          </div>
        ) : (
          <motion.img
            initial={{ opacity: 0, filter: "blur(8px)" }}
            animate={{ opacity: 1, filter: "blur(0)" }}
            src={bannerImage}
            alt=""
          />
        )}
      </div>
      <div className="img-position">
        <div className="img">
          <img src="/image/21.png" alt="" />
        </div>
      </div>
      <div className="content-inner !translate-x-[-50%] [&_ol]:justify-start container">
        <div className="breadcrum">
          <BreadcrumbBanner breadcrumbHead={false} />
        </div>
        <div className="title">khuyến mãi</div>
      </div>
    </div>
  );
};

const LazyBanner = () => {
  return (
    <div className="fx-skeleton w-full h-full">
      <div className="overlay" />
      <div className="skeholder w-full h-full" />
      <div
        style={{
          background:
            "linear-gradient(to top,rgba(255,255,255,1) 30%, rgba(255,255,255,0))",
        }}
        className="absolute bottom-0 left-0 right-0 h-32"
      />
    </div>
  );
};
