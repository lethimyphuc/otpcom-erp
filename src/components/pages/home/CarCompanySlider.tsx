/* eslint-disable @next/next/no-img-element */
/**
 * /* eslint-disable @next/next/no-img-element
 *
 * @format
 */

import { Carousel } from 'antd'
import React, { useEffect, useRef, useState } from 'react'
import { CompanyItem } from './CompanyItem'
import { MdOutlineKeyboardArrowLeft, MdOutlineKeyboardArrowRight } from 'react-icons/md'
import { UseFormSetValue, UseFormWatch } from 'react-hook-form'

const DATA_FAKE = [
  {
    name: 'kia',
    image: '/image/kia-logo.png',
  },
  { name: 'hyundai', image: '/image/hyundai-logo.png' },
  { name: 'toyota', image: '/image/toyota-logo.png' },
  { name: 'honda', image: '/image/honda-logo.png' },
  { name: 'yamaha', img: '/image/yamaha-logo.png' },
]

type I = {
  setValue: UseFormSetValue<BoxSearchForm>
  watch: UseFormWatch<BoxSearchForm>
  dataCompany?: TChildrenCategories[]
}
export const CarCompanySlider: React.FC<I> = ({ setValue, watch, dataCompany }) => {
  const carouselRef = useRef<any>(null)
  const handlePrev = () => {
    carouselRef.current.prev()
  }
  const handleNext = () => {
    carouselRef.current.next()
  }

  const config = {
    slidesToScroll: 1,
    slidesToShow:
      !!dataCompany && !!dataCompany?.length && dataCompany?.length < 3 ? dataCompany?.length : 4,
    arrows: false,
    dots: false,
    infinite: true,
    cssEase: 'linear',
    speed: 500,
    draggable: true,
    responsive: [
      {
        breakpoint: 1000,
        settings: {
          arrows: false,
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 500,
        settings: {
          arrows: false,
          slidesToShow: 2,
        },
      },
    ],
  }
  useEffect(() => {
    if (!!dataCompany && !!dataCompany?.length) {
      setValue('company', dataCompany?.[0]?.url_key)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dataCompany?.length])

  return (
    <div className="wrapper-carousel">
      <div className="arrow-button-prev">
        <div className="icon icon-prev" onClick={handlePrev}>
          <MdOutlineKeyboardArrowLeft />
        </div>
      </div>

      <div className="arrow-button-next">
        <div className="icon icon-next" onClick={handleNext}>
          <MdOutlineKeyboardArrowRight />
        </div>
      </div>
      <div className="car-company-slider">
        {!!dataCompany && !!dataCompany?.length ? (
          <Carousel {...config} ref={!!dataCompany ? carouselRef : null}>
            {!!dataCompany?.length &&
              dataCompany?.map((item: TChildrenCategories, index: number) => {
                return (
                  <div
                    key={index}
                    className={`car-item ${index == 0 && 'first'} ${
                      watch('company') == item?.url_key && 'isSelect'
                    } `}
                    onClick={() => setValue('company', item?.url_key)}
                  >
                    <div className={`img`}>
                      <img src={item?.image || '/image/car-default.png'} alt="" />
                    </div>
                  </div>
                )
              })}
          </Carousel>
        ) : (
          <Carousel {...config} ref={!!DATA_FAKE && !!DATA_FAKE?.length ? carouselRef : null}>
            {!!DATA_FAKE?.length &&
              DATA_FAKE?.map((item: any, index: number) => (
                <div
                  key={index}
                  className={`car-item ${index == 0 && 'first'}`}
                  // onClick={() => setSelectCompany(item?.url_key)}
                >
                  <div className={`img`}>
                    <img src={'/image/car-default.png'} alt="" />
                  </div>
                </div>
              ))}
          </Carousel>
        )}
      </div>
    </div>
  )
}
