/* eslint-disable @next/next/no-img-element */
import { Carousel } from 'antd'
import { format } from 'date-fns'
import Link from 'next/link'
import React, { useEffect, useRef, useState } from 'react'
import { MdOutlineKeyboardArrowLeft, MdOutlineKeyboardArrowRight } from 'react-icons/md'
import { Swiper, SwiperSlide } from 'swiper/react'

type DiarySliderProps = {
  dataBlogPosts?: {
    items: TPost[]
    page_info: TPageInfo
    total_count: number
  }
}
export const DiarySlider = (props: DiarySliderProps) => {
  const { dataBlogPosts } = props
  const [swiper, setSwiper] = useState<any>(null)
  const [data, setData] = useState<any>([
    { name: 'hyundai', img: '/image/image-car1.jpg' },
    { name: 'toyota', img: '/image/car-slide.jpg' },
    { name: 'honda', img: '/image/image-car2.jpg' },
    { name: 'toyota', img: '/image/car-slide.jpg' },
    { name: 'honda', img: '/image/image-car2.jpg' },
    { name: 'toyota', img: '/image/car-slide.jpg' },
  ])

  const handleNext = () => {
    swiper.slideNext()
  }

  const handlePrev = () => {
    swiper.slidePrev()
  }
  const config = {
    slidesPerView: 1.3,
    spaceBetween: 20,
    centeredSlides: true,
    onSwiper: (s: any) => {
      setSwiper(s)
    },

    loop: true,
    breakpoints: {
      280: {
        centeredSlides: false,
        slidesPerView: 1,
        spaceBetween: 10,
      },
      640: {
        centeredSlides: true,
        slidesPerView: 1.3,
      },
    },
  }

  return (
    <div className="diary-slider">
      <Swiper {...config}>
        {!!dataBlogPosts?.items &&
          !!dataBlogPosts?.items?.length &&
          dataBlogPosts?.items?.map((item: TPost, index: number) => (
            <SwiperSlide key={index}>
              <Link
                href={{
                  pathname: `/diary/${item?.post_id}`,
                }}
              >
                <div className="cursor-pointer">
                  <div className="arrow-group">
                    <div className="arrow-button prev">
                      <div
                        className="icon icon-prev"
                        onClick={(e) => {
                          e.stopPropagation()
                          handlePrev()
                        }}
                      >
                        <MdOutlineKeyboardArrowLeft />
                      </div>
                    </div>
                    <div className="arrow-button next">
                      <div
                        className="icon icon-next"
                        onClick={(e) => {
                          e.stopPropagation()
                          handleNext()
                        }}
                      >
                        <MdOutlineKeyboardArrowRight />
                      </div>
                    </div>
                  </div>
                  <div className={`item-slider cursor-pointer`}>
                    <div className="inner-slider-item">
                      <div className={`image`}>
                        <img src={item?.image} alt="" />
                        <div className="text-inner">
                          <div className="date">
                            {!!item?.publish_date
                              ? format(new Date(item?.publish_date), 'dd/MM/yyyy')
                              : 'Chưa có'}
                          </div>
                          <div className="name">{item?.name || ''}</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Link>
            </SwiperSlide>
          ))}
      </Swiper>
    </div>
  )
}
