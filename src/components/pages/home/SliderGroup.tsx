/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useMemo } from 'react'
import { CarCompanySlider } from './CarCompanySlider'
import { CarTypeSlider } from './CarTypeSlider'
import { CarSelectSlider } from './CarSelectSlider'
import { UseFormGetValues, UseFormSetValue, UseFormWatch } from 'react-hook-form'

type I = {
  dataCategoriesByCompany?: TChildrenCategories[]
  dataCategoriesByType?: TChildrenCategories[]
  dataCategoriesByEnd?: TChildrenCategories[]
  setValue: UseFormSetValue<BoxSearchForm>
  getValues: UseFormGetValues<BoxSearchForm>
  watch: UseFormWatch<BoxSearchForm>
  onSubmit: Function
}
export const SliderGroup: React.FC<I> = ({
  dataCategoriesByCompany,
  dataCategoriesByType,
  dataCategoriesByEnd,
  setValue,
  getValues,
  watch,
  onSubmit,
}) => {
  const selectedCarCompany = getValues('company')
  useEffect(() => {
    if (selectedCarCompany && (dataCategoriesByType?.length ?? 0 > 0)) {
      setValue('type', dataCategoriesByType![0].url_key)
    } else {
      setValue('type', null)
    }
  }, [selectedCarCompany, dataCategoriesByType?.length])

  return (
    <div className="slider-group">
      <CarCompanySlider setValue={setValue} watch={watch} dataCompany={dataCategoriesByCompany} />
      <CarTypeSlider setValue={setValue} watch={watch} dataCarType={dataCategoriesByType} />
      <CarSelectSlider
        setValue={setValue}
        watch={watch}
        dataCarEnd={dataCategoriesByEnd}
        onSubmit={onSubmit}
      />
    </div>
  )
}
