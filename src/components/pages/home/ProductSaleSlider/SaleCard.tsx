/* eslint-disable @next/next/no-img-element */
import clsx from 'clsx'
import Link from 'next/link'
import React from 'react'
import { useMutation, useQueryClient } from 'react-query'
import { toast } from 'react-toastify'
import { graphQLClient } from '~src/api/graphql'
import { ADD_PRODUCT_TO_CART } from '~src/api/product'
import { ButtonMain } from '~src/components/global/Button/ButtonMain/ButtonMain'
import { _format } from '~src/util'

export const SaleCard = ({ item }: { item: TItemDailySale }) => {
  const queryClient = useQueryClient()

  const { isLoading: isLoadingDataAddToCart, mutate: mutateAddToCart } = useMutation({
    mutationFn: async (data: TAddToCart[]) => {
      return (await graphQLClient.request(ADD_PRODUCT_TO_CART, {
        cartId: localStorage?.getItem('cartId'),
        cartItems: data,
      })) as { addProductsToCart: TDataAddToCart }
    },
    onSuccess: async (response: { addProductsToCart: TDataAddToCart }) => {
      await queryClient.refetchQueries(['getCartId'])
      if (!!response?.addProductsToCart?.user_errors?.length) {
        toast.error('Sản phẩm đã hết hàng')
      } else {
        queryClient.setQueryData<boolean>('miniCartVisible', true)
      }
    },
  })
  const handleAddToCart = (event: any) => {
    event?.stopPropagation()
    const DATA_SUBMIT: TAddToCart[] = [
      {
        sku: item?.product?.sku || '',
        quantity: 1,
      },
    ]
    mutateAddToCart(DATA_SUBMIT)
  }
  return (
    <div className={`item-slider`}>
      <Link
        href={{
          pathname: `/product-detail/Hãng xe/${item?.product?.name}`,
          query: {
            sku: item?.product?.sku,
          },
        }}
      >
        <div className="inner-slider-item">
          <div
            className={`image ${item?.product?.stock_status === 'IN_STOCK' ? '' : 'out-of-stock'}`}
          >
            <img src={item?.product?.image?.url} alt="" />
            <div className="hot">
              <div className="bg">
                <img src="/icons/Star.svg" alt="" />
              </div>
              <div className="text">
                -
                {100 -
                  Math.floor(
                    ((item?.sale_price || 0) /
                      (item?.product?.price_range?.maximum_price?.final_price?.value || 0)) *
                      100
                  )}
                %
              </div>
            </div>

            <div className="hot-icon">
              <img src="/icons/hot.svg" alt="" />
            </div>
            <ButtonMain
              type="button"
              background="green"
              className="cart-icon"
              onClick={handleAddToCart}
              loading={isLoadingDataAddToCart}
            >
              <img src="/icons/shopping-cart.svg" alt="" />
            </ButtonMain>
          </div>
          <div className="text-inner">
            <div className="name">{item?.product?.name}</div>
            <div className="price">
              <div className="price-new">
                {!!item?.product?.daily_sale &&
                !!item?.product?.daily_sale?.end_date &&
                !!item?.product?.daily_sale?.sale_price &&
                item?.product?.daily_sale?.sold_qty !== undefined &&
                item?.product?.daily_sale?.sale_qty !== undefined &&
                new Date(item?.product?.daily_sale?.end_date) >= new Date() &&
                item?.product?.daily_sale?.sold_qty < item?.product?.daily_sale?.sale_qty
                  ? _format.getVND(item?.product?.daily_sale?.sale_price)
                  : _format.getVND(
                      item?.product?.price_range?.minimum_price?.final_price?.value || 0
                    )}
                {item?.product?.price_range?.minimum_price?.final_price?.currency || ' VND'}
              </div>
              <div className="price-old">
                {_format.getVND(
                  item?.product?.price_range?.maximum_price?.regular_price?.value || 0
                )}
                {item?.product?.price_range?.maximum_price?.regular_price?.currency || ' VND'}
              </div>
            </div>
            <div className="fire">
              <div
                style={
                  item?.sold_qty && item?.sale_qty
                    ? { width: `${(item?.sold_qty / item?.sale_qty) * 100}%` }
                    : undefined
                }
                className={clsx('layer')}
              ></div>
              <div className="text">
                <div className="img">
                  <img src="/icons/flame.svg" alt="" />
                </div>

                <div className="txt">
                  <div className="t">Đã bán {item?.product?.daily_sale?.sold_qty}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Link>
    </div>
  )
}
