import React from "react";
import {
  MdOutlineKeyboardArrowLeft,
  MdOutlineKeyboardArrowRight,
} from "react-icons/md";
import { useSwiper } from "swiper/react";

export const SwiperControl = ({
  isShowNext,
  isShowPrev,
}: {
  isShowPrev: boolean;
  isShowNext: boolean;
}) => {
  const swiper = useSwiper();

  const handleNext = () => {
    swiper.slideNext();
  };

  const handlePrev = () => {
    swiper.slidePrev();
  };
  return (
    <>
      <div className="arrow-button prev">
        {isShowPrev && (
          <div className="icon icon-prev" onClick={handlePrev}>
            <MdOutlineKeyboardArrowLeft />
          </div>
        )}
      </div>
      <div className="arrow-button next">
        {isShowNext && (
          <div className="icon icon-next" onClick={handleNext}>
            <MdOutlineKeyboardArrowRight />
          </div>
        )}
      </div>
    </>
  );
};
