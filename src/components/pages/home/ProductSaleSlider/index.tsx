'use client'
/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from 'react'
import { _format } from '~src/util'
import { Swiper, SwiperSlide } from 'swiper/react'
import { SaleCard } from './SaleCard'
import { useWindowSize } from '~src/hook'
import { SwiperControl } from './SwiperControl'
import { Swiper as SwiperEvent } from 'swiper/types'
import clsx from 'clsx'

type ProductSaleSliderProps = {
  dataDailySales?: TItemDailySale[]
}
export const ProductSaleSlider = (props: ProductSaleSliderProps) => {
  const { isTablet, isMobile, isDesktop } = useWindowSize()
  const [numOfSwiperItem, setNumberOfSwiper] = useState(3)
  const [swiperState, setSwiperState] = useState({
    isBegin: true,
    isEnd: false,
  })

  useEffect(() => {
    isMobile && setNumberOfSwiper(1)
    isTablet && setNumberOfSwiper(2)
    isDesktop && setNumberOfSwiper(4)
  }, [isMobile, isTablet, isDesktop])

  const { dataDailySales } = props

  return (
    <div className="product-sale-slider">
      {dataDailySales && dataDailySales?.length > 0 && (
        <Swiper
          onRealIndexChange={(e: any) => setSwiperState({ isBegin: e.isBeginning, isEnd: e.isEnd })}
          spaceBetween={24}
          slidesPerView={numOfSwiperItem}
          className={clsx({
            '[&_.swiper-wrapper]:justify-center': dataDailySales.length < numOfSwiperItem,
          })}
        >
          {dataDailySales?.map((v, k) => (
            <SwiperSlide key={k}>
              <SaleCard key={k} item={v} />
            </SwiperSlide>
          ))}
          <SwiperControl
            isShowNext={!swiperState.isEnd && dataDailySales.length > numOfSwiperItem}
            isShowPrev={!swiperState.isBegin}
          />
        </Swiper>
      )}
    </div>
  )
}
