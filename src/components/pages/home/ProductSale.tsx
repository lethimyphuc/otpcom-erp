/* eslint-disable @next/next/no-img-element */
import React, { useEffect } from 'react'
import { ProductSaleSlider } from './ProductSaleSlider'
import { ButtonMain } from '~src/components/global/Button/ButtonMain/ButtonMain'
import { TitleProductSale } from '~src/components/global/TitlePage/TitleProductSale'
import { useQuery } from 'react-query'
import { graphQLClient,graphQLClientGET } from '~src/api/graphql'
import { GET_DAILYSALES, GET_PRODUCT_DAILY_SALE } from '~src/api/product'
import Link from 'next/link'
import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger'

export const ProductSale = () => {
  const { data: dataDailySales, isLoading: isLoadingDataDailySales } = useQuery({
    queryKey: ['getDailySales'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_PRODUCT_DAILY_SALE, {
        filter: {},
        pageSize: 8,
      })) as {
        ProductDailySales: {
          items: TDailySales[]
          page_info: TPageInfo
          total_count: number
        }
      }
      return data
    },
  })

  return (
    <>
      {(dataDailySales?.ProductDailySales?.items?.length ?? 0) > 0 && (
        <div className="product-sale">
          <div className="inner-product-sale">
            <div className="containers">
              <div className="title">
                <TitleProductSale
                  dataDailySales={dataDailySales?.ProductDailySales?.items}
                  title={null}
                />
              </div>
              <ProductSaleSlider
                dataDailySales={
                  !!dataDailySales?.ProductDailySales?.items?.length
                    ? dataDailySales?.ProductDailySales?.items
                    : []
                }
              />
              <div className="button-all">
                <Link
                  href={{
                    pathname: '/promotion',
                  }}
                >
                  <a>
                    <ButtonMain background="green" type="button">
                      <div className="text-all">
                        <span>Xem tất cả</span>
                      </div>
                    </ButtonMain>
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  )
}
