/* eslint-disable @next/next/no-img-element */

import React, { useEffect, useMemo, useState } from 'react'
import { useQuery } from 'react-query'
import { GET_CATEGORIES } from '~src/api/categories'
import { graphQLClient } from '~src/api/graphql'
import { motion } from 'framer-motion'

type TProps = {
  dataBrand: any
}

export const OnWorld: React.FC<TProps> = ({ dataBrand }) => {
  const dataWorld = dataBrand?.items?.find((item: any) => item?.identifier == 'home-page-branchs')

  const content = dataWorld?.nodes?.items?.find(
    (item: any) => item?.classes == 'description'
  )?.content

  const [numberActive, setNumberActive] = useState(1)
  const [selectCompany, setSelectCompany] = useState<TChildrenCategories | null>(null)
  useEffect(() => {
    // console.log(selectCompany)
  }, [selectCompany])

  // Danh sách Hãng xe
  const {
    data: dataCategoriesByCompany,
    isLoading: isLoadingDataCategoriesByCompany,
  }: {
    data?: TChildrenCategories[]
    isLoading?: boolean
  } = useQuery({
    queryKey: ['getCategoriesByCompany'],
    queryFn: async () => {
      const data = (await graphQLClient.request(GET_CATEGORIES, {
        eq: 'Ng==',
      })) as TCategories
      return data.categories?.items[0]?.children
    },
  })

  const arrayCompany = useMemo(() => {
    if (!!dataCategoriesByCompany) {
      const dataClone = JSON.parse(JSON.stringify(dataCategoriesByCompany))
      if (numberActive === 1) {
        setSelectCompany(dataClone?.[0])
        return dataClone?.splice(0, 8)
      }
      if (numberActive === 2) {
        setSelectCompany(dataClone?.[0])
        return dataClone?.splice(8, 8)
      }
      if (numberActive === 3) {
        setSelectCompany(dataClone?.[0])
        return dataClone?.splice(16, 8)
      }
    }
  }, [numberActive, dataCategoriesByCompany])

  return (
    <div className="on-world">
      <div className="inner-on-world">
        <div className="animation-arrow">
          <div className="anima">
            <div className="triangle">
              <img src="/icons/arrow-green-bold.svg" alt="" />
            </div>
            <div className="triangle">
              <img src="/icons/arrow-green.svg" alt="" />
            </div>
            <div className="triangle">
              <img src="/icons/arrow-green.svg" alt="" />
            </div>
            <div className="triangle">
              <img src="/icons/arrow-green.svg" alt="" />
            </div>
            <div className="triangle">
              <img src="/icons/arrow-green.svg" alt="" />
            </div>
          </div>
        </div>
        <div className="text-position !bottom-1/4 md:!bottom-[40%] xl:!bottom-[50%] 2xl:!bottom-[60%] !z-10">
          <span>otpcom</span>
        </div>
        <div className="city-position">
          <img src="/image/City.png" alt="" />
        </div>
        <div className="containers">
          <div className="title">
            <div className="title-left">
              <div className="icon">
                <img src="/icons/search.svg" alt="" />
              </div>
              <div className="text">Thương hiệu trên khắp thế giới</div>
              <div className="sub-title" dangerouslySetInnerHTML={{ __html: content }}></div>
            </div>
          </div>
          <div className="content-on-world">
            <div className="content-left">
              <div className="content-slide">
                {!!arrayCompany &&
                  !!arrayCompany?.length &&
                  arrayCompany?.map((item: TChildrenCategories, index: number) => (
                    <div
                      key={index}
                      onClick={() => {
                        setSelectCompany(item)
                      }}
                      className={`car-item ${index == 0 && 'first'}  ${
                        selectCompany?.image == item?.image && 'isSelect'
                      } `}
                    >
                      <div className={`img`}>
                        <img src={item?.image} alt="" />
                      </div>
                    </div>
                  ))}
              </div>
            </div>
            <div className="content-right">
              <div className="inner-content-right">
                <div className="the-road">
                  <div className="the-car__wrapper js-scroll gap-4 flex flex-col">
                    {/* <div className="text-position">
                      <span>otpcom</span>
                    </div> */}
                    {/* <img
                      id="img-car"
                      className="js-scroll"
                      src={`${selectCompany?.image_car}`}
                      alt=""
                      srcSet={selectCompany?.image_car}
                    /> */}
                    {selectCompany?.image_car && (
                      <motion.div
                        key={selectCompany?.image_car}
                        initial={{ opacity: 0, x: '100%', top: '10% !important' }}
                        animate={{ opacity: 1, x: '0%' }}
                        transition={{ duration: 0.8 }}
                      >
                        <div className="img">
                          <img src={selectCompany?.image_car} alt="" className="w-[930px] " />
                        </div>
                      </motion.div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
