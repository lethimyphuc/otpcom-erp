/* eslint-disable @next/next/no-img-element */
import React, { useEffect } from 'react'
import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger'
import { motion } from 'framer-motion'
import {graphQLClient, graphQLClientGET} from '~src/api/graphql'
import { useQuery } from 'react-query'
import { GET_BANNER_PAGE } from '~src/api'

type TProps = {
  data: any
}

export const BannerHome: React.FC<TProps> = ({ data }) => {
  const { data: dataBanner } = useQuery({
    queryKey: ['getBannerHome'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_BANNER_PAGE, {
        identifier: 'home-page-banner',
      })) as {
        managebanner: any
      }
      return data?.managebanner
    },
    cacheTime: Infinity,
  })

  const { data: carImage } = useQuery({
    queryKey: ['getBannerHome2'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_BANNER_PAGE, {
        identifier: 'home-page-banner-2',
      })) as {
        managebanner: any
      }
      return data?.managebanner
    },
    cacheTime: Infinity,
  })

  // useEffect(() => {
  //   gsap.registerPlugin(ScrollTrigger)

  //   const title = document.querySelector('.text-group')

  //   gsap.set(title, { opacity: 0, y: 100 })

  //   ScrollTrigger.create({
  //     trigger: title,
  //     start: 'top 80%',
  //     end: 'top 30%',
  //     onEnter: () => {
  //       gsap.to(title, {
  //         opacity: 1,
  //         y: 0,
  //         duration: 1,
  //         ease: 'power2.out',
  //       })
  //     },
  //     onLeaveBack: () => {
  //       gsap.to(title, {
  //         opacity: 0,
  //         y: 100,
  //         duration: 1,
  //         ease: 'power2.out',
  //       })
  //     },
  //   })
  // }, [])

  return (
    <div className="banner">
      <div className="content-banner">
        <div className="img relative">
          {!dataBanner ? (
            <div className="z-[0] absolute inset-0 h-full w-full">
              <LazyBanner />
            </div>
          ) : (
            <motion.img
              initial={{ opacity: 0, filter: 'blur(8px)' }}
              animate={{ opacity: 1, filter: 'blur(0)' }}
              src={dataBanner.image}
              alt=""
            />
          )}
        </div>
        {/* <div className="text-group">
          <div className="title">otpcom</div>
          <div className="text marquee">
            <motion.div className="track" variants={marqueeVariants} animate="animate">
              <p>
                Chuyên <span>phụ tùng ô tô</span> chính hãng &nbsp;&nbsp;&nbsp; Chuyên{' '}
                <span>phụ tùng ô tô</span> chính hãng &nbsp;&nbsp;&nbsp;Chuyên{' '}
                <span>phụ tùng ô tô</span> chính hãng &nbsp;&nbsp;&nbsp; Chuyên{' '}
                <span>phụ tùng ô tô</span> chính hãng &nbsp;&nbsp;&nbsp; Chuyên{' '}
                <span>phụ tùng ô tô</span> chính hãng &nbsp;&nbsp;&nbsp; Chuyên{' '}
                <span>phụ tùng ô tô</span> chính hãng &nbsp;&nbsp;&nbsp; Chuyên{' '}
                <span>phụ tùng ô tô</span> chính hãng &nbsp;&nbsp;&nbsp; Chuyên{' '}
                <span>phụ tùng ô tô</span> chính hãng &nbsp;&nbsp;&nbsp; Chuyên{' '}
                <span>phụ tùng ô tô</span> chính hãng &nbsp;&nbsp;&nbsp; Chuyên{' '}
                <span>phụ tùng ô tô</span> chính hãng &nbsp;&nbsp;&nbsp; Chuyên{' '}
                <span>phụ tùng ô tô</span> chính hãng &nbsp;&nbsp;&nbsp; Chuyên{' '}
                <span>phụ tùng ô tô</span> chính hãng &nbsp;&nbsp;&nbsp; Chuyên{' '}
                <span>phụ tùng ô tô</span> chính hãng
              </p>
            </motion.div>
          </div>
        </div> */}
        <div className="cars">
          <img src={carImage?.image} alt="" />
        </div>
      </div>
    </div>
  )
}

const LazyBanner = () => {
  return (
    <div className="fx-skeleton w-full h-full">
      <div className="overlay" />
      <div className="skeholder w-full h-full" />
      <div
        style={{
          background: 'linear-gradient(to top,rgba(255,255,255,1) 30%, rgba(255,255,255,0))',
        }}
        className="absolute bottom-0 left-0 right-0 h-24"
      />
    </div>
  )
}
