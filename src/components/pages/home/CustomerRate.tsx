/**
 * /* eslint-disable react-hooks/exhaustive-deps
 *
 * @format
 */

/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from 'react'
import { MdOutlineKeyboardArrowLeft, MdOutlineKeyboardArrowRight } from 'react-icons/md'
import { useMediaQuery } from 'react-responsive'
import { Swiper, SwiperSlide } from 'swiper/react'

type TProps = {
  data: any
}

export const CustomerRate: React.FC<TProps> = ({ data }) => {
  let isOrTablet = useMediaQuery({ query: '(max-width: 1200px)' })
  let isOrMobile = useMediaQuery({ query: '(max-width: 800px)' })
  let isOrMobileSmall = useMediaQuery({ query: '(max-width: 500px)' })
  const newData = data?.items?.find(
    (item: { identifier: string }) => item?.identifier == 'home-page-customer-reviews'
  )

  const [swiper, setSwiper] = useState<any>(null)
  const handleNext = () => {
    swiper.slideNext()
  }

  const handlePrev = () => {
    swiper.slidePrev()
  }

  // useEffect(() => {
  //   gsap.registerPlugin(ScrollTrigger);
  //   const title = document.querySelector(".customer-rate .title");
  //   gsap.set(title, { opacity: 0, y: 100 });
  //   ScrollTrigger.create({
  //     trigger: title,
  //     start: "top 80%",
  //     end: "top 30%",
  //     onEnter: () => {
  //       gsap.to(title, {
  //         opacity: 1,
  //         y: 0,
  //         duration: 1,
  //         ease: "power2.out",
  //       });
  //     },
  //     onLeaveBack: () => {
  //       gsap.to(title, {
  //         opacity: 0,
  //         y: 100,
  //         duration: 1,
  //         ease: "power2.out",
  //       });
  //     },
  //   });
  // }, []);

  // useEffect(() => {
  //   gsap.registerPlugin(ScrollTrigger);
  //   const title = document.querySelector(".swiper-customer-rate");
  //   gsap.set(title, { opacity: 0, x: 100 });
  //   ScrollTrigger.create({
  //     trigger: title,
  //     start: "top 80%",
  //     end: "top 30%",
  //     onEnter: () => {
  //       gsap.to(title, {
  //         opacity: 1,
  //         x: 0,
  //         duration: 1,
  //         ease: "power2.out",
  //       });
  //     },
  //     onLeaveBack: () => {
  //       gsap.to(title, {
  //         opacity: 0,
  //         x: 100,
  //         duration: 1,
  //         ease: "power2.out",
  //       });
  //     },
  //   });
  // }, []);
  if (newData?.nodes?.items.length === 0) return null
  return (
    <div className="customer-rate">
      <div className="bg-otp">
        <div className="img">
          <img src="/image/otpcom.png" alt="" />
        </div>
      </div>
      <div className="img-position">
        <img src="/image/11.png" alt="" />
      </div>
      <div className="title">
        <div className="icon">
          <img src="/icons/search.svg" alt="" />
        </div>
        <div className="text">Đánh giá khách hàng</div>
      </div>
      <div className="content-customer-rate">
        <div className="layer-car">
          <div className="img">
            <img src="/image/layer-car.png" alt="" />
          </div>
        </div>
        <div className="elip">
          <div className="img">
            <img src="/image/elip.png" alt="" />
          </div>
        </div>
        <div className="arrow-group z-10">
          <div className="arrow-button prev">
            <div className="icon icon-prev" onClick={handlePrev}>
              <MdOutlineKeyboardArrowLeft />
            </div>
          </div>
          <div className="arrow-button next">
            <div className="icon icon-next" onClick={handleNext}>
              <MdOutlineKeyboardArrowRight />
            </div>
          </div>
        </div>
        <div>
          {newData?.nodes?.items && (
            <Swiper
              slidesPerView={isOrMobileSmall ? 1.5 : isOrMobile ? 2.5 : isOrTablet ? 3.5 : 4.5}
              initialSlide={Math.ceil((newData?.nodes?.items?.length ?? 0) / 2)}
              spaceBetween={isOrMobileSmall ? 4 : isOrMobile ? 24 : 100}
              centeredSlides={true}
              onSwiper={(s: any) => {
                setSwiper(s)
              }}
              allowTouchMove={false}
              loop={true}
              slideToClickedSlide={true}
              lazy={true}
              speed={1000}
            >
              {newData?.nodes?.items?.map((item: any, index: number) => {
                return (
                  <SwiperSlide key={index}>
                    <div className={`item-slide`}>
                      <div className="inner-item">
                        <div className="content-inner">
                          <div className="icon">
                            <img src="/icons/Quote.svg" alt="" />
                          </div>
                          <div
                            dangerouslySetInnerHTML={{
                              __html: item?.content,
                            }}
                          ></div>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                )
              })}
            </Swiper>
          )}
        </div>
      </div>
    </div>
  )
}
