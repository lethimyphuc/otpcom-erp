/* eslint-disable @next/next/no-img-element */
import { Carousel } from 'antd'
import React, { useRef, useState } from 'react'
import { CompanyItem } from './CompanyItem'
import { MdOutlineKeyboardArrowLeft, MdOutlineKeyboardArrowRight } from 'react-icons/md'
import { UseFormSetValue, UseFormWatch } from 'react-hook-form'

const DATA_FAKE = [
  {
    name: 'kia',
    image: '/image/18.png',
  },
  { name: 'hyundai', image: '/image/18.png' },
  { name: 'toyota', image: '/image/18.png' },
  { name: 'honda', image: '/image/18.png' },
  { name: 'yamaha', image: '/image/18.png' },
]

type I = {
  setValue: UseFormSetValue<BoxSearchForm>
  watch: UseFormWatch<BoxSearchForm>
  dataCarEnd?: TChildrenCategories[]
  onSubmit: Function
}
export const CarSelectSlider: React.FC<I> = ({ setValue, watch, dataCarEnd, onSubmit }) => {
  const carouselRef = useRef<any>(null)
  const handlePrev = () => {
    carouselRef.current.prev()
  }

  const handleNext = () => {
    carouselRef.current.next()
  }

  const config = {
    slidesToScroll: 1,
    slidesToShow:
      !!dataCarEnd && !!dataCarEnd?.length && dataCarEnd?.length < 3 ? dataCarEnd?.length : 4,
    arrows: false,
    dots: false,
    infinite: dataCarEnd && dataCarEnd?.length < 4 ? false : true,
    cssEase: 'linear',
    speed: 500,
    draggable: true,
    responsive: [
      {
        breakpoint: 1000,
        settings: {
          arrows: false,
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 500,
        settings: {
          arrows: false,
          slidesToShow: 2,
        },
      },
    ],
  }

  return (
    <div className="wrapper-carousel">
      <div className="arrow-button-prev">
        <div className="icon icon-prev" onClick={handlePrev}>
          <MdOutlineKeyboardArrowLeft />
        </div>
      </div>
      <div className="arrow-button-next">
        <div className="icon icon-next" onClick={handleNext}>
          <MdOutlineKeyboardArrowRight />
        </div>
      </div>
      <div className="car-company-slider select-end">
        {!!dataCarEnd && !!dataCarEnd?.length ? (
          <Carousel {...config} ref={!!dataCarEnd ? carouselRef : null}>
            {!!dataCarEnd?.length &&
              dataCarEnd?.map((item: TChildrenCategories, index: number) => (
                <div
                  key={index}
                  className={`car-item ${watch('end') === item?.url_key && 'isSelect'} `}
                  onClick={() => {
                    setValue('end', item?.url_key)
                    onSubmit()
                  }}
                >
                  <div className={`img`}>
                    <img src={item?.image || '/image/car-default.png'} alt="" />
                  </div>
                  <div className="text">
                    <span>{item?.name}</span>
                  </div>
                </div>
              ))}
          </Carousel>
        ) : (
          <Carousel {...config} ref={!!DATA_FAKE && DATA_FAKE.length > 4 ? carouselRef : null}>
            {!!DATA_FAKE?.length &&
              DATA_FAKE?.map((item: any, index: number) => (
                <div
                  key={index}
                  className={`car-item`}
                  // onClick={() => setSelectEnd(item?.url_key)}
                >
                  <div className={`img`}>
                    <img src={'/image/car-default.png'} alt="" />
                  </div>
                  <div className="text">
                    <span>Vui lòng chọn dòng xe</span>
                  </div>
                </div>
              ))}
          </Carousel>
        )}
      </div>
    </div>
  )
}
