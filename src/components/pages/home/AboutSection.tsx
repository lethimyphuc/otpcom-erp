/* eslint-disable @next/next/no-img-element */
import React, { useEffect } from 'react'
import LazyLoad from 'react-lazyload'
import parse from 'html-react-parser'

export const AboutSection = ({ data }: any) => {
  const dataAbout = data?.items?.find((item: any) => item?.identifier == 'home-page-services')
  return (
    <LazyLoad>
      <div className="about-section">
        <div className="inner">
          <div className="img-position">
            <div className="img-main">
              <div className="pos-main">
                <img src="/image/lop-xe.png" alt="" />
              </div>
              <div className="pos1">
                <img src="/image/dolce.png" alt="" />
              </div>
              <div className="pos2">
                <img src="/image/remote-ha.png" alt="" />
              </div>
              <div className="pos3">
                <img src="/image/go-dau.png" alt="" />
              </div>
              <div className="pos4">
                <img src="/image/po-do.png" alt="" />
              </div>
            </div>
          </div>
          <div className="img-position-right">
            <img src="/image/21.png" alt="" />
          </div>
          <div className="bg-about-top">
            <img src="/image/bg-about-top.png" alt="" />
          </div>
          <div className="bg-about-mid">
            <div className="img">
              <img src="/image/bg-about-mid.png" alt="" />
            </div>
          </div>
          <div className="bg-about-bot">
            <img src="/image/bg-about-bot.png" alt="" />
          </div>
          <div className="containers">
            <div className="content-about-section">
              <div className="content-inner">
                {dataAbout?.nodes.items.map((item: any, index: number) => {
                  return (
                    <div className="item" key={index}>
                      <div className="img w-full">
                        <img src={item.image} alt="" />
                      </div>
                      <div className="text">
                        <div className="title">{item.title}</div>
                        <div className="subtext">{item.content ? parse(item.content) : ''}</div>
                      </div>
                    </div>
                  )
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    </LazyLoad>
  )
}
