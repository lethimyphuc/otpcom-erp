/* eslint-disable @next/next/no-img-element */
import { Carousel } from 'antd'
import React, { useRef, useState } from 'react'
import { CompanyItem } from './CompanyItem'
import { MdOutlineKeyboardArrowLeft, MdOutlineKeyboardArrowRight } from 'react-icons/md'
import { UseFormSetValue, UseFormWatch } from 'react-hook-form'
import { useWindowSize } from '~src/hook'

const DATA_FAKE = [
  {
    name: 'kia',
    img: '/image/17.png',
  },
  { name: 'hyundai', img: '/image/17.png' },
  { name: 'toyota', img: '/image/17.png' },
  { name: 'honda', img: '/image/17.png' },
  { name: 'yamaha', img: '/image/17.png' },
]

type I = {
  setValue: UseFormSetValue<BoxSearchForm>
  watch: UseFormWatch<BoxSearchForm>
  dataCarType?: TChildrenCategories[]
}
export const CarTypeSlider: React.FC<I> = ({ setValue, watch, dataCarType }) => {
  const { isTablet } = useWindowSize()

  const carouselRef = useRef<any>(null)
  const handlePrev = () => {
    carouselRef.current.prev()
  }

  const handleNext = () => {
    carouselRef.current.next()
  }

  const config = {
    slidesToScroll: 1,
    slidesToShow:
      !!dataCarType && !!dataCarType?.length && dataCarType?.length < 3 ? dataCarType?.length : 4,
    arrows: false,
    dots: false,
    infinite: (dataCarType?.length ?? 0) > 3 || (isTablet && (dataCarType?.length ?? 0) > 2),
    cssEase: 'linear',
    speed: 500,
    draggable: true,
    responsive: [
      {
        breakpoint: 1000,
        settings: {
          arrows: false,
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 500,
        settings: {
          arrows: false,
          slidesToShow: 2,
        },
      },
    ],
  }

  return (
    <div className="wrapper-carousel">
      <div className="arrow-button-prev">
        <div className="icon icon-prev" onClick={handlePrev}>
          <MdOutlineKeyboardArrowLeft />
        </div>
      </div>
      <div className="arrow-button-next">
        <div className="icon icon-next" onClick={handleNext}>
          <MdOutlineKeyboardArrowRight />
        </div>
      </div>
      <div className="car-company-slider car-type">
        {!!dataCarType && !!dataCarType?.length ? (
          <Carousel {...config} ref={!!dataCarType ? carouselRef : null}>
            {!!dataCarType?.length &&
              dataCarType?.map((item: TChildrenCategories, index: number) => (
                <div
                  key={index}
                  className={`car-item ${watch('type') === item?.url_key && 'isSelect'} `}
                  onClick={() => setValue('type', item?.url_key)}
                >
                  <div className={`img`}>
                    <img src={item?.image || '/image/car-default.png'} alt="" />
                  </div>
                </div>
              ))}
          </Carousel>
        ) : (
          <Carousel {...config} ref={!!DATA_FAKE && DATA_FAKE?.length > 4 ? carouselRef : null}>
            {!!DATA_FAKE?.length &&
              DATA_FAKE?.map((item: any, index: number) => (
                <div
                  key={index}
                  //  ${carType === item?.url_key && "isSelect"}
                  className={`car-item`}
                  // onClick={() => setCarType(item?.url_key)}
                >
                  <div className={`img`}>
                    <img src={'/image/car-default.png'} alt="" />
                  </div>
                </div>
              ))}
          </Carousel>
        )}
      </div>
    </div>
  )
}
