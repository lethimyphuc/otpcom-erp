/* eslint-disable @next/next/no-img-element */
/**
 * /* eslint-disable @next/next/no-img-element
 *
 * @format
 */

import { DefaultOptionType } from 'antd/lib/select'
import { useRouter } from 'next/router'
import { useForm } from 'react-hook-form'
import { useQuery } from 'react-query'
import { GET_CATEGORIES, GET_CATEGORIES_BY_URL_KEY } from '~src/api/categories'
import { graphQLClient, graphQLClientGET } from '~src/api/graphql'
import { ButtonMain } from '~src/components/global/Button/ButtonMain/ButtonMain'
import { FormSelect } from '~src/components/global/FormControls/FormSelect'
import { removeEmptyValueFromObject } from '~src/util'
import { ListSearch } from './ListSearch'
import { Input } from 'antd'

export const BoxSearch = () => {
  const router = useRouter()
  const { control, handleSubmit, setValue, getValues, watch } = useForm<BoxSearchForm>({
    defaultValues: {
      type: null,
      company: null,
      end: null,
      groupAccessary: null,
      search: null,
    },
  })

  // Danh sách Hãng xe
  const {
    data: dataCategoriesByCompany,
    isLoading: isLoadingDataCategoriesByCompany,
  }: {
    data?: TChildrenCategories[]
    isLoading?: boolean
  } = useQuery({
    queryKey: ['getCategoriesByCompany'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_CATEGORIES, {
        eq: 'Ng==',
      })) as TCategories
      return data.categories?.items[0]?.children
    },
  })

  // Danh sách Dòng xe
  const {
    data: dataCategoriesByType,
    isLoading: isLoadingDataCategoriesByType,
  }: {
    data?: TChildrenCategories[]
    isLoading?: boolean
  } = useQuery({
    queryKey: ['getCategoriesByType', { eq: watch('company') }],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_CATEGORIES_BY_URL_KEY, {
        eq: `${watch('company')}`,
      })) as TCategories
      return data.categories?.items[0]?.children
    },
    enabled: !!watch('company'),
  })

  // Danh sách Đời xe
  const {
    data: dataCategoriesByEnd,
    isLoading: isLoadingDataCategoriesByEnd,
  }: {
    data?: TChildrenCategories[]
    isLoading?: boolean
  } = useQuery({
    queryKey: ['getCategoriesByEnd', { eq: watch('type') }],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_CATEGORIES_BY_URL_KEY, {
        eq: `${watch('type')}`,
      })) as TCategories
      return data.categories?.items[0]?.children
    },
    enabled: !!watch('type'),
  })

  // Danh sách nhóm phụ tùng
  // const {
  //   data: dataSpareParts,
  //   isLoading: isLoadingDataSpareParts,
  // }: {
  //   data?: TChildrenCategories[]
  //   isLoading?: boolean
  // } = useQuery({
  //   queryKey: ['getSpareParts'],
  //   queryFn: async () => {
  //     const data = (await graphQLClient.request(GET_CATEGORIES, {
  //       eq: 'NTU=',
  //     })) as TCategories
  //     return data.categories?.items[0]?.children
  //   },
  // })

  const handleSelectCategoryCompany = (
    value: any,
    option?: DefaultOptionType | DefaultOptionType[] | undefined
  ) => {
    setValue('type', null)
  }

  const onSubmit = (data: BoxSearchForm) => {
    const pathname =
      '/catalog-category' +
      (data?.company ? `/${data?.company}` : '') +
      (data?.type ? `/${data?.type}` : '') +
      (data?.end ? `/${data?.end}` : '') +
      (data?.tag ? `/${data?.tag}` : '')
    router.push({
      pathname: pathname,
      query: removeEmptyValueFromObject({ ...data, submitted: true }),
    })
  }

  return (
    <div className="box-search">
      <div className="bg-search">
        <img src="/image/search-banner.png" alt="" />
      </div>
      <div className="containers">
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="group-box">
            <div className="box">
              <div className="items">
                <div className="item">
                  <FormSelect
                    control={control}
                    name={'company'}
                    optionList={dataCategoriesByCompany?.map((company: TChildrenCategories) => {
                      return {
                        label: company?.name,
                        value: company?.url_key,
                      }
                    })}
                    label="Hãng xe"
                    placeholder="Chọn hãng xe"
                    onChange={handleSelectCategoryCompany}

                    // allowClear
                  />
                </div>
                <div className="item">
                  <FormSelect
                    control={control}
                    name={'type'}
                    optionList={dataCategoriesByType?.map((type: TChildrenCategories) => {
                      return {
                        label: type?.name,
                        value: type?.url_key,
                      }
                    })}
                    label="Dòng xe"
                    placeholder="Chọn dòng xe"
                    disabled={!watch('company')}
                  />
                </div>
                <div className="item ">
                  <div className="label-form">
                    <label className="" htmlFor={'search'}>
                      Từ khóa tìm kiếm
                    </label>
                  </div>
                  <Input
                    autoFocus
                    // prefix={<img src="/icons/search-icon.svg" alt="" />}
                    placeholder="Nhập tên sản phẩm, thương hiệu,..."
                    // onKeyDown={(e) => e.key === 'Enter' && onSubmit()}
                    onChange={(event) => {
                      setValue('search', event.target.value)
                    }}
                  />
                  {/* <FormSelect
                    control={control}
                    name={'groupAccessary'}
                    optionList={dataSpareParts?.map((sparePart: TChildrenCategories) => {
                      return {
                        label: sparePart?.name,
                        value: sparePart?.uid,
                      }
                    })}
                    label="Nhóm phụ tùng"
                    placeholder="Chọn nhóm phụ tùng"
                  /> */}
                </div>
              </div>

              <div className="button-search">
                <ButtonMain background="green" type="submit">
                  <div className="flex items-center gap-[0.4rem]">
                    <div className="icon">
                      <img src="/icons/search-icon.svg" alt="" />
                    </div>
                    <div className="text">Tìm kiếm</div>
                  </div>
                </ButtonMain>
              </div>
            </div>
          </div>

          <ListSearch
            dataCategoriesByCompany={dataCategoriesByCompany || []}
            dataCategoriesByType={dataCategoriesByType || []}
            dataCategoriesByEnd={dataCategoriesByEnd || []}
            setValue={setValue}
            getValues={getValues}
            watch={watch}
            onSubmit={handleSubmit(onSubmit)}
          />
        </form>
      </div>
    </div>
  )
}
