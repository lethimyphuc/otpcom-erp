/* eslint-disable @next/next/no-img-element */
import React, { useState } from 'react'
import { BannerHome } from './BannerHome'
import { BoxSearch } from './BoxSearch'
import { AboutSection } from './AboutSection'
import { CatalogsSection } from './CatalogsSection'
import { ProductSale } from './ProductSale'
import { OnWorld } from './OnWorld'
import { ProductUs } from './ProductUs'
import { DiarySection } from './DiarySection'
import { CustomerRate } from './CustomerRate'
import { useQuery } from 'react-query'
import {graphQLClient, graphQLClientGET} from '~src/api/graphql'
import { GET_CONTENT_HOME_PAGE } from '~src/api'

export const HomePage = () => {
  // GET_CONTENT_HOME_PAGE
  const { data, isLoading, refetch } = useQuery({
    queryKey: ['getContentHomePage'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_CONTENT_HOME_PAGE, {
        identifiers: [
          'home-page-banner',
          'home-page-branchs',
          'home-page-services',
          'info-contact',
          'home-page-customer-reviews',
        ],
      })) as {
        snowdogMenus: any
      }
      return data?.snowdogMenus
    },
    cacheTime: Infinity,
  })

  return (
    <div className="home-page">
      <BannerHome data={data} />
      <BoxSearch />
      <AboutSection data={data} />
      <CatalogsSection />
      <ProductSale />
      <OnWorld dataBrand={data} />
      <ProductUs />
      <DiarySection />
      <CustomerRate data={data} />
    </div>
  )
}
