/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from 'react'
import { ButtonMain } from '~src/components/global/Button/ButtonMain/ButtonMain'
import { DiarySlider } from './DiarySlider'
import { useRouter } from 'next/router'
import { useQuery } from 'react-query'
import {graphQLClient, graphQLClientGET} from '~src/api/graphql'
import { GET_LIST_POST } from '~src/api/post'
import Link from 'next/link'
import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger'

export const DiarySection = () => {
  const router = useRouter()
  const { data: dataBlogPosts, isLoading: isLoadingDataBlogPosts } = useQuery({
    queryKey: ['getBlogPosts', router.query],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_LIST_POST, {
        sortBy: !!router.query?.sortBy ? `${router.query?.sortBy}` : 'Popular',
        action: 'get_post_by_categoryId',
        pageSize: !!router?.query?.pageSize ? parseInt(`${router.query.pageSize}`) : 20,
        currentPage: !!router?.query?.currentPage ? parseInt(`${router.query.currentPage}`) : 1,
        categoryId: 2,
      })) as {
        mpBlogPosts: {
          items: TPost[]
          page_info: TPageInfo
          total_count: number
        }
      }
      return data?.mpBlogPosts
    },
  })

  // console.log('dataBlogPosts: ', dataBlogPosts)

  return (
    <div className="diary-section">
      <div className="inner-diary-section">
        <div className="containers">
          <div className="title relative">
            <div className="title-left">
              <div className="icon">
                <img src="/icons/search.svg" alt="" />
              </div>
              <div className="text whitespace-nowrap">
                <span>nhật ký</span>
              </div>
            </div>
            <div className="title-right">
              <div className="button-all">
                <Link
                  href={{
                    pathname: '/diary',
                  }}
                >
                  <a>
                    <ButtonMain
                      background="green"
                      type="button"
                      className="right-0 bottom-0 sm:mb-10 mb-2"
                    >
                      <div className="text-all">
                        <span>Xem tất cả</span>
                      </div>
                    </ButtonMain>
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>
        <DiarySlider dataBlogPosts={dataBlogPosts} />
      </div>
    </div>
  )
}
