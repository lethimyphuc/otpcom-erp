/* eslint-disable @next/next/no-img-element */
/**
 * /* eslint-disable @next/next/no-img-element
 *
 * @format
 */

import Link from 'next/link'
import React, { useEffect } from 'react'
import { useQuery } from 'react-query'
import { GET_CATEGORIES } from '~src/api/categories'
import { graphQLClient, graphQLClientGET } from '~src/api/graphql'
import { ButtonMain } from '~src/components/global/Button/ButtonMain/ButtonMain'
import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger'
import { _format } from '~src/util'

const data = [
  {
    name: 'Phụ tùng thân vỏ',
    icon: '/icons/002-suspension.svg',
  },
  {
    name: 'Phụ kiện phụ trợ khác',
    icon: '/icons/004-cross-wrench.svg',
  },
  {
    name: 'Phụ kiện theo hãng',
    icon: '/icons/003-mirror.svg',
  },
  {
    name: 'Phụ kiện phụ trợ',
    icon: '/icons/001-wheel.svg',
  },
]
export const CatalogsSection = () => {
  // Danh sách nhóm phụ tùng
  const {
    data: dataSpareParts,
    isLoading: isLoadingDataSpareParts,
  }: {
    data?: TChildrenCategories[]
    isLoading?: boolean
  } = useQuery({
    queryKey: ['getSpareParts'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_CATEGORIES, {
        eq: 'NTU=',
      })) as TCategories
      return data.categories?.items[0]?.children
    },
  })

  // const query = query

  return (
    <div className="catalog-section">
      <div className="bg-left">
        <img src="/image/bg-catalog-left.png" alt="" />
      </div>
      <div className="bg-right">
        <img src="/image/bg-catalog-left.png" alt="" />
      </div>

      <div className="containers">
        <div className="title relative">
          <div className="title-left">
            <div className="icon">
              <img src="/icons/search.svg" alt="" />
            </div>
            <div className="text">
              <h3>Danh mục sản phẩm</h3>
            </div>
          </div>
          <div className="title-right">
            <Link
              href={{
                pathname: '/catalog-category',
              }}
            >
              <a>
                <ButtonMain background="green" type="button" className="right-0 bottom-0">
                  <div className="text-all">
                    <span>Xem tất cả</span>
                  </div>
                </ButtonMain>
              </a>
            </Link>
          </div>
        </div>
        <div className="content-home-catalog">
          <div className="gallery-top">
            <div className="items item-left">
              <div className="img">
                <img
                  src={!!dataSpareParts && !!dataSpareParts[0] ? dataSpareParts[0]?.image : ''}
                  alt=""
                />
                <div className="inner-content">
                  <div className="icon">
                    <img src="/icons/008-pistons.svg" alt="" />
                  </div>
                  <div className="txt">
                    <span>
                      {!!dataSpareParts && !!dataSpareParts[0] ? dataSpareParts[0]?.name : ''}
                    </span>
                  </div>
                </div>
                <Link
                  href={{
                    pathname: `/accessories/${_format.converStringToString(
                      dataSpareParts?.[0]?.url_key as string
                    )}`,
                  }}
                >
                  <div className="button-center">
                    <div className="inner-button">
                      <div className="inner-text">
                        <div className="total">
                          {!!dataSpareParts && !!dataSpareParts[0]
                            ? dataSpareParts[0]?.product_count
                            : 0}
                        </div>
                        <div className="text">sản phẩm</div>
                        <div className="icon">
                          <img src="/icons/right-up-red.svg" alt="" />
                        </div>
                      </div>
                    </div>
                  </div>
                </Link>
              </div>
            </div>
            <div className="items item-right">
              <div className="item">
                <div className="img">
                  <img
                    src={!!dataSpareParts && !!dataSpareParts[1] ? dataSpareParts[1]?.image : ''}
                    alt=""
                  />
                  <div className="inner-content">
                    <div className="icon">
                      <img src="/icons/008-pistons.svg" alt="" />
                    </div>
                    <div className="txt">
                      <span>
                        {!!dataSpareParts && !!dataSpareParts[1] ? dataSpareParts[1]?.name : ''}
                      </span>
                    </div>
                  </div>
                  <Link
                    href={{
                      pathname: `/accessories/${_format.converStringToString(
                        dataSpareParts?.[1]?.url_key as string
                      )}`,
                    }}
                  >
                    <div className="button-center">
                      <div className="inner-button">
                        <div className="inner-text">
                          <div className="total">
                            {!!dataSpareParts && dataSpareParts[1]
                              ? dataSpareParts[1]?.product_count
                              : 0}
                          </div>
                          <div className="text">sản phẩm</div>
                          <div className="icon">
                            <img src="/icons/right-up-red.svg" alt="" />
                          </div>
                        </div>
                      </div>
                    </div>
                  </Link>
                </div>
              </div>
              <div className="item">
                <div className="img">
                  <img
                    src={!!dataSpareParts && !!dataSpareParts[2] ? dataSpareParts[2]?.image : ''}
                    alt=""
                  />
                  <div className="inner-content">
                    <div className="icon">
                      <img src="/icons/008-pistons.svg" alt="" />
                    </div>
                    <div className="txt">
                      <span>
                        {!!dataSpareParts && !!dataSpareParts[2] ? dataSpareParts[2]?.name : ''}
                      </span>
                    </div>
                  </div>
                  <Link
                    href={{
                      pathname: `/accessories/${_format.converStringToString(
                        dataSpareParts?.[2]?.url_key as string
                      )}`,
                    }}
                  >
                    <div className="button-center">
                      <div className="inner-button">
                        <div className="inner-text">
                          <div className="total">
                            {' '}
                            {!!dataSpareParts && !!dataSpareParts[2]
                              ? dataSpareParts[2]?.product_count
                              : 0}
                          </div>
                          <div className="text">sản phẩm</div>
                          <div className="icon">
                            <img src="/icons/right-up-red.svg" alt="" />
                          </div>
                        </div>
                      </div>
                    </div>
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className="gallery-bottom">
            {dataSpareParts?.map((item: TChildrenCategories, index: number) => {
              return (
                <React.Fragment key={index}>
                  {index > 2 && index < 7 ? (
                    <div className="item">
                      <div className="img">
                        <img src={item?.image} alt="" />
                        <div className="inner-content">
                          <div className="icon">
                            <img src="/icons/008-pistons.svg" alt="" />
                          </div>
                          <div className="txt">
                            <span>{item?.name}</span>
                          </div>
                        </div>
                        <Link
                          href={{
                            pathname: `/accessories/${_format.converStringToString(item?.url_key)}`,
                          }}
                        >
                          <div className="button-center">
                            <div className="inner-button">
                              <div className="inner-text">
                                <div className="total">{item?.product_count || 0}</div>
                                <div className="text">sản phẩm</div>
                                <div className="icon">
                                  <img src="/icons/right-up-red.svg" alt="" />
                                </div>
                              </div>
                            </div>
                          </div>
                        </Link>
                      </div>
                    </div>
                  ) : (
                    <></>
                  )}
                </React.Fragment>
              )
            })}
          </div>
        </div>
      </div>
    </div>
  )
}
