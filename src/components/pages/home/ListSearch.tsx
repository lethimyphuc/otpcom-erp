/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from "react";
import { SliderGroup } from "./SliderGroup";
import {
  UseFormGetValues,
  UseFormSetValue,
  UseFormWatch,
} from "react-hook-form";

type ListSearchProps = {
  dataCategoriesByCompany?: TChildrenCategories[];
  dataCategoriesByType?: TChildrenCategories[];
  dataCategoriesByEnd?: TChildrenCategories[];
  setValue: UseFormSetValue<BoxSearchForm>;
  getValues: UseFormGetValues<BoxSearchForm>;
  watch: UseFormWatch<BoxSearchForm>;
  onSubmit: Function;
};
export const ListSearch = (props: ListSearchProps) => {
  const {
    dataCategoriesByCompany,
    dataCategoriesByType,
    dataCategoriesByEnd,
    setValue,
    getValues,
    watch,
    onSubmit,
  } = props;
  // const [selectCompany, setSelectCompany] = useState<string | null>(
  //   !!dataCategoriesByCompany && !!dataCategoriesByCompany?.length
  //     ? dataCategoriesByCompany[0]?.uid
  //     : null
  // );
  // const [carType, setCarType] = useState<any>(null);
  // const [selectEnd, setSelectEnd] = useState<any>(null);

  // console.log("selectCompany: ", selectCompany);

  return (
    <div className="list-search">
      <div className="title-search">
        <div className="icon-center">
          <img src="/icons/search.svg" alt="" />
        </div>
        <div className="content-text">
          <span className="text">tìm kiếm nhanh</span>
        </div>
      </div>
      <div className="inner-content">
        <div className="progress">
          <div
            className={`progress-item  ${!!watch("company") ? "is-done" : ""}`}
            onClick={() => {
              setValue("type", null);
              setValue("end", null);
            }}
          >
            <div className="progress-text">Lựa chọn</div>
            <div className="progress-title">Hãng xe</div>
          </div>
          <div
            className={`progress-item ${!!watch("company") ? "current" : ""} ${
              !!watch("type") ? "is-done" : ""
            }`}
            onClick={() => {
              setValue("type", null);
              setValue("end", null);
            }}
          >
            <div className="progress-text">Lựa chọn</div>
            <div className="progress-title">Dòng xe</div>
          </div>
          <div
            className={`progress-item ${!!watch("type") ? "current" : ""} ${
              !!watch("end") ? "is-done" : ""
            }`}
            onClick={() => {
              setValue("end", null);
            }}
          >
            <div className="progress-text ">Lựa chọn</div>
            <div className="progress-title">Đời xe</div>
          </div>
        </div>
        <div className="search-content">
          <SliderGroup
            dataCategoriesByCompany={dataCategoriesByCompany}
            dataCategoriesByType={dataCategoriesByType}
            dataCategoriesByEnd={dataCategoriesByEnd}
            setValue={setValue}
            getValues={getValues}
            watch={watch}
            onSubmit={onSubmit}
          />
        </div>
      </div>
    </div>
  );
};
