/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from 'react'
import { ProductList } from '../product/ProductList'
import { ButtonMain } from '~src/components/global/Button/ButtonMain/ButtonMain'
import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger'
import { useQuery } from 'react-query'
import { graphQLClient, graphQLClientGET } from '~src/api/graphql'
import { GET_PRODUCTS } from '~src/api/product'
import Link from 'next/link'
export const ProductUs = () => {
  const { data: dataProducts, isLoading: isLoadingDataProducts } = useQuery({
    queryKey: ['getProductsUs'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_PRODUCTS, {
        search: '',
        pageSize: 12,
        currentPage: 1,
      })) as {
        products: {
          items: TProduct[]
          page_info: {
            current_page: number
            page_size: number
            total_pages: number
          }
        }
      }
      return data?.products
    },
  })

  // useEffect(() => {
  //   gsap.registerPlugin(ScrollTrigger);

  //   const title = document.querySelector(".title-product-us");

  //   gsap.set(title, { opacity: 0, y: 100 });

  //   ScrollTrigger.create({
  //     trigger: title,
  //     start: "top 80%",
  //     end: "top 30%",
  //     onEnter: () => {
  //       gsap.to(title, {
  //         opacity: 1,
  //         y: 0,
  //         duration: 1,
  //         ease: "power2.out",
  //       });
  //     },
  //     onLeaveBack: () => {
  //       gsap.to(title, {
  //         opacity: 0,
  //         y: 100,
  //         duration: 1,
  //         ease: "power2.out",
  //       });
  //     },
  //   });
  // }, []);

  return (
    <div className="product-us">
      <div className="bg-product-us">
        <img src="/image/product-us.png" alt="" />
        <div className="hidden xl:block">
          <div className="arrow-red">
            <img src="/image/arrow-red.png" alt="" />
          </div>
          <div className="arrow-white">
            <img src="/image/arrow-white.png" alt="" />
          </div>
        </div>
        <div className="img-position">
          <img src="/image/img-product-us.png" alt="" />
        </div>
      </div>
      <div className="containers">
        <div className="content-product-us">
          <div className="title title-product-us">
            <div className="icon">
              <img src="/icons/red-white.svg" alt="" />
            </div>
            <div className="text">Sản phẩm của chúng tôi</div>
          </div>
          <div className="wrapper-product">
            <div className="list-product">
              <ProductList data={dataProducts?.items || []} loading={isLoadingDataProducts} />
            </div>
          </div>

          <div className="button-all">
            <Link
              href={{
                pathname: '/catalog-category',
              }}
            >
              <a>
                <ButtonMain background="green" type="button">
                  <div className="text-all">
                    <span>Xem tất cả</span>
                  </div>
                </ButtonMain>
              </a>
            </Link>
          </div>
        </div>
      </div>
    </div>
  )
}
