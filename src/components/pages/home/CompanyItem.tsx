/* eslint-disable @next/next/no-img-element */
import React from "react";

type I = {
  data: any;
};
export const CompanyItem: React.FC<I> = ({ data }) => {
  return (
    <div className="car-item">
      <div className="img">
        <img src={data?.img} alt="" />
      </div>
    </div>
  );
};
