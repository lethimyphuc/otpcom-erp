/* eslint-disable @next/next/no-img-element */
import { Route } from 'antd/lib/breadcrumb/Breadcrumb'
import Link from 'next/link'
import { useRouter } from 'next/router'
import React, { useEffect, useMemo, useState } from 'react'
import { useQuery } from 'react-query'
import { GET_BANNER_PAGE, GET_LIST_CATALOG } from '~src/api'
import { GET_CATEGORIES_BY_URL_KEY } from '~src/api/categories'
import { graphQLClient, graphQLClientGET } from '~src/api/graphql'
import { ProductSkeleton } from '~src/components/ProductSkeleton'
import { CatalogFilter } from '~src/components/common/CatalogFilter'
import { PaginationCustom } from '~src/components/common/PaginationCustom'
import { removeEmptyValueFromObject } from '~src/util'
import { BannerBrand } from '../../brand/BannerBrand'
import { Empty } from 'antd'

type Props = {}

export const CatalogCategoryPage: React.FC<Props> = ({}) => {
  const router = useRouter()
  const [bannerImage, setBannerImage] = useState<string | undefined>(undefined)
  const [filter, setFilter] = useState<ProductFilterReq>({})
  const [imageCar, setImageCar] = useState<string | undefined>(undefined)

  const { data: dataListCataLog, isLoading: isLoadingGetListCatalog } = useQuery({
    queryKey: ['DATA_LIST_CATALOG', router.query],
    queryFn: async () => {
      try {
        let filter: any = {}
        if (router.query?.tagId) {
          filter.tag_id = { finset: router.query?.tagId }
        }
        if (router.query?.category) {
          filter.category_url_key = { eq: router.query?.category }
        }
        if (router.query?.search) {
          filter.name = { like: `%${router.query?.search}%` }
        }
        const data: any = await graphQLClient.request(GET_LIST_CATALOG, {
          filter,
          currentPage: router?.query?.currentPage || 1,
          pageSize: 10,
          sort: {
            name: JSON.parse((router?.query?.sort as string) || '{}')?.name || 'ASC',
          },
        })
        return data?.catalogues
      } catch (error: any) {
        console.log(error)
      }
    },
    enabled: !!router.query?.category || !!router.query?.search,
  })

  // const { data: currentCate } = useQuery({
  //   queryKey: ['currentCate', router.query?.category],
  //   queryFn: async () => getMenuCatalog({ filter: { url_key: { eq: router.query?.category } } }),
  //   select: (data) => data?.[0],
  //   onSuccess: (data) => {
  //     const findItem = (lever: number) => {
  //       return data?.breadcrumbs?.find((item) => item?.category_level === lever)
  //     }
  //     if (data) {
  //       setFilter({
  //         ...filter,
  //         company: findItem(0)?.category_url_key,
  //         type: findItem(1)?.category_url_key,
  //         end: findItem(2)?.category_url_key,
  //         sort: (router.query?.sort as string) || null,
  //       })
  //     }
  //   },
  // })
  // console.log('currentCate', currentCate)
  const { data: contentCategoryPage, isLoading: isloadingCategoryPage } = useQuery({
    queryKey: ['getCategoryPage'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_BANNER_PAGE, {
        identifier: 'default-category-banner',
      })) as {
        managebanner: any
      }
      return data?.managebanner
    },
    cacheTime: Infinity,
  })

  useEffect(() => {
    const query = router.query || {}
    const data: ProductFilterReq = removeEmptyValueFromObject({
      company: query?.category as string,
      end: query?.end as string,
      groupAccessary: query?.groupAccessary as string,
      sort: query?.sort as string,
      tag: query?.tag as string,
      type: query?.type as string,
      search: query?.search as string,
    })
    setFilter(data)
  }, [router.query])

  const handleFilter = (data: any, sort?: any) => {
    const pathname =
      '/catalog-category' +
      (data?.company ? `/${data?.company}` : '') +
      (data?.type ? `/${data?.type}` : '') +
      (data?.end ? `/${data?.end}` : '') +
      (data?.tag ? `/${data?.tag}` : '')
    router.push(
      {
        pathname: pathname,
        query: { sort: sort?.sort, tagId: sort.tag },
      },
      undefined,
      {
        scroll: false,
      }
    )
  }
  const { data: companies }: any = useQuery({
    queryKey: ['getCategoriesByCompany'],
    enabled: false,
  })
  const { data: types, isLoading: isLoadingTag }: any = useQuery({
    queryKey: ['getCategoriesByType', filter?.company],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_CATEGORIES_BY_URL_KEY, {
        eq: filter?.company,
      })) as TCategories
      return data.categories?.items[0]?.children
    },
    enabled: !!filter?.company,
  })
  const { data: models, isLoading: isLoadingModels }: any = useQuery({
    queryKey: ['getCategoriesByEnd', { eq: filter?.type }],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_CATEGORIES_BY_URL_KEY, {
        eq: filter?.type,
      })) as TCategories
      return data.categories?.items[0]?.children
    },
    enabled: !!filter?.type,
  })
  const { data: tags }: any = useQuery({
    queryKey: ['getCategoriesByTags', { eq: filter?.end }],
    enabled: false,
  })
  const breadcrumb: Route[] = useMemo(() => {
    let breadcrumb: Route[] = []

    if (filter?.company && companies?.length) {
      breadcrumb.push({
        breadcrumbName: companies?.find((item: any) => item?.url_key === filter?.company)?.name,
        path: '/catalog-category/' + filter?.company,
      })
    }
    if (filter?.type && types?.length) {
      breadcrumb.push({
        breadcrumbName: types?.find((item: any) => item?.url_key === filter?.type)?.name,
        path: '/catalog-category/' + filter?.company + '/' + filter?.type,
      })
    }
    if (filter?.end && models?.length) {
      breadcrumb.push({
        breadcrumbName: models?.find((item: any) => item?.url_key === filter?.end)?.name,
        path: '/catalog-category/' + filter?.company + '/' + filter?.type + '/' + filter?.end,
      })
    }
    if (filter?.tag && tags?.length) {
      breadcrumb.push({
        breadcrumbName: tags?.find((item: any) => item?.id === filter?.tag)?.name,
        path:
          '/catalog-category/' +
          filter?.company +
          '/' +
          filter?.type +
          '/' +
          filter?.end +
          '&tagId=' +
          filter?.tag,
      })
    }
    if (breadcrumb.length === 0) {
      return [
        {
          breadcrumbName: 'Hãng xe',
          path: '/catalog-category',
        },
      ]
    }
    return breadcrumb
  }, [companies, types, models, tags])

  const title = useMemo(() => {
    if (isLoadingTag || isLoadingModels) return
    let title
    if (filter?.tag && tags?.length) {
      title = tags?.find((item: any) => item?.id === filter.tag)?.name
    } else if (filter?.end && models?.length) {
      title = models?.find((item: any) => item?.url_key === filter.end)?.name
    } else if (filter?.type && types?.length) {
      title = types?.find((item: any) => item?.url_key === filter.type)?.name
    } else if (filter?.company && companies?.length) {
      title = companies?.find((item: any) => item?.url_key === filter.company)?.name
    }

    return title
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter, companies, types, models, tags])

  return (
    <div className="car-company branch-page">
      <div className="banner-page">
        <BannerBrand
          bannerImage={
            !isloadingCategoryPage &&
            (bannerImage
              ? bannerImage
              : contentCategoryPage?.image
              ? contentCategoryPage.image
              : '/image/bg-product.png')
          }
          // breadcrumbs={currentCate?.breadcrumbs.map((item: any) => ({
          //   breadcrumbName: item.category_name,
          //   path: item.category_url_path,
          // }))}
          breadcrumbs={[...breadcrumb]}
          imageCar={imageCar}
        />
      </div>
      <div id="otpcom--products-list" className="containers">
        <div className="title-promotion">
          <div className="list-search">
            <div className="title-search">
              <div className="icon-center">
                <img src="/icons/search.svg" alt="" />
              </div>
              <div className="content-text">
                <span className="text">{title}</span>
              </div>
            </div>
          </div>
        </div>
        <div className="content-inner-promotion">
          <div className="filter-promotion">
            <CatalogFilter
              onChange={handleFilter}
              value={filter}
              onChangeBanner={setBannerImage}
              onChangeImageCar={setImageCar}
              setFilter={setFilter}
              // currentCate={currentCate}
            />
          </div>
        </div>
      </div>
      <section className="section-cata-otp">
        <div className="cata-otp">
          <div className="containers">
            <div className="cata-otp-wrapper">
              <div className="cata-otp-list row">
                {(!!filter?.end || !!router?.query?.tagId || !!router.query?.search) && (
                  <>
                    {isLoadingGetListCatalog ? (
                      <>
                        {[...Array(4)].map((_, index) => (
                          <div className="cata-otp-item col col-3" key={index}>
                            <ProductSkeleton />
                          </div>
                        ))}
                      </>
                    ) : (
                      <>
                        {dataListCataLog?.items?.map((data: any, index: any) => {
                          return (
                            <div className="cata-otp-item col col-3" key={index}>
                              <div className="cata-otp-wr">
                                <Link href={`/catalog/${data?.url_key}`}>
                                  <a className="cata-otp-img">
                                    <img src={data?.catalogue_image} alt={data?.catalogue_image} />
                                  </a>
                                </Link>
                                <div className="cata-otp-content">
                                  <Link href={`/catalog/${data?.url_key}`}>
                                    <a className="cata-otp-name fw-7">{data?.name}</a>
                                  </Link>
                                  <div className="cata-otp-control">
                                    <div className="inner">
                                      <Link href={`/catalog/${data?.url_key}`}>
                                        <a className="cata-otp-btn btn full">
                                          <span className="txt">XEM CHI TIẾT</span>
                                        </a>
                                      </Link>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          )
                        })}
                        {dataListCataLog?.items?.length === 0 && (
                          <div className="w-full text-center pb-[10rem]">
                            <Empty description="Không có catalog" />
                          </div>
                        )}
                        <PaginationCustom
                          current={dataListCataLog?.page_info?.current_page}
                          total={dataListCataLog?.total_count}
                          pageSize={dataListCataLog?.page_info?.page_size}
                          onChange={(page: number) =>
                            router.push(
                              {
                                query: {
                                  ...router.query,
                                  currentPage: page,
                                },
                              },
                              undefined,
                              { scroll: false }
                            )
                          }
                        />
                      </>
                    )}
                  </>
                )}
                {!filter?.end && !router?.query?.tagId && (
                  <>
                    {(!!filter.type ? models : filter?.company ? types : companies)
                      ?.sort((a: any, b: any) =>
                        router?.query?.sort?.includes('ASC')
                          ? ('' + a.name).localeCompare(b.name)
                          : ('' + b.name).localeCompare(a.name)
                      )
                      ?.map((data: any, index: any) => {
                        const pathname =
                          '/catalog-category' +
                          (filter?.company ? `/${filter?.company}` : '') +
                          (filter?.type ? `/${filter?.type}` : '') +
                          (filter?.end ? `/${filter?.end}` : '') +
                          (filter?.tag ? `/${filter?.tag}` : '')
                        return (
                          <div className="cata-otp-item col col-3" key={index}>
                            <div className="cata-otp-wr">
                              <Link href={`${pathname}/${data?.url_key}`}>
                                <a className="cata-otp-img">
                                  <img
                                    src={data?.image_catalogue || '/image/logo/placeholder.svg'}
                                    alt={data?.catalogue_image}
                                  />
                                </a>
                              </Link>
                              <div className="cata-otp-content">
                                <Link href={`${pathname}/${data?.url_key}`}>
                                  <a className="cata-otp-name fw-7">{data?.name}</a>
                                </Link>
                                <div className="cata-otp-control">
                                  <div className="inner">
                                    <Link href={`${pathname}/${data?.url_key}`}>
                                      <a className="cata-otp-btn btn full">
                                        <span className="txt">XEM CHI TIẾT</span>
                                      </a>
                                    </Link>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        )
                      })}
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}
