/* eslint-disable @next/next/no-img-element */
'use client'
import { createPopper } from '@popperjs/core'
import { Empty } from 'antd'
import React, { useEffect } from 'react'
import { isMobile } from 'react-device-detect'

type Props = {
  dataTem?: ICatalogData | undefined
  sizeMarker?: number
  handleCloseInsert: () => void
  errorStt?: string
  isOpenInsert: boolean
  handleSearchForm: (e: any) => void
  hanndleChangeStt: (e: any) => void
  handleChecked: (e: any, item: any, index: number) => void
  handleAdd: () => void
  dataChoose?: ICatalogDataITems[]
  refCheckbox: any
  txtSearch: string
  dataPost?: ICatalogData[]
}

export const CatalogAdminForm: React.FC<Props> = ({
  sizeMarker,
  dataTem,
  handleCloseInsert,
  isOpenInsert,
  errorStt,
  handleSearchForm,
  hanndleChangeStt,
  handleChecked,
  handleAdd,
  dataChoose,
  refCheckbox,
  txtSearch,
}) => {
  useEffect(() => {
    const cataMarkerForm: any = document.querySelector('.cataMarkerForm')
    const cataSpForm: any = document.querySelector('.cataSpForm')
    createPopper(cataMarkerForm, cataSpForm, {
      placement: 'bottom-start',
      modifiers: [
        {
          name: 'offset',
          options: {
            offset: [-10, 10],
          },
        },
        { name: 'arrow' },
        {
          name: 'preventOverflow',
          options: {
            boundary: document.querySelector('body'), // Giới hạn hiển thị của popper
            padding: 10, // Khoảng cách tối thiểu từ popper đến biên của boundary
          },
        },
      ],
    })
  }, [dataTem?.posX, dataTem?.posY, isOpenInsert])
  return (
    <>
      <div
        className="catalog-marker cataMarkerForm"
        style={{
          minWidth: `${sizeMarker}px`,
          minHeight: `${sizeMarker}px`,
          left: `${dataTem?.posX}`,
          top: `${dataTem?.posY}`,
          fontSize: `${isMobile ? '1rem' : '1.6rem'}`,
        }}
      >
        <div className="number">
          <span>{dataTem?.position ?? 0}</span>
        </div>
        <div className="catalog-form cataSpForm">
          <div className="catalog-form-top">
            <span className="txt">Chọn sản phẩm</span>
            <div className="catalog-form-close" onClick={() => handleCloseInsert()}>
              <i className="fa-sharp fa-solid fa-xmark-large"></i>
            </div>
          </div>
          <div className="catalog-form-control">
            <div className="catalog-form-input">
              <input
                type="text"
                className="re-input"
                placeholder="Tìm kiếm"
                onChange={(e) => handleSearchForm(e)}
              />
            </div>
            <div className="catalog-form-input">
              <input
                type="number"
                className="re-input"
                placeholder="Nhập số thứ tự"
                onChange={(e) => hanndleChangeStt(e)}
                autoFocus={isOpenInsert}
              />
              {errorStt != '' && errorStt != 'Vui lòng chọn sản phẩm' && (
                <span className={`error`}>{errorStt}</span>
              )}
            </div>
          </div>
          <div className="catalog-form-body">
            {errorStt != '' && errorStt == 'Vui lòng chọn sản phẩm' && (
              <span className={`error ${errorStt == 'Vui lòng chọn sản phẩm' ? '--second' : ''}`}>
                {errorStt}
              </span>
            )}
            <div className="catalog-form-body-inner">
              {dataChoose?.length == 0 ? (
                <div className="catalog-form-empty">
                  {/* <div className="inner">
                    <img src={URL_IMG + 'empty.png'} alt="" />
                    <p className="txt">Không tìm thấy sản phẩm phù hợp</p>
                  </div> */}
                  <Empty description="Không tìm thấy sản phẩm phù hợp" />
                </div>
              ) : (
                <ul>
                  {dataChoose?.map((data: any, index: any) => {
                    return (
                      <li key={index}>
                        <label htmlFor={`check-${index}`} className="checkbox-row">
                          <input
                            type="checkbox"
                            name=""
                            id={`check-${index}`}
                            hidden
                            ref={refCheckbox}
                            onChange={(e) => handleChecked(e, data, index)}
                          />
                          <div className="checkbox"></div>
                          <div className="content">
                            <p className="name">{data?.name}</p>
                            <p className="code t12">SKU: {data?.sku}</p>
                          </div>
                        </label>
                      </li>
                    )
                  })}
                </ul>
              )}
            </div>
          </div>
          <div className="catalog-form-bot">
            <div className="btn --second" onClick={() => handleCloseInsert()}>
              <span>Hủy</span>
            </div>
            <div className="btn" onClick={() => handleAdd()}>
              <i className="fas fa-plus"></i>
              <span>Thêm</span>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
