/* eslint-disable @next/next/no-img-element */
import { createPopper } from '@popperjs/core'
import Link from 'next/link'
import React, { Fragment, useEffect } from 'react'
import { isMobile } from 'react-device-detect'
import { ButtonAddCartSecond } from '~src/components/common/ButtonAddCartSecond'
import { _format } from '~src/util'

type Props = {
  dataPost?: any
  handleDeleteTem?: any
  sizeMarker?: any
  handleHoverCircle?: any
  isLoadingDataCatalogDetail?: any
  isPermission?: boolean
}

export const CataLogSpAdmin: React.FC<Props> = ({
  dataPost,
  handleDeleteTem,
  sizeMarker,
  isLoadingDataCatalogDetail,
  isPermission,
}) => {
  useEffect(() => {
    const cataMarker: any = document.querySelectorAll('.cataMarker')
    const cataSp: any = document.querySelectorAll('.cataSp')
    cataMarker?.forEach((item: any, index: any) => {
      createPopper(item, cataSp[index], {
        placement: 'bottom-start',
        modifiers: [
          {
            name: 'offset',
            options: {
              offset: [-10, 10],
            },
          },
          { name: 'arrow' },
          {
            name: 'preventOverflow',
            options: {
              boundary: document.querySelector('body'), // Giới hạn hiển thị của popper
              padding: 10, // Khoảng cách tối thiểu từ popper đến biên của boundary
            },
          },
        ],
      })
    })
  }, [dataPost, isLoadingDataCatalogDetail])

  return (
    <Fragment>
      {dataPost?.length != 0 &&
        dataPost.map((data: any, index: any) => {
          return (
            data?.products.length > 0 && (
              <div
                className="catalog-marker cataMarker"
                style={{
                  top: data?.posY,
                  left: data?.posX,
                  minWidth: `${sizeMarker}px`,
                  minHeight: `${sizeMarker}px`,
                  fontSize: `${isMobile ? '1rem' : '1.6rem'}`,
                }}
                key={index}
              >
                <span className="number">{data?.position}</span>
                <div className={`catalog-sp cataSp`}>
                  <div className="prose-list">
                    {data?.products.map((item: any, i: any) => {
                      return (
                        <div className="prose-item" key={i}>
                          <div className="prose-wr">
                            <div className="prose-img">
                              {isPermission && (
                                <div
                                  className="prose-close"
                                  onClick={(e) => handleDeleteTem(e, index, i)}
                                >
                                  <i className="fas fa-times"></i>
                                </div>
                              )}

                              <Link
                                href={{
                                  pathname: `/product-detail/Hãng xe/${item?.name}`,
                                  query: {
                                    sku: item?.sku,
                                  },
                                }}
                              >
                                <a className="inner">
                                  <img src={item?.image?.url} alt={item?.name} />
                                </a>
                              </Link>
                            </div>
                            <div className="prose-content">
                              <Link
                                href={{
                                  pathname: `/product-detail/Hãng xe/${item?.name}`,
                                  query: {
                                    sku: item?.sku,
                                  },
                                }}
                              >
                                <a className="prose-name">{item?.name}</a>
                              </Link>
                              <div className="prose-price">
                                {item?.price_range?.maximum_price?.final_price?.value ==
                                item?.price_range?.maximum_price?.regular_price?.value ? (
                                  <span className="price">
                                    {_format.getVND(
                                      item?.price_range?.maximum_price?.final_price?.value ?? 0,
                                      'VNĐ'
                                    )}
                                  </span>
                                ) : (
                                  <>
                                    <span className="price">
                                      {_format.getVND(
                                        item?.price_range?.maximum_price?.final_price?.value ?? 0,
                                        'VNĐ'
                                      )}
                                    </span>
                                    <span className="price old">
                                      {_format.getVND(
                                        item?.price_range?.maximum_price?.regular_price?.value ?? 0,
                                        'VNĐ'
                                      )}
                                    </span>
                                  </>
                                )}
                              </div>
                              <p className="prose-code">
                                <span className="fw-6">SKU:</span>
                                {item?.sku}
                              </p>
                            </div>
                            {!isPermission && <ButtonAddCartSecond product={item} />}
                          </div>
                        </div>
                      )
                    })}
                  </div>
                </div>
              </div>
            )
          )
        })}
    </Fragment>
  )
}
