/* eslint-disable @next/next/no-img-element */
import { useRouter } from 'next/router'
import React, { useState } from 'react'
import ProductItem from '~src/components/global/Product/ProductItem'

type Props = {
  dataPost?: any
  isPermission?: boolean
}

export const CatalogAdminShow: React.FC<Props> = ({ dataPost, isPermission = false }) => {
  console.log('dataPost', dataPost)
  const router = useRouter()
  return (
    <div className="catalog-otp-show">
      <div className="containers">
        <div className="re-head">
          <h2 className="t-title">Sản phẩm trên Catalog</h2>
        </div>
        <div className="wrapper-product">
          <div className="product-list">
            {dataPost?.map((item: any) =>
              item?.products?.map((data: any) => (
                <ProductItem
                  position={item?.position}
                  product={data}
                  key={`${data?.sku}`}
                  router={router}
                  isPermission={isPermission}
                />
              ))
            )}
          </div>
        </div>
      </div>
    </div>
  )
}
