/* eslint-disable @next/next/no-img-element */
import Link from 'next/link'
import { useRouter } from 'next/router'
import React from 'react'
import { useQuery } from 'react-query'
import { GET_LIST_CATALOG } from '~src/api'
import { graphQLClient } from '~src/api/graphql'
import { ProductSkeleton } from '~src/components/ProductSkeleton'
import { PaginationCustom } from '~src/components/common/PaginationCustom'
import { BreadcrumbHead } from '~src/components/global/Layout/BreadcrumHead'

type Props = {}

export const CatalogPage: React.FC<Props> = ({}) => {
  const router = useRouter()
  const { data: dataListCataLog, isLoading: isLoadingGetListCatalog } = useQuery({
    queryKey: ['DATA_LIST_CATALOG'],
    queryFn: async () => {
      try {
        const data: any = await graphQLClient.request(GET_LIST_CATALOG, {
          filter: {},
          currentPage: router?.query?.pageSize || 1,
          pageSize: 10,
        })
        return data?.catalogues
      } catch (error: any) {
        console.log(error)
      }
    },
  })
  return (
    <>
      <BreadcrumbHead breadcrumbs={[{ breadcrumbName: 'Catalog', path: '/catalog' }]} />
      <section className="section-cata-otp">
        <div className="cata-otp">
          <div className="containers">
            <div className="re-head">
              <h1 className="t-title">Catalog</h1>
            </div>
            <div className="cata-otp-wrapper">
              {isLoadingGetListCatalog ? (
                <div className="cata-otp-list row">
                  {[...Array(4)].map((_, index) => (
                    <div className="cata-otp-item col col-3" key={index}>
                      <ProductSkeleton />
                    </div>
                  ))}
                </div>
              ) : (
                <>
                  <div className="cata-otp-list row">
                    {dataListCataLog?.items?.map((data: any, index: any) => {
                      return (
                        <div className="cata-otp-item col col-3" key={index}>
                          <div className="cata-otp-wr">
                            <Link href={`/catalog/${data?.url_key}`}>
                              <a className="cata-otp-img">
                                <img src={data?.catalogue_image} alt={data?.catalogue_image} />
                              </a>
                            </Link>
                            <div className="cata-otp-content">
                              <Link href={`/catalog/${data?.url_key}`}>
                                <a className="cata-otp-name fw-7">{data?.name}</a>
                              </Link>
                              <div className="cata-otp-control">
                                <div className="inner">
                                  <Link href={`/catalog/${data?.url_key}`}>
                                    <a className="cata-otp-btn btn full">
                                      <span className="txt">XEM CHI TIẾT</span>
                                    </a>
                                  </Link>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      )
                    })}
                    {/* <div className="re-pagination">
                <PaginationCT
                  currentPage={dataListCataLog?.page_info?.current_page}
                  pageSize={dataListCataLog?.page_info?.page_size}
                  total={dataListCataLog?.total_count}
                />
              </div> */}
                  </div>
                  <PaginationCustom
                    current={dataListCataLog?.page_info?.current_page}
                    total={dataListCataLog?.total_count}
                    pageSize={dataListCataLog?.page_info?.page_size}
                    onChange={(page: number) =>
                      router.push(
                        {
                          query: {
                            ...router.query,
                            currentPage: page,
                          },
                        },
                        undefined,
                        { scroll: false }
                      )
                    }
                  />
                </>
              )}
            </div>
          </div>
        </div>
      </section>
    </>
  )
}
