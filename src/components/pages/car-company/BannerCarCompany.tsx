/* eslint-disable @next/next/no-img-element */
import React from 'react'
import { BreadcrumbBanner } from '~src/components/global/Layout/BreadcrumbBanner'

type I = {
  title: string
}
export const BannerCarCompany: React.FC<I> = ({ title }) => {
  return (
    <div className="banner-promotion">
      <div className="img">
        <img src="/image/bg-product.png" alt="" />
      </div>
      <div className="img-position">
        <div className="img">
          <img src="/image/21.png" alt="" />
        </div>
      </div>
      <div className="content-inner !translate-x-[-50%] [&_ol]:justify-start container">
        <div className="breadcrum">
          <BreadcrumbBanner
            breadcrumbHead={false}
            breadcrumbs={[
              { breadcrumbName: 'Hãng xe', path: '/car-company' },
              { breadcrumbName: title, path: '' },
            ]}
          />
        </div>
        <div className="title">{title}</div>
      </div>
    </div>
  )
}
