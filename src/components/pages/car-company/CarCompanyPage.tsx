/** @format */

import React, { useState } from "react";
import { ProductList } from "../product/ProductList";
import { PaginationCustom } from "~src/components/common/PaginationCustom";
import { TitleProduct } from "~src/components/global/TitlePage/TitleProduct";
import { BannerCarCompany } from "./BannerCarCompany";
import { useRouter } from "next/router";
import { FormFilterProduct } from "~src/components/common/FormFilterProduct";

export const CarCompanyPage = () => {
  const router = useRouter();
  const [data, setData] = useState([
    {
      name: "Piston Carens 12 2.0L",
      img: "/image/image1.jpg",
      price_new: 500000,
      price_old: 16690000,
    },
    {
      name: "Piston Carens 12 2.0L",
      img: "/image/image1.jpg",
      price_new: 500000,
      price_old: 16690000,
    },
    {
      name: "Piston Carens 12 2.0L",
      img: "/image/image1.jpg",
      price_new: 500000,
      price_old: 16690000,
    },
    {
      name: "Piston Carens 12 2.0L",
      img: "/image/image1.jpg",
      price_new: 500000,
      price_old: 16690000,
    },
    {
      name: "Piston Carens 12 2.0L",
      img: "/image/image1.jpg",
      price_new: 500000,
      price_old: 16690000,
    },
    {
      name: "Piston Carens 12 2.0L",
      img: "/image/image1.jpg",
      price_new: 500000,
      price_old: 16690000,
    },
    {
      name: "Piston Carens 12 2.0L",
      img: "/image/image1.jpg",
      price_new: 500000,
      price_old: 16690000,
    },
    {
      name: "Piston Carens 12 2.0L",
      img: "/image/image1.jpg",
      price_new: 500000,
      price_old: 16690000,
    },
    {
      name: "Piston Carens 12 2.0L",
      img: "/image/image1.jpg",
      price_new: 500000,
      price_old: 16690000,
    },
    {
      name: "Piston Carens 12 2.0L",
      img: "/image/image1.jpg",
      price_new: 500000,
      price_old: 16690000,
    },
    {
      name: "Piston Carens 12 2.0L",
      img: "/image/image1.jpg",
      price_new: 500000,
      price_old: 16690000,
    },
    {
      name: "Piston Carens 12 2.0L",
      img: "/image/image1.jpg",
      price_new: 500000,
      price_old: 16690000,
    },
  ]);
  return (
    <div className='car-company'>
      <div className='banner-page'>
        <BannerCarCompany title='Hãng xe' />
      </div>
      <div className='containers'>
        <div className='title-promotion'>
          <TitleProduct />
        </div>
        <div className='content-inner-promotion'>
          <div className='filter-promotion'>
            {/* <FormFilterProduct router={router} /> */}
          </div>
          <div className='list-product'>
            <ProductList data={data} loading={false} />
          </div>
          <div className='pagination-promotion'>
            <PaginationCustom
              current={1}
              pageSize={20}
              total={100}
              onChange={(val: number) => console.log(val)}
            />
          </div>
        </div>
      </div>
    </div>
  );
};
