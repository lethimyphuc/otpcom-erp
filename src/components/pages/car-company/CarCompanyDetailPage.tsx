import { NextRouter } from 'next/router'
import React from 'react'
import Banner from '~src/components/global/Layout/mainLayouts/Banner'
import { BannerCarCompany } from './BannerCarCompany'

type CarCompanyDetailProps = {
  router: NextRouter
}
const CarCompanyDetail = (props: CarCompanyDetailProps) => {
  const { router } = props
  return (
    <div className="car-company">
      <div className="banner-page">
        <BannerCarCompany title={'Hãng xe'} />
        {/* <Banner title="Hãng xe" /> */}
      </div>

      {/* <p>Test</p> */}
    </div>
  )
}

export default CarCompanyDetail
