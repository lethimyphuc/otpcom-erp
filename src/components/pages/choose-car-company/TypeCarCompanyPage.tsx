/* eslint-disable @next/next/no-img-element */
/**
 * /* eslint-disable @next/next/no-img-element
 *
 * @format
 */

import React, { useState } from 'react'
import clsx from 'clsx'
import styles from './TypeCarCompany.module.scss'
import { BannerCarCompany } from '../car-company/BannerCarCompany'
import { ProductList } from '../product/ProductList'
import { PaginationCustom } from '~src/components/common/PaginationCustom'
import { BreadcrumbBanner } from '~src/components/global/Layout/BreadcrumbBanner'
import { NextRouter } from 'next/router'
import { useQuery } from 'react-query'
import {graphQLClient, graphQLClientGET} from '~src/api/graphql'
import { GET_PRODUCTS } from '~src/api/product'
import { GET_CATEGORIES } from '~src/api/categories'
import { Loading } from '~src/components/screen/status/Loading'

export interface ITypeCarCompanyPageProps {
  router: NextRouter
}

const categories = [
  {
    name: 'Tất cả',
  },
  {
    name: 'Phụ tùng điện',
  },
  {
    name: 'Phụ tùng máy',
  },
  {
    name: 'Phụ tùng thân vỏ',
  },
  {
    name: 'Khung dẫn',
  },
  {
    name: 'Phụ kiện',
  },
  {
    name: 'Khác',
  },
]

export default function TypeCarCompanyPage(props: ITypeCarCompanyPageProps) {
  const { router } = props

  // Danh sách nhóm phụ tùng
  const {
    data: dataSpareParts,
    isLoading: isLoadingDataSpareParts,
  }: {
    data?: TChildrenCategories[]
    isLoading?: boolean
  } = useQuery({
    queryKey: ['getSpareParts'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_CATEGORIES, {
        eq: 'Nw==',
      })) as TCategories
      return data.categories?.items[0]?.children
    },
  })

  const { data: dataProducts, isLoading: isLoadingDataProducts } = useQuery({
    queryKey: ['getProducts', router.query],
    queryFn: async () => {
      const convertFilterIn = [
        router.query?.company,
        router.query?.type,
        router.query?.end,
        router.query?.groupAccessary,
      ].filter(Boolean)
      const data = (await graphQLClientGET.request(GET_PRODUCTS, {
        filter: {
          category_uid: {
            in: convertFilterIn,
          },
        },
        search: !!router.query?.search ? `${router.query?.search}`.trim() : '',
        pageSize: !!router?.query?.pageSize ? parseInt(`${router.query.pageSize}`) : 20,
        currentPage: !!router?.query?.currentPage ? parseInt(`${router.query.currentPage}`) : 1,
      })) as {
        products: {
          items: TProduct[]
          page_info: {
            current_page: number
            page_size: number
            total_pages: number
          }
        }
      }
      return data?.products
    },
  })

  console.log('dataSpareParts: ', dataSpareParts)
  console.log('dataProducts: ', dataProducts)

  const handlerFilterProductByCategory = (categoryId: string) => {
    console.log('categoryId: ', categoryId)
    router.push({
      query: {
        ...router.query,
        groupAccessary: categoryId,
      },
    })
  }

  console.log('isLoadingDataProducts: ', isLoadingDataProducts)

  // if (!!isLoadingDataSpareParts && !!isLoadingDataProducts) {
  //   return <Loading />;
  // }
  return (
    <div className={clsx(styles.TypeCarContainer)}>
      {/* <div className="banner-page">
        <BannerCarCompany title="Hyundai Accent 2021" />
      </div> */}
      <div className={clsx(styles.titleSection)}>
        <img className={clsx(styles.union)} src="/image/hyundai-banner.png" alt="" />
        {/* <div className={clsx(styles.breadcrumb)}>
          <p>Trang chủ</p>
          <img src={"/icons/vector.svg"} alt="" />
          <p className="font-bold">Nhật ký</p>
        </div> */}
        <div className={clsx(styles.breadcrumb)}>
          <BreadcrumbBanner breadcrumbHead={false} />
        </div>
        <div className={clsx(styles.title)}>
          <h1>Hyundai Accent 2021</h1>
        </div>
        <img className={clsx(styles.hyundaiCar)} src="/image/hyundai-car.png" alt="" />
        <img className={clsx(styles.gear)} src="/image/212.png" alt="" />
      </div>
      <div className="containers">
        {/* <div className="title-promotion">
          <TitleProduct />
        </div> */}
        <div className={clsx(styles.categories)}>
          <div
            className={clsx(
              styles.categoryItem,
              !router.query?.groupAccessary || router.query?.groupAccessary === ''
                ? styles.categorySelected
                : ''
            )}
            onClick={() => handlerFilterProductByCategory('')}
          >
            Tất cả
          </div>
          {dataSpareParts?.map((item: TChildrenCategories, index: number) => (
            <>
              <div
                className={clsx(
                  styles.categoryItem,
                  router.query?.groupAccessary === item?.uid ? styles.categorySelected : ''
                )}
                key={`${item?.uid}`}
                onClick={() => handlerFilterProductByCategory(item?.uid)}
              >
                {item.name}
              </div>
            </>
          ))}
        </div>
        <div className="content-inner-promotion">
          {/* <div className="filter-promotion">
            <FormFilterProduct />
          </div> */}

          <div className="wrapper-product">
            <div className="list-product">
              {!!dataProducts?.items?.length ? (
                <ProductList data={dataProducts?.items || []} loading={isLoadingDataProducts} />
              ) : (
                <>{!!isLoadingDataProducts ? 'Loading...' : 'Không có sản phẩm'}</>
              )}
            </div>
          </div>

          {(dataProducts?.page_info?.total_pages as number) > 1 && (
            <div className={clsx(styles.paginationBar)}>
              <PaginationCustom
                current={!!router.query?.currentPage ? parseInt(`${router.query?.currentPage}`) : 1}
                pageSize={!!router?.query?.pageSize ? parseInt(`${router.query?.pageSize}`) : 20}
                total={
                  !!dataProducts?.page_info?.total_pages && !!dataProducts?.page_info?.page_size
                    ? parseInt(`${dataProducts?.page_info?.total_pages}`) *
                      parseInt(`${dataProducts?.page_info?.page_size}`)
                    : !!router?.query?.pageSize
                    ? parseInt(`${router.query?.pageSize}`)
                    : 20
                }
                onChange={(page: number) =>
                  router.push({
                    query: {
                      ...router.query,
                      currentPage: page,
                    },
                  })
                }
                totalPage={
                  !!dataProducts?.page_info?.total_pages
                    ? parseInt(`${dataProducts?.page_info?.total_pages}`)
                    : 1
                }
              />
            </div>
          )}
        </div>
      </div>
    </div>
  )
}
