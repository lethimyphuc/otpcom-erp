/* eslint-disable @next/next/no-img-element */
import { Radio, Rate, Skeleton } from 'antd'
import clsx from 'clsx'
import { NextRouter } from 'next/router'
import React, { Fragment, useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { toast } from 'react-toastify'
import { FreeMode, Navigation, Thumbs } from 'swiper/modules'
import { Swiper, SwiperClass, SwiperSlide } from 'swiper/react'
import { GET_CITY, GET_DISTRICT } from '~src/api/address'
import { graphQLClient, graphQLClientGET } from '~src/api/graphql'
import {
  ADD_PRODUCT_TO_CART,
  GET_PRODUCTS,
  GET_REVIEW_RATING_PRODUCT,
  GET_SHIPPING,
} from '~src/api/product'
import { Fancybox } from '~src/components/global'
import { ButtonMain } from '~src/components/global/Button/ButtonMain/ButtonMain'
import { FormSelect } from '~src/components/global/FormControls/FormSelect'
import { BreadcrumbHead } from '~src/components/global/Layout/BreadcrumHead'
import { _format } from '~src/util'
import { CatalogImage } from './components/Catalog'
import DetailItem from './components/DetailItem'
import ProductComment from './components/ProductComment'
import { ProductDetailedArticle } from './components/ProductDetailedArticle'
import ProductReview from './components/ProductReview'
import SaleProductSwiper from './components/SaleProductSwiper'
import styles from './ProductDetail.module.scss'

export interface IProductDetailPageProps {
  router: NextRouter
}

export default function ProductDetailPage(props: IProductDetailPageProps) {
  const { router } = props
  const { setValue, control, watch } = useForm<{
    city: number | null
    district: number | null
    ward: number | null
  }>({
    defaultValues: {
      city: null,
      district: null,
      ward: null,
    },
  })
  const queryClient = useQueryClient()
  const [tabIdSelected, setTabIdSelected] = useState(1)
  const [showImage, setShowImage] = useState('')
  const [swiper, setSwiper] = useState<any>(null)
  const [thumbsSwiper, setThumbsSwiper] = useState<SwiperClass | null>(null)

  const [quantity, setQuantity] = useState<number>(1)
  const [shipping, setShipping] = useState<{
    district: string
    city: string
  }>()
  const tabs = [
    {
      id: 1,
      name: 'Chi tiết sản phẩm',
    },
    {
      id: 2,
      name: 'Bài viết chi tiết',
    },
    {
      id: 3,
      name: 'Đánh giá sản phẩm',
    },
    {
      id: 4,
      name: 'Bình luận',
    },
  ]

  const { data: dataRatingProduct, isLoading: isLoadingDataRatingProduct } = useQuery({
    queryKey: ['getRatingProduct'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_REVIEW_RATING_PRODUCT)) as {
        productReviewRatingsMetadata: {
          items: TReviewRating[]
        }
      }
      return data?.productReviewRatingsMetadata
    },
  })

  const {
    data: dataProductDetail,
    isLoading: isLoadingDataProductDetail,
    refetch: fetchingDataProductDetail,
  } = useQuery({
    queryKey: ['getProductDetail', router.query],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_PRODUCTS, {
        filter: {
          sku: {
            eq: router.query?.sku,
          },
        },
      })) as {
        products: {
          items: TProduct[]
          page_info: {
            current_page: number
            page_size: number
            total_pages: number
          }
        }
      }
      setShowImage(
        !!data?.products?.items[0]?.media_gallery &&
          !!data?.products?.items[0]?.media_gallery[0]?.url
          ? data?.products?.items[0]?.media_gallery[0]?.url
          : ''
      )
      return data?.products?.items[0]
    },
  })

  const { data: dataCity } = useQuery({
    queryKey: ['getCity'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_CITY, {
        locationType: 'CITY',
      })) as {
        Locations: {
          items: TCity[]
        }
      }
      return data?.Locations?.items
    },
  })

  const { data: dataDistrict } = useQuery({
    queryKey: ['getDistrict', watch('city')],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_DISTRICT, {
        parentId: !!watch('city') ? parseInt(`${watch('city')}`) : 0,
        locationType: 'DISTRICT',
      })) as {
        Locations: {
          items: TCity[]
        }
      }
      return data?.Locations?.items
    },
    enabled: !!watch('city'),
  })

  const handleNext = () => {
    swiper.slideNext()
  }

  const handlePrev = () => {
    swiper.slidePrev()
  }
  const config = {
    spaceBetween: 10,
    slidesPerView: 4,
    onSwiper: (s: any) => {
      setSwiper(s)
    },
    loop: true,
  }

  useEffect(() => {
    if (!!dataProductDetail?.daily_sale?.end_date) {
      const zeroFill = (n: any) => {
        return n < 10 ? ('0' + n).slice(-2) : n
      }

      let timeId = setInterval(() => {
        const now = new Date().getTime()
        const dateEnd = !!dataProductDetail?.daily_sale?.end_date
          ? new Date(dataProductDetail?.daily_sale?.end_date).getTime()
          : 0
        let timeLeft = dateEnd - now
        if (timeLeft > 0) {
          const days = Math.floor(timeLeft / (1000 * 60 * 60 * 24))
          const hours = Math.floor((timeLeft % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))
          const minutes = Math.floor((timeLeft % (1000 * 60 * 60)) / (1000 * 60))
          const seconds = Math.floor((timeLeft % (1000 * 60)) / 1000)
          const getElementDays = document.querySelector('#days')
          const getElementHours = document.querySelector('#hours')
          const getElementMinutes = document.querySelector('#minutes')
          const getElementSeconds = document.querySelector('#seconds')

          if (!!getElementDays) {
            getElementDays.innerHTML = zeroFill(`${days}`)
          }
          if (!!getElementHours) {
            getElementHours.innerHTML = zeroFill(`${hours}`)
          }
          if (!!getElementMinutes) {
            getElementMinutes.innerHTML = zeroFill(`${minutes}`)
          }
          if (!!getElementSeconds) {
            getElementSeconds.innerHTML = zeroFill(`${seconds}`)
          }
        }
      }, 1000)
      return () => {
        clearInterval(timeId)
      }
    }
  }, [dataProductDetail?.daily_sale?.end_date])

  const {
    isLoading: isLoadingDataAddToCart,
    mutate: mutateAddToCart,
    variables,
  } = useMutation({
    mutationFn: async (data: { data: TAddToCart[]; gotoCart: boolean }) => {
      return (await graphQLClient.request(ADD_PRODUCT_TO_CART, {
        cartId: localStorage?.getItem('cartId'),
        cartItems: data.data,
      })) as { addProductsToCart: TDataAddToCart }
    },
    onSuccess: async (
      response: { addProductsToCart: TDataAddToCart },
      variables: { data: TAddToCart[]; gotoCart: boolean }
    ) => {
      await queryClient.refetchQueries(['getCartId'])
      if (!!response?.addProductsToCart?.user_errors?.length) {
        toast.error('Sản phẩm đã hết hàng')
        return
      } else {
        if (!!variables?.gotoCart) {
          router.push('/shopping-cart')
        } else {
          queryClient.setQueryData<boolean>('miniCartVisible', true)
        }
      }
    },
  })

  const handleAddToCart = async (event: any, gotoCart?: boolean) => {
    event?.stopPropagation()
    if (quantity <= 0) {
      toast.error('Vui lòng nhập số lượng hợp lệ')
      return
    }

    const DATA_SUBMIT: TAddToCart[] = [
      {
        sku: dataProductDetail?.sku || '',
        quantity: quantity,
      },
    ]
    await mutateAddToCart({
      data: DATA_SUBMIT,
      gotoCart: gotoCart || false,
    })
  }

  const handleGoToShoppingCart = (event: any) => {
    event?.stopPropagation()
    handleAddToCart(event, true)
  }

  const { data: dataShipping, isLoading: isLoadingDataShipping } = useQuery({
    queryKey: ['getShipping', watch('district')],
    queryFn: async () => {
      const data: any = await graphQLClient.request(GET_SHIPPING, {
        sku: dataProductDetail?.sku,
        toCity: shipping?.city,
        toDistrict: shipping?.district,
        shippingType: 'product',
      })
      return data?.ShippingFee
    },
    onSuccess: (data) => {
      if (!data) {
        toast.error('Không lấy được phí vận chuyển')
      }
    },
    enabled: !!watch('city') && !!watch('district'),
  })

  return (
    <div className={clsx(styles.billContainer, 'wrapper-product-detail')}>
      <BreadcrumbHead
        breadcrumbs={[{ breadcrumbName: dataProductDetail?.name || 'Chi tiết sản phẩm', path: '' }]}
      />
      <div className="containers">
        <div className={clsx(styles.productDetailSection)}>
          <div className={clsx(styles.leftSection)}>
            <div className={clsx(styles.itemImage)}>
              {!!isLoadingDataProductDetail || !showImage ? (
                <Skeleton.Input
                  size={'large'}
                  block={true}
                  style={{ height: '400px', borderRadius: '20px' }}
                />
              ) : (
                <Fancybox>
                  <Swiper
                    thumbs={{
                      swiper: thumbsSwiper && !thumbsSwiper.destroyed ? thumbsSwiper : null,
                    }}
                    modules={[FreeMode, Thumbs, Navigation]}
                    className="rounded-[24px]"
                  >
                    {dataProductDetail?.media_gallery?.map((item: any) => {
                      return (
                        <SwiperSlide key={item?.url}>
                          <a href={item?.url} data-fancybox="gallery">
                            <img src={item?.url} alt="" className="w-full h-full" />
                          </a>
                        </SwiperSlide>
                      )
                    })}
                  </Swiper>
                  {!!dataProductDetail?.price_tiers?.discount?.percent_off ? (
                    <div className={clsx(styles.hot)}>
                      <div className={clsx(styles.bg)}>
                        <img src="/icons/Star-red.svg" alt="" />
                      </div>
                      <div className={clsx(styles.text)}>
                        -{dataProductDetail?.price_tiers?.discount?.percent_off}%{/* -23% */}
                      </div>
                    </div>
                  ) : (
                    <></>
                  )}
                </Fancybox>
              )}
            </div>

            <div className={clsx(styles.swiperContainer)}>
              <div className={clsx(styles.iconSwiper)} onClick={handlePrev}>
                <img src="/icons/prev.svg" alt="" />
              </div>
              <div className={clsx(styles.listImage)}>
                <Swiper
                  {...config}
                  modules={[Thumbs, FreeMode]}
                  onSwiper={setThumbsSwiper}
                  freeMode={true}
                >
                  {!!isLoadingDataProductDetail ? (
                    <>
                      <div className="flex gap-3">
                        <Skeleton.Avatar
                          shape="square"
                          size={'large'}
                          style={{
                            borderRadius: '5px',
                            width: '60px',
                            height: '60px',
                          }}
                        />
                        <Skeleton.Avatar
                          shape="square"
                          size={'large'}
                          style={{
                            borderRadius: '5px',
                            width: '60px',
                            height: '60px',
                          }}
                        />
                        <Skeleton.Avatar
                          shape="square"
                          size={'large'}
                          style={{
                            borderRadius: '5px',
                            width: '60px',
                            height: '60px',
                          }}
                        />
                        <Skeleton.Avatar
                          shape="square"
                          size={'large'}
                          style={{
                            borderRadius: '5px',
                            width: '60px',
                            height: '60px',
                          }}
                        />
                      </div>
                    </>
                  ) : (
                    <>
                      {dataProductDetail?.media_gallery?.map(
                        (item: { url?: string }, index: number) => (
                          <SwiperSlide key={index}>
                            <div
                              className={clsx(
                                styles.imageItem,
                                item?.url === showImage ? styles.selectedImage : ''
                              )}
                              onClick={() => setShowImage(!!item?.url ? item?.url : '')}
                            >
                              <img src={item?.url} alt="" />
                            </div>
                          </SwiperSlide>
                        )
                      )}
                    </>
                  )}
                </Swiper>
              </div>
              <div className={clsx(styles.iconSwiper)} onClick={handleNext}>
                <img src="/icons/next.svg" alt="" />
              </div>
            </div>
          </div>
          <div className={clsx(styles.rightSection)}>
            {!!isLoadingDataProductDetail ? (
              <>
                <div className="grid gap-3">
                  <Skeleton.Input size={'large'} block={true} />
                  <Skeleton.Input size={'large'} block={true} />
                </div>
              </>
            ) : (
              <>
                {!!dataProductDetail?.daily_sale ? (
                  <div className={`${clsx(styles.flashSale)} flex`}>
                    <p>FLASH SALE</p>
                    <div className={clsx(styles.countdown)}>
                      <p>Kết thúc trong</p>

                      <p>
                        <span id="days" className={clsx(styles.time, styles.hour)}>
                          00
                        </span>
                        :
                        <span id="hours" className={clsx(styles.time, styles.hour)}>
                          00
                        </span>
                        :
                        <span id="minutes" className={clsx(styles.time, styles.minutes)}>
                          00
                        </span>
                        :
                        <span id="seconds" className={clsx(styles.time, styles.second)}>
                          00
                        </span>
                      </p>
                    </div>
                  </div>
                ) : (
                  <></>
                )}
                <h1 className="!text-[28px] sm:!text-[32px]">
                  {dataProductDetail?.name || 'Đang cập nhật'}
                </h1>
              </>
            )}

            <div className={clsx(styles.boxInfo)}>
              <div className={clsx(styles?.infoProduct)}>
                {!!isLoadingDataProductDetail ? (
                  <>
                    <div className="grid gap-3">
                      <Skeleton.Input size={'large'} block={true} />
                      <Skeleton.Input size={'large'} block={true} />
                      <Skeleton.Input size={'large'} block={true} />
                      <Skeleton.Input size={'large'} block={true} />
                    </div>
                  </>
                ) : (
                  <>
                    <div className={clsx(styles.itemCode)}>
                      <p>Mã sản phẩm:</p>
                      <p>{dataProductDetail?.sku || 'Đang cập nhật'}</p>
                    </div>
                    <div className={clsx(styles.itemRate)}>
                      <span className={clsx(styles.rate)}>
                        {!!dataProductDetail?.rating_summary
                          ? (dataProductDetail?.rating_summary * 5) / 100
                          : 5}
                      </span>
                      <Rate
                        disabled
                        allowHalf
                        value={
                          !!dataProductDetail?.rating_summary
                            ? (dataProductDetail?.rating_summary * 5) / 100
                            : 5
                        }
                        className={clsx(styles.rating_star)}
                      />
                      <p className={clsx(styles.stroke)}>|</p>
                      <p className={clsx(styles.rateNum)}>
                        <span>{dataProductDetail?.review_count}</span> đánh giá
                      </p>
                    </div>
                    <div className={clsx(styles.itemPrice)}>
                      <p>
                        {!!dataProductDetail?.daily_sale &&
                        !!dataProductDetail?.daily_sale?.end_date &&
                        !!dataProductDetail?.daily_sale?.sale_price &&
                        dataProductDetail?.daily_sale?.sold_qty != undefined &&
                        dataProductDetail?.daily_sale?.sale_qty != undefined &&
                        new Date(dataProductDetail?.daily_sale?.end_date) >= new Date() &&
                        dataProductDetail?.daily_sale?.sold_qty <
                          dataProductDetail?.daily_sale?.sale_qty
                          ? _format.getVND(dataProductDetail?.daily_sale?.sale_price, ' đ')
                          : _format.getVND(
                              dataProductDetail?.price_range?.minimum_price?.final_price?.value ||
                                0,
                              dataProductDetail?.price_range?.minimum_price?.final_price
                                ?.currency || ' VNĐ'
                            )}
                      </p>
                      {(!!dataProductDetail?.daily_sale &&
                      !!dataProductDetail?.daily_sale?.end_date &&
                      !!dataProductDetail?.daily_sale?.sale_price &&
                      dataProductDetail?.daily_sale?.sold_qty != undefined &&
                      dataProductDetail?.daily_sale?.sale_qty != undefined &&
                      new Date(dataProductDetail?.daily_sale?.end_date) >= new Date() &&
                      dataProductDetail?.daily_sale?.sold_qty <
                        dataProductDetail?.daily_sale?.sale_qty
                        ? dataProductDetail?.daily_sale?.sale_price
                        : dataProductDetail?.price_range?.minimum_price?.final_price?.value) !=
                        dataProductDetail?.price_range?.maximum_price?.regular_price?.value && (
                        <p className={clsx(styles.oldPrice)}>
                          {_format.getVND(
                            dataProductDetail?.price_range?.maximum_price?.regular_price?.value ||
                              0,
                            dataProductDetail?.price_range?.maximum_price?.regular_price
                              ?.currency || ' VND'
                          )}
                        </p>
                      )}

                      <p className={'text-[#7F8080] text-[14px] mt-0.5 line-through'}>
                        {_format.getVND(
                          dataProductDetail?.price_range?.maximum_price?.regular_price?.value || 0,
                          dataProductDetail?.price_range?.maximum_price?.regular_price?.currency ||
                            ' VND'
                        )}
                      </p>
                    </div>
                  </>
                )}

                {!!isLoadingDataProductDetail ? (
                  <>
                    <div className="flex gap-3 mt-3">
                      <Skeleton.Avatar
                        shape="square"
                        size={'large'}
                        style={{
                          borderRadius: '5px',
                          width: '60px',
                          height: '60px',
                        }}
                      />
                      <Skeleton.Avatar
                        shape="square"
                        size={'large'}
                        style={{
                          borderRadius: '5px',
                          width: '60px',
                          height: '60px',
                        }}
                      />
                      <Skeleton.Avatar
                        shape="square"
                        size={'large'}
                        style={{
                          borderRadius: '5px',
                          width: '60px',
                          height: '60px',
                        }}
                      />
                    </div>
                  </>
                ) : (
                  <div className={clsx(styles.related)}>
                    <ItemPrice
                      handleSellectPrice={() => {}}
                      item={{
                        sort_name: dataProductDetail?.sort_name,
                        daily_sale: dataProductDetail?.daily_sale,
                        price_range: dataProductDetail?.price_range,
                      }}
                      isChecked={true}
                    />
                    {dataProductDetail?.related_products?.map((item: any) => {
                      const handleSellectPrice = () => {
                        router.push({
                          query: {
                            ...router.query,
                            sku: item?.sku,
                          },
                        })
                      }
                      return (
                        <Fragment key={item?.sku}>
                          <ItemPrice
                            handleSellectPrice={handleSellectPrice}
                            item={item}
                            isChecked={item?.sku == router?.query?.sku}
                          />
                        </Fragment>
                      )
                    })}
                  </div>
                )}
                {!!isLoadingDataProductDetail ? (
                  <>
                    <div className="flex gap-3 mt-3">
                      <Skeleton.Input size={'large'} />
                      <Skeleton.Input size={'large'} />
                    </div>
                  </>
                ) : (
                  <>
                    <a href="tel:0123456789" className={clsx(styles.contact)}>
                      <img src="/icons/phone-detail-product.svg" alt="" />
                      <p>Liên hệ để nhận giá khuyến mãi</p>
                    </a>
                    <div className={clsx(styles.itemQuantity)}>
                      <p>Số lượng:</p>
                      <div className={clsx(styles.quantity)}>
                        <button
                          onClick={() => {
                            quantity > 1 && setQuantity((prev) => prev - 1)
                          }}
                        >
                          <img src={'/icons/decrease.svg'} alt="" />
                        </button>
                        <input
                          type="text"
                          value={quantity}
                          onChange={(e) =>
                            setQuantity(isNaN(+e.target.value) ? 1 : +e.target.value)
                          }
                        />
                        <button onClick={() => setQuantity((prev) => prev + 1)}>
                          <img src={'/icons/increase.svg'} alt="" />
                        </button>
                      </div>
                    </div>
                  </>
                )}
                {!!isLoadingDataProductDetail ? (
                  <>
                    <div className="flex gap-3 mt-3">
                      <Skeleton.Button size={'large'} style={{ borderRadius: '20px' }} />
                      <Skeleton.Button size={'large'} style={{ borderRadius: '20px' }} />
                    </div>
                  </>
                ) : (
                  <div className={clsx(styles.btnContainer)}>
                    <ButtonMain
                      type="button"
                      background="transparent"
                      className="!bg-white text-black"
                      onClick={handleAddToCart}
                      loading={isLoadingDataAddToCart && !variables?.gotoCart}
                    >
                      Thêm vào giỏ
                    </ButtonMain>
                    <ButtonMain
                      type="button"
                      background="green"
                      loading={isLoadingDataAddToCart && !!variables?.gotoCart}
                      onClick={handleGoToShoppingCart}
                    >
                      Mua ngay
                    </ButtonMain>
                  </div>
                )}
              </div>
              {!!isLoadingDataProductDetail ? (
                <>
                  <Skeleton.Input
                    size={'large'}
                    block={true}
                    style={{ height: '200px', borderRadius: '20px' }}
                  />
                </>
              ) : (
                <>
                  <div className={clsx(styles.deliveryBox)}>
                    <p>Vận chuyển tới:</p>
                    <div className={clsx(styles.selectBox)}>
                      <FormSelect
                        className="dropdown-delivery"
                        control={control}
                        name={'city'}
                        optionList={dataCity?.map((city: TCity) => {
                          return {
                            value: city?.id,
                            label: city?.name,
                          }
                        })}
                        label=""
                        placeholder="Tỉnh / Thành phố"
                        onChange={() => {
                          setValue('district', null)
                        }}
                      />
                      <FormSelect
                        className="dropdown-delivery"
                        control={control}
                        name={'district'}
                        optionList={dataDistrict?.map((district: TCity) => {
                          return {
                            value: district?.id,
                            label: district?.name,
                          }
                        })}
                        label=""
                        placeholder="Quận / Huyện"
                        onChange={(val) => {
                          const district: any = dataDistrict?.find(
                            (district: TCity) => district?.id == val
                          )?.name
                          const city: any = dataCity?.find(
                            (district: TCity) => district?.id == watch('city')
                          )?.name
                          const dataAddressShipping: any = {
                            district: district,
                            city: city,
                          }
                          setShipping(dataAddressShipping)
                        }}
                      />
                    </div>
                    <div className={clsx(styles.shippingPrice)}>
                      <p>Phí vận chuyển</p>
                      {isLoadingDataShipping ? (
                        <>
                          <div className="h-[30px]">
                            <Skeleton.Input size={'large'} block={true} active />
                          </div>
                        </>
                      ) : (
                        <>
                          <div className="grid gap-2 text-left">
                            {dataShipping?.items?.map((item: any) => (
                              <Fragment key={item?.carrierId}>
                                <div className="flex items-start justify-between">
                                  <div className="grid">
                                    <div className="flex items-center gap-2">
                                      <img src={item?.logo} alt="" width={40} height={40} />
                                      <span>{item?.carrierName}</span>
                                    </div>
                                    <span className="text-[12px]">
                                      {`( ${item?.serviceDescription} )`}
                                    </span>
                                  </div>
                                  <div className="grid text-right">
                                    <span>{_format.getVND(item?.shipFee, 'đ')}</span>
                                    <span className="text-[12px] text-green">
                                      {item?.serviceName}
                                    </span>
                                  </div>
                                </div>
                              </Fragment>
                            ))}
                          </div>
                        </>
                      )}
                    </div>
                  </div>
                </>
              )}
            </div>
          </div>
        </div>
        {dataProductDetail?.catalogues && (
          <div className={clsx(styles.mainSection)}>
            <CatalogImage url_key={dataProductDetail?.catalogues} />
          </div>
        )}
        <div className={clsx(styles.mainSection)}>
          <div className={clsx(styles.tabTile)}>
            <ul>
              {tabs.map((item) => (
                <li
                  key={item.id}
                  className={clsx(`${item.id === tabIdSelected ? styles.active : ''}`)}
                  onClick={() => setTabIdSelected(item.id)}
                >
                  {!!isLoadingDataProductDetail ? (
                    <Skeleton.Input size={'large'} block={true} />
                  ) : (
                    <>
                      <span> {item.name}</span>
                    </>
                  )}
                </li>
              ))}
            </ul>
          </div>
          {!!isLoadingDataProductDetail ? (
            <Skeleton.Input
              size={'large'}
              block={true}
              style={{ height: '400px', borderRadius: '20px' }}
            />
          ) : (
            <div className={clsx(styles.mainContent)}>
              {tabIdSelected === 1 && <DetailItem dataProductDetail={dataProductDetail} />}
              {tabIdSelected === 2 && (
                <ProductDetailedArticle dataProductDetail={dataProductDetail} />
              )}
              {tabIdSelected === 3 && (
                <ProductReview
                  dataProductDetail={dataProductDetail}
                  dataRatingProduct={dataRatingProduct?.items}
                />
              )}

              {tabIdSelected === 4 && (
                <ProductComment
                  dataProductDetail={dataProductDetail}
                  dataRatingProduct={dataRatingProduct?.items}
                  fetchingDataProductDetail={fetchingDataProductDetail}
                />
              )}
            </div>
          )}
        </div>
        {!!isLoadingDataProductDetail ? (
          <></>
        ) : (
          <div className={clsx(styles.promotionTitle)}>
            <img src={'/icons/half-gear.svg'} alt="" />
            <h2>SẢN PHẨM KHUYẾN MÃI</h2>
          </div>
        )}
      </div>

      {!!isLoadingDataProductDetail ? <></> : <SaleProductSwiper />}
    </div>
  )
}

type TProps = {
  handleSellectPrice: () => void
  item: any
  isChecked: boolean
}
export const ItemPrice: React.FC<TProps> = ({ handleSellectPrice, item, isChecked }) => {
  return (
    <button onClick={handleSellectPrice} type="button" className={clsx(styles.item_related)}>
      <div
        className={clsx(
          styles.item_related_detail,
          !!isChecked && 'bg-[#f4f2f2]',
          'relative h-fit'
        )}
      >
        <div className="flex items-start">
          <Radio checked={isChecked} className="absolute top-[0.4rem]" />
          <p className="text-[12px] ml-8">{item?.sort_name}</p>
        </div>
        <p className="text-red">
          {!!item?.daily_sale &&
          !!item?.daily_sale?.end_date &&
          !!item?.daily_sale?.sale_price &&
          item?.daily_sale?.sold_qty != undefined &&
          item?.daily_sale?.sale_qty != undefined &&
          new Date(item?.daily_sale?.end_date) >= new Date() &&
          item?.daily_sale?.sold_qty < item?.daily_sale?.sale_qty
            ? _format.getVND(item?.daily_sale?.sale_price, item?.daily_sale?.currency || ' VNĐ')
            : _format.getVND(
                item?.price_range?.minimum_price?.final_price?.value || 0,
                item?.price_range?.minimum_price?.final_price?.currency || ' VNĐ'
              )}
        </p>
      </div>
    </button>
  )
}
