import parse from 'html-react-parser'

type TProps = {
  dataProductDetail?: TProduct
}

export const ProductDetailedArticle: React.FC<TProps> = ({ dataProductDetail }) => {
  if (!dataProductDetail?.description?.html) {
    return (
      <div className="w-full text-center">
        <p>Chưa có bài viết</p>
      </div>
    )
  }
  return <div className="resetTable">{parse(dataProductDetail?.description?.html || '')}</div>
}
