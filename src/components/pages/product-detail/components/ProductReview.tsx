/* eslint-disable @next/next/no-img-element */
import React, { Fragment, useState } from 'react'
import clsx from 'clsx'
import styles from '../ProductDetail.module.scss'
import { PaginationCustom } from '~src/components/common/PaginationCustom'
import { Rate, Switch, UploadFile } from 'antd'
import FormReview from '~src/components/global/Product/FormReview'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { graphQLClient } from '~src/api/graphql'
import { INFO_CUSTOMER } from '~src/api'
import { useForm } from 'react-hook-form'
import { useRouter } from 'next/router'
import { format } from 'date-fns'
import { CREATE_PRODUCT_REVIEW } from '~src/api/product'
import { toast } from 'react-toastify'

export interface IProductReviewProps {
  dataProductDetail?: TProduct
  dataRatingProduct?: TReviewRating[]
}

export default function ProductReview(props: IProductReviewProps) {
  const { dataProductDetail, dataRatingProduct } = props
  const {
    setValue,
    control,
    handleSubmit,
    watch,
    reset,
    formState: { errors },
  } = useForm({
    defaultValues: {
      nickname: '',
      email: '',
      ratings: 0,
      text: '',
      anonymous: false,
      images: [],
      sku: '',
      summary: '',
    },
  })
  const router = useRouter()
  const [openFormReview, setOpenFormReview] = useState(false)
  const queryClient = useQueryClient()
  const [fileList, setFileList] = useState<UploadFile[]>([])
  const [isHideName, setIsHideName] = useState<boolean>(false)
  const {
    data: dataInfoCustomer,
    isLoading: isLoadingDataInfoCustomer,
    refetch: refetchDataInfoCustomer,
  } = useQuery({
    queryKey: ['getInfoCustomer'],
    queryFn: async () => {
      const data = await graphQLClient.request(INFO_CUSTOMER)
      console.log('DATA USER: ', data)
      return data
    },
    cacheTime: Infinity,
    enabled: false,
    onSuccess: (response: any) => {
      if (!!response?.customer) {
        setOpenFormReview(true)
      } else {
        router.push('/login')
      }
    },
  })

  const { isLoading: isLoadingCreateProductReview, mutate: mutateCreateProductReview } =
    useMutation({
      mutationFn: async (data: {
        nickname: string
        email: string
        ratings: any
        text: string
        anonymous: boolean
        images: any
        sku: string
        summary: string
      }) => {
        return (await graphQLClient.request(CREATE_PRODUCT_REVIEW, {
          input: data,
        })) as { createProductReview: { review: TReviewProduct } }
      },
      onSuccess: (response: { createProductReview: { review: TReviewProduct } }) => {
        toast('Đánh gía của bạn đã được gửi thành công và chờ được xét duyệt ', {
          type: 'success',
        })
        setOpenFormReview(false)
        reset()
        setFileList([])
        setIsHideName(false)
        queryClient.setQueryData(['getProductDetail', router.query], (oldData: any) => {
          if (!!oldData) {
            return {
              ...oldData,
              reviews: response.createProductReview?.review?.product?.reviews,
            }
          } else {
            return oldData
          }
        })
      },
    })

  const handleCheckLogin = () => {
    refetchDataInfoCustomer()
  }

  const handleSelectRating = (rate: number) => {
    handleCheckLogin()
    setValue('ratings', rate)
  }

  const onSubmit = (data: {
    nickname: string
    email: string
    ratings: any
    text: string
    anonymous: boolean
    images: any
    sku: string
    summary: string
  }) => {
    const convertRatings = Object.keys(data?.ratings).map((key: string) => {
      const valueRating = data?.ratings[key]
      const getIDRating = dataRatingProduct?.find((rating) => rating?.id === key)
      const getValueID = getIDRating?.values?.find(
        (value) => parseInt(value?.value) === parseInt(valueRating)
      )
      return {
        id: getIDRating?.id,
        value_id: getValueID?.value_id,
      }
    })

    const listPicture = fileList?.map((item) => {
      return {
        name: item?.name,
        base64_encoded_file: item?.thumbUrl,
      }
    })

    const DATA_SUBMIT: any = {
      nickname: !!`${data?.nickname}`.trim() && !!isHideName ? `${data?.nickname}`.trim() : '',
      text: !!`${data?.text}`.trim() ? `${data?.text}` : '',
      ratings: convertRatings,
      summary: !!`${data?.summary}`.trim() ? `${data?.summary}`.trim() : '',
      sku: dataProductDetail?.sku,
      images: listPicture,
      incognito: isHideName,
    }
    mutateCreateProductReview(DATA_SUBMIT)
  }

  return (
    <>
      {openFormReview || dataProductDetail?.reviews?.items?.length == 0 ? (
        <form onSubmit={handleSubmit(onSubmit)}>
          <FormReview
            control={control}
            setValue={setValue}
            watch={watch}
            errors={errors}
            dataRatingProduct={dataRatingProduct}
            isLoadingCreateProductReview={isLoadingCreateProductReview}
            fileList={fileList}
            setFileList={setFileList}
            setIsHideName={setIsHideName}
            dataReviews={dataProductDetail?.reviews?.items}
          />
        </form>
      ) : (
        <>
          <div className={clsx(styles.productReviewContainer)}>
            <div className={clsx(styles.flexItemRate)}>
              <div className={clsx(styles.leftRate)}>
                <div className={clsx(styles.leftRateNumber)}>
                  <span>{((dataProductDetail?.rating_summary as any) * 5) / 100}</span>
                  <span>/5</span>
                </div>
                <div className="">
                  <Rate
                    allowHalf
                    defaultValue={((dataProductDetail?.rating_summary as any) * 5) / 100}
                    className="!text-[1.2rem]"
                    disabled={true}
                  />
                </div>
              </div>
              <div className={clsx(styles.rightRate)}>
                <div className={clsx(styles.filterRate)}>
                  <button className={clsx(styles.active)}>Tất cả</button>
                  <button
                    type="button"
                    onClick={(event) => {
                      event.stopPropagation()
                      handleSelectRating(5)
                    }}
                  >
                    5 sao
                  </button>
                  <button
                    type="button"
                    onClick={(event) => {
                      event.stopPropagation()
                      handleSelectRating(4)
                    }}
                  >
                    4 sao
                  </button>
                  <button
                    type="button"
                    onClick={(event) => {
                      event.stopPropagation()
                      handleSelectRating(3)
                    }}
                  >
                    3 sao
                  </button>
                  <button
                    type="button"
                    onClick={(event) => {
                      event.stopPropagation()
                      handleSelectRating(2)
                    }}
                  >
                    2 sao
                  </button>
                  <button
                    type="button"
                    onClick={(event) => {
                      event.stopPropagation()
                      handleSelectRating(1)
                    }}
                  >
                    1 sao
                  </button>
                </div>
                <div className={clsx(styles.question)}>
                  <p>Bạn đã mua sản phẩm này?</p>
                  <span className="cursor-pointer" onClick={handleCheckLogin}>
                    Để lại đánh giá
                  </span>
                </div>
              </div>
            </div>
            <div className={clsx(styles.listReview)}>
              {dataProductDetail?.reviews?.items?.map((review, index) => {
                const rate = (review?.average_rating * 5) / 100
                return (
                  <>
                    <div className={clsx(styles.reviewItem)}>
                      <div className={clsx(styles.avatar)}>
                        <img
                          src={review?.customer_avatar || '/image/avatar-default.jpg'}
                          alt=""
                          className="rounded-full"
                        />
                      </div>
                      <div className={clsx(styles.reviewMain)}>
                        <div className={clsx(styles.reviewAccount)}>
                          <div>
                            <p>{review?.nickname}</p>
                            <Rate
                              allowHalf
                              defaultValue={rate}
                              className="text-[1.4rem]"
                              disabled={true}
                            />
                          </div>
                          <p>
                            {!!review?.created_at
                              ? format(new Date(review?.created_at), 'dd/MM/yyyy')
                              : ''}
                          </p>
                        </div>
                        <p className={clsx(styles.review)}>{review?.text}</p>
                        {!!review?.link_video && (
                          <a href={review?.link_video}>{review?.link_video}</a>
                        )}
                        <div className="flex flex-wrap gap-3 py-2">
                          {review?.images?.map((image: { content: string }, index: number) => {
                            return (
                              <Fragment key={image?.content}>
                                <img
                                  src={image?.content}
                                  alt="Ảnh sản phẩm"
                                  width={90}
                                  height={90}
                                  className="rounded-3xl border border-solid border-[#edebeb]"
                                />
                              </Fragment>
                            )
                          })}
                        </div>
                      </div>
                    </div>
                  </>
                )
              })}
              {(dataProductDetail?.reviews?.page_info?.total_pages as number) > 1 && (
                <div className={clsx(styles.paginationBar)}>
                  <PaginationCustom
                    current={
                      !!router.query?.currentPageReview
                        ? parseInt(`${router.query?.currentPageReview}`)
                        : 1
                    }
                    pageSize={
                      !!router.query?.pageSizeReview
                        ? parseInt(`${router.query?.pageSizeReview}`)
                        : 20
                    }
                    total={
                      !!dataProductDetail?.reviews?.page_info?.total_pages &&
                      !!dataProductDetail?.reviews?.page_info?.page_size
                        ? parseInt(`${dataProductDetail?.reviews?.page_info?.total_pages}`) *
                          parseInt(`${dataProductDetail?.reviews?.page_info?.page_size}`)
                        : !!router?.query?.pageSize
                        ? parseInt(`${router.query?.pageSize}`)
                        : 20
                    }
                    onChange={(page: number) =>
                      router.push({
                        query: {
                          ...router.query,
                          currentPageReview: page,
                        },
                      })
                    }
                    totalPage={
                      !!dataProductDetail?.reviews?.page_info?.total_pages
                        ? parseInt(`${dataProductDetail?.reviews?.page_info?.total_pages}`)
                        : 1
                    }
                  />
                </div>
              )}
            </div>
            {!dataInfoCustomer && (
              <div className={clsx(styles.invite)}>
                <p>
                  Mời bạn <span>Đăng nhập</span> hoặc <span>Đăng ký</span> để đánh giá sản phẩm này.
                </p>
              </div>
            )}
          </div>
        </>
      )}
    </>
  )
}
