/* eslint-disable @next/next/no-img-element */
/**
 * /* eslint-disable @next/next/no-img-element
 *
 * @format
 */

import clsx from 'clsx'
import React, { useState } from 'react'
import styles from '../ProductDetail.module.scss'
import parse from 'html-react-parser'

export interface IDetailItemProps {
  dataProductDetail?: TProduct
}

export default function DetailItem(props: IDetailItemProps) {
  const { dataProductDetail } = props
  return (
    <div className={clsx(styles.detailItemContainer)}>
      <h3>{dataProductDetail?.name || 'Đang cập nhật'}</h3>
      <div className={clsx(styles.flexItem)}>
        <div className={clsx(styles.itemDetail)}>
          <div className={clsx(styles.item)}>
            <p>Đơn vị tính:</p>
            <p>{dataProductDetail?.unit || 'Đang cập nhật'}</p>
          </div>
          <div className={clsx(styles.item)}>
            <p>Kích thước đóng gói:</p>
            <p>
              {dataProductDetail?.length && dataProductDetail?.width && dataProductDetail?.height
                ? `${dataProductDetail?.length} x ${dataProductDetail?.width} x ${dataProductDetail?.height}`
                : 'Đang cập nhật'}
            </p>
          </div>
          <div className={clsx(styles.item)}>
            <p>Thương hiệu:</p>
            <p>{dataProductDetail?.brand?.name || 'Đang cập nhật'}</p>
          </div>
          <div className={clsx(styles.item)}>
            <p>Xuất xứ:</p>
            <p>{dataProductDetail?.country || 'Đang cập nhật'}</p>
          </div>
        </div>
        <div className={clsx(styles.imageDetail)}>
          {!!dataProductDetail?.media_gallery?.length ? (
            dataProductDetail?.media_gallery?.map((gallery, index) => {
              return (
                <>
                  {index > 1 ? (
                    <></>
                  ) : (
                    <img
                      className="rounded-xl"
                      key={`${gallery?.url}`}
                      src={`${gallery?.url}`}
                      alt=""
                    />
                  )}
                </>
              )
            })
          ) : (
            <></>
          )}
        </div>
      </div>
      <div
        className={clsx(styles.itemDescription)}
        dangerouslySetInnerHTML={{
          __html: dataProductDetail?.short_description?.html || '',
        }}
      ></div>
    </div>
  )
}
