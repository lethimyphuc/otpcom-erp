/* eslint-disable @next/next/no-img-element */
import clsx from 'clsx'
import React, { useEffect, useState } from 'react'
import styles from '../ProductDetail.module.scss'
import { Swiper, SwiperSlide } from 'swiper/react'
import { _format } from '~src/util'
import { useQuery } from 'react-query'
import { graphQLClient, graphQLClientGET } from '~src/api/graphql'
import { GET_DAILYSALES } from '~src/api/product'
import ProductItem from '~src/components/global/Product/ProductItem'
import { useRouter } from 'next/router'
import ProductItemSlide from '~src/components/global/Product/ProductItemSlide'
import { useWindowSize } from '~src/hook'

export interface ISaleProductSwiperProps {
  // saleList: any[]
}

export default function SaleProductSwiper({}: ISaleProductSwiperProps) {
  const { isTablet, isMobile, isDesktop } = useWindowSize()
  const router = useRouter()

  const { data: dataDailySales, isLoading: isLoadingDataDailySales } = useQuery({
    queryKey: ['getDailySales'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_DAILYSALES, {
        filter: {},
        pageSize: 8,
      })) as {
        DailySales: {
          items: TDailySales[]
          page_info: TPageInfo
          total_count: number
        }
      }
      return data
    },
  })

  const [swiper, setSwiper] = useState<any>(null)

  const handleNext = () => {
    swiper.slideNext()
  }

  const handlePrev = () => {
    swiper.slidePrev()
  }

  const [numOfSwiperItem, setNumberOfSwiper] = useState(3)

  useEffect(() => {
    isMobile && setNumberOfSwiper(1)
    isTablet && setNumberOfSwiper(2)
    isDesktop && setNumberOfSwiper(4)
  }, [isMobile, isTablet, isDesktop])
  const config = {
    spaceBetween: 24,
    slidesPerView: numOfSwiperItem,
    onSwiper: (s: any) => {
      setSwiper(s)
    },
    loop: true,
  }

  if (dataDailySales?.DailySales?.items[0]?.items?.length == 0) {
    return (
      <div className="w-full text-center pb-[10rem]">
        <p>Chưa có sản phẩm</p>
      </div>
    )
  }
  return (
    <div className={clsx(styles.swiperSaleContainer, '')}>
      <div className="w-[40px] relative">
        <button onClick={handlePrev} className={clsx(styles.iconSwiper)}>
          <img src="/icons/prev.svg" alt="" />
        </button>
      </div>
      <div className={clsx(styles.listImage, 'wrapper-product')}>
        <Swiper {...config} className="product-list">
          {!!dataDailySales?.DailySales?.items &&
            !!dataDailySales?.DailySales?.items?.length &&
            dataDailySales?.DailySales?.items[0]?.items?.map(
              (item: TItemDailySale, index: number) => (
                <SwiperSlide key={index} className="product-item product-item-slide">
                  <ProductItemSlide product={item?.product} router={router} />
                </SwiperSlide>
              )
            )}
        </Swiper>
      </div>
      <div className="w-[40px] relative sm:right-0 right-[20px]">
        <button onClick={handleNext} className={clsx(styles.iconSwiper)}>
          <img src="/icons/next.svg" alt="" />
        </button>
      </div>
    </div>
  )
}
