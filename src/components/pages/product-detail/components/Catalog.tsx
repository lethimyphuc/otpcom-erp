import { Skeleton } from 'antd'
import { useRef, useState } from 'react'
import { isMobile } from 'react-device-detect'
import { useMutation, useQuery } from 'react-query'
import { toast } from 'react-toastify'
import { GET_CATALOG_URL_KEY, UPDATE_CATALOG } from '~src/api'
import { graphQLClient, graphQLClientGET } from '~src/api/graphql'
import { ProductSkeleton } from '~src/components/ProductSkeleton'
import { CataLogSpAdmin } from '../../catalog/catalog-detail/CataLogSpAdmin'

type Props = {
  url_key?: string
}

export const CatalogImage = ({ url_key }: Props) => {
  const [dataPost, setDataPost] = useState<ICatalogData[]>([])
  const [dataChoose, setDataChoose] = useState<ICatalogDataITems[]>([])
  const [isOpenInsert, setIsOpenInsert] = useState<boolean>(false)
  const [sizeMarker] = useState<number>(isMobile ? 15 : 26)
  const [isChecked, setIsChecked] = useState(Array(50).fill(false))
  const [errorStt, setErrorStt] = useState<string>('')
  const [txtSearch, setTxtSearch] = useState<string>('')
  const [isAddMutation, setIsAddMutation] = useState<boolean>(false)
  const refCheckbox = useRef<any>(null)
  const [dataTem, setDataTem] = useState<ICatalogData>({
    posX: '',
    posY: '',
    position: 0,
    products: [],
  })
  const refWr = useRef<HTMLDivElement>(null)
  const { data: dataCatalogDetail, isLoading: isLoadingDataCatalogDetail } = useQuery({
    queryKey: ['GET_CATALOG_DETAIL', url_key],
    queryFn: async () =>
      await graphQLClientGET
        .request(GET_CATALOG_URL_KEY, {
          url_key: url_key,
        })
        .then((data: any) => {
          setDataPost([...JSON.parse(data?.catalogueUrlkey?.points)] ?? [])
          setDataChoose([...data?.catalogueUrlkey?.products?.items])
          return data?.catalogueUrlkey
        }),

    enabled: !!url_key,
  })

  const handleClickLocation = (e: any) => {
    if (e.target.closest('.catalog-marker')) return
    const locationLeft =
      ((e?.clientX - (refWr.current?.getBoundingClientRect()?.left || 0) - sizeMarker / 2) /
        (refWr.current?.getBoundingClientRect()?.width || 1)) *
      100

    const locationTop =
      ((e?.clientY - (refWr.current?.getBoundingClientRect()?.top || 0) - sizeMarker / 2) /
        (refWr.current?.getBoundingClientRect()?.height || 1)) *
      100

    setDataTem({
      ...dataTem,
      posX: locationLeft + '%',
      posY: locationTop + '%',
    })
    setIsOpenInsert(true)
  }
  const handleCloseInsert = () => {
    setIsOpenInsert(false)
    setDataTem({
      posX: '',
      posY: '',
      position: 0,
      products: [],
    })
    setIsChecked(Array(50).fill(false))
    setErrorStt('')
  }
  const handleSearchForm = (e: any) => {
    const value = e.target.value
    setTxtSearch(value)
    const dataSearch = dataCatalogDetail?.products?.items.filter((data: any) => {
      return data.name.toLowerCase().includes(value.toLowerCase())
    })
    setDataChoose(dataSearch)
  }
  const hanndleChangeStt = (e: any) => {
    if (e.target.value < 0) {
      setErrorStt('Không được nhập số âm')
      setDataTem({
        ...dataTem,
        position: -1,
      })
      return
    }
    setDataTem({
      ...dataTem,
      position: e.target.value,
    })
    setErrorStt('')
  }
  const handleChecked = (e: any, item: any, index: number) => {
    if (e.target.checked) {
      setDataTem({
        ...dataTem,
        products: [...dataTem?.products, item],
      })
    } else {
      const index = dataTem?.products?.findIndex((data: any) => data.index === item.index)
      if (index > -1) {
        dataTem?.products.splice(index, 1)
      }
    }

    const newCheckedState = [...isChecked]
    newCheckedState[index] = !isChecked[index]
    setIsChecked(newCheckedState)
  }
  const handleAdd = () => {
    const anyChecked = isChecked.some((checked) => checked)
    if (dataTem?.position < 0) {
      setErrorStt('Không được nhập số âm')
      return
    }
    if (dataTem?.position == 0) {
      setErrorStt('Số thứ tự không được để trống')
      return
    }
    if (!anyChecked) {
      setErrorStt('Vui lòng chọn sản phẩm')
      return
    }
    setDataPost([...dataPost, dataTem])
    setIsAddMutation(true)
    setIsOpenInsert(false)
    setIsChecked(Array(50).fill(false))
    setErrorStt('')
  }
  const { mutate: mutationUpdateCatalog, isLoading: isPendingUpdateCatalog } = useMutation({
    mutationFn: async (data: any) => {
      return await graphQLClient.request(UPDATE_CATALOG, {
        input: {
          id: dataCatalogDetail?.id,
          points: data.point,
        },
      })
    },
    onSuccess(data: any) {
      if (data?.updateCatalogue?.id == dataCatalogDetail?.id) {
        toast('Cập nhật thành công')
        setIsAddMutation(false)
      }
    },
    onError(error: any) {
      console.log('error', error)
    },
  })
  const handleDeleteTem = (e: any, keyParrent: number, keyChild: number) => {
    let resultChild = dataPost.filter((data: any, index: any) => {
      if (index === keyParrent) {
        if (data?.products?.length > 0) {
          data?.products.splice(keyChild, 1)
        }
      }
      return data
    })
    let resultParrent = resultChild.filter((data: any) => {
      return data?.products?.length > 0
    })
    if (resultParrent?.length == 1) {
      if (resultParrent[0]?.products?.length == 0) {
        resultParrent = []
      }
    }
    setDataPost(resultParrent)
    mutationUpdateCatalog({ point: JSON.stringify(resultParrent) })
  }
  return (
    <div className="catalog-otp">
      <div className="containers">
        <div className="catalog-area" ref={refWr} onClick={(e) => handleClickLocation(e)}>
          {isLoadingDataCatalogDetail ? (
            <div className="skeleton-gr">
              <div className="re-block skeleton-gr-inner">
                <Skeleton active />
                <Skeleton active />
              </div>
              <div className="re-block">
                <div className="pro-list row">
                  {[...Array(4)].map((_, index) => (
                    <div className="pro-item col col-3" key={index}>
                      <ProductSkeleton />
                    </div>
                  ))}
                </div>
              </div>
            </div>
          ) : (
            <div className="catalog-area-img">
              <img src={dataCatalogDetail?.catalogue_image} alt="" />
            </div>
          )}
          {/* {true && (
            <CatalogAdminForm
              sizeMarker={sizeMarker}
              dataTem={dataTem}
              handleCloseInsert={handleCloseInsert}
              isOpenInsert={isOpenInsert}
              errorStt={errorStt}
              handleSearchForm={handleSearchForm}
              hanndleChangeStt={hanndleChangeStt}
              handleChecked={handleChecked}
              handleAdd={handleAdd}
              dataChoose={dataChoose}
              refCheckbox={refCheckbox}
              txtSearch={txtSearch}
              dataPost={dataPost}
            />
          )} */}
          <CataLogSpAdmin
            dataPost={dataPost}
            sizeMarker={sizeMarker}
            handleDeleteTem={handleDeleteTem}
            isLoadingDataCatalogDetail={isLoadingDataCatalogDetail}
            isPermission={false}
          />
        </div>
      </div>
    </div>
  )
}
