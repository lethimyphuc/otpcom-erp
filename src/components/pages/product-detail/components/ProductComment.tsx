/**
 * /* eslint-disable jsx-a11y/alt-text
 *
 * @format
 */

/* eslint-disable @next/next/no-img-element */
import clsx from 'clsx'
import React, { Fragment, useEffect, useState } from 'react'
import styles from '../ProductDetail.module.scss'
import { PaginationCustom } from '~src/components/common/PaginationCustom'
import { useForm } from 'react-hook-form'
import { useRouter } from 'next/router'
import FormComment from '~src/components/global/Product/FormComment'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { graphQLClient } from '~src/api/graphql'
import { INFO_CUSTOMER } from '~src/api'
import { Input, Rate } from 'antd'
import { _format } from '~src/util'
import { CREATE_COMMENT, CREATE_PRODUCT_REVIEW } from '~src/api/product'
import { ModalShowNotification } from '~src/components'
import { toast } from 'react-toastify'

export interface IProductCommentProps {
  dataProductDetail?: TProduct
  dataRatingProduct?: TReviewRating[]
  fetchingDataProductDetail: () => void
}
let isComment = false
export default function ProductComment(props: IProductCommentProps) {
  const { dataProductDetail, fetchingDataProductDetail } = props
  const [pagination, setPagination] = useState({
    current: 1,
    pageSize: 20,
    total: dataProductDetail?.reviews?.page_info?.total_pages,
  })
  const {
    setValue,
    control,
    handleSubmit,
    watch,
    reset,
    formState: { errors },
  } = useForm({
    defaultValues: {
      nickname: '',
      email: '',
      text: '',
      anonymous: false,
      sku: '',
    },
  })
  const router = useRouter()
  const [openFormComment, setOpenFormComment] = useState(false)
  const [contentComment, setContentComment] = useState('')
  const [idRepplyComment, setIdRepplyComment] = useState('')
  const queryClient = useQueryClient()
  const [isSuccess, setIsSucces] = useState(false)
  const handleOpenFormComment = (value: boolean) => {
    setOpenFormComment(value)
  }

  const { data: infoDataCustomer, refetch: refetchDataInfoCustomer } = useQuery({
    queryKey: ['getInfoCustomer'],
    queryFn: async () => {
      const data = await graphQLClient.request(INFO_CUSTOMER)
      return data
    },
    cacheTime: Infinity,
    enabled: false,
    onSuccess: (response: any) => {
      if (!!response?.customer) {
        setOpenFormComment(true)
      } else {
        router.push('/login')
      }
    },
  })

  const createRepplyCommnet = useMutation({
    mutationKey: 'createRepplyComment',
    mutationFn: async () => {
      return await graphQLClient.request(CREATE_COMMENT, {
        input: {
          content: contentComment,
          review_id: idRepplyComment,
        },
      })
    },
    onSuccess: (response: any) => {
      fetchingDataProductDetail()
      setContentComment('')
      setIsSucces(true)
    },
  })

  const handleReppyComment = async () => {
    if (!isComment) {
      isComment = true
      await createRepplyCommnet.mutateAsync()
    }
  }

  const handleCheckLogin = () => {
    refetchDataInfoCustomer()
  }

  const createCreateProductReview = useMutation({
    mutationKey: 'createEmptyCart',
    mutationFn: async (data: {
      nickname: string
      email: string
      ratings: any
      text: string
      anonymous: boolean
      images: any
      sku: string
      summary: string
    }) => {
      return (await graphQLClient.request(CREATE_PRODUCT_REVIEW, {
        input: data,
      })) as { createProductReview: { review: TReviewProduct } }
    },
    onSuccess: (response: any) => {
      toast('Bình luận của bạn đã được gửi thành công và chờ được xét duyệt ', {
        type: 'success',
      })
      setOpenFormComment(false)
      reset({})
      setIsSucces(true)
      queryClient.setQueryData(['getProductDetail', router.query], (oldData: any) => {
        if (!!oldData) {
          return {
            ...oldData,
            reviews: response.createProductReview?.review?.product?.reviews,
          }
        } else {
          return oldData
        }
      })
    },
  })

  const onSubmit = async (data: Record<string, any>) => {
    try {
      setIsSucces(true)
      const DATA_SUBMIT: any = {
        nickname: !!`${data?.nickname}`.trim() ? `${data?.nickname}`.trim() : '',
        text: !!`${data?.text}`.trim() ? `${data?.text}` : '',
        summary: !!`${data?.text}`.trim() ? `${data?.text}` : '',
        ratings: [],
        sku: dataProductDetail?.sku,
      }
      await createCreateProductReview.mutateAsync(DATA_SUBMIT)
    } finally {
      setIsSucces(false)
    }
  }

  useEffect(() => {
    if (!!isSuccess) {
      setTimeout(() => {
        setIsSucces(false)
      }, 2000)
    }
  }, [isSuccess])

  return (
    <>
      <div>
        {openFormComment || !dataProductDetail?.reviews?.items?.length ? (
          <form onSubmit={handleSubmit(onSubmit)}>
            <FormComment
              control={control}
              setValue={setValue}
              watch={watch}
              errors={errors}
              dataProductDetail={dataProductDetail as any}
              setOpen={handleOpenFormComment}
            />
          </form>
        ) : (
          <>
            <div className={clsx(styles.productCommentContainer)}>
              <div className={clsx(styles.flexItem)}>
                <h3>Bình luận</h3>
                <p>{`(${dataProductDetail?.reviews?.page_info?.total_pages})`}</p>
              </div>

              <div className={clsx(styles.question)}>
                <p>Bạn đã mua sản phẩm này?</p>
                <button
                  className="cursor-pointer font-semibold text-red"
                  onClick={handleCheckLogin}
                >
                  Để lại bình luận
                </button>
              </div>

              <div className={clsx(styles.listComment)}>
                {dataProductDetail?.reviews?.items?.map((item: any, index: number) => {
                  const rate = (item?.average_rating * 5) / 100
                  return (
                    <Fragment key={index + item?.review_id}>
                      <div className={clsx(styles.commentItem)} key={index}>
                        <div className={clsx(styles.avatar)}>
                          <img src={item?.customer_avatar || '/image/avatar-default.jpg'} alt="" />
                        </div>
                        <div className={clsx(styles.commentMain)}>
                          <div className={clsx(styles.commentAccount)}>
                            <div>
                              <p>{item.nickname}</p>
                              <Rate
                                allowHalf
                                defaultValue={rate}
                                className="text-[1.4rem]"
                                disabled={true}
                              />
                            </div>
                            <p>{_format.formatDate(item.created_at)}</p>
                          </div>
                          <div>
                            <p className={clsx(styles.comment)}>{item.text}</p>
                            {!!item?.link_video && (
                              <a href={item?.link_video}>{item?.link_video}</a>
                            )}
                          </div>
                          <div className="flex flex-wrap gap-3 py-2">
                            {item?.images?.map((image: { content: string }, index: number) => {
                              return (
                                <Fragment key={image?.content}>
                                  <img
                                    src={image?.content}
                                    alt="Ảnh sản phẩm"
                                    width={90}
                                    height={90}
                                    className="rounded-3xl border border-solid border-[#edebeb]"
                                  />
                                </Fragment>
                              )
                            })}
                          </div>
                          {!!infoDataCustomer && (
                            <div className={clsx('mb-4')}>
                              <button
                                className={clsx(styles.replyComment, 'items-center')}
                                onClick={() => setIdRepplyComment(item?.review_id)}
                              >
                                <img src="/icons/chat.svg" alt="" />
                                <p>Trả lời</p>
                              </button>
                            </div>
                          )}

                          <div className={clsx(styles.boxComment)}>
                            {item.comments &&
                              item.comments.map((rep: any, index: number) => (
                                <div key={index}>
                                  <div
                                    className={clsx(styles.commentItem, styles.replyItem)}
                                    key={index}
                                  >
                                    <div className={clsx(styles.avatar)}>
                                      <img
                                        src={rep.customer_avatar || '/image/avatar-default.jpg'}
                                        alt=""
                                      />
                                    </div>
                                    <div className={clsx(styles.commentMain)}>
                                      <div className={clsx(styles.commentAccount)}>
                                        <div>
                                          <p>{rep.post_by}</p>
                                          <div></div>
                                        </div>
                                        <p>{_format.formatDate(rep.created_at)}</p>
                                      </div>
                                      <p className={clsx(styles.comment)}>{rep.content}</p>
                                      {!!infoDataCustomer && (
                                        <div>
                                          <button
                                            className={clsx(styles.replyComment, 'items-center')}
                                          >
                                            <img src="/icons/chat.svg" alt="" />
                                            <p>Trả lời</p>
                                          </button>
                                        </div>
                                      )}
                                    </div>
                                  </div>
                                </div>
                              ))}
                          </div>

                          {item?.review_id == idRepplyComment ? (
                            <div className="mt-4">
                              <Input
                                value={contentComment || ''}
                                onChange={(e) => setContentComment(e.target.value)}
                                onPressEnter={handleReppyComment}
                              />
                            </div>
                          ) : (
                            <></>
                          )}
                        </div>
                      </div>
                    </Fragment>
                  )
                })}
                <div className={clsx(styles.paginationBar)}>
                  {(pagination.total as number) > 20 && (
                    <PaginationCustom
                      current={pagination.current}
                      pageSize={pagination.pageSize}
                      total={pagination.total as number}
                      onChange={(val: number) => console.log(val)}
                    />
                  )}
                </div>
                {!infoDataCustomer && (
                  <div className={clsx(styles.invite)}>
                    <p>
                      Mời bạn <span>Đăng nhập</span> hoặc <span>Đăng ký</span> để đánh giá sản phẩm
                      này.
                    </p>
                  </div>
                )}
              </div>
            </div>
          </>
        )}
        <ModalShowNotification
          visible={isSuccess}
          onCancel={() => setIsSucces(false)}
          content={'Thêm bình luận thành công!'}
        />
      </div>
    </>
  )
}
