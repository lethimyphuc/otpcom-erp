/* eslint-disable react-hooks/exhaustive-deps */
import { DatePicker } from 'antd'
import clsx from 'clsx'
import moment from 'moment'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { INFO_CUSTOMER, UPDATE_CUSTOMER } from '~src/api'
import { graphQLClient } from '~src/api/graphql'
import { ButtonMain } from '~src/components/global/Button/ButtonMain/ButtonMain'
import ErrorMessage from '~src/components/global/ErrorMessage'
import FormUploadSingle from '~src/components/global/FormControls/FormUploadSingle'
import { TChangeInfoCustomer } from '~src/types/authenticate'
import styles from './AccountInfo.module.scss'
import { tr } from 'date-fns/locale'
import { toast } from 'react-toastify'

export default function AccountInfo() {
  const queryClient = useQueryClient()
  const router = useRouter()

  const { data: userInfo } = useQuery<any>({
    queryKey: ['getInfoCustomerOrder', router.query],
    queryFn: async () => {
      return (await graphQLClient.request(INFO_CUSTOMER)) as {
        customer: any
      }
    },
    cacheTime: Infinity,
    enabled: true,
  })

  const {
    handleSubmit,
    setValue,
    getValues,
    control,
    formState: { errors },
    watch,
  } = useForm<TChangeInfoCustomer>({
    defaultValues: {
      lastname: '',
      firstname: '',
      date_of_birth: '',
      email: '',
      phone_number: '',
    },
  })

  useEffect(() => {
    if (!!userInfo) {
      setValue('lastname', userInfo?.customer.lastname)
      setValue('firstname', userInfo?.customer.firstname)
      setValue('email', userInfo?.customer.email)
      setValue('date_of_birth', userInfo?.customer.date_of_birth)
      setValue('phone_number', userInfo?.customer.phone_number)
      setValue('avatar', userInfo?.customer.profile_picture || '')
    }
  }, [userInfo])

  const { mutate: updateCustomer, isLoading } = useMutation({
    mutationKey: 'updateCustomer',
    mutationFn: async (data) => {
      return await graphQLClient.request(UPDATE_CUSTOMER, {
        input: data,
      })
    },
    onSuccess: () => {
      queryClient.invalidateQueries(['getInfoCustomer'])
      toast.success('Cập nhật thông tin thành công')
    },
    onError: () => {
      toast.error('Cập nhật thông tin thất bại')
    },
  })

  const onSubmit = (data: TChangeInfoCustomer) => {
    let DATA_SUBMIT_UPDATE_CUSTOMER: any = {
      allow_remote_shopping_assistance: true,
      date_of_birth: !!data?.date_of_birth ? new Date(data?.date_of_birth) : '',
      firstname: !!`${data?.firstname}`.trim() ? `${data?.firstname}`.trim() : '',
      lastname: !!`${data?.lastname}`.trim() ? `${data?.lastname}`.trim() : '',
      phone_number: !!`${data?.phone_number}`.trim() ? `${data?.phone_number}`.trim() : '',
      is_subscribed: true,
    }
    if (data.avatar?.url) {
      DATA_SUBMIT_UPDATE_CUSTOMER = {
        ...DATA_SUBMIT_UPDATE_CUSTOMER,
        profile_upload_picture: {
          name: 'file_name',
          base64_encoded_file: data?.avatar?.url,
        },
      }
    }

    updateCustomer(DATA_SUBMIT_UPDATE_CUSTOMER)
  }

  return (
    <div className={clsx(styles.accountBoxRight)}>
      <h3>Thông tin tài khoản</h3>
      <div className={clsx(styles.accountInfo)}>
        <div className={clsx(styles.avatar)}>
          <FormUploadSingle title={'Chọn ảnh'} setValue={setValue} watch={watch} />
        </div>
        <div className={clsx(styles.formInfo)}>
          <div>
            <div className={clsx(styles.formItem)}>
              <p>
                Họ <span>*</span>
              </p>
              <Controller
                name="lastname"
                control={control}
                rules={{
                  required: true,
                }}
                render={({ field }) => (
                  <input
                    {...field}
                    className={clsx(styles.formInput)}
                    type="text"
                    name=""
                    id=""
                    placeholder="Họ của bạn"
                  />
                )}
              />
              <div className={clsx(styles.formUnderline)}></div>
            </div>
            {errors?.lastname ? <ErrorMessage>Vui lòng không để trống</ErrorMessage> : <></>}
          </div>

          <div>
            <div className={clsx(styles.formItem)}>
              <p>
                Tên <span>*</span>
              </p>
              <Controller
                name="firstname"
                control={control}
                rules={{
                  required: true,
                }}
                render={({ field }) => (
                  <input
                    {...field}
                    className={clsx(styles.formInput)}
                    type="text"
                    name=""
                    id=""
                    placeholder="Tên của bạn"
                  />
                )}
              />

              <div className={clsx(styles.formUnderline)}></div>
            </div>
            {errors?.firstname ? <ErrorMessage>Vui lòng không để trống</ErrorMessage> : <></>}
          </div>

          <div>
            <div className={clsx(styles.formItem)}>
              <p>
                Email <span>*</span>
              </p>
              <Controller
                name="email"
                control={control}
                rules={{
                  required: false,
                }}
                render={({ field }) => (
                  <input
                    {...field}
                    className={clsx(styles.formInput)}
                    type="text"
                    name=""
                    id=""
                    placeholder="Email của bạn"
                    disabled={false}
                  />
                )}
              />

              <div className={clsx(styles.formUnderline)}></div>
            </div>
            {errors?.email ? <ErrorMessage>Vui lòng không để trống</ErrorMessage> : <></>}
          </div>

          <div>
            <div className={clsx(styles.formItem)}>
              <p>
                Số điện thoại <span>*</span>
              </p>
              <Controller
                name="phone_number"
                control={control}
                render={({ field }) => (
                  <input
                    {...field}
                    className={clsx(styles.formInput)}
                    type="text"
                    name=""
                    id=""
                    placeholder="Số điện thoại của bạn"
                  />
                )}
              />
              <div className={clsx(styles.formUnderline)}></div>
            </div>
            {errors?.phone_number ? <ErrorMessage>Vui lòng không để trống</ErrorMessage> : <></>}
          </div>

          <div>
            <div className={clsx(styles.formItem)}>
              <p>Ngày sinh</p>
              <Controller
                name="date_of_birth"
                control={control}
                render={({ field }) => (
                  <DatePicker
                    {...field}
                    value={!!field.value ? moment(field.value) : null}
                    picker="date"
                    className={clsx(styles.formInput)}
                    placeholder="Ngày sinh của bạn"
                  />
                )}
              />
              <div className={clsx(styles.formUnderline)}></div>
            </div>
          </div>
          <div className="col-span-2 flex lg:justify-end justify-center mt-14 lg:mt-0">
            <ButtonMain
              background="green"
              loading={isLoading}
              type="button"
              onClick={handleSubmit(onSubmit)}
              className={clsx(styles.saveBtn, 'w-[135px] h-[45px]')}
            >
              {!isLoading ? 'Lưu thông tin' : ''}
            </ButtonMain>
          </div>
        </div>
      </div>
    </div>
  )
}
