/* eslint-disable @next/next/no-img-element */
import { Empty, Spin } from 'antd'
import clsx from 'clsx'
import { format } from 'date-fns'
import { useRouter } from 'next/router'
import { useRef, useState } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useQuery } from 'react-query'
import { GET_ORDERS_LIST, INFO_CUSTOMER } from '~src/api'
import { graphQLClient, graphQLClientGET } from '~src/api/graphql'
import { PaginationCustom } from '~src/components/common/PaginationCustom'
import { ButtonMain } from '~src/components/global/Button/ButtonMain/ButtonMain'
import { _format } from '~src/util'
import styles from '../AccountInfo.module.scss'

export default function OrderHistory() {
  const { control, watch } = useForm()
  const router = useRouter()
  const inputRef = useRef<any>()

  const { data, isFetching } = useQuery<any>({
    queryKey: ['getOrdersList', watch('search'), router.query?.currentPage],
    queryFn: async () => {
      const data: any = await graphQLClientGET.request(GET_ORDERS_LIST, {
        sort: {
          sort_field: 'CREATED_AT',
          sort_direction: 'DESC',
        },
        orderCurrentPage: router.query?.currentPage || 1,
        orderPageSize: 20,
        filter: {
          number: {
            match: watch('search') || '',
          },
        },
      })
      return data?.customer
    },
    cacheTime: Infinity,
    // enabled: !!watch('search'),
  })

  const getStatusStyle = (status: string) => {
    switch (status) {
      case 'Đợi duyệt':
        return styles.waiting
      case 'Đã hủy bỏ':
        return styles.canceled
      case 'Đang tiến hành':
        return styles.shipping
      case 'Hoàn tất':
        return styles.success
      default:
        return ''
    }
  }

  return (
    <div className={clsx(styles.accountBoxRight)}>
      <h3>Lịch sử đơn hàng</h3>
      <Controller
        name="search"
        control={control}
        render={({ field: { onChange, value } }) => (
          <div className={clsx(styles.orderSearch)}>
            <div className={clsx(styles.searchInput)}>
              <input
                ref={inputRef}
                className={clsx(styles.formInput)}
                type="text"
                name=""
                id=""
                placeholder="Tìm theo mã đơn hàng hoặc tên sản phẩm"
              />
              <div className={clsx(styles.formUnderline)}></div>
            </div>
            <div className={clsx(styles.searchBtnContainer)}>
              <ButtonMain
                type="button"
                background="green"
                onClick={() => {
                  onChange(inputRef?.current?.value)
                }}
                // className={clsx(styles.searchBtn)}
                className="min-w-[165px]"
                disable={isFetching}
              >
                Tra cứu đơn hàng
              </ButtonMain>
            </div>
          </div>
        )}
      />

      {isFetching ? (
        <div className="flex-1 flex items-center justify-center py-[48px]">
          <Spin />
        </div>
      ) : !data?.orders?.items?.length ? (
        <Empty className="py-[24px]" description="Không có đơn hàng nào được tìm thấy" />
      ) : (
        <div className={clsx(styles.tableContainer)}>
          <table className={clsx(styles.orderList)}>
            <thead>
              <tr>
                <th>Mã đơn hàng</th>
                <th>Giá tiền</th>
                <th>Ngày mua hàng</th>
                <th>Trạng thái</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {data?.orders?.items?.map((item: any, index: number) => (
                <tr key={index}>
                  {/* #OTP */}
                  <td>{item?.number}</td>

                  <td>
                    {_format.getVND(item?.total?.grand_total?.value || 0)}
                    {item?.total?.grand_total?.currency || ' VNĐ'}
                  </td>
                  <td>
                    {!!item?.order_date ? format(new Date(item?.order_date), 'dd/MM/yyyy') : ''}
                  </td>
                  <td>
                    <span className={getStatusStyle(item.status)}>
                      <p>{item.status}</p>
                    </span>
                  </td>
                  <td>
                    <div onClick={() => router.push(`/account/orders/${item.number}`)}>
                      <p>Xem chi tiết</p>
                      <img
                        className={clsx(styles.arrow, 'hidden sm:block')}
                        src={'/icons/arrow-right.svg'}
                        alt=""
                      />
                      <img
                        className={clsx(styles.greenArrow, 'hidden sm:block')}
                        src={'/icons/arrow-right-green.svg'}
                        alt=""
                      />
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
          <div className={clsx(styles.paginationBar)}>
            {/* <PaginationCustom
            current={currentPage}
            pageSize={20}
            total={orders?.total_count}
            onChange={(val: number) => setCurrentPage(val)}
          /> */}

            <PaginationCustom
              current={!!router.query?.currentPage ? parseInt(`${router.query?.currentPage}`) : 1}
              pageSize={!!router?.query?.pageSize ? parseInt(`${router.query?.pageSize}`) : 20}
              total={
                !!data?.orders?.page_info?.total_pages && !!data?.orders?.page_info?.page_size
                  ? parseInt(`${data?.orders?.page_info?.total_pages}`) *
                    parseInt(`${data?.orders?.page_info?.page_size}`)
                  : !!router?.query?.pageSize
                  ? parseInt(`${router.query?.pageSize}`)
                  : 20
              }
              onChange={(page: number) =>
                router.push({
                  query: {
                    ...router.query,
                    currentPage: page,
                  },
                })
              }
              totalPage={
                !!data?.orders?.page_info?.total_pages
                  ? parseInt(`${data?.orders?.page_info?.total_pages}`)
                  : 1
              }
            />
          </div>
        </div>
      )}
    </div>
  )
}
