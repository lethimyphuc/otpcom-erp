import { Popconfirm } from "antd";
import { useState } from "react";
import { UserAddress } from "~src/types/user";

type AddressItemProps = {
  data: UserAddress;
  onDelete?: (id: number) => void;
  onUpdate?: (data: UserAddress) => void;
};

export const AddressItem = ({ data, onDelete, onUpdate }: AddressItemProps) => {
  const [visible, setVisible] = useState<boolean>(false);

  return (
    <div className="border border-solid border-black10 mb-[8px] last:mb-0 p-[12px] rounded-[5px]">
      <div className="flex items-center justify-between mb-[12px]">
        <div className="flex-1 flex items-center mr-[12px]">
          <p className="text-[14px]">
            {data.firstname} {data.lastname}
            {data.default_shipping ? (
              <span className="text-[12px] ml-[8px] text-success">
                {data.default_shipping ? "Địa chỉ mặc định" : ""}
              </span>
            ) : null}
          </p>
        </div>
        <p
          onClick={() => onUpdate?.(data)}
          className="text-[12px] text-info cursor-pointer select-none"
        >
          Chỉnh sửa
        </p>
      </div>
      <div className="flex">
        <div className="flex-1">
          <div className="flex items-center mb-[8px]">
            <p className="text-[13px] font-normal text-gray70 mr-[4px]">
              Địa chỉ:{" "}
            </p>
            <p className="text-[13px] font-normal">
              {`${data.street?.[0]}, ${data.ward}, ${data.district}, ${data.city}`}
            </p>
          </div>
          <div className="flex items-center">
            <p className="text-[13px] font-normal text-gray70 mr-[4px]">
              Số điện thoại:{" "}
            </p>
            <p className="text-[13px] font-normal">{data.telephone}</p>
          </div>
        </div>

        <div className="flex items-end">
          {!data.default_shipping ? (
            <Popconfirm
              title="Bạn có chắc muốn xoá địa chỉ này?"
              visible={visible}
              onCancel={() => setVisible(false)}
              okText="Đồng ý"
              okType='primary'
              onConfirm={() => {
                onDelete?.(data.id), setVisible(false);
              }}
            >
              <p
                onClick={() => setVisible(true)}
                className="text-[12px] text-red cursor-pointer select-none "
              >
                Xoá
              </p>
            </Popconfirm>
          ) : null}
        </div>
      </div>
    </div>
  );
};
