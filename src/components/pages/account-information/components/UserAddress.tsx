/* eslint-disable react-hooks/exhaustive-deps */
import { Button, Empty, Modal, Skeleton } from 'antd'
import { useState } from 'react'
import { useMutation, useQuery } from 'react-query'
import { DELETE_CUSTOMER_ADDRESS, INFO_CUSTOMER } from '~src/api'
import { graphQLClient } from '~src/api/graphql'
import { UserAddress as IUserAddress } from '~src/types/user'
import { AddressItem } from './AddressItem'
import { CreateAddress } from './CreateAddress'

export const UserAddress = () => {
  const [visible, setVisible] = useState<boolean>(false)
  const [addressUpdate, setAddressUpdate] = useState<IUserAddress | undefined>()

  const { data, isFetching, refetch } = useQuery<any>({
    queryKey: ['getCustomerAddress'],
    queryFn: async () => {
      const data: any = await graphQLClient.request(INFO_CUSTOMER, {})
      return data?.customer
    },
    cacheTime: Infinity
  })

  const onSuccess = () => {
    setVisible(false)
    setAddressUpdate(undefined)
    refetch()
  }

  const { isLoading, mutate } = useMutation<any, any, number>({
    mutationFn: async (id) => {
      return await graphQLClient.request(DELETE_CUSTOMER_ADDRESS, {
        id
      })
    },
    onSuccess
  })

  return (
    <section className="flex-1">
      <div className="flex items-center justify-between p-[16px]">
        <h3 className="flex-1 mr-[12px]">Sổ địa chỉ</h3>
        <Button onClick={() => setVisible(true)}>Thêm địa chỉ</Button>
      </div>

      <div className="p-[16px]">
        {isFetching || isLoading ? (
          <>
            <Skeleton loading={true} className="mb-[24px]" />
            <Skeleton loading={true} className="mb-[24px]" />
            <Skeleton loading={true} className="mb-[24px]" />
          </>
        ) : data?.addresses?.length ? (
          data?.addresses.map((item: IUserAddress) => (
            <AddressItem
              key={item.id}
              data={item}
              onDelete={(id) => {
                mutate(id)
              }}
              onUpdate={setAddressUpdate}
            />
          ))
        ) : (
          <Empty description="Không có địa chỉ nào" />
        )}
      </div>

      {visible ? (
        <Modal
          maskClosable={false}
          visible={visible}
          title={'Tạo địa chỉ'}
          footer={null}
          onCancel={() => setVisible(false)}
        >
          <CreateAddress
            defaultValues={{ telephone: data?.phone_number }}
            onSuccess={onSuccess}
          />
        </Modal>
      ) : null}

      {addressUpdate?.id ? (
        <Modal
          maskClosable={false}
          visible={!!addressUpdate}
          title={'Cập nhật địa chỉ'}
          footer={null}
          onCancel={() => setAddressUpdate(undefined)}
        >
          <CreateAddress onSuccess={onSuccess} defaultValues={addressUpdate} />
        </Modal>
      ) : null}
    </section>
  )
}
