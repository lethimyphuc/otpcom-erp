/* eslint-disable @next/next/no-img-element */
import clsx from 'clsx'
import { useRouter } from 'next/router'
import { useState } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useMutation } from 'react-query'
import { CHANGE_CUSTOMER_PASSWORD } from '~src/api'
import { graphQLClient } from '~src/api/graphql'
import { ButtonMain } from '~src/components/global/Button/ButtonMain/ButtonMain'
import ErrorMessage from '~src/components/global/ErrorMessage'
import { TChangePassword } from '~src/types/authenticate'
import styles from '../AccountInfo.module.scss'
import { useCookies } from 'react-cookie'
import { toast } from 'react-toastify'
import { AiOutlineEye, AiOutlineEyeInvisible } from 'react-icons/ai'

export default function ChangePasswordTab() {
  const router = useRouter()
  const [showPassword, setShowPassword] = useState(false)
  const [showNewPassword, setShowNewPassword] = useState(false)
  const [showConfirmPassword, setShowConfirmPassword] = useState(false)
  const [cookies, setCookie, removeCookie] = useCookies(['token'])

  const defaultValues = {
    currentPassword: '',
    newPassword: '',
    confirmPassword: '',
  }

  const {
    handleSubmit,
    control,
    watch,
    reset,
    formState: { errors },
  } = useForm<TChangePassword>({
    defaultValues: defaultValues,
  })

  const { isLoading: isLoadingDataChangeCustomerPassword, mutate: mutateChangeCustomerPassword } =
    useMutation({
      mutationFn: async (data: TChangePassword) => {
        return (await graphQLClient.request(CHANGE_CUSTOMER_PASSWORD, data)) as {
          changeCustomerPassword: { id: string; email: string }
        }
      },
      onSuccess: (response: { changeCustomerPassword: { id: string; email: string } }) => {
        localStorage.removeItem('token')
        removeCookie('token', { path: '/' })
        toast.success('Đổi mật khẩu thành công')
        reset(defaultValues)
        // router.push('/login', undefined, {
        //   shallow: true,
        // })
      },
    })

  const onSubmit = (data: TChangePassword) => {
    const DATA_SUBMIT = {
      currentPassword: !!data?.currentPassword ? `${data?.currentPassword}`.trim() : '',
      newPassword: !!data?.newPassword ? `${data?.newPassword}`.trim() : '',
    }
    mutateChangeCustomerPassword(DATA_SUBMIT)
  }
  return (
    <div className={clsx(styles.accountBoxRight)}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className={clsx(styles.changePwContainer)}>
          <h3>Thay đổi mật khẩu</h3>
          <div className={clsx(styles.changePassword)}>
            <div className={clsx(styles.formItem)}>
              <p>
                Mật khẩu hiện tại <span>*</span>
              </p>
              <Controller
                name="currentPassword"
                control={control}
                rules={{
                  required: true,
                }}
                render={({ field }) => {
                  return (
                    <input
                      {...field}
                      className={clsx(styles.formInput)}
                      type={showPassword ? 'text' : 'password'}
                      name=""
                      id=""
                      placeholder="Nhập mật khẩu hiện tại"
                    />
                  )
                }}
              />
              <div className={clsx(styles.formUnderline)} />
              <div
                className="absolute top-[2.5rem] lg:top-[4.8rem] right-2 cursor-pointer"
                onClick={() => setShowPassword(!showPassword)}
              >
                {!showPassword ? (
                  <AiOutlineEyeInvisible className="text-[#239f40]" />
                ) : (
                  <AiOutlineEye className="text-[#239f40]" />
                )}
              </div>
            </div>
            {!!errors?.currentPassword ? (
              <ErrorMessage>Vui lòng không để trống</ErrorMessage>
            ) : (
              <></>
            )}

            <div className={clsx(styles.formItem)}>
              <p>
                Mật khẩu mới <span>*</span>
              </p>
              <Controller
                name="newPassword"
                control={control}
                rules={{
                  required: true,
                }}
                render={({ field }) => {
                  return (
                    <input
                      {...field}
                      className={clsx(styles.formInput)}
                      type={showNewPassword ? 'text' : 'password'}
                      name=""
                      id=""
                      placeholder="Nhập mật khẩu mới"
                    />
                  )
                }}
              />

              <div className={clsx(styles.formUnderline)} />
              <div
                className="absolute top-[2.5rem] lg:top-[4.8rem] right-2 cursor-pointer"
                onClick={() => setShowNewPassword(!showNewPassword)}
              >
                {!showNewPassword ? (
                  <AiOutlineEyeInvisible className="text-[#239f40]" />
                ) : (
                  <AiOutlineEye className="text-[#239f40]" />
                )}
              </div>
            </div>
            {!!errors?.newPassword ? <ErrorMessage>Vui lòng không để trống</ErrorMessage> : <></>}

            <div className={clsx(styles.formItem)}>
              <p>
                Xác nhận mật khẩu <span>*</span>
              </p>
              <Controller
                name="confirmPassword"
                control={control}
                rules={{
                  required: true,
                  validate: (value) => {
                    console.log('CLM')
                    if (watch('newPassword') != value) {
                      return 'Mật khẩu không khớp'
                    }
                  },
                }}
                render={({ field }) => {
                  return (
                    <input
                      {...field}
                      className={clsx(styles.formInput)}
                      type={showConfirmPassword ? 'text' : 'password'}
                      name=""
                      id=""
                      placeholder="Nhập lại mật khẩu mới"
                    />
                  )
                }}
              />

              <div className={clsx(styles.formUnderline)} />
              <div
                className="absolute top-[2.5rem] lg:top-[4.8rem] right-2 cursor-pointer"
                onClick={() => setShowConfirmPassword(!showConfirmPassword)}
              >
                {!showConfirmPassword ? (
                  <AiOutlineEyeInvisible className="text-[#239f40]" />
                ) : (
                  <AiOutlineEye className="text-[#239f40]" />
                )}
              </div>
            </div>
            {!!errors?.confirmPassword ? (
              errors?.confirmPassword?.type === 'validate' ? (
                <ErrorMessage>{errors?.confirmPassword?.message}</ErrorMessage>
              ) : (
                <ErrorMessage>Vui lòng không để trống</ErrorMessage>
              )
            ) : (
              <></>
            )}
          </div>

          <div className={clsx(styles.saveBtnContainer)}>
            <ButtonMain
              className={`mt-[30px] w-[130px] h-[44px] ${
                isLoadingDataChangeCustomerPassword ? '!bg-[#ddd]' : ''
              }`}
              loading={isLoadingDataChangeCustomerPassword}
              background="green"
              type="submit"
              onClick={handleSubmit(onSubmit)}
            >
              {!isLoadingDataChangeCustomerPassword ? 'Lưu thay đổi' : ''}
            </ButtonMain>
          </div>

          <img
            className={clsx(styles.backgroundImg, styles.cityBg)}
            src="/icons/city-background.svg"
            alt=""
          />
          <img className={clsx(styles.backgroundImg)} src="/image/change-pw-bg.png" alt="" />
          <img className={clsx(styles.backgroundImg)} src="/image/otp-phone.png" alt="" />
        </div>
      </form>
    </div>
  )
}
