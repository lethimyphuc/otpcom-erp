import { yupResolver } from '@hookform/resolvers/yup'
import { Button } from 'antd'
import { Controller, useForm } from 'react-hook-form'
import { useMutation, useQuery } from 'react-query'
import * as Yup from 'yup'
import { CREATE_CUSTOMER_ADDRESS, UPDATE_CUSTOMER_ADDRESS } from '~src/api'
import { GET_CITY, GET_DISTRICT, GET_WARD } from '~src/api/address'
import { graphQLClient } from '~src/api/graphql'
import { Select, TextField } from '~src/components/global'
import { regex } from '~src/config/appConfig'
import { CreateAddressForm, CreateAddressReq } from '~src/types/authenticate'
import { UserAddress } from '~src/types/user'

type CreateAddressProps = {
  defaultValues?: Partial<UserAddress>
  onSuccess?: () => void
}

export const CreateAddress = ({ onSuccess, defaultValues }: CreateAddressProps) => {
  const { control, handleSubmit, getValues, watch, setValue } =
    useForm<CreateAddressForm>({
      mode: 'all',
      resolver: yupResolver(schema),
      defaultValues: defaultValues
        ? {
            firstname: defaultValues?.firstname || undefined,
            lastname: defaultValues?.lastname || undefined,
            district: defaultValues?.district,
            street: defaultValues?.street?.[0] || '',
            telephone: defaultValues?.telephone,
            city: defaultValues?.city,
            ward: defaultValues?.ward
          }
        : undefined
    })

  const { isLoading, mutate } = useMutation<any, any, CreateAddressReq>({
    mutationFn: async (data) => {
      return await graphQLClient.request(
        defaultValues?.id ? UPDATE_CUSTOMER_ADDRESS : CREATE_CUSTOMER_ADDRESS,
        {
          id: defaultValues?.id,
          input: data
        }
      )
    },
    onSuccess
  })

  const { data: dataCity, isFetching: isFetchingCity } = useQuery({
    queryKey: ['getCity'],
    queryFn: async () => {
      const data = (await graphQLClient.request(GET_CITY, {
        locationType: 'CITY'
      })) as {
        Locations: {
          items: TCity[]
        }
      }
      return data?.Locations?.items
    }
  })

  const { data: dataDistrict, isFetching: isFetchingDistrict } = useQuery({
    queryKey: ['getDistrict', watch('city')],
    queryFn: async () => {
      const parentId = dataCity?.find((item) => item.name === watch('city'))?.id
      if (!parentId) return null

      const data = (await graphQLClient.request(GET_DISTRICT, {
        parentId,
        locationType: 'DISTRICT'
      })) as {
        Locations: {
          items: TCity[]
        }
      }
      return data?.Locations?.items
    },
    enabled: !!watch('city')
  })

  const { data: dataWard, isFetching: isFetchingWard } = useQuery({
    queryKey: ['getWard', watch('district')],
    queryFn: async () => {
      const parentId = dataDistrict?.find((item) => item.name === watch('district'))?.id
      if (!parentId) return null

      const data = (await graphQLClient.request(GET_WARD, {
        parentId,
        locationType: 'WARD'
      })) as {
        Locations: {
          items: TCity[]
        }
      }
      return data?.Locations?.items
    },
    enabled: !!watch('district')
  })

  const onSubmitHandler = handleSubmit((data) => {
    mutate({
      firstname: data.firstname,
      lastname: data.lastname,
      telephone: data.telephone,
      city: data?.city,
      ward: data?.ward,
      district: data?.district,
      country_id: 'VI',
      postcode: '70000',
      street: data?.street ? [data.street] : []
    })
  }, console.log)

  return (
    <section className={`${isLoading ? 'pointer-events-none' : ''} shippingInfo`}>
      <div className="mb-[24px]">
        <div className="mb-[24px]">
          <TextField
            required
            label="Số điện thoại"
            name="telephone"
            control={control}
            placeholder="Số điện thoại"
          />
        </div>
        <div className="grid gap-x-[12px] gap-y-[24px] sm:grid-cols-2 mb-[24px]">
          <div className="flex-1">
            <TextField
              required
              label="Họ"
              name="firstname"
              control={control}
              placeholder="Họ"
            />
          </div>
          <div className="flex-1">
            <TextField
              required
              label="Tên"
              name="lastname"
              control={control}
              placeholder="Tên"
            />
          </div>
        </div>

        <div className="mb-[24px]">
          <Controller
            name="city"
            control={control}
            rules={{ required: true }}
            render={({ field: { onChange, value }, fieldState: { error } }) => (
              <Select
                required
                error={error}
                label="Tỉnh / Thành phố"
                loading={isFetchingCity}
                value={value}
                placeholder="Chọn Tỉnh / Thành phố"
                options={dataCity?.map((city: TCity) => ({
                  value: city?.name,
                  label: city?.name
                }))}
                onChange={(id: any) => {
                  if (id !== value) {
                    onChange(id)
                    if (getValues('district')) {
                      setValue('district', null as any)
                    }
                    if (getValues('ward')) {
                      setValue('ward', null as any)
                    }
                  }
                }}
              />
            )}
          />
        </div>

        <div className="mb-[24px]">
          <Controller
            name="district"
            control={control}
            rules={{ required: true }}
            render={({ field: { onChange, value }, fieldState: { error } }) => (
              <Select
                required
                loading={isFetchingDistrict}
                label="Quận / Huyện"
                error={error}
                className={'formSelect'}
                placeholder="Chọn Quận / Huyện"
                value={value}
                options={dataDistrict?.map((district: TCity) => ({
                  value: district?.name,
                  label: district?.name
                }))}
                onChange={(id: any) => {
                  if (id !== value) {
                    onChange(id)
                    if (getValues('ward')) {
                      setValue('ward', null as any)
                    }
                  }
                }}
              />
            )}
          />
        </div>

        <div className="mb-[24px]">
          <Controller
            name="ward"
            control={control}
            rules={{ required: true }}
            render={({ field: { onChange, value }, fieldState: { error } }) => (
              <Select
                required
                loading={isFetchingWard}
                placeholder="Chọn Phường / Xã"
                label="Phường / Xã"
                value={value}
                error={error}
                onChange={onChange}
                options={dataWard?.map((ward: TCity) => ({
                  value: ward?.name,
                  label: ward?.name
                }))}
              />
            )}
          />
        </div>

        <div className="">
          <TextField
            name="street"
            required
            control={control}
            placeholder="Địa chỉ cụ thể"
            label="Địa chỉ cụ thể"
          />
        </div>
      </div>

      <div className="flex justify-end">
        <Button onClick={onSubmitHandler} title="Lưu" loading={isLoading}>
          Lưu
        </Button>
      </div>
    </section>
  )
}

const schema: Yup.ObjectSchema<CreateAddressForm> = Yup.object().shape({
  telephone: Yup.string()
    .matches(regex.PHONE, 'Vui lòng nhập đúng định dạng số điện thoại')
    .required('Vui lòng nhập trường này'),
  lastname: Yup.string().required('Vui lòng nhập trường này'),
  firstname: Yup.string().required('Vui lòng nhập trường này'),
  city: Yup.string().required('Vui lòng nhập trường này'),
  district: Yup.string().required('Vui lòng nhập trường này'),
  ward: Yup.string().required('Vui lòng nhập trường này'),
  street: Yup.string().required('Vui lòng nhập trường này')
})
