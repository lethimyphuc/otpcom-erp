/* eslint-disable @next/next/no-img-element */
import { Empty, Skeleton, Spin } from 'antd'
import clsx from 'clsx'
import { format } from 'date-fns'
import { useRouter } from 'next/router'
import { useState } from 'react'
import { useQuery } from 'react-query'
import { GET_DETAIL_ORDER } from '~src/api'
import { graphQLClient } from '~src/api/graphql'
import { _format } from '~src/util'
import styles from '../AccountInfo.module.scss'
import { date } from 'yup'

const steps = [
  {
    name: 'Đơn hàng đã đặt',
    icon: '/icons/placed-order-step.svg',
    step: 0,
  },
  {
    name: 'Xác nhận thông tin',
    icon: '/icons/credit-card-step.svg',
    step: 1,
  },
  {
    name: 'Giao cho ĐVVC',
    icon: '/icons/delivery-step.svg',
    step: 2,
  },
  {
    name: 'Nhận được hàng',
    icon: '/icons/parcel-step.svg',
    step: 3,
  },
]

export default function OrderDetail() {
  const {
    query: { orderId },
  } = useRouter()

  const { data, isFetching } = useQuery<any>({
    queryKey: ['orderDetail', orderId],
    queryFn: async () => {
      const data: any = await graphQLClient.request(GET_DETAIL_ORDER, { orderId })
      return data?.orderDetailCustomer
    },
    onSuccess: (data) => {
      if (data?.state == 'new') {
        setCurrentStep(1)
      } else if (data?.state == 'processing') {
        setCurrentStep(2)
      } else if (data?.state == 'complete') {
        setCurrentStep(3)
      } else {
        setCurrentStep(0)
      }
    },
    cacheTime: Infinity,
    enabled: !!orderId,
  })
  const [currentStep, setCurrentStep] = useState(0)

  const getStatusStyle = (status: string) => {
    switch (status) {
      case 'Đợi duyệt':
        return '!bg-[#FFF2D6] !text-[#F6AF15]'
      case 'Đã hủy bỏ':
        return '!bg-[#FFDED6] !text-[#F63E15]'
      case 'Đang tiến hành':
        return '!bg-[#D8EAFD] !text-[#1582F6]'
      case 'Hoàn tất':
        return '!bg-[#E8F5EB] !text-[#239F40]'
      default:
        return ''
    }
  }

  const getColorCurrentStep = (step: number) => {
    if (step === currentStep) {
      return '/icons/history-red-step.svg'
    }
    if (step < currentStep) {
      return '/icons/history-green-step.svg'
    }
    if (step > currentStep) {
      return '/icons/history-step.svg'
    }
    return ''
  }

  if (isFetching) {
    return (
      <div className="p-[24px] flex items-center justify-center flex-1">
        <Spin />
      </div>
    )
  }
  if (!data?.id) {
    return (
      <div className="flex-1 items-center justify-center py-[24px]">
        <Empty description="Không tìm thấy đơn hàng này" />
      </div>
    )
  }
  const datetmp = new Date(data?.order_date)
  return (
    <div className={clsx(styles.accountBoxRight)}>
      <div className={clsx(styles.orderDetailTitle, ' gap-4')}>
        <h3>Chi tiết đơn hàng:</h3>
        <div className="flex justify-start gap-2 pl-9 xs:pl-0 ">
          <p>{data?.number}</p>
          <span className={`${getStatusStyle(data?.status)} font-bold`}>{data?.status}</span>
        </div>
      </div>

      <div className={clsx(styles.orderStepperContainer)}>
        <div className={clsx(styles.orderStepper)}>
          {steps.map((item, i) => (
            <div key={i} className={clsx(styles.step)}>
              <img src={getColorCurrentStep(item.step)} alt="" />
              <div
                className={clsx(
                  styles.stepInfo,
                  item.step === 0 ? styles.firstStep : '',
                  item.step <= currentStep ? styles.active : ''
                )}
              >
                <img src={item.icon} alt="" />
                <p>{item.name}</p>
                {item.step === 0 && (
                  <span>
                    {!!data?.order_date
                      ? format(new Date(datetmp.setHours(datetmp.getHours())), 'HH:mm dd/MM/yyyy')
                      : ''}
                  </span>
                )}
              </div>
            </div>
          ))}
        </div>
      </div>
      <div className={clsx(styles.infoItem)}>
        <p>Ngày đặt hàng:</p>
        <p>{!!data?.order_date ? format(new Date(data?.order_date), 'dd/MM/yyyy') : ''}</p>
      </div>
      <div className={clsx(styles.infoItem)}>
        <p>Lưu ý đơn hàng:</p>
        <p>{data?.shipping_address.customer_note}</p>
      </div>
      <div className={clsx(styles.orderListInfo)}>
        <div className={clsx(styles.leftInfo)}>
          <h4>Chi tiết đơn hàng</h4>
          {data?.items?.map((item: Record<string, any>, index: number) => (
            <div key={index} className={clsx(styles.orderItem)}>
              <div className={clsx(styles.orderItemLeft)}>
                {!!item?.product_image ? (
                  <img src={item?.product_image || ''} alt="" />
                ) : (
                  <Skeleton.Avatar
                    shape="square"
                    style={{
                      width: '8rem',
                      height: '8rem',
                      borderRadius: '1rem',
                    }}
                  />
                )}

                <div>
                  <p>{item?.product_name}</p>
                  <p>
                    {item?.quantity_ordered} x{' '}
                    {_format.getVND(item?.product_sale_price?.value || 0)}
                    {item?.product_sale_price?.currency || ' VNĐ'}
                  </p>
                </div>
              </div>
              <p className={clsx(styles.price)}>
                {_format.getVND(
                  (item?.product_sale_price?.value || 0) * (item?.quantity_ordered || 0)
                )}
                {item?.product_sale_price?.currency || ' VNĐ'}
              </p>
            </div>
          ))}
        </div>
        <div className={clsx(styles.rightInfo)}>
          <div className={clsx(styles.rightInfoItem)}>
            <p>Thành tiền</p>
            <p>
              {!!data?.total?.subtotal?.value ? _format.getVND(data?.total?.subtotal?.value) : 0}
              {data?.total?.subtotal?.currency || ' VNĐ'}
            </p>
          </div>

          <div className={clsx(styles.rightInfoItem, 'mt-[14px]')}>
            <p>Giảm giá</p>
            <p>
              {!!data?.total?.discounts && data?.total?.discounts?.length
                ? _format.getVND(data?.total?.discounts[0]?.amount?.value)
                : 0}
              {!!data?.total?.discounts && data?.total?.discounts?.length
                ? data?.total?.discounts[0]?.amount?.currency
                : ' VNĐ'}
            </p>
          </div>

          <div className={clsx(styles.rightInfoItem, 'mt-[14px]')}>
            <p>Vận chuyển</p>
            <p>
              {!!data?.total?.total_shipping?.value
                ? _format.getVND(data?.total?.total_shipping?.value)
                : 0}
              {data?.total?.total_shipping?.currency || ' VNĐ'}
            </p>
          </div>
          {/* <div className={clsx(styles.paymentMethod)}>
            <p>Hình thức thanh toán</p>
            <p>{data?.shipping_method}</p>
          </div> */}
          <div className={clsx(styles.paymentMethod)}>
            <p>Phương thức giao hàng</p>
            <p>{data?.shipping_method}</p>
          </div>
          <div className={clsx(styles.rightLine)}></div>
          <div className={clsx(styles.rightInfoItem, styles.totalOrder)}>
            <p>Tổng cộng</p>
            <p>
              {_format.getVND(data?.total?.grand_total?.value)}
              {data?.total?.grand_total?.currency || ' VNĐ'}
            </p>
          </div>
        </div>
      </div>
      {/* <div className={clsx(styles.btnContainer)}>
        <button>Hủy đơn hàng</button>
      </div> */}
    </div>
  )
}
