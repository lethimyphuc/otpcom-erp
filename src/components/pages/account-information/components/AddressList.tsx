/* eslint-disable react-hooks/exhaustive-deps */
import { Modal } from 'antd'
import { useRouter } from 'next/router'
import { useState } from 'react'
import { useQuery } from 'react-query'
import { INFO_CUSTOMER } from '~src/api'
import { graphQLClient } from '~src/api/graphql'
import { CreateAddress } from './CreateAddress'

export const AddressList = ({ userInfo }: { userInfo: any }) => {
  const router = useRouter()
  const {
    data: dataInfoCustomer,
    isLoading: isLoadingDataInfoCustomer,
    refetch
  } = useQuery({
    queryKey: ['getCustomerAddresses', router.query],
    queryFn: async () => {
      const data = (await graphQLClient.request(INFO_CUSTOMER, {
        customer: {
          orders: {
            filter: {
              number: {
                eq: !!router.query?.search ? router.query?.search : ''
              }
            },
            currentPage: !!router.query?.currentPage
              ? parseInt(`${router.query?.currentPage}`)
              : 1
          }
        }
      })) as {
        customer: any
      }
      return data?.customer
    },
    cacheTime: Infinity,
    enabled: false
  })

  const [visible, setVisible] = useState<boolean>(false)

  return (
    <section className="flex-1">
      <div className="flex items-center justify-between p-[16px]">
        <h3>Thông tin giao hàng</h3>
        <button onClick={() => setVisible(true)}>Thêm địa chỉ</button>
      </div>

      <Modal
        maskClosable={false}
        visible={visible}
        title={'Tạo địa chỉ'}
        footer={null}
        onCancel={() => setVisible(false)}
      >
        <CreateAddress onSuccess={() => setVisible(false)} />
      </Modal>

      <div className="">
        {/* {dataInfoCustomer?.customer?.addresses?.map((item: any) => (
          <div key={item.}></div>
        ))} */}
      </div>
    </section>
  )
}
