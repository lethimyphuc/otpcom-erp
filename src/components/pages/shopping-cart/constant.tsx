export const STEPS = [
  {
    name: 'Đăng nhập / đăng ký',
    icon: '/icons/user-cart-step.svg',
    step: 0,
  },
  {
    name: 'Giỏ hàng',
    icon: '/icons/shopping-cart-step.svg',
    step: 1,
  },
  {
    name: 'Thanh toán',
    icon: '/icons/credit-card-step.svg',
    step: 2,
  },
  {
    name: 'Xác nhận đơn hàng',
    icon: '/icons/phone-step.svg',
    step: 3,
  },
  {
    name: 'Giao hàng',
    icon: '/icons/delivery-step.svg',
    step: 4,
  },
]

export const COLOR_STEP = {
  verified: '/icons/history-green-step.svg',
  current: '/icons/history-red-step.svg',
  upcoming: '/icons/history-step.svg',
}
// export const COLOR_STEP = {
//   verified: "/icons/cart-green-step.svg",
//   current: "/icons/cart-red-step.svg",
//   upcoming: "/icons/cart-step.svg",
// };
