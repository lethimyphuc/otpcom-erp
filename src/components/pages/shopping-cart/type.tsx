export type productProps = {
  name: string;
  img: string;
  description: string;
  code: string;
  price: string | number;
  quantity: number;
};
