/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @next/next/no-img-element */
import { Select, Skeleton } from 'antd'
import clsx from 'clsx'
import Link from 'next/link'
import React, { useState } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { toast } from 'react-toastify'
import { INFO_CUSTOMER } from '~src/api'
import { GET_CITY, GET_DISTRICT, GET_WARD } from '~src/api/address'
import { CREATE_EMPTY_CART, SHIPPING_FEE } from '~src/api/cart'
import { graphQLClient } from '~src/api/graphql'
import { PLACE_ORDER_CUSTOMER, PLACE_ORDER_GUEST } from '~src/api/order'
import ErrorMessage from '~src/components/global/ErrorMessage'
import { _format } from '~src/util'
import { regex } from '~src/config/appConfig'
import styles from '../ShoppingCart.module.scss'
import request from 'graphql-request'

export interface IPaymentProps {
  dataCart?: TDataUpdateToCart
  setCurrentStep: React.Dispatch<React.SetStateAction<number>>
  setDataOrder: (data: any) => void
}

export default function Payment({ dataCart, setCurrentStep, setDataOrder }: IPaymentProps) {
  const queryClient = useQueryClient()

  const { data: infoCustomer } = useQuery({
    queryKey: ['getInfoCustomer'],
    queryFn: async () => {
      const data = (await graphQLClient.request(INFO_CUSTOMER)) as {
        customer: any
      }
      return data
    },
    cacheTime: Infinity,
    enabled: !!localStorage?.getItem('token'),
  })

  const {
    handleSubmit,
    setValue,
    watch,
    getValues,
    control,
    reset,
    formState: { errors },
  } = useForm<{
    list_address: number | any
    city: string | null
    region: string | null
    ward: string | null
    country_code: string
    firstname: string
    lastname: string
    email: string
    street: string
    telephone: string
    save_in_address_book: boolean
    postcode?: string
    customer_notes: string
    payment_method: null | TMethodPayment
    shipping_method: null | TMethodShipping
  }>({
    defaultValues: {
      list_address: null,
      city: null,
      country_code: '',
      firstname: '',
      lastname: '',
      email: '',
      street: '',
      telephone: '',
      region: null,
      save_in_address_book: true,
      postcode: '70000',
      customer_notes: '',
      payment_method: null,
      shipping_method: null,
    },
  })
  const [shipping, setShipping] = useState<{
    district: string
    city: string
  }>()
  const [feeShipping, setFeeShipping] = useState<any>({})
  const { data: dataCity, isLoading: isLoadingDataCity } = useQuery({
    queryKey: ['getCity'],
    queryFn: async () => {
      const data = (await graphQLClient.request(GET_CITY, {
        locationType: 'CITY',
      })) as {
        Locations: {
          items: TCity[]
        }
      }
      return data?.Locations?.items
    },
  })

  const { data: dataDistrict, isLoading: isLoadingDataDistrict } = useQuery({
    queryKey: ['getDistrict', watch('city')],
    queryFn: async () => {
      const parentId = dataCity?.find((item) => item.name === watch('city'))?.id
      if (!parentId) return null

      const data = (await graphQLClient.request(GET_DISTRICT, {
        parentId,
        locationType: 'DISTRICT',
      })) as {
        Locations: {
          items: TCity[]
        }
      }
      return data?.Locations?.items
    },
    enabled: !!watch('city'),
  })

  const { data: dataWard, isFetching: isFetchingWard } = useQuery({
    queryKey: ['getWard', watch('region')],
    queryFn: async () => {
      const parentId = dataDistrict?.find((item: any) => item.name === watch('region'))?.id
      if (!parentId) return null

      const data = (await graphQLClient.request(GET_WARD, {
        parentId,
        locationType: 'WARD',
      })) as {
        Locations: {
          items: TCity[]
        }
      }
      return data?.Locations?.items
    },
    enabled: !!watch('region'),
  })

  // const { data: dataShipping, isLoading: isLoadingDataShipping } = useQuery({
  //   queryKey: ['getShipping', watch('region')],
  //   queryFn: async () => {
  //     const data: any = await graphQLClient.request(SHIPPING_FEE, {
  //       quoteId: localStorage?.getItem('cartId'),
  //       toCity: shipping?.city,
  //       toDistrict: shipping?.district,
  //       shippingType: 'quote',
  //     })
  //     return data?.ShippingFee || null
  //   },
  //   enabled: !!watch('city') && !!watch('region'),
  // })
  const {
    data: dataShipping,
    isLoading: isLoadingDataShipping,
    status: shippingStatus,
  } = useQuery({
    queryKey: ['getShipping', watch('region')],
    queryFn: async () => {
      const data: any = await request(`${process.env.NEXT_PUBLIC_API_URL}`, SHIPPING_FEE, {
        quoteId: localStorage?.getItem('cartId'),
        toCity: shipping?.city,
        toDistrict: shipping?.district,
        shippingType: 'quote',
      })
      return data?.ShippingFee || null
    },
    retry: 2,
    onError: () => {
      setFeeShipping({ shipFee: 0 })
    },

    enabled: !!watch('city') && !!watch('region'),
  })

  const createEmptyCart = useMutation({
    mutationKey: 'createEmptyCart',
    mutationFn: async () => {
      return await graphQLClient.request(CREATE_EMPTY_CART, {
        input: {},
      })
    },
    onSuccess: (response: any) => {
      localStorage.setItem('cartId', response?.createEmptyCart)
    },
  })

  const { mutateAsync, isLoading } = useMutation({
    mutationKey: 'createOrder',
    mutationFn: async (data: any) => {
      if (data?.isLogin) {
        return await graphQLClient.request(PLACE_ORDER_CUSTOMER, data)
      }
      return await graphQLClient.request(PLACE_ORDER_GUEST, data)
    },
    onSuccess: (response: any) => {
      setDataOrder(response)
      createEmptyCart.mutateAsync().then(() => {
        queryClient.invalidateQueries(['getCartId'])
      })

      setCurrentStep((prev) => prev + 1)
    },
    onError: () => {
      setCurrentStep((prev) => prev)
    },
  })

  const onSubmitFormOrder = async (data: {
    city: string | null
    country_code: string
    firstname: string
    lastname: string
    street: string
    telephone: string
    region: string | null
    save_in_address_book: boolean
    postcode?: string
    customer_notes: string
    payment_method: null | TMethodPayment
    shipping_method: null | TMethodShipping
    save_form?: boolean
    email?: string
  }) => {
    if (!data?.shipping_method && !!dataShipping) {
      toast.error('Vui lòng chọn phương thức vận chuyển')
      return
    }
    if (!data?.payment_method) {
      toast.error('Vui lòng chọn phương thức thanh toán')
      return
    }

    const DATA_SUBMIT_SET_GUEST_EMAIL = {
      email: !!data?.email ? `${data?.email}`.trim() : '',
      cart_id: localStorage?.getItem('cartId'),
    }
    const DATA_SUBMIT_SHIPPING_ADDRESS = {
      address: {
        city: data?.city || '',
        region: data?.region || '',
        country_code: data?.country_code || 'vi',
        firstname: !!data?.firstname ? `${data?.firstname}`.trim() : '',
        lastname: !!data?.lastname ? `${data?.lastname}`.trim() : '',
        street: [`${data?.street}`],
        telephone: !!data?.telephone ? `${data?.telephone}`.trim() : '',
        postcode: data?.postcode || '',
        save_in_address_book: false,
        company: '',
        ward: '',
        district: shipping?.district,
        carrier_service_id: feeShipping?.serviceId,
        carrier_id: feeShipping?.carrierId,
      },
      customer_notes: !!data?.customer_notes ? `${data?.customer_notes}`.trim() : '',
    }

    await mutateAsync({
      isLogin: !!infoCustomer,
      inputSetGuestEmailOnCart: DATA_SUBMIT_SET_GUEST_EMAIL,
      inputSetShippingAddresses: {
        cart_id: localStorage?.getItem('cartId'),
        shipping_addresses: [{ ...DATA_SUBMIT_SHIPPING_ADDRESS }],
      },
      inputSetBillingAddress: {
        cart_id: localStorage?.getItem('cartId'),
        billing_address: {
          same_as_shipping: true,
        },
      },
      inputSetShippingMethods: {
        cart_id: localStorage?.getItem('cartId'),
        shipping_methods: [
          {
            carrier_code:
              DATA_SUBMIT_SHIPPING_ADDRESS?.address?.carrier_id &&
              DATA_SUBMIT_SHIPPING_ADDRESS?.address?.carrier_service_id
                ? 'shipping'
                : 'freeshipping',
            method_code:
              DATA_SUBMIT_SHIPPING_ADDRESS?.address?.carrier_id &&
              DATA_SUBMIT_SHIPPING_ADDRESS?.address?.carrier_service_id
                ? 'shipping'
                : 'freeshipping',
          },
        ],
      },
      inputSetPaymentMethod: {
        cart_id: localStorage?.getItem('cartId'),
        payment_method: {
          code: data?.payment_method?.code,
        },
      },
      inputPlaceOrder: {
        cart_id: localStorage?.getItem('cartId'),
      },
    })

    // to reset cart quantity purpose
    //
  }

  return (
    <div className={clsx(styles.cartDetail)}>
      <div className={clsx(styles.leftSection, styles.paymentLeftSection)}>
        <div className={clsx(styles.leftSectionTitle)}>
          <div>
            <h1>Thanh toán</h1>
          </div>
        </div>
        <form onSubmit={handleSubmit(onSubmitFormOrder)}>
          <div className={clsx(styles.shippingInfo)}>
            <div className="flex justify-between">
              <h3>Thông tin giao hàng</h3>
              {!!infoCustomer && (
                <div className="w-[50%] border-b">
                  <Controller
                    name="list_address"
                    control={control}
                    rules={{
                      required: false,
                    }}
                    render={({ field }) => (
                      <Select
                        {...field}
                        onChange={(value: string) => {
                          field?.onChange(value)
                          const itemAddress = infoCustomer?.customer?.addresses?.find(
                            (item: any) => item?.id == value
                          )

                          if (!itemAddress) return

                          reset({
                            list_address: value,
                            city: itemAddress?.city,
                            country_code: itemAddress?.country_code,
                            firstname: itemAddress?.firstname,
                            lastname: itemAddress?.lastname,
                            email: infoCustomer?.customer?.email,
                            street: itemAddress?.street[0],
                            telephone: itemAddress?.telephone,
                            region: itemAddress?.district,
                            ward: itemAddress?.ward,
                            save_in_address_book: false,
                            postcode: itemAddress?.postcode,
                            customer_notes: '',
                            payment_method: null,
                            shipping_method: null,
                          })

                          setShipping({
                            district: itemAddress?.district,
                            city: itemAddress?.city,
                          })
                        }}
                        className={'formSelect'}
                        options={infoCustomer?.customer?.addresses?.map((item: any) => {
                          return {
                            value: item?.id,
                            label: `${item?.street?.[0] || ''}, ${item?.ward || ''}, ${
                              item?.district || ''
                            }, ${item?.city || ''}`,
                          }
                        })}
                        placeholder="Địa chỉ nhận hàng"
                        allowClear
                      />
                    )}
                  />
                </div>
              )}
            </div>
            <div className={clsx(styles.shippingInfoRow, 'mt-[20px]')}>
              <div className="w-[50%]">
                <div className={clsx(styles.formItem)}>
                  <p>
                    Họ <span>*</span>
                  </p>
                  <Controller
                    name="lastname"
                    control={control}
                    rules={{
                      required: true,
                    }}
                    render={({ field }) => (
                      <input
                        {...field}
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                          field.onChange(event?.target?.value)
                        }}
                        className={clsx(styles.formInput)}
                        type="text"
                        name=""
                        id=""
                        placeholder="Họ của bạn"
                      />
                    )}
                  />

                  <div className={clsx(styles.underline)}></div>
                </div>
                <div className={clsx(!!errors?.lastname ? 'block' : 'hidden')}>
                  <ErrorMessage>Vui lòng không để trống</ErrorMessage>
                </div>
              </div>

              <div className="w-[50%]">
                <div className={clsx(styles.formItem)}>
                  <p>
                    Tên <span>*</span>
                  </p>
                  <Controller
                    name="firstname"
                    control={control}
                    rules={{
                      required: true,
                    }}
                    render={({ field }) => (
                      <input
                        {...field}
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                          field.onChange(event?.target?.value)
                        }}
                        className={clsx(styles.formInput)}
                        type="text"
                        name=""
                        id=""
                        placeholder="Tên của bạn"
                      />
                    )}
                  />
                  <div className={clsx(styles.underline)}></div>
                </div>

                <div className={clsx(!!errors?.firstname ? 'block' : 'hidden')}>
                  <ErrorMessage>Vui lòng không để trống</ErrorMessage>
                </div>
              </div>
            </div>
            <div className={clsx(styles.shippingInfoRow, 'mt-[30px]')}>
              <Controller
                name="telephone"
                control={control}
                rules={{
                  required: true,
                  pattern: {
                    value: regex.PHONE,
                    message: 'Vui lòng nhập đúng định dạng SĐT',
                  },
                }}
                render={({ field, fieldState: { error } }) => (
                  <div className="w-[50%]">
                    <div className={clsx(styles.formItem)}>
                      <p>
                        Số điện thoại <span>*</span>
                      </p>
                      <input
                        {...field}
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                          field.onChange(event?.target?.value)
                        }}
                        className={clsx(styles.formInput)}
                        type="tel"
                        name=""
                        id=""
                        placeholder="Số điện thoại của bạn"
                      />
                      <div className={clsx(styles.underline)}></div>
                    </div>
                    <div className={clsx(!!errors?.telephone ? 'block' : 'hidden')}>
                      <ErrorMessage>{error?.message || 'Vui lòng không để trống'}</ErrorMessage>
                    </div>
                  </div>
                )}
              />

              <Controller
                name="email"
                control={control}
                rules={{
                  required: true,
                  pattern: {
                    value: regex.EMAIL,
                    message: 'Vui lòng nhập đúng định dạng email',
                  },
                }}
                render={({ field, fieldState: { error } }) => (
                  <div className="w-[50%]">
                    <div className={clsx(styles.formItem)}>
                      <p>
                        Email <span>*</span>
                      </p>
                      <input
                        {...field}
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                          field.onChange(event?.target?.value)
                        }}
                        className={clsx(styles.formInput)}
                        type="text"
                        name=""
                        id=""
                        placeholder="Email của bạn"
                      />
                      <div className={clsx(styles.underline)}></div>
                    </div>
                    <div className={clsx(!!errors?.email ? 'block' : 'hidden')}>
                      <ErrorMessage>{error?.message || 'Vui lòng không để trống'}</ErrorMessage>
                    </div>
                  </div>
                )}
              />
            </div>
            <div className="mt-[30px] grid sm:grid-cols-3 gap-y-[30px] gap-x-[12px]">
              <div className="">
                <div
                  className={clsx(styles.formItem, 'border-b-[1px] border-b-[#ccc] border-solid')}
                >
                  <p className="text-[16px] font-bold text-[#2C0709]">
                    Tỉnh / Thành phố <span className="text-[#C82127]">*</span>
                  </p>
                  <Controller
                    name="city"
                    control={control}
                    rules={{
                      required: true,
                    }}
                    render={({ field: { onChange, value } }) => (
                      <Select
                        value={value}
                        onChange={(value: string) => {
                          onChange(value)
                          setValue('region', null)
                          setValue('ward', null)
                          if (feeShipping) {
                            setFeeShipping({})
                          }
                        }}
                        className={'formSelect'}
                        options={dataCity?.map((city: TCity) => {
                          return {
                            value: city?.name,
                            label: city?.name,
                          }
                        })}
                        placeholder="Chọn tỉnh / thành phố"
                        allowClear
                      />
                    )}
                  />

                  <div className={clsx(styles.underline, 'underline')}></div>
                </div>
                <div className={clsx(!!errors?.city ? 'block' : 'hidden')}>
                  <ErrorMessage>Vui lòng không để trống</ErrorMessage>
                </div>
              </div>
              <div className="">
                <div
                  className={clsx(styles.formItem, 'border-b-[1px] border-b-[#ccc] border-solid')}
                >
                  <p className="text-[16px] font-bold text-[#2C0709]">
                    Quận / Huyện <span className="text-[#C82127]">*</span>
                  </p>

                  <Controller
                    name="region"
                    control={control}
                    rules={{
                      required: true,
                    }}
                    render={({ field: { onChange, value } }) => (
                      <Select
                        value={value}
                        onChange={(value: string) => {
                          onChange(value)
                          setValue('ward', null)
                          if (feeShipping) {
                            setFeeShipping({})
                          }

                          const district: any = dataDistrict?.find(
                            (district: TCity) => district?.name == value
                          )?.name
                          const city: any = dataCity?.find(
                            (district: TCity) => district?.name == watch('city')
                          )?.name
                          if (district && city) {
                            setShipping({ city, district })
                          }
                        }}
                        className={'formSelect'}
                        options={dataDistrict?.map((district: TCity) => {
                          return {
                            value: district?.name,
                            label: district?.name,
                          }
                        })}
                        placeholder="Chọn quận / huyện"
                        allowClear
                      />
                    )}
                  />

                  <div className={clsx(styles.underline, 'underline')}></div>
                </div>
                <div className={clsx(!!errors?.region ? 'block' : 'hidden')}>
                  <ErrorMessage>Vui lòng không để trống</ErrorMessage>
                </div>
              </div>
              <div className="">
                <div
                  className={clsx(styles.formItem, 'border-b-[1px] border-b-[#ccc] border-solid')}
                >
                  <p className="text-[16px] font-bold text-[#2C0709]">
                    Xã / Phường <span className="text-[#C82127]">*</span>
                  </p>
                  <Controller
                    name="ward"
                    control={control}
                    rules={{
                      required: true,
                    }}
                    render={({ field: { onChange, value } }) => (
                      <Select
                        value={value}
                        onChange={onChange}
                        className={'formSelect'}
                        options={dataWard?.map((city: TCity) => {
                          return {
                            value: city?.name,
                            label: city?.name,
                          }
                        })}
                        placeholder="Chọn Xã / Phường"
                        allowClear
                      />
                    )}
                  />

                  <div className={clsx(styles.underline, 'underline')}></div>
                </div>
                <div className={clsx(!!errors?.ward ? 'block' : 'hidden')}>
                  <ErrorMessage>Vui lòng không để trống</ErrorMessage>
                </div>
              </div>
            </div>
            <div className={clsx(styles.shippingInfoRow, styles.shippingInfoCol, 'mt-[30px]')}>
              {/* <div className="w-[50%]">
                <div className={clsx(styles.formItem)}>
                  <p>
                    Mã bưu chính <span>*</span>
                  </p>
                  <Controller
                    name="postcode"
                    control={control}
                    render={({ field }) => (
                      <input
                        {...field}
                        onChange={(
                          event: React.ChangeEvent<HTMLInputElement>
                        ) => {
                          field.onChange(event?.target?.value);
                        }}
                        className={clsx(styles.formInput)}
                        type="text"
                        name=""
                        id=""
                        placeholder="Mã bưu chính"
                        readOnly
                      />
                    )}
                  />
                  <div className={clsx(styles.underline)}></div>
                </div>
              </div> */}

              <div className="w-[100%]">
                <p>
                  Địa chỉ <span>*</span>
                </p>
                <Controller
                  name="street"
                  control={control}
                  rules={{
                    required: true,
                  }}
                  render={({ field }) => (
                    <input
                      {...field}
                      onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                        field.onChange(event?.target?.value)
                      }}
                      className={clsx(styles.formInput)}
                      type="text"
                      name=""
                      id=""
                      placeholder="Địa chỉ của bạn"
                    />
                  )}
                />

                <div className={clsx(styles.underline)}></div>
              </div>
            </div>
            <div className={clsx(!!errors?.street ? 'block' : 'hidden')}>
              <ErrorMessage>Vui lòng không để trống</ErrorMessage>
            </div>

            <div className="flex items-center justify-between mt-3 w-full">
              <div className={clsx(styles.register)}>
                {!infoCustomer && (
                  <p>
                    Bạn chưa có tài khoản?{' '}
                    <Link href={{ pathname: '/login' }}>
                      <a className="cursor-pointer">Tạo ngay</a>
                    </Link>
                  </p>
                )}
              </div>
              {/* <div className="relative">
                {!!dataCart?.cart?.shipping_addresses[0] &&
                dataCart?.cart?.shipping_addresses[0]
                  ?.available_shipping_methods?.length ? (
                  <></>
                ) : (
                  <ButtonMain
                    loading={isLoadingSetInfoOrder}
                    background="green"
                    type="button"
                    onClick={() =>
                      handleSubmit((data) =>
                        onSubmitFormOrder({ ...data, save_form: true })
                      )()
                    }
                    className="min-w-[135px] right-0"
                  >
                    Lưu thông tin
                  </ButtonMain>
                )}
              </div> */}
            </div>
          </div>
          <div className={clsx(styles.additionalInfo)}>
            <h3>Thông tin bổ sung</h3>
            <div>
              <p>Lưu ý cho đơn hàng</p>
              <Controller
                name="customer_notes"
                control={control}
                render={({ field }) => (
                  <textarea
                    {...field}
                    name=""
                    id=""
                    placeholder="Ghi chú về đơn hàng, ví dụ: thời gian hay chỉ dẫn địa điểm giao hàng chi tiết hơn"
                  ></textarea>
                )}
              />

              <div className={clsx(styles.underline)}></div>
            </div>
          </div>
          <div className={clsx(styles.paymentMethod)}>
            <h3>Phương thức vận chuyển</h3>
            <div className={clsx(styles.formRadio)}>
              {isLoadingDataShipping ? (
                <div className="w-full">
                  <Skeleton.Input active />
                </div>
              ) : (
                <>
                  {!!dataShipping ? (
                    dataShipping?.items?.map((method: TMethodShipping) => {
                      return (
                        <label
                          key={`${method?.serviceName}`}
                          className={clsx(styles.radioItem)}
                          htmlFor={`${method?.serviceName}`}
                          onClick={() => setFeeShipping(method)}
                        >
                          <input
                            type="radio"
                            name="shipping_method"
                            id={`${method?.serviceName}`}
                            value={watch('shipping_method')?.serviceName}
                            onClick={() => {
                              console.log('SHIPPING_METHOD: ', method)
                              setValue('shipping_method', method)
                            }}
                          />
                          <div className="grid ml-[12px]">
                            <p className="mb-[8px]">{method?.carrierName}</p>
                            <span>{_format.getVND(method?.shipFee, ' đ')}</span>
                            <img src={method?.logo} alt="" />
                          </div>
                        </label>
                      )
                    })
                  ) : dataShipping === null ? (
                    <p>Phí vận chuyển đang được cập nhật...</p>
                  ) : shippingStatus == 'error' ? (
                    <p> Liên hệ phương thức vận chuyển sau.</p>
                  ) : (
                    <p>Vui lòng chọn địa chỉ nhận hàng</p>
                  )}
                </>
              )}
            </div>
          </div>
          <div className={clsx(styles.paymentMethod)}>
            <h3>Phương thức thanh toán</h3>
            <div className={clsx(styles.formRadio)}>
              {dataCart?.cart?.available_payment_methods.map((method: TMethodPayment) => {
                return (
                  <label
                    key={`${method?.code}`}
                    className={clsx(styles.radioItem)}
                    htmlFor={`${method?.code}`}
                  >
                    <input
                      name="payment_method"
                      type="radio"
                      id={`${method?.code}`}
                      onClick={() => {
                        setValue('payment_method', method)
                      }}
                    />
                    <div className="grid ml-4">
                      <p>{method?.title}</p>
                      <img src={'/icons/bank-method.svg'} alt="" />
                    </div>
                  </label>
                )
              })}
            </div>
          </div>
        </form>
      </div>
      <div className={clsx(styles.rightSection, styles.paymentRightSection)}>
        <div className={`${clsx(styles.summaryContainer)} xs:!pt-0 mt-20 lg:mt-0 px-10 xs:p-20`}>
          <h3 className="mb-6 sm:mb-10">Đơn hàng của bạn</h3>
          <div className={clsx(styles.totalProduct)}>
            <p>Sản phẩm</p>
            <span>{dataCart?.cart?.items?.length}</span>
          </div>
          {/* <div className="grid sm:grid-cols-2 gap-4 md:grid-cols-1"> */}
          <div className="grid sm:grid-cols-2 gap-8 mt-4 sm:mt-8 lg:grid-cols-1 px-6">
            {dataCart?.cart?.items?.map((item: any, index: number) => (
              <div
                className={clsx(styles.paymentProductItem, 'flex items-center h-full  ')}
                key={index}
              >
                <div className={clsx(styles.product, 'h-full')}>
                  <img className={clsx(styles.itemImage)} src={item?.product?.image?.url} alt="" />
                  <div className={clsx(styles.itemInfo, 'flex flex-col justify-around h-full')}>
                    <p className={`${clsx(styles.itemTitle)} `}>{item?.product?.name}</p>
                    <p>
                      {item?.quantity} x {_format.getVND(item?.prices?.price?.value || 0)}
                      {item?.product?.price_range?.minimum_price?.final_price?.currency || ' VNĐ'}
                    </p>
                  </div>
                </div>
              </div>
            ))}
          </div>
          <div className={clsx(styles.summaryInfo, styles.cartPrice, 'mt-[30px] flex')}>
            <p>Thành tiền</p>
            <p>
              {_format.getVND(dataCart?.cart?.prices?.subtotal_excluding_tax?.value || 0)}
              {dataCart?.cart?.prices?.subtotal_excluding_tax?.currency || ' VNĐ'}
            </p>
          </div>
          <div className={clsx(styles.summaryInfo, styles.shipping, 'flex')}>
            <p>Vận chuyển</p>
            <p>
              {feeShipping?.shipFee == 0
                ? 'Liên hệ phí vận chuyển sau'
                : _format.getVND(feeShipping?.shipFee, ' đ')}
            </p>
          </div>
          <div className={clsx(styles.summaryInfo, 'flex')}>
            <p>Mã giảm giá</p>
            {dataCart?.cart?.prices?.discounts?.map((dicount) => {
              return (
                <>
                  <p>{dicount?.label}</p>
                  <p className={'text-[#C82127] font-bold'}>
                    {_format.getVND(dicount?.amount?.value || 0)}
                    {dicount?.amount?.currency || ' VNĐ'}
                  </p>
                </>
              )
            })}
          </div>
          <div className={clsx(styles.underline)}></div>
          <div className={clsx(styles.summaryInfo, styles.total, 'flex')}>
            <p>Tổng cộng</p>
            <p>
              {_format.getVND(
                parseInt(dataCart?.cart?.prices?.grand_total?.value as any) +
                  parseInt(feeShipping?.shipFee || '0')
              )}
              {dataCart?.cart?.prices?.grand_total?.currency || ' VNĐ'}
            </p>
          </div>
          <button
            className={clsx(
              styles.orderButton,
              isLoading ? '!bg-[#bbb] pointer-events-none cursor-none' : ''
            )}
            onClick={() => {
              handleSubmit(onSubmitFormOrder)()
              // setCurrentStep((prev) => prev + 1)
            }}
          >
            {isLoading ? (
              <img className={'keyframe-rotate'} src="/icons/refresh-cart.svg" alt="" />
            ) : (
              <>
                <img src={'/icons/cart-icon.svg'} alt="" />
                <p>Mua ngay</p>
              </>
            )}
          </button>
          <div className={clsx(styles.comeback)} onClick={() => setCurrentStep((prev) => prev - 1)}>
            <img src="/icons/prev.svg" alt="" />
            <p>Quay lại</p>
          </div>
        </div>
      </div>
    </div>
  )
}
