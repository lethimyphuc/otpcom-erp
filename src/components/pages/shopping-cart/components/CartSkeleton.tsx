/* eslint-disable @next/next/no-img-element */
import { Skeleton } from 'antd'
import clsx from 'clsx'
import React from 'react'
import styles from '../ShoppingCart.module.scss'

const CartSkeleton = ({ isFetchingDataCart }: any) => {
  return (
    <div className="lg:flex w-full justify-center gap-20">
      <div className={clsx(styles.leftSection, styles.cartLeftSection)}>
        <div className={`${clsx(styles.leftSectionTitle)} flex-col xs:flex-row`}>
          <div>
            <h1 className="!text-[24px] lg:!text-[48px]">Giỏ hàng</h1>
            <span></span>
          </div>
          <button className="justify-center items-center">
            <div>
              <img
                className={isFetchingDataCart ? 'keyframe-rotate' : ''}
                src="/icons/refresh-cart.svg"
                alt=""
              />
              <p className="!text-[12px] lg:!text-[16px]">Cập nhật giỏ hàng</p>
            </div>
          </button>
        </div>
        <div className={clsx(styles.cartInfo)}>
          <div className={clsx(styles.cartInfoHead, '!hidden xs:!flex')}>
            <div>
              <p>Sản phẩm</p>
            </div>
            <div>
              <p>Số lượng</p>
            </div>
            <div>
              <p>Giá tiền</p>
            </div>
          </div>
          <div className="mt-6 gap-10 flex flex-col">
            <div className="flex gap-6 sm:gap-14 border-b-[1px] border-gray20 pb-6 md:pb-12">
              <button className="text-[#adabab] focus:!text-red ml-4">
                <i className="fas fa-times-circle"></i>
              </button>
              <div className="flex items-center">
                <Skeleton.Image active className="hidden sm:block"></Skeleton.Image>
              </div>
              <Skeleton active paragraph={{ rows: 2 }}></Skeleton>
            </div>
            <div className="flex gap-6 sm:gap-14 border-b-[1px] border-gray20 pb-6 md:pb-12">
              <button className="text-[#adabab] focus:!text-red ml-4">
                <i className="fas fa-times-circle"></i>
              </button>
              <div className="flex items-center">
                <Skeleton.Image active className="hidden sm:block"></Skeleton.Image>
              </div>
              <Skeleton active paragraph={{ rows: 2 }}></Skeleton>
            </div>{' '}
            <div className="flex gap-6 sm:gap-14 border-b-[1px] border-gray20 pb-6 md:pb-12">
              <button className="text-[#adabab] focus:!text-red ml-4">
                <i className="fas fa-times-circle"></i>
              </button>
              <div className="flex items-center">
                <Skeleton.Image active className="hidden sm:block"></Skeleton.Image>
              </div>
              <Skeleton active paragraph={{ rows: 2 }}></Skeleton>
            </div>
          </div>
        </div>
      </div>
      <div className={clsx(styles.rightSection, styles.cartRightSection)}>
        <div className={clsx(styles.summaryContainer)}>
          <h3>Tóm tắt đơn hàng</h3>
          <div className={clsx(styles.summaryInfo, styles.cartPrice, 'flex')}>
            <div className="flex items-start">
              <Skeleton.Input></Skeleton.Input>
              <Skeleton.Input></Skeleton.Input>
            </div>
          </div>
          <div className={clsx(styles.summaryInfo, styles.shipping, 'flex')}>
            <div className="flex items-start">
              <Skeleton.Input></Skeleton.Input>
              <Skeleton.Input></Skeleton.Input>
            </div>
          </div>
          <div className={clsx(styles.summaryInfo)}>
            <div className="flex items-start">
              <Skeleton.Input></Skeleton.Input>
              <Skeleton.Input></Skeleton.Input>
            </div>
          </div>
          <div className={clsx(styles.underline)}></div>
          <div className={clsx(styles.voucher)}></div>
          <div className={clsx(styles.summaryInfo, styles.total)}>
            <div className="flex justify-between">
              <p>Tổng cộng</p>
              <Skeleton.Input active></Skeleton.Input>
            </div>
          </div>
          <button className={clsx(styles.orderButton)}></button>
        </div>
      </div>
    </div>
  )
}

export default CartSkeleton
