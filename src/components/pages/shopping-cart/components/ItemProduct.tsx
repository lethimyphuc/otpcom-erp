/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @next/next/no-img-element */
import clsx from 'clsx'
import React, { useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { useMutation, useQueryClient } from 'react-query'
import { toast } from 'react-toastify'
import { graphQLClient } from '~src/api/graphql'
import { REMOVE_PRODUCT_TO_CART } from '~src/api/product'
import { _format } from '~src/util'
import styles from '../ShoppingCart.module.scss'
import { useRouter } from 'next/router'
import Link from 'next/link'

type ItemProductProps = {
  item: TDataCart
  index?: number
  update?: any
  setSubmitCart?: React.Dispatch<React.SetStateAction<boolean>>
}
const ItemProduct = (props: ItemProductProps) => {
  const router = useRouter()
  const { item, index, update, setSubmitCart } = props

  const queryClient = useQueryClient()
  const { setValue, control, watch } = useForm({
    defaultValues: {
      quantity: item?.quantity,
    },
  })

  useEffect(() => {
    setValue('quantity', item?.quantity)
  }, [item?.quantity])

  const { isLoading: isLoadingDataRemoveToCart, mutate: mutateRemoveToCart } = useMutation({
    mutationFn: async (data: TRemoveToCart) => {
      return (await graphQLClient.request(REMOVE_PRODUCT_TO_CART, {
        input: {
          cart_id: localStorage?.getItem('cartId'),
          cart_item_uid: data.cart_item_uid,
        },
      })) as { removeItemFromCart: TDataUpdateToCart }
    },
    onSuccess: (response: { removeItemFromCart: TDataUpdateToCart }) => {
      if (!!response?.removeItemFromCart?.user_errors?.length) {
        toast.error(response?.removeItemFromCart?.user_errors[0].message)
        return
      }
      queryClient.setQueryData(['getCartId'], (oldData: any) => {
        if (!!oldData) {
          return {
            ...oldData,
            cart: {
              ...oldData?.cart,
              total_quantity:
                response?.removeItemFromCart?.cart?.total_quantity ?? oldData?.cart?.total_quantity,
              items: response?.removeItemFromCart?.cart?.items,
              prices: response?.removeItemFromCart?.cart?.prices,
            },
          }
        } else {
          return oldData
        }
      })
    },
  })

  const handleRemoveItemToCart = (cart_item_uid: string) => {
    const DATA_SUBMIT = {
      cart_item_uid: cart_item_uid,
    }
    mutateRemoveToCart(DATA_SUBMIT)
  }

  return (
    // <div className="flex flex-col gap-8 h-fit mb-20">
    <div
      className={clsx(
        styles.cartInfoHead,
        styles.cartItem,
        'xs:justify-between justify-start flex-row-reverse px-4 xs:px-0 gap-2 items-center pb-16 xs:pb-0'
      )}
    >
      <div className="flex flex-col xs:flex-row !justify-start xs:justify-between !items-start xs:!items-center  xs:gap-8 mt-4 xs:mt-0 w-full gap-1">
        <div>
          <button
            className="text-[#adabab] focus:!text-red ml-4 hidden xs:block"
            type="button"
            onClick={() => handleRemoveItemToCart(item?.uid)}
          >
            {/* <CiCircleRemove /> */}
            <i className="fas fa-times-circle"></i>
          </button>
        </div>
        <div className={clsx(styles.product, '!w-full !ml-4 flex items-center')}>
          <Link
            passHref
            href={{
              pathname: `/product-detail/Hãng xe/${item.product?.name}`,
              query: {
                sku: item.product?.sku,
              },
            }}
          >
            <a>
              <img className={clsx(styles.itemImage)} src={item?.product?.image?.url} alt="" />
            </a>
          </Link>
          <div className={clsx(styles.itemInfo, 'flex flex-col justify-between')}>
            <Link
              href={{
                pathname: `/product-detail/Hãng xe/${item.product?.name}`,
                query: {
                  sku: item.product?.sku,
                },
              }}
            >
              <a className={clsx(styles.itemTitle)}>{item?.product?.name}</a>
            </Link>

            <p className={clsx(styles.itemCode)}>Mã sản phẩm: {item?.product?.sku}</p>
          </div>
        </div>
        <div className={clsx(styles.itemQuantity, '!w-full ml-4')}>
          <button
            onClick={() => {
              if (item?.quantity > 1) {
                update(index, {
                  ...item,
                  quantity: item?.quantity - 1,
                })
                !!setSubmitCart && setSubmitCart(true)
              }
            }}
            className="flex items-center justify-center focus:text-green focus:bg-[#eef6f0] w-[20px] h-[20px] p-2 m-4 rounded-full"
          >
            <i className="fal fa-minus"></i>
          </button>
          <span className="px-3">{watch('quantity')}</span>
          <button
            onClick={() => {
              update(index, {
                ...item,
                quantity: item?.quantity + 1,
              })
              !!setSubmitCart && setSubmitCart(true)
            }}
            className="flex items-center justify-center focus:text-green focus:bg-[#eef6f0] w-[20px] h-[20px] p-2 m-4 rounded-full"
          >
            <i className="fal fa-plus"></i>
          </button>
        </div>
        <div className="ml-4">
          <p className={clsx(styles.itemPrice, '!w-full xs:!w-1/4 xs:!justify-end !justify-start')}>
            {!!item?.prices?.price?.value ? _format.getVND(item?.prices?.price?.value) : '0'}
            {item?.prices?.price?.currency || ' VNĐ'}
          </p>
        </div>
        {/* <Tooltip title="Xóa sản phẩm">

      </Tooltip> */}
      </div>
      <div className="!w-[90px] !h-[90px] xs:hidden rounded-lg flex items-center">
        <Link
          passHref
          href={{
            pathname: `/product-detail/Hãng xe/${item.product?.name}`,
            query: {
              sku: item.product?.sku,
            },
          }}
        >
          <a>
            <img className="rounded-2xl" src={item?.product?.image?.url} alt="" />
          </a>
        </Link>
      </div>
    </div>
  )
}

export default ItemProduct
