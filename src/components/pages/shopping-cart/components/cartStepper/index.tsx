/* eslint-disable @next/next/no-img-element */
import React, { useState } from 'react'
import clsx from 'clsx'
import styles from './CartStepper.module.scss'
// import { STEPS } from "../../constant";

export interface cartStepProps {
  steps: any[]
  colorStep: any
  currentStep: number
  setCurrentStep: React.Dispatch<React.SetStateAction<number>>
}

export const CartStepper = ({ steps, colorStep, currentStep, setCurrentStep }: cartStepProps) => {
  const getColorCurrentStep = (step: number) => {
    if (step === currentStep) {
      return colorStep.current
    }
    if (step < currentStep) {
      return colorStep.verified
    }
    if (step > currentStep) {
      return colorStep.upcoming
    }
    return ''
  }

  return (
    <div className={clsx(styles.cartStepperContainer)}>
      <div className={`${clsx(styles.cartStepper)} `}>
        {steps.map((item, i) => (
          <div key={i} className={clsx(styles.step)}>
            <img src={getColorCurrentStep(item.step)} alt="" />
            <div className={clsx(styles.stepInfo, item.step <= currentStep ? styles.active : '')}>
              <img src={item.icon} alt="" className="!w-8 !h-8" />
              <p>{item.name}</p>
            </div>
          </div>
        ))}
      </div>
    </div>
  )
}
