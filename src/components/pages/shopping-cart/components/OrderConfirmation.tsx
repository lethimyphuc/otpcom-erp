/* eslint-disable @next/next/no-img-element */
import clsx from 'clsx'
import React from 'react'
import styles from '../ShoppingCart.module.scss'
import { useRouter } from 'next/router'
import moment from 'moment'
import { _format } from '~src/util'
import { useQuery } from 'react-query'
import { graphQLClient } from '~src/api/graphql'
import { GET_BANK_ACCOUNT } from '~src/api/cart'
import { BankAccount } from '~src/types/user'
import { Empty, Spin } from 'antd'

export interface IOrderConfirmationProps {
  dataOrder: any
  dataCart: any
}

export default function OrderConfirmation(props: IOrderConfirmationProps) {
  const { dataOrder, dataCart } = props
  const router = useRouter()

  const { data, isFetching } = useQuery<BankAccount[]>({
    queryKey: ['getBankAccount'],
    queryFn: async () => {
      const data: any = await graphQLClient.request(GET_BANK_ACCOUNT, {
        pageSize: 20,
      })
      return data?.Banks?.items || []
    },
  })

  console.log({ data })

  return (
    <div className={clsx(styles.orderConfirmContainer)}>
      <h1>Đặt hàng thành công</h1>
      <p className={clsx(styles.report)}>
        Đơn hàng đã thiết lập thành công. Chúng tôi sẽ liên lạc trực tiếp với quý khách để xác nhận.
      </p>
      <div className={clsx(styles.confirmTicketContainer)}>
        <img className={clsx(styles.orderConfirmIcon)} src="/icons/order-confirm.svg" alt="" />
        <div className={clsx(styles.orderSumary)}>
          <h3>Tóm tắt đơn hàng</h3>
          <div className={clsx(styles.orderInfoTitle)}>
            <div>
              <p>Mã đơn hàng:</p>
              <p>#{dataOrder?.placeOrder?.order?.order_number}</p>
            </div>
            <div>
              <p>Ngày mua hàng:</p>
              <p>{moment().format('DD/MM/YYYY')}</p>
            </div>
            <div>
              <p>Tổng cộng:</p>
              <p>
                {_format.getVND(
                  parseInt(
                    dataOrder?.setPaymentMethodOnCart?.cart?.prices?.grand_total?.value as any
                  )
                )}
                {dataOrder?.setPaymentMethodOnCart?.cart?.prices?.grand_total?.currency || ' VNĐ'}
              </p>
            </div>
            <div>
              <p>Hình thức thanh toán:</p>
              <p>{dataOrder?.setPaymentMethodOnCart?.cart?.selected_payment_method?.title}</p>
            </div>
          </div>
        </div>
        <div className={clsx(styles.bankingInfo)}>
          <h3>Thông tin chuyển khoản</h3>
          {isFetching ? (
            <div className="flex items-center justify-center flex-1 py-[24px]">
              <Spin />
            </div>
          ) : data?.length ? (
            data?.map(
              (item) =>
                item.payment_method === 'banktransfer' && (
                  <div key={item.bank_id} className={clsx(styles.bankingAccount)}>
                    <div className={clsx(styles.bankingLeft)}>
                      <img className="w-[100px] h-[40px] object-contain" src={item.logo} alt="" />
                      <div className={clsx(styles.bankingInfoTitle)}>
                        <div>
                          <p>Ngân hàng</p>
                          <p>{item?.bank}</p>
                        </div>
                        <div className="lg:text-center">
                          <p>Chủ tài khoản</p>
                          <p>{item.account_holder}</p>
                        </div>
                        <div className="lg:text-center">
                          <p>Số tài khoản</p>
                          <p>{item.account_number}</p>
                        </div>
                      </div>
                    </div>
                    <div className={`${clsx(styles.bankingRight)}`}>
                      <img
                        className="w-[136px] h-[136px] object-contain"
                        src={item.qr_code}
                        alt="bg"
                      />
                    </div>
                  </div>
                )
            )
          ) : (
            <Empty description="Không có thông tin chuyển khoản nào" />
          )}
        </div>
        <img
          className={`${clsx(styles.bankTicket)} h-full object-cover`}
          src="/icons/bank-confirm-ticket.svg"
          alt=""
        />
      </div>
      <div className={clsx(styles.buttonContainer, '')}>
        <button onClick={() => router.push('/home')}>Về trang chủ</button>
        <button
          onClick={() => {
            const orderId = dataOrder?.placeOrder?.order?.order_number
            if (orderId) {
              router.push(`/account/orders/${orderId}`)
            } else {
              router.push('/')
            }
          }}
        >
          Xem chi tiết đơn
        </button>
      </div>
    </div>
  )
}
