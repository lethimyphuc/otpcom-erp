/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @next/next/no-img-element */
import React, { useEffect } from 'react'
import clsx from 'clsx'
import styles from '../ShoppingCart.module.scss'
import { _format } from '~src/util'
import SumaryOrder from '~src/components/global/SumaryOrder'
import WrapperShoppingCart from '~src/components/global/WrapperShoppingCart'
import Link from 'next/link'
import { Skeleton } from 'antd'
import CartSkeleton from './CartSkeleton'

export interface ICartProps {
  dataCart?: TDataUpdateToCart
  setCurrentStep: React.Dispatch<React.SetStateAction<number>>
  isFetchingDataCart?: boolean
}

export default function Cart({ dataCart, setCurrentStep, isFetchingDataCart }: ICartProps) {
  return (
    <div className={clsx(styles.cartDetail)}>
      {isFetchingDataCart && <CartSkeleton isFetchingDataCart={isFetchingDataCart} />}

      {dataCart?.cart?.total_quantity !== 0
        ? !isFetchingDataCart && (
            <>
              <WrapperShoppingCart dataCart={dataCart} isFetchingDataCart={isFetchingDataCart} />
              <SumaryOrder dataCart={dataCart} setCurrentStep={setCurrentStep} />
            </>
          )
        : !isFetchingDataCart && (
            <>
              <div className="flex justify-center items-center w-full flex-col gap-16">
                <h1 className="text-6xl">Giỏ hàng chưa có sản phẩm nào</h1>
                <Link href={'/'}>
                  <a className="text-4xl py-5 border bg-success rounded-3xl px-10 text-white cursor-pointer">
                    Quay lại trang chủ
                  </a>
                </Link>
              </div>
            </>
          )}
    </div>
  )
}
