/* eslint-disable react-hooks/exhaustive-deps */
import clsx from "clsx";
import { useEffect, useState } from "react";
import { useQuery } from "react-query";
import { GET_CART } from "~src/api/cart";
import { graphQLClient } from "~src/api/graphql";
import { CartStepper } from "~src/components/pages/shopping-cart/components/cartStepper";
import styles from "./ShoppingCart.module.scss";
import Cart from "./components/Cart";
import OrderConfirmation from "./components/OrderConfirmation";
import Payment from "./components/Payment";
import { COLOR_STEP, STEPS } from "./constant";

export default function ShoppingCart() {
  const [currentStep, setCurrentStep] = useState(STEPS[1].step);
  const [dataOrder, setDataOrder] = useState({});

  const { data: dataCart, isFetching: isFetchingDataCart } = useQuery({
    queryKey: ["getCartId"],
    queryFn: async () => {
      if (!localStorage.getItem("cartId")) return;
      const data = (await graphQLClient.request(GET_CART, {
        cart_id: localStorage.getItem("cartId"),
      })) as TDataUpdateToCart;
      return data;
    },
    cacheTime: Infinity,
  });

  return (
    <div className={clsx(styles.cartContainer)}>
      <CartStepper
        steps={STEPS}
        colorStep={COLOR_STEP}
        currentStep={currentStep}
        setCurrentStep={setCurrentStep}
      />
      {currentStep === STEPS[1].step && (
        <Cart
          dataCart={dataCart}
          setCurrentStep={setCurrentStep}
          isFetchingDataCart={isFetchingDataCart}
        />
      )}
      {currentStep === STEPS[2].step && (
        <Payment
          dataCart={dataCart}
          setCurrentStep={setCurrentStep}
          setDataOrder={setDataOrder}
        />
      )}
      {currentStep === STEPS[3].step && (
        <OrderConfirmation dataOrder={dataOrder} dataCart={dataCart} />
      )}
    </div>
  );
}
