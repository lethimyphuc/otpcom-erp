/* eslint-disable @next/next/no-img-element */
import clsx from 'clsx'
import React, { useState } from 'react'
import styles from './BillOfLading.module.scss'
import { Controller, useForm } from 'react-hook-form'
import { ButtonMain } from '~src/components/global/Button/ButtonMain/ButtonMain'
import { ORDER_TRACKING } from '~src/api/order-tracking'
import {graphQLClient, graphQLClientGET} from '~src/api/graphql'
import { useMutation, useQuery } from 'react-query'
import { toast } from 'react-toastify'
import ErrorMessage from '~src/components/global/ErrorMessage'
import { GET_BANNER_PAGE } from '~src/api'
import { BannerBrand } from '../brand/BannerBrand'

export interface IBillOfLadingProps {}

type TFormSearchBill = {
  email: string
  orderId: string
}

type TOrderTracking = {
  address: string
  carrier_name: string
  carrier_service: string
  carrier_tracking_code: string
  city: string
  customer_name: string
  district: string
  logo: string
  order_increment: string
  phone_number: string
  url: string
  ward: string
}

export default function BillOfLading(props: IBillOfLadingProps) {
  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<TFormSearchBill>({
    defaultValues: {
      email: '',
      orderId: '',
    },
  })
  const [orderTracking, setOrderTracking] = useState<TOrderTracking>()

  const createOrderTracking = useMutation({
    mutationKey: 'createOrderTracking',
    mutationFn: async (data: any) => {
      return await graphQLClient.request(ORDER_TRACKING, data).then((res: any) => {
        if (!!res?.OrderTracking) {
          if (!!res?.OrderTracking?.carrier_tracking_code) {
            setOrderTracking(res?.OrderTracking)
            toast('Tra mã vận đơn thành công', { type: 'success' })
          } else {
            toast('Đơn hàng này chưa có mã vận đơn', { type: 'error' })
          }
        }
      })
    },
    onError: (err: any) => {
      toast(err.error, { type: 'error' })
    },
  })
  const { data: dataContentBillOfLanding, isLoading: isLoadingContentBillOfLanding } = useQuery({
    queryKey: ['getContentBillOfLandingPage'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_BANNER_PAGE, {
        identifier: 'tracking-banner',
      })) as {
        managebanner: any
      }
      return data?.managebanner
    },
    cacheTime: Infinity,
  })
  let isSubmit = false
  const onSubmit = async (data: TFormSearchBill) => {
    if (!isSubmit && !!data?.orderId && !!data?.email) {
      isSubmit = true
      await createOrderTracking.mutateAsync({
        increment_id: `${data?.orderId}`,
        email: data?.email,
      })
    }
  }

  return (
    <div className={clsx(styles.billContainer)}>
      <div className="w-full relative bill-of-landing">
        <BannerBrand
          bannerImage={dataContentBillOfLanding?.image}
          breadcrumbs={[{ breadcrumbName: 'Khuyến mãi', path: '' }]}
        />
      </div>
      <div className="containers">
        <div className={clsx(styles.tabTitle, 'mb-14 md:mb-20')}>
          <div className="list-search">
            <div className="title-search">
              <div className="icon-center">
                <img src="/icons/search.svg" alt="" />
              </div>
              <div className="content-text">
                <span className="text ">{'tra vận đơn'}</span>
              </div>
            </div>
          </div>
          <div>
            <p>
              {`Để theo dõi đơn hàng của bạn xin vui lòng nhập ID đơn hàng của bạn vào ô dưới đây và nhấn nút "Tìm kiếm".`}
            </p>
            <p>{`ID đơn hàng đã được gửi cho bạn qua biên lai và qua email xác nhận mà bạn nhận được.`}</p>
          </div>
        </div>
        <div className={clsx(styles.billFormSearch)}>
          <div className="flex-1">
            <p>Mã đơn hàng</p>
            <Controller
              name="orderId"
              control={control}
              rules={{
                required: true,
              }}
              render={({ field }) => (
                <input
                  {...field}
                  className={clsx(styles.billCode)}
                  type="text"
                  placeholder="Nhập mã đơn hàng"
                />
              )}
            />
            {/* <div className={clsx(styles.underline)}></div> */}
            <div className={clsx(!!errors?.email ? 'block' : 'hidden')}>
              <ErrorMessage>Vui lòng không để trống</ErrorMessage>
            </div>
          </div>
          <div className="flex-1">
            <p>Email đặt hàng</p>

            <Controller
              name="email"
              control={control}
              rules={{
                required: true,
              }}
              render={({ field }) => (
                <input
                  {...field}
                  className={clsx(styles.billEmail)}
                  type="text"
                  placeholder="Nhập email đặt hàng"
                />
              )}
            />
            {/* <div className={clsx(styles.underline)}></div> */}
            <div className={clsx(!!errors?.email ? 'block' : 'hidden')}>
              <ErrorMessage>Vui lòng không để trống</ErrorMessage>
            </div>
          </div>
          <ButtonMain
            loading={false}
            type="button"
            background="green"
            onClick={handleSubmit(onSubmit)}
          >
            <div className="flex items-center gap-1">
              <img src={'/icons/search-icon-button.svg'} alt="" />
              <p className="!text-white">Tìm kiếm</p>
            </div>
          </ButtonMain>
        </div>
        {!!orderTracking ? (
          <div className={clsx(styles.billContentSearch)}>
            <div className={clsx(styles.titleBillSearch)}>
              <p>Thông tin mã vận đơn</p>
            </div>
            <div className={clsx(styles?.logoExpress)}>
              <img src={orderTracking?.logo || ''} alt="" width={80} />
            </div>
            <div className={clsx(styles.boxContentTracking)}>
              <div className={clsx(styles.itemInfoTracking)}>
                <p className={styles.title}>Mã vận đơn</p>
                <p>{orderTracking?.carrier_tracking_code}</p>
              </div>
              <div className={clsx(styles.itemInfoTracking)}>
                <p className={styles.title}>Đơn vị vận chuyển</p>
                <p>{orderTracking?.carrier_name}</p>
              </div>
              <div className={clsx(styles.itemInfoTracking)}>
                <p className={styles.title}>Dịch vụ vận chuyển</p>
                <p>{orderTracking?.carrier_service}</p>
              </div>
              <div className={clsx(styles.itemInfoTracking)}>
                <p className={styles.title}>Mã đơn</p>
                <p>{orderTracking?.order_increment}</p>
              </div>
              <div className={clsx(styles.itemInfoTracking)}>
                <p className={styles.title}>Họ tên khách hàng</p>
                <p>{orderTracking?.customer_name}</p>
              </div>
              <div className={clsx(styles.itemInfoTracking)}>
                <p className={styles.title}>Số điện thoại</p>
                <p>{orderTracking?.phone_number}</p>
              </div>
            </div>
            <div className={styles.billContentInfoAddress}>
              <div className="flex items-center gap-3 py-8 justify-center">
                <p className="">Địa chỉ: </p>
                {/* <p>
                  {`${orderTracking?.address}, ${orderTracking?.ward}, ${orderTracking?.district}, ${orderTracking?.city}.`}
                  <span className="text-blue">
                    <a href={orderTracking?.url || '/'}> Xem thông tin vận đơn</a>
                  </span>
                </p> */}
              </div>
            </div>
          </div>
        ) : (
          <></>
        )}

        <div className={clsx(styles.cardSection)}>
          <div className={clsx(styles.cardItem)}>
            <img className={clsx(styles.union)} src="/image/card1.png" alt="" />
          </div>
          <div className={clsx(styles.cardItem)}>
            <img className={clsx(styles.union)} src="/image/card2.png" alt="" />
          </div>
        </div>
      </div>
    </div>
  )
}
