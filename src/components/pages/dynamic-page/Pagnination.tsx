"use client";
import { useRouter } from "next/router";
import React from "react";
import { PaginationCustom } from "~src/components/common/PaginationCustom";
import { useFilterContext } from "./FilterContext";

export const Pagnination = () => {
  const router = useRouter();
  const { selectors } = useFilterContext();
  const { dataProducts } = selectors;

  return (
    <>
      {(dataProducts?.page_info?.total_pages as any) > 1 && (
        <div className="pagination-promotion">
          <PaginationCustom
            current={
              !!router.query?.currentPage
                ? parseInt(`${router.query?.currentPage}`)
                : 1
            }
            pageSize={
              !!router?.query?.pageSize
                ? parseInt(`${router.query?.pageSize}`)
                : 20
            }
            total={
              !!dataProducts?.page_info?.total_pages &&
              !!dataProducts?.page_info?.page_size
                ? parseInt(`${dataProducts?.page_info?.total_pages}`) *
                  parseInt(`${dataProducts?.page_info?.page_size}`)
                : !!router?.query?.pageSize
                ? parseInt(`${router.query?.pageSize}`)
                : 20
            }
            onChange={(page: number) => {
              const el = document.getElementById(
                "otpcom--products-list"
              ) as HTMLDivElement;
              const y = el!.getBoundingClientRect().top + window.scrollY + -120;
              window.scrollTo({ top: y });

              router.push(
                {
                  query: {
                    ...router.query,
                    currentPage: page,
                  },
                },
                undefined,
                { shallow: true, scroll: false }
              );
            }}
            totalPage={
              !!dataProducts?.page_info?.total_pages
                ? parseInt(`${dataProducts?.page_info?.total_pages}`)
                : 1
            }
          />
        </div>
      )}
    </>
  );
};
