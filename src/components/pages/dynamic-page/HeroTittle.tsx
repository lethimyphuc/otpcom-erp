/* eslint-disable @next/next/no-img-element */
import { useRouter } from 'next/router'
import { useMemo } from 'react'
import { useQuery } from 'react-query'

type Props = {
  data: ProductFilterReq
}

export const HeroTitle = ({ data }: Props) => {
  const router = useRouter()
  const { data: companies }: any = useQuery({
    queryKey: ['getCategoriesByCompany'],
    enabled: false,
  })
  const { data: types }: any = useQuery({
    queryKey: ['getCategoriesByType', { eq: data?.company }],
    enabled: false,
  })
  const { data: models }: any = useQuery({
    queryKey: ['getCategoriesByEnd', { eq: data?.type }],
    enabled: false,
  })
  const { data: tags }: any = useQuery({
    queryKey: ['getCategoriesByTags', { eq: data?.end }],
    enabled: false,
  })

  const title = useMemo(() => {
    let title

    if (data?.tag && tags?.length) {
      title = tags?.find((item: any) => item?.id === data.tag)?.name
    } else if (data?.end && models?.length) {
      title = models?.find((item: any) => item?.uid === data.end)?.name
    } else if (data?.type && types?.length) {
      title = types?.find((item: any) => item?.uid === data.type)?.name
    } else if (data?.company && companies?.length) {
      title = companies?.find((item: any) => item?.uid === data.company)?.name
    }

    return title ? title : router.pathname === '/promotion' ? 'Sản phẩm khuyến mãi' : 'Hãng xe'
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data, companies, types, models, tags])

  return (
    <div className="list-search">
      <div className="title-search">
        <div className="icon-center">
          <img src="/icons/search.svg" alt="" />
        </div>
        <div className="content-text">
          <span className="text">{title}</span>
        </div>
      </div>
    </div>
  )
}
