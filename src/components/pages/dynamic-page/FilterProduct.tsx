/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useMemo } from 'react'
import { ButtonMain } from '~src/components/global/Button/ButtonMain/ButtonMain'
import { FormSelect } from '~src/components/global/FormControls/FormSelect'
import { useFilterContext } from './FilterContext'
import { useRouter } from 'next/router'

export const FilterProduct = () => {
  const router = useRouter()
  const slugs = useMemo(() => {
    return ((router.query.slug as string[]) ?? []).join('/')
  }, [router.query.slug])

  const { actions, selectors, misc, atoms } = useFilterContext()
  const { setFilterFormValue, getFilterFormValue } = actions
  const { filterForm } = misc
  const { carCompanyDataAll, carTypeByCompanyDataAll, carEndByTypeDataAll, carTagsByEndDataAll } =
    selectors
  const { sort } = atoms

  const handleSelectCarCompany = (value: any, option: any) => {
    setFilterFormValue('car_company_uid', value)
  }

  const handleSelectCarType = (value: any, option: any) => {
    setFilterFormValue('car_type_uid', value)
  }

  const handleSelectCarEnd = (value: any, option: any) => {
    setFilterFormValue('car_end_uid', value)
  }

  const handleSelectCarTag = (value: any, option: any) => {
    setFilterFormValue('car_tag_uid', value)
  }
  const handleSelectSort = (value: any, option: any) => {
    setFilterFormValue('sort', value)
  }
  useEffect(() => {
    if (!!router.query && !!Object.keys(router.query).length) {
      router.replace(
        {
          query: {
            ...router.query,
            currentPage: !!router.query?.currentPage ? router.query.currentPage : '1',
            pageSize: !!router.query?.pageSize ? router.query.pageSize : '20',
            sort: !!sort ? Object.keys(JSON.parse(sort ?? '{}')) : 'name',
            //lấy giá trị đầu tiên của object sort
            dir: !!sort
              ? JSON.parse(sort || '{}')[Object.keys(JSON.parse(sort || '{}'))[0]]
              : 'ASC',
          },
        },
        undefined,
        { shallow: true, scroll: false }
      )
    } else {
      router.replace(
        {
          query: {
            currentPage: '1',
            pageSize: '20',
            sort: 'name',
            dir: 'ASC',
          },
        },
        undefined,
        { shallow: true, scroll: false }
      )
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [slugs, sort])

  return (
    <div className="form-filter-product">
      <form onSubmit={() => {}}>
        <div className="box">
          <div className="items">
            <div className="item">
              <div className="item-label">Sắp xếp theo:</div>
              <FormSelect
                control={filterForm.control}
                name={'sort'}
                optionList={[
                  { label: 'Tên A -> Z', value: '{"name":"ASC"}' },
                  { label: 'Tên Z -> A', value: '{"name":"DESC"}' },
                  { label: 'Giá tăng dần', value: '{"price":"ASC"}' },
                  { label: 'Giá giảm dần', value: '{"price":"DESC"}' },
                  // { label: "Vị trí tăng dần", value: "ascPosition" },
                  // { label: "Vị trí giảm dần", value: "descPosition" },
                  // { label: "Kết quả tìm kiếm tăng dần", value: "ascRelevance" },
                  // {
                  //   label: "Kết quả tìm kiếm giảm dần",
                  //   value: "descRelevance",
                  // },
                ]}
                label=""
                onChange={handleSelectSort}
                placeholder="Sắp xếp theo"
                allowClear
              />
            </div>
            <div className="item">
              <div className="item-label">Lọc theo:</div>
              <FormSelect
                control={filterForm.control}
                name={'car_company_uid'}
                optionList={carCompanyDataAll?.map((company: TChildrenCategories) => {
                  return {
                    label: company?.name,
                    value: company?.uid,
                  }
                })}
                label=""
                placeholder="Hãng xe"
                onChange={handleSelectCarCompany}
                allowClear
              />
            </div>
            <div className="item">
              <FormSelect
                control={filterForm.control}
                name={'car_type_uid'}
                optionList={carTypeByCompanyDataAll?.map((type: TChildrenCategories) => {
                  return {
                    label: type?.name,
                    value: type?.uid,
                  }
                })}
                onChange={handleSelectCarType}
                label=""
                placeholder="Dòng xe"
                allowClear
              />
            </div>
            <div className="item">
              <div className="item-label"></div>
              <FormSelect
                control={filterForm.control}
                name={'car_end_uid'}
                optionList={carEndByTypeDataAll?.map((end: TChildrenCategories) => {
                  return {
                    label: end?.name,
                    value: end?.uid,
                  }
                })}
                onChange={handleSelectCarEnd}
                label=""
                placeholder="Đời xe"
                allowClear
              />
            </div>
            <div className="item">
              <div className="item-label"></div>
              <FormSelect
                control={filterForm.control}
                name={'car_tag_uid'}
                optionList={carTagsByEndDataAll?.tags?.map((part: { id: number; name: string }) => {
                  return {
                    label: part?.name,
                    value: part?.id,
                  }
                })}
                onChange={handleSelectCarTag}
                label=""
                placeholder="Tag "
                allowClear
              />
            </div>
          </div>
          <div className="button-search h-[45px]">
            <ButtonMain background="green" type="button" onClick={() => {}}>
              <div className="flex items-center gap-[0.4rem]">
                <div className="icon">
                  <img src="/icons/search-icon.svg" alt="" />
                </div>
                <div className="text">Tìm kiếm</div>
              </div>
            </ButtonMain>
          </div>
        </div>
      </form>
    </div>
  )
}
