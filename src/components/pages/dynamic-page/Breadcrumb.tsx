import { Breadcrumb } from 'antd'
import clsx from 'clsx'
import Link from 'next/link'
import React from 'react'

type BreadcrumbProps = {
  breadcrumbHead: boolean
  customsBreadcrumb?: { label: string; href: string }[]
}

export const BreadcrumbBanner = ({ breadcrumbHead, customsBreadcrumb }: BreadcrumbProps) => {
  const breadcrumbs: any = [{ name: 'Trang chủ', key: '/home' }]

  return (
    <div className={clsx(`breadcrumb-banner ${breadcrumbHead ? 'breadcrumb-head' : ''}`)}>
      {breadcrumbs.length > 0 ? (
        <Breadcrumb separator=">">
          {breadcrumbs?.map((item: any, index: number) => (
            <Breadcrumb.Item key={index}>
              {item.key ? (
                <Link href={item.key || '/'} className="font-medium">
                  {item.name}
                </Link>
              ) : (
                item.name
              )}
            </Breadcrumb.Item>
          ))}
          {customsBreadcrumb?.map((v, k) => (
            <Breadcrumb.Item key={k}>
              <Link href={v.href} scroll={false} className="font-medium">
                {v.label}
              </Link>
            </Breadcrumb.Item>
          ))}
        </Breadcrumb>
      ) : null}
    </div>
  )
}
