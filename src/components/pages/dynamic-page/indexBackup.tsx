/* eslint-disable react-hooks/exhaustive-deps */
/** @format */

import { NextRouter, useRouter } from 'next/router'
import { useEffect, useMemo, useState } from 'react'
import { useQuery } from 'react-query'
import { graphQLClient,graphQLClientGET } from '~src/api/graphql'
import { GET_PRODUCTS } from '~src/api/product'
import { FormFilterProduct } from '~src/components/common/FormFilterProduct'
import { PaginationCustom } from '~src/components/common/PaginationCustom'
import { useCategoriesContent } from '~src/hook/useCategoriesContent'
import { BannerBrand } from '../brand/BannerBrand'
import { ProductList } from '../product/ProductList'
import { FilterContextProvider } from './FilterContext'
import { HeroTitle } from './HeroTittle'
import _ from 'lodash'
import { ProductFilter } from '~src/components/common/ProductFilter'

type DynamicPageProps = {
  router: NextRouter
}

const DynamicPageSection = (props: DynamicPageProps) => {
  const router = useRouter()
  const [filter, setFilter] = useState<any>({})
  const [bannerImage, setBannerImage] = useState<string | null>(null)
  const setBanerImage = (childData: string) => {
    setBannerImage(childData)
  }

  const getFilterOfChild = (childData: any) => {
    setFilter(childData)
  }

  useEffect(() => {
    if (router.query.slug) {
      if (!!router.query && !!Object.keys(router.query).length) {
        router.replace({
          pathname:
            router.pathname.split('/').length > 2
              ? router.pathname.split('[...slug]')[0] + (router.query.slug as string[])?.join('/')
              : '/category',
          query: {
            ...router.query,
            currentPage: !!router.query?.currentPage ? router.query.currentPage : '1',
            pageSize: !!router.query?.pageSize ? router.query.pageSize : '20',
            sort: !!filter.sort ? Object.keys(JSON.parse(filter.sort ?? '{}')) : 'name',
            dir: !!filter.sort
              ? JSON.parse(filter.sort || '{}')[Object.keys(JSON.parse(filter.sort || '{}'))[0]]
              : 'ASC',
          },
        })
      } else {
        router.replace({
          pathname:
            router.pathname.split('/').length > 2
              ? router.pathname.split('[...slug]')[0] + (router.query.slug as string[])?.join('/')
              : '/category',
          query: {
            currentPage: '1',
            pageSize: '20',
            sort: 'name',
            dir: 'ASC',
          },
        })
      }
    }
  }, [router.isReady])
  const {
    data: productList,
    isLoading: isLoadingDataProductList,
    isFetching: isfetchingDataProductList,
  } = useQuery({
    queryKey: ['getProductList', filter],
    queryFn: async () => {
      const convertFilterIn = [filter.company, filter.type, filter.end].filter(Boolean).pop()
      const data = (await graphQLClientGET.request(GET_PRODUCTS, {
        filter: {
          category_uid: {
            eq: convertFilterIn ?? 'Ng==',
          },
          tags: {
            eq: filter.tag ? filter.tag : null,
          },
        },
        pageSize: !!router?.query?.pageSize ? parseInt(`${router.query.pageSize}`) : 20,
        currentPage: !!router?.query?.currentPage ? parseInt(`${router.query.currentPage}`) : 1,
        sort: JSON.parse(filter.sort ?? '{}'),
      })) as {
        products: {
          items: TProduct[]
          page_info: TPageInfo
          total_count: number
        }
      }

      return data
    },
  })

  const { data: dataCategoriesByCompany, isLoading: isLoadingDataCategoriesByCompany } =
    useCategoriesContent({
      key: 'getCategoriesByCompany',
      uid: 'Ng==',
    })

  const { data: dataCategoriesByType, isLoading: isLoadingDataCategoriesByType } =
    useCategoriesContent({
      key: 'getCategoriesByType',
      uid: (router.query?.company as string) ?? '',
    })

  const { data: dataCategoriesByEnd, isLoading: isLoadingDataCategoriesByEnd } =
    useCategoriesContent({
      key: 'getCategoriesByEnd',
      uid: (router.query?.type as string) ?? '',
    })

  const customBreadcrumb = useMemo<{ label: string; href: string }[]>(() => {
    const _array = []
    const companyParamUid = router.query?.company ?? null
    const companyBreadscumb = companyParamUid
      ? {
          label: dataCategoriesByCompany?.find((item) => item.uid === companyParamUid)?.name ?? '',
          href: `/${router.pathname.split('/')[1]}?company=${companyParamUid}`,
        }
      : null
    const companyQuery = companyParamUid ? `company=${companyParamUid}` : ''

    const typeParamUid = router.query?.type ?? null
    const typeBreadscumb = typeParamUid
      ? {
          label: dataCategoriesByType?.find((item) => item.uid === typeParamUid)?.name ?? '',
          href: `/${router.pathname.split('/')[1]}?${companyQuery}&type=${typeParamUid}`,
        }
      : null
    const typeQuery = typeParamUid ? `type=${typeParamUid}` : ''

    const endParamUid = router.query?.end ?? null
    const endBreadscumb = typeParamUid
      ? {
          label: dataCategoriesByEnd?.find((item) => item.uid === endParamUid)?.name ?? '',
          href: `/${router.pathname.split('/')[1]}?${companyQuery}&${typeQuery}&end=${endParamUid}`,
        }
      : null

    companyBreadscumb && _array.push(companyBreadscumb)
    typeBreadscumb && _array.push(typeBreadscumb)
    endBreadscumb && _array.push(endBreadscumb)
    return _array
  }, [router, dataCategoriesByCompany, dataCategoriesByType, dataCategoriesByEnd])

  const convertListProduct = (productList: any) => {
    return productList?.items?.map((item: any) => {
      return { ...item, ...item?.product }
    })
  }

  const breadcrumbs = [{ breadcrumbName: 'Hãng xe', path: '/category' }]
  const slug = router.query?.slug
  if (slug?.length) {
    if (_.isArray(slug)) {
      slug.forEach((item) => {
        const index = slug.findIndex((path) => path === item)
        const slugTerm = slug.slice(0, index + 1)
        breadcrumbs.push({
          breadcrumbName: item,
          path: slugTerm?.length ? `/category/${slugTerm?.join('/')}` : '',
        })
      })
    } else {
      breadcrumbs.push({ breadcrumbName: slug, path: '' })
    }
  }

  return (
    <FilterContextProvider>
      <div className="car-company branch-page">
        <div className="banner-page">
          {/* <Banner /> */}
          <BannerBrand bannerImage={bannerImage} breadcrumbs={breadcrumbs} />
        </div>
        <div id="otpcom--products-list" className="containers">
          <div className="title-promotion">
            <HeroTitle
              data={{
                type: undefined,
                company: undefined,
                end: undefined,
                groupAccessary: undefined,
                sort: undefined,
                tag: undefined,
              }}
            />
          </div>
          <div className="content-inner-promotion">
            <div className="filter-promotion">
              <FormFilterProduct
                filterData={getFilterOfChild}
                router={router}
                banner={setBannerImage}
              />
              <br />
              <ProductFilter onChange={(value) => console.log(value)} />
            </div>
            <div className="wrapper-product mb-12">
              <ProductList
                data={
                  !!productList?.products?.items?.length &&
                  !!convertListProduct(productList?.products)
                    ? convertListProduct(productList?.products)
                    : []
                }
                loading={isLoadingDataProductList || isfetchingDataProductList}
              />
            </div>
            <div className="w-full flex justify-center">
              {Number(productList?.products.page_info.total_pages) > 0 && (
                <PaginationCustom
                  current={
                    !!router.query?.currentPage ? parseInt(`${router.query?.currentPage}`) : 1
                  }
                  pageSize={!!router?.query?.pageSize ? parseInt(`${router.query?.pageSize}`) : 20}
                  total={
                    !!productList?.products?.page_info?.total_pages &&
                    !!productList?.products?.page_info?.page_size
                      ? parseInt(`${productList?.products?.page_info?.total_pages}`) *
                        parseInt(`${productList?.products?.page_info?.page_size}`)
                      : !!router?.query?.pageSize
                      ? parseInt(`${router.query?.pageSize}`)
                      : 20
                  }
                  onChange={(page: number) =>
                    router.push({
                      query: {
                        ...router.query,
                        currentPage: page,
                      },
                    })
                  }
                  totalPage={
                    !!productList?.products?.page_info?.total_pages
                      ? parseInt(`${productList?.products?.page_info?.total_pages}`)
                      : 1
                  }
                />
              )}
            </div>
          </div>
        </div>
      </div>
    </FilterContextProvider>
  )
}

export default DynamicPageSection
