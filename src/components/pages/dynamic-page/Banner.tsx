/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useMemo } from 'react'
import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger'
import { BreadcrumbBanner } from './Breadcrumb'
import { useFilterContext } from './FilterContext'
import { motion } from 'framer-motion'

export const Banner = ({}) => {
  const { atoms, selectors, misc } = useFilterContext()
  const { carCompanyPickedUID, carTypePickedUID, carEndPickedUID } = atoms
  const { carCompanyPickedDataByUID, carTypePickedDataByUID, carEndPickedDataByUID } = selectors
  const { isFetchingCarCompanyData, isFetchingCarTypeData, isFetchingCarEndData } = misc

  const customBreadcrumb = useMemo<{ label: string; href: string }[]>(() => {
    const _array = []
    const companyParamUid = carCompanyPickedUID
    const companyPath = companyParamUid ? `/${carCompanyPickedDataByUID?.url_key}` : ''
    const companyBreadscumb = companyParamUid
      ? {
          label: carCompanyPickedDataByUID?.name ?? '',
          href: companyPath,
        }
      : null

    const typeParamUid = carTypePickedUID
    const typePath = typeParamUid ? `/${carTypePickedDataByUID?.url_key}` : ''
    const typeBreadscumb = typeParamUid
      ? {
          label: carTypePickedDataByUID?.name ?? '',
          href: `${companyPath}${typePath}`,
        }
      : null

    const endParamUid = carEndPickedUID
    const endPath = endParamUid ? `/${carEndPickedDataByUID?.url_key}` : ''
    const endBreadscumb = typeParamUid
      ? {
          label: carEndPickedDataByUID?.name ?? '',
          href: `${companyPath}${typePath}${endPath}`,
        }
      : null

    companyBreadscumb && _array.push(companyBreadscumb)
    typeBreadscumb && _array.push(typeBreadscumb)
    endBreadscumb && _array.push(endBreadscumb)
    return _array
  }, [
    carCompanyPickedUID,
    carTypePickedUID,
    carEndPickedUID,
    carCompanyPickedDataByUID,
    carTypePickedDataByUID,
    carEndPickedDataByUID,
  ])

  const bannerImage = useMemo(() => {
    return carEndPickedDataByUID
      ? carEndPickedDataByUID?.image_banner
      : carTypePickedDataByUID
      ? carTypePickedDataByUID.image_banner
      : carCompanyPickedDataByUID?.image_banner
  }, [carCompanyPickedDataByUID, carEndPickedDataByUID, carTypePickedDataByUID])

  useEffect(() => {
    gsap.registerPlugin(ScrollTrigger)

    const title = document.querySelector('#effect')

    gsap.set(title, { opacity: 0, y: 100 })

    ScrollTrigger.create({
      trigger: title,
      start: 'top 80%',
      end: 'top 30%',
      onEnter: () => {
        gsap.to(title, {
          opacity: 1,
          y: 0,
          duration: 1,
          ease: 'power2.out',
        })
      },
      onLeaveBack: () => {
        gsap.to(title, {
          opacity: 0,
          y: 100,
          duration: 1,
          ease: 'power2.out',
        })
      },
    })
  }, [])

  return (
    <div className="banner-promotion h-[35vw]">
      <div className="img h-full w-full">
        {[isFetchingCarCompanyData, isFetchingCarTypeData, isFetchingCarEndData].some((v) => v) ? (
          <div className="z-[0] absolute inset-0 h-full w-full">
            <LazyBanner />
          </div>
        ) : (
          <motion.img
            initial={{ opacity: 0, filter: 'blur(8px)' }}
            animate={{ opacity: 1, filter: 'blur(0)' }}
            src={bannerImage ?? '/image/default-banner.png'}
            alt=""
            className="w-full h-full !object-cover rounded-b-3xl"
          />
        )}
      </div>
      <div className="img-position">
        <div className="img">
          <img src="/image/21.png" alt="" />
        </div>
      </div>
      <div id="effect" className="content-inner !translate-x-[-50%] [&_ol]:justify-start container">
        <div className="breadcrum">
          <BreadcrumbBanner breadcrumbHead={false} customsBreadcrumb={customBreadcrumb} />
        </div>
      </div>
    </div>
  )
}

const LazyBanner = () => {
  return (
    <div className="fx-skeleton w-full h-full">
      <div className="overlay" />
      <div className="skeholder w-full h-full" />
      <div
        style={{
          background: 'linear-gradient(to top,rgba(255,255,255,1) 30%, rgba(255,255,255,0))',
        }}
        className="absolute bottom-0 left-0 right-0 h-32"
      />
    </div>
  )
}
