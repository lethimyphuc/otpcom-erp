import React from "react";
import { ProductList } from "../product/ProductList";
import { useFilterContext } from "./FilterContext";

export const Products = () => {
  const { selectors, misc } = useFilterContext();
  const { dataProducts } = selectors;
  const { isFetchingDataProducts } = misc;

  return (
    <div>
      <ProductList
        data={dataProducts?.items}
        loading={isFetchingDataProducts}
      />
    </div>
  );
};
