/* eslint-disable react-hooks/exhaustive-deps */
/** @format */

'use client'
import { set } from 'lodash'
import { useRouter } from 'next/router'
import React, { createContext, useContext, useEffect, useMemo, useState } from 'react'
import { UseFormGetValues, UseFormReturn, UseFormSetValue, useForm } from 'react-hook-form'
import { useQuery } from 'react-query'
import { graphQLClient,graphQLClientGET } from '~src/api/graphql'
import { GET_PRODUCTS } from '~src/api/product'
import { useCategorieDetailContent } from '~src/hook/useCategorieDetailContent'
import { useCategoriesContent } from '~src/hook/useCategoriesContent'
import { useCategoriesContentBySlug } from '~src/hook/useCategoriesContentBySlug'

type FilterForm = {
  car_company_uid: string | null
  car_type_uid: string | null
  car_end_uid: string | null
  car_tag_uid: string | null
  sort: string | null
}

type FilterStore = {
  constants: {}
  atoms: {
    carCompanyPickedUID: FilterForm['car_company_uid']
    carTypePickedUID: FilterForm['car_type_uid']
    carEndPickedUID: FilterForm['car_end_uid']
    carTagsPickedUID: FilterForm['car_tag_uid']
    sort: FilterForm['sort']
  }
  selectors: {
    carCompanySlug: string | null
    carTypeSlug: string | null
    carEndSlug: string | null
    carCompanyDataAll: TChildrenCategories[] | null
    carTypeByCompanyDataAll: TChildrenCategories[] | null
    carEndByTypeDataAll: TChildrenCategories[] | null
    carTagsByEndDataAll: TChildrenCategories | null
    carCompanyPickedDataBySlug: TChildrenCategories | null
    carTypePickedDataBySlug: TChildrenCategories | null
    carEndPickedDataBySlug: TChildrenCategories | null
    carCompanyPickedDataByUID: TChildrenCategories | null
    carTypePickedDataByUID: TChildrenCategories | null
    carEndPickedDataByUID: TChildrenCategories | null
    carTagPickedDataByUID: TChildrenCategories | null
    dataProducts:
      | {
          items: TProduct[]
          page_info: {
            current_page: number
            page_size: number
            total_pages: number
          }
        }
      | undefined
  }
  actions: {
    getFilterFormValue: UseFormGetValues<FilterForm>
    setFilterFormValue: UseFormSetValue<FilterForm>
  }
  misc: {
    filterForm: UseFormReturn<FilterForm, any>
    isFetchingCarCompanyData: boolean
    isFetchingCarTypeData: boolean
    isFetchingCarEndData: boolean
    isFetchingDataProducts: boolean
    isFetchingCarTags: boolean
  }
}

const FilterContext = createContext<FilterStore | undefined>(undefined)

export const FilterContextProvider: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  const router = useRouter()
  const slugs = useMemo(() => router.query.slug!, [router])
  const [isFirtLoad, setIsFirstLoad] = useState(true)

  // STORE:Misc
  const filterForm = useForm<FilterForm>({
    defaultValues: {
      car_company_uid: null,
      car_type_uid: null,
      car_end_uid: null,
      car_tag_uid: null,
      sort: null,
    },
  })

  console.log('data from filterForm: ', filterForm.getValues())

  const carCompanyPickedUID = filterForm.watch('car_company_uid')
  const carTypePickedUID = filterForm.watch('car_type_uid')
  const carEndPickedUID = filterForm.watch('car_end_uid')
  const carTagsPickedUID = filterForm.watch('car_tag_uid')
  const sort = filterForm.watch('sort')

  // STORE:Selectors
  const carCompanySlug = useMemo<string | null>(() => {
    return (slugs || [])[1] ?? null
  }, [slugs])

  const carTypeSlug = useMemo<string | null>(() => {
    return (slugs || [])[2] ?? null
  }, [slugs])

  const carEndSlug = useMemo<string | null>(() => {
    return (slugs || [])[3] ?? null
  }, [slugs])
  const carTagSlug = useMemo<string | null>(() => {
    return (slugs || [])[4] ?? null
  }, [slugs])

  const { data: carCompanyDataAll, isLoading: isFetchingCarCompanyData } = useCategoriesContent({
    key: 'getCategoriesByCompany',
    uid: 'Ng==',
  }) as { data: TChildrenCategories[] | null; isLoading: boolean }

  const { data: carTypeByCompanyDataAll, isLoading: isFetchingCarTypeData } = useCategoriesContent({
    key: `getCategoriesByType--${carCompanyPickedUID}`,
    uid: carCompanyPickedUID ?? '',
  }) as { data: TChildrenCategories[] | null; isLoading: boolean }

  const { data: carEndByTypeDataAll, isLoading: isFetchingCarEndData } = useCategoriesContent({
    key: `getCategoriesByEnd--${carTypePickedUID}`,
    uid: carTypePickedUID ?? '',
  }) as { data: TChildrenCategories[] | null; isLoading: boolean }

  const { data: carTagsByEndDataAll, isLoading: isFetchingCarTags } = useCategorieDetailContent({
    key: `getCategoriesByParts--${carEndPickedUID}`,
    uid: carEndPickedUID ?? '',
  }) as unknown as { data: TChildrenCategories | null; isLoading: boolean }

  const carCompanyPickedDataBySlug = useMemo<TChildrenCategories | null>(() => {
    return (carCompanyDataAll || []).find((v) => v.url_key === carCompanySlug) ?? null
  }, [carCompanyDataAll, carCompanySlug])

  const carTypePickedDataBySlug = useMemo<TChildrenCategories | null>(() => {
    return (carTypeByCompanyDataAll || []).find((v) => v.url_key === carTypeSlug) ?? null
  }, [carTypeByCompanyDataAll, carTypeSlug])

  const carEndPickedDataBySlug = useMemo<TChildrenCategories | null>(() => {
    return (carEndByTypeDataAll || []).find((v) => v.url_key === carEndSlug) ?? null
  }, [carEndByTypeDataAll, carEndSlug])

  const carTagsPickedDataBySlug = useMemo<any>(() => {
    return (carTagsByEndDataAll?.tags || [])?.find((v: any) => v.id === carTagSlug) ?? null
  }, [carTagsByEndDataAll, carTagSlug])

  const carCompanyPickedDataByUID = useMemo<TChildrenCategories | null>(() => {
    return (carCompanyDataAll || []).find((v) => v.uid === carCompanyPickedUID) ?? null
  }, [carCompanyDataAll, carCompanyPickedUID])

  const carTypePickedDataByUID = useMemo<TChildrenCategories | null>(() => {
    return (carTypeByCompanyDataAll || []).find((v) => v.uid === carTypePickedUID) ?? null
  }, [carTypeByCompanyDataAll, carTypePickedUID])

  const carEndPickedDataByUID = useMemo<TChildrenCategories | null>(() => {
    return (carEndByTypeDataAll || []).find((v) => v.uid === carEndPickedUID) ?? null
  }, [carEndByTypeDataAll, carEndPickedUID])
  const carTagPickedDataByUID = useMemo<any>(() => {
    return (carTagsByEndDataAll?.tags || []).find((v: any) => v.id === carTagsPickedUID) ?? null
  }, [carTagsByEndDataAll, carTagsPickedUID])

  const { data: dataProducts, isLoading: isFetchingDataProducts } = useQuery({
    queryKey: ['getProducts', router.query],
    queryFn: async () => {
      const convertFilterIn = [carCompanyPickedUID, carTypePickedUID, carEndPickedUID]
        .filter(Boolean)
        .pop()
      const data = (await graphQLClientGET.request(GET_PRODUCTS, {
        filter: {
          category_uid: {
            eq: convertFilterIn ?? 'Ng==',
          },
          tags: {
            eq: carTagsPickedUID,
          },
        },
        search: '',
        pageSize: !!router?.query?.pageSize ? parseInt(`${router.query.pageSize}`) : 20,
        currentPage: !!router?.query?.currentPage ? parseInt(`${router.query.currentPage}`) : 1,
        sort: JSON.parse(sort ?? '{}'),
      })) as {
        products: {
          items: TProduct[]
          page_info: {
            current_page: number
            page_size: number
            total_pages: number
          }
        }
      }
      return data?.products
    },
  })

  useEffect(() => {
    carCompanyPickedDataBySlug &&
      filterForm.setValue('car_company_uid', carCompanyPickedDataBySlug?.uid)
  }, [carCompanyPickedDataBySlug, filterForm])

  useEffect(() => {
    carTypePickedDataBySlug && filterForm.setValue('car_type_uid', carTypePickedDataBySlug?.uid)
  }, [carTypePickedDataBySlug, filterForm])

  useEffect(() => {
    carEndPickedDataBySlug && filterForm.setValue('car_end_uid', carEndPickedDataBySlug?.uid)
  }, [carEndPickedDataBySlug, filterForm])

  useEffect(() => {
    carTagsPickedDataBySlug &&
      filterForm.setValue('car_tag_uid', carTagsPickedDataBySlug?.id?.toString())
  }, [carTagsPickedDataBySlug, filterForm])

  useEffect(() => {
    if (isFirtLoad && (slugs?.length ?? 0) === 1) {
      setIsFirstLoad(false)
      return
    }
    if (isFirtLoad) return
    filterForm.setValue('car_type_uid', null)
    filterForm.setValue('car_end_uid', null)
    filterForm.setValue('car_tag_uid', null)
    if (carCompanyPickedDataByUID) {
      const carCompanyPath = `/category/${carCompanyPickedDataByUID?.url_key ?? ''}`

      router.push(
        {
          pathname: carCompanyPath,
          query: {
            currentPage: '1',
            pageSize: !!router.query?.pageSize ? router.query.pageSize : '20',
            sort: !!sort ? Object.keys(JSON.parse(sort ?? '{}')) : 'name',
            dir: !!sort ? JSON.parse(sort ?? '{}').value : 'ASC',
          },
        },
        undefined,
        { shallow: true }
      )
    } else {
      router.push(
        {
          pathname: '/category',
          query: {
            currentPage: '1',
            pageSize: !!router.query?.pageSize ? router.query.pageSize : '20',
            sort: !!sort ? Object.keys(JSON.parse(sort ?? '{}')) : 'name',
            dir: !!sort ? JSON.parse(sort ?? '{}').value : 'ASC',
          },
        },
        undefined,
        { shallow: true }
      )
    }
  }, [carCompanyPickedUID])

  useEffect(() => {
    if (isFirtLoad && (slugs?.length ?? 0) === 2) {
      setIsFirstLoad(false)
      return
    }
    if (isFirtLoad) return
    filterForm.setValue('car_end_uid', null)
    filterForm.setValue('car_tag_uid', null)

    if (carCompanyPickedDataByUID || carTypePickedDataByUID) {
      const carCompanyPath = `/${carCompanyPickedDataByUID?.url_key ?? ''}`
      const carTypePath = `/${carTypePickedDataByUID?.url_key ?? ''}`
      router.push(
        {
          pathname: '/category' + carCompanyPath + carTypePath,
          query: {
            currentPage: '1',
            pageSize: !!router.query?.pageSize ? router.query.pageSize : '20',
            sort: !!sort ? Object.keys(JSON.parse(sort ?? '{}')) : 'name',
            dir: !!sort ? JSON.parse(sort ?? '{}').value : 'ASC',
          },
        },
        undefined,
        { shallow: true }
      )
    }

    if (filterForm.getValues('car_type_uid') === null) filterForm.setValue('car_type_uid', null)
  }, [carTypePickedUID])

  useEffect(() => {
    if (isFirtLoad && (slugs?.length ?? 0) === 3) {
      setIsFirstLoad(false)
      return
    }
    if (isFirtLoad) return
    filterForm.setValue('car_tag_uid', null)
    if (carCompanyPickedDataByUID || carTypePickedDataByUID || carEndPickedDataByUID) {
      const carCompanyPath = `/${carCompanyPickedDataByUID?.url_key ?? ''}`
      const carTypePath = `/${carTypePickedDataByUID?.url_key ?? ''}`
      const carEndPath = `/${carEndPickedDataByUID?.url_key ?? ''}`

      router.push(
        {
          pathname: '/category' + carCompanyPath + carTypePath + carEndPath,
          query: {
            currentPage: '1',
            pageSize: !!router.query?.pageSize ? router.query.pageSize : '20',
            sort: !!sort ? Object.keys(JSON.parse(sort ?? '{}')) : 'name',
            dir: !!sort ? JSON.parse(sort ?? '{}').value : 'ASC',
          },
        },
        undefined,
        { shallow: true }
      )
    }
  }, [carEndPickedUID])

  useEffect(() => {
    if (isFirtLoad && (slugs?.length ?? 0) === 3) {
      setIsFirstLoad(false)
      return
    }
    if (isFirtLoad) return
    if (
      carCompanyPickedDataByUID ||
      carTypePickedDataByUID ||
      carEndPickedDataByUID ||
      carTagPickedDataByUID
    ) {
      const carCompanyPath = `/${carCompanyPickedDataByUID?.url_key ?? ''}`
      const carTypePath = `/${carTypePickedDataByUID?.url_key ?? ''}`
      const carEndPath = `/${carEndPickedDataByUID?.url_key ?? ''}`
      const carTagPath = `/${carTagPickedDataByUID?.id ?? ''}`

      router.push(
        {
          pathname: '/category' + carCompanyPath + carTypePath + carEndPath + carTagPath,
          query: {
            currentPage: '1',
            pageSize: !!router.query?.pageSize ? router.query.pageSize : '20',
            sort: !!sort ? Object.keys(JSON.parse(sort ?? '{}')) : 'name',
            dir: !!sort ? JSON.parse(sort ?? '{}').value : 'ASC',
          },
        },
        undefined,
        { shallow: true }
      )
    }
  }, [carTagsPickedUID])

  useEffect(() => {
    if (!slugs) return
    slugs.length < 5 && filterForm.setValue('car_tag_uid', null)
    slugs.length < 4 && filterForm.setValue('car_end_uid', null)
    slugs.length < 3 && filterForm.setValue('car_type_uid', null)
  }, [slugs])

  return (
    <FilterContext.Provider
      value={{
        constants: {},
        atoms: {
          carCompanyPickedUID,
          carTypePickedUID,
          carEndPickedUID,
          carTagsPickedUID,
          sort,
        },
        actions: {
          getFilterFormValue: filterForm.getValues,
          setFilterFormValue: filterForm.setValue,
        },
        selectors: {
          carCompanySlug,
          carTypeSlug,
          carEndSlug,
          carCompanyDataAll,
          carTypeByCompanyDataAll,
          carEndByTypeDataAll,
          carTagsByEndDataAll,
          carCompanyPickedDataBySlug,
          carTypePickedDataBySlug,
          carEndPickedDataBySlug,
          carCompanyPickedDataByUID,
          carTypePickedDataByUID,
          carEndPickedDataByUID,
          dataProducts,
          carTagPickedDataByUID,
        },
        misc: {
          filterForm,
          isFetchingCarCompanyData,
          isFetchingCarTypeData,
          isFetchingCarEndData,
          isFetchingDataProducts,
          isFetchingCarTags,
        },
      }}
    >
      {children}
    </FilterContext.Provider>
  )
}

FilterContext.displayName = 'context.Filter'
FilterContextProvider.displayName = 'contextProvider.Filter'

export const useFilterContext = (): FilterStore => {
  const context = useContext(FilterContext)
  if (!context) {
    throw new Error('FilterContext must be used within a FilterContextProvider')
  }
  return context
}
