/* eslint-disable react-hooks/exhaustive-deps */
/** @format */

import { Route } from 'antd/lib/breadcrumb/Breadcrumb'
import { useRouter } from 'next/router'
import { useEffect, useMemo, useState } from 'react'
import { useQuery } from 'react-query'
import { GET_BANNER_PAGE } from '~src/api'
import { graphQLClientGET } from '~src/api/graphql'
import { GET_PRODUCTS } from '~src/api/product'
import { PaginationCustom } from '~src/components/common/PaginationCustom'
import { ProductFilter } from '~src/components/common/ProductFilter'
import { removeEmptyValueFromObject } from '~src/util'
import { BannerBrand } from '../brand/BannerBrand'
import { ProductList } from '../product/ProductList'
import { HeroTitle } from './HeroTittle'

const DynamicPageSection = () => {
  const router = useRouter()
  const [filter, setFilter] = useState<ProductFilterReq>({})
  const [bannerImage, setBannerImage] = useState<string | undefined>(undefined)
  const [imageCar, setImageCar] = useState<string | undefined>(undefined)

  const { data, isFetching } = useQuery({
    queryKey: ['getProductList', router?.query?.currentPage, filter],
    queryFn: async () => {
      const convertFilterIn = [filter.groupAccessary, filter.company, filter.type, filter.end]
        .filter(Boolean)
        .pop()
      const data = (await graphQLClientGET.request(GET_PRODUCTS, {
        filter: {
          category_uid: {
            eq: convertFilterIn ?? 'Ng==',
          },
          tags: {
            eq: filter.tag ? filter.tag : null,
          },
        },
        pageSize: !!router?.query?.pageSize ? parseInt(`${router.query.pageSize}`) : 20,
        currentPage: !!router?.query?.currentPage ? parseInt(`${router.query.currentPage}`) : 1,
        sort: JSON.parse(filter.sort ?? '{}'),
        search: filter.search,
      })) as {
        products: {
          items: TProduct[]
          page_info: TPageInfo
          total_count: number
        }
      }

      return data
    },
  })

  const convertListProduct = (productList: any) => {
    return productList?.items?.map((item: any) => {
      return { ...item, ...item?.product }
    })
  }

  useEffect(() => {
    const query = router.query || {}
    const data: ProductFilterReq = removeEmptyValueFromObject({
      company: query?.company as string,
      end: query?.end as string,
      groupAccessary: query?.groupAccessary as string,
      sort: query?.sort as string,
      tag: query?.tag as string,
      type: query?.type as string,
      search: query?.search as string,
    })
    setFilter(data)
  }, [router.query])

  const handleFilter = (data: ProductFilterReq) => {
    router.push({ query: data }, undefined, { scroll: false })
  }
  const { data: contentCategoryPage, isLoading: isloadingCategoryPage } = useQuery<any>({
    queryKey: ['getCategoryPage'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_BANNER_PAGE, {
        identifier: 'default-category-banner',
      })) as {
        managebanner: any
      }
      return data?.managebanner
    },
    cacheTime: Infinity,
  })
  const { data: companies }: any = useQuery({
    queryKey: ['getCategoriesByCompany'],
    enabled: false,
  })
  const { data: types }: any = useQuery({
    queryKey: ['getCategoriesByType', { eq: filter?.company }],
    enabled: false,
  })
  const { data: models }: any = useQuery({
    queryKey: ['getCategoriesByEnd', { eq: filter?.type }],
    enabled: false,
  })
  const { data: tags }: any = useQuery({
    queryKey: ['getCategoriesByTags', { eq: filter?.end }],
    enabled: false,
  })
  const breadcrumb: Route[] = useMemo(() => {
    let breadcrumb: Route[] = []

    if (filter?.company && companies?.length) {
      breadcrumb.push({
        breadcrumbName: companies?.find((item: any) => item?.uid === filter?.company)?.name,
        path: '/category/?company=' + filter?.company,
      })
    }
    if (filter?.type && types?.length) {
      breadcrumb.push({
        breadcrumbName: types?.find((item: any) => item?.uid === filter?.type)?.name,
        path: '/category/?company=' + filter?.company + '&type=' + filter?.type,
      })
    }
    if (filter?.end && models?.length) {
      breadcrumb.push({
        breadcrumbName: models?.find((item: any) => item?.uid === filter?.end)?.name,
        path:
          '/category/?company=' + filter?.company + '&type=' + filter?.type + '&end=' + filter?.end,
      })
    }
    if (filter?.tag && tags?.length) {
      breadcrumb.push({
        breadcrumbName: tags?.find((item: any) => item?.id === filter?.tag)?.name,
        path:
          '/category/?company=' +
          filter?.company +
          '&type=' +
          filter?.type +
          '&end=' +
          filter?.end +
          '&tag=' +
          filter?.tag,
      })
    }
    return breadcrumb
  }, [data, companies, types, models, tags])

  return (
    <div className="car-company branch-page">
      <div className="banner-page">
        <BannerBrand
          bannerImage={
            !isloadingCategoryPage &&
            (bannerImage
              ? bannerImage
              : contentCategoryPage?.image
              ? contentCategoryPage.image
              : '/image/bg-product.png')
          }
          breadcrumbs={[...breadcrumb]}
          imageCar={imageCar}
        />
      </div>
      <div id="otpcom--products-list" className="containers">
        <div className="title-promotion">
          <HeroTitle data={filter} />
        </div>
        <div className="content-inner-promotion">
          <div className="filter-promotion">
            <ProductFilter
              onChange={handleFilter}
              value={filter}
              onChangeBanner={setBannerImage}
              onChangeImageCar={setImageCar}
            />
          </div>
          <div className="wrapper-product mb-12">
            <ProductList
              loading={isFetching}
              data={
                !!data?.products?.items?.length && !!convertListProduct(data?.products)
                  ? convertListProduct(data?.products)
                  : []
              }
            />
          </div>
          <div className="w-full flex justify-center">
            {Number(data?.products.page_info.total_pages) > 0 && (
              <PaginationCustom
                current={!!router.query?.currentPage ? parseInt(`${router.query?.currentPage}`) : 1}
                pageSize={!!router?.query?.pageSize ? parseInt(`${router.query?.pageSize}`) : 20}
                total={
                  !!data?.products?.page_info?.total_pages && !!data?.products?.page_info?.page_size
                    ? parseInt(`${data?.products?.page_info?.total_pages}`) *
                      parseInt(`${data?.products?.page_info?.page_size}`)
                    : !!router?.query?.pageSize
                    ? parseInt(`${router.query?.pageSize}`)
                    : 20
                }
                onChange={(page: number) =>
                  router.push({ query: { ...router.query, currentPage: page } })
                }
                totalPage={
                  !!data?.products?.page_info?.total_pages
                    ? parseInt(`${data?.products?.page_info?.total_pages}`)
                    : 1
                }
              />
            )}
          </div>
        </div>
      </div>
    </div>
  )
}

export default DynamicPageSection
