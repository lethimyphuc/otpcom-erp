import React from "react";
import * as loading from "../../../assets/json/loading-KT.json";
import Lottie from "react-lottie";

export const LoadingCP = () => {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: loading,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  return (
    <div className="w-full h-full">
      <div className="relative inset-0 w-full h-full z-[999999999] bg-[#dfdede] opacity-20 flex items-center justify-center">
        {/* <Lottie options={defaultOptions} height={200} width={200} /> */}
      </div>
      <i className="fal fa-spinner fa-spin absolute top-[50%] left-[50%] text-red"></i>
    </div>
  );
};
