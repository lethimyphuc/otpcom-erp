import React from "react";
import * as loading from "../../../assets/json/loading-KT.json";
import Lottie from "react-lottie";

export const Loading = () => {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: loading,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  return (
    <div className="fixed inset-0 w-full h-full z-[999999999] bg-[#6a6767] flex items-center justify-center opacity-20">
      <Lottie options={defaultOptions} height={200} width={200} />
    </div>
  );
};
