import React from 'react'
import Lottie from 'react-lottie'
import * as notFound from '../../../assets/json/error-404.json'
import Link from 'next/link'

type NotFoundProps = {
  title?: string
  desc?: string
}

export const NotFound = ({
  title = '404',
  desc = 'Rất tiếc, chúng tôi không thể tìm thấy đường dẫn này',
}: NotFoundProps) => {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: notFound,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice',
    },
  }

  return (
    <>
      <div id="clouds">
        <div className="cloud x1"></div>
        <div className="cloud x1_5"></div>
        <div className="cloud x2 "></div>
        <div className="cloud x3 "></div>
        <div className="cloud x4"></div>
        <div className="cloud x5 "></div>
      </div>
      <div className="!w-full overflow-hidden rounded h-screen flex justify-center bg-[#329932]">
        {/* <Lottie options={defaultOptions} width={"auto"} height={450} /> */}

        <div className="w-fit h-2/3 flex justify-center items-center flex-col gap-4 container">
          <h1 className="text-[150px]  md:text-[220px] text-white leading-none ">{title}</h1>
          <hr className="inline-block border-2 border-white md:w-full w-[80%]" />
          <p className="text-white text-[14px] md:text-[20px] mt-3 flex justify-center">{desc}</p>
          <Link href={'/'}>
            <a className="buttonRedirectHome text-[18px] md:text-[25px]">Trang Chủ</a>
          </Link>
        </div>
      </div>
    </>
  )
}
