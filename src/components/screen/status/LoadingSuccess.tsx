import React from "react";
import * as loading from "../../../assets/json/loading-success.json";
import Lottie from "react-lottie";

export const LoadingSuccess = () => {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: loading,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  return (
    <div className="">
      <Lottie options={defaultOptions} height={200} width={200} />
    </div>
  );
};
