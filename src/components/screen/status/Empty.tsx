import { Empty as Emp, EmptyProps } from "antd";
import clsx from "clsx";
import React from "react";

export const Empty: React.FC<EmptyProps> = (props) => {
  return (
    <div
      className={clsx(
        props.className,
        "bg-white overflow-hidden rounded-xl h-[calc(100vh-54px-12px-45px-8px-32px-32px)] flex items-center justify-center"
      )}
    >
      <Emp {...props} />
    </div>
  );
};
