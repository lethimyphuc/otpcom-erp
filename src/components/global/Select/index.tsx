import { Select as RSelect, SelectProps as RSelectProps } from 'antd'
import clsx from 'clsx'
import { FieldError } from 'react-hook-form'
import ErrorMessage from '../ErrorMessage'
import styles from './Select.module.scss'

export type SelectProps = RSelectProps & {
  label?: string
  required?: boolean
  error?: FieldError
}

export const Select = ({ label, required, error, ...props }: SelectProps) => {
  return (
    <div className={clsx(styles.formItem, 'border-b-[1px] border-b-[#ccc] border-solid')}>
      <p className="text-[14px] leading-4 font-bold mb-[8px]">
        {label} {required ? <span className="text-red">*</span> : null}
      </p>

      <RSelect {...props} className="formSelect" />

      {!!error ? (
        <div className="mt-[8px]">
          <div className={clsx(error ? 'overflow-visible' : 'hidden')}>
            <ErrorMessage>{error?.message || 'Vui lòng nhập trường này'}</ErrorMessage>
          </div>
        </div>
      ) : null}
    </div>
  )
}
