/* eslint-disable @next/next/no-img-element */
/**
 * /* eslint-disable @next/next/no-img-element
 *
 * @format
 */

import { Dropdown, Menu } from 'antd'
import React, { useEffect, useMemo, useState } from 'react'
import { useQuery, useQueryClient } from 'react-query'
import {graphQLClient, graphQLClientGET} from '~src/api/graphql'
import { GET_AVAILABLESTORES } from '~src/api/language'

const LanguageDropdown = () => {
  const queryClient = useQueryClient()
  const [selectedLanguage, setSelectedLanguage] = useState('')
  const { data: dataLanguage, isLoading: isLoadingDataLanguage } = useQuery({
    queryKey: ['getLanguage'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_AVAILABLESTORES)) as {
        availableStores: TLanguage[]
      }
      // console.log('DATA USER: ', data)
      return data?.availableStores
    },
    cacheTime: Infinity,
  })

  const handleSelectedLanguage = (language: string) => {
    // console.log('Language: ', language)
    graphQLClient.setHeaders({
      store: language,
    })
    localStorage.setItem('language', language)
    // queryClient.refetchQueries();
    setSelectedLanguage(language)
    window.location.reload()
  }

  useEffect(() => {
    const getLanguage = localStorage.getItem('language')
    if (!!getLanguage) {
      if (getLanguage === 'vi') {
        setSelectedLanguage('vi')
      } else {
        setSelectedLanguage('en')
      }
    } else {
      setSelectedLanguage('vi')
    }
  }, [])

  const items = dataLanguage?.map((language: TLanguage, index: number) => {
    return {
      key: index,
      label: '',
      icon: (
        <div
          className="w-full flex items-center gap-3 px-4 py-2 !bg-white hover:!bg-[#f5f5f5] duration-200"
          key={`${language?.store_code}`}
          onClick={() => handleSelectedLanguage(language?.store_code)}
        >
          <div className="!w-[22px] !h-[22px]">
            <img
              className="rounded-full object-fill h-full w-full"
              src={language?.store_code === 'vi' ? '/image/svg/vietnam.svg' : '/image/english.png'}
              alt=""
            />
          </div>
          <span className="hidden xs:block text-[14px]">{language?.store_name}</span>
        </div>
      ),
    }
  })

  return (
    <>
      <Dropdown
        placement="bottom"
        overlay={
          <Menu className="sm:flex flex-col items-center " items={items}>
            {/* {dataLanguage?.map((language: TLanguage) => {
              return (
                <Menu.Item
                  className="w-full"
                  key={`${language?.store_code}`}
                  onClick={() => handleSelectedLanguage(language?.store_code)}
                >
                  <div className="flex items-center gap-3">
                    <div className="w-[22px] h-[22px]">
                      <img
                        className="rounded-full object-fill h-full w-full"
                        src={
                          language?.store_code === 'vi'
                            ? '/image/svg/vietnam.svg'
                            : '/image/english.png'
                        }
                        alt=""
                      />
                    </div>
                    <span className="hidden xs:block">{language?.store_name}</span>
                  </div>
                </Menu.Item>
              )
            })} */}
          </Menu>
        }
        trigger={['click']}
      >
        {selectedLanguage === 'en' ? (
          <button className="nation">
            <div className="img">
              <img src={'/image/english.png'} alt="" />
            </div>
            <span className="hidden xs:block">EN</span>
            <i className="fas fa-caret-down"></i>
          </button>
        ) : (
          <button className="nation">
            <div className="img">
              <img src={'/image/svg/vietnam.svg'} alt="" />
            </div>
            <span className="hidden xs:block">VN</span>
            <i className="fas fa-caret-down"></i>
          </button>
        )}
      </Dropdown>
    </>
  )
}

export default LanguageDropdown
