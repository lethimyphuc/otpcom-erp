/* eslint-disable @next/next/no-img-element */
import { Input, Modal } from 'antd'
import router, { NextRouter, useRouter } from 'next/router'
import React, { FC, useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { ButtonMain } from '../Button/ButtonMain/ButtonMain'

type SearchBoxProps = {
  router: NextRouter
}
export const SearchBox = (props: SearchBoxProps) => {
  const { router } = props
  const [searchValue, setSearchValue] = useState('')
  const [openModal, setOpenModal] = useState(false)

  const handleOpen = () => {
    setOpenModal(true)
  }

  const handleClose = () => {
    setOpenModal(false)
  }

  useEffect(() => {
    setOpenModal(false)
  }, [router.asPath])

  const onSubmit = () => {
    router.push({
      pathname: '/category',
      query: {
        currentPage: 1,
        pageSize: 20,
        search: `${searchValue}`.trim(),
      },
    })
  }

  return (
    <>
      <button className="icon-search " onClick={handleOpen}>
        <div className="img min-w-[12px]" onClick={() => handleClose}>
          <img src={'/image/svg/search.svg'} alt="" className="!w-fit text-white" />
        </div>
      </button>
      <Modal
        className="wrapper-modal-search"
        style={{ top: 0 }}
        visible={openModal}
        onCancel={handleClose}
        footer={null}
        width={'100vw'}
        closeIcon={<img src="/icons/cancel.svg" alt="" />}
      >
        <div>
          <div className="image-box">
            <img src="/image/logo/logo-footer.png" alt="" />
          </div>
          <p className="title-search">Bạn đang tìm gì?</p>
          <div className="wrapper-box-search">
            <Input
              autoFocus
              prefix={<img src="/icons/search-icon.svg" alt="" />}
              placeholder="Nhập tên sản phẩm, thương hiệu,..."
              onKeyDown={(e) => e.key === 'Enter' && onSubmit()}
              onChange={(event) => {
                setSearchValue(event.target.value)
              }}
            />
            <div className="button-search">
              <ButtonMain
                background="green"
                type="button"
                className="rounded-[6px!important] h-[44px] z-10"
                onClick={onSubmit}
              >
                <div className="flex items-center gap-[0.4rem]">
                  <div className="icon">
                    <img src="/icons/search-icon.svg" alt="" />
                  </div>
                  <div className="text">Tìm kiếm</div>
                </div>
              </ButtonMain>
            </div>
          </div>

          <div className="wrapper-image">
            <img src="/image/car-search-modal.png" alt="" />
          </div>
        </div>
      </Modal>
    </>
  )
}
