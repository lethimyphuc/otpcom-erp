/* eslint-disable @next/next/no-img-element */
import clsx from 'clsx'
import styles from '../../pages/shopping-cart/ShoppingCart.module.scss'
import React from 'react'
import { Controller, useForm } from 'react-hook-form'
import { _format } from '~src/util'
import { useMutation, useQueryClient } from 'react-query'
import { graphQLClient } from '~src/api/graphql'
import { APPLY_COUPON_TO_CART, REMOVE_COUPON_TO_CART } from '~src/api/coupon'
import { toast } from 'react-toastify'
import { ButtonMain } from '../Button/ButtonMain/ButtonMain'
import { CiCircleRemove } from 'react-icons/ci'
import { Tooltip } from 'antd'

type SumaryOrderProps = {
  dataCart?: TDataUpdateToCart
  setCurrentStep: React.Dispatch<React.SetStateAction<number>>
}
const SumaryOrder = (props: SumaryOrderProps) => {
  const { dataCart, setCurrentStep } = props
  const queryClient = useQueryClient()
  const { handleSubmit, control, setValue, watch } = useForm<TApplyCouponToCart>({
    defaultValues: {
      coupon_code: '',
    },
  })

  const { isLoading: isLoadingDataApplyCouponToCart, mutate: mutateApplyCouponToCart } =
    useMutation({
      mutationFn: async (data: TApplyCouponToCart) => {
        return (await graphQLClient.request(APPLY_COUPON_TO_CART, {
          input: {
            cart_id: localStorage?.getItem('cartId'),
            coupon_code: data?.coupon_code,
          },
        })) as {
          applyCouponToCart: TDataUpdateToCart
          errors: { message: string }[]
        }
      },
      onSuccess: (response: any) => {
        if (!!response?.errors?.length) {
          toast.error(response?.errors[0]?.message)
          return
        } else {
          toast.success('Áp dụng mã giảm giá thành công')
          setValue('coupon_code', '')
          queryClient.setQueryData(['getCartId'], (oldData: any) => {
            console.log('OLD DATA: ', oldData)
            return {
              ...oldData,
              cart: {
                ...oldData?.cart,
                total_quantity:
                  response?.applyCouponToCart?.cart?.total_quantity ??
                  oldData?.cart?.total_quantity,
                applied_coupons: response?.applyCouponToCart?.cart?.applied_coupons,
                prices: {
                  ...oldData?.cart?.prices,
                  grand_total: response?.applyCouponToCart?.cart?.prices?.grand_total,
                  discounts: response?.applyCouponToCart?.cart?.prices?.discounts,
                },
              },
            }
          })
        }
      },
    })

  const { isLoading: isLoadingDataRemoveCouponToCart, mutate: mutateRemoveCouponToCart } =
    useMutation({
      mutationFn: async () => {
        return await graphQLClient.request(REMOVE_COUPON_TO_CART, {
          input: {
            cart_id: localStorage?.getItem('cartId'),
          },
        })
      },
      onSuccess: (response: any) => {
        queryClient.setQueryData(['getCartId'], (oldData: any) => {
          console.log('OLD DATA: ', oldData, response)
          return {
            ...oldData,
            cart: {
              ...oldData?.cart,
              total_quantity:
                response?.removeCouponFromCart?.cart?.total_quantity ||
                oldData?.cart?.total_quantity,
              applied_coupons: null,
              prices: {
                ...oldData?.cart?.prices,
                grand_total: response?.removeCouponFromCart?.cart?.prices?.grand_total,
                discounts: response?.removeCouponFromCart?.cart?.prices?.discounts,
              },
            },
          }
        })
      },
    })

  const onSubmit = (data: TApplyCouponToCart) => {
    console.log('Data: ', data, dataCart?.cart?.applied_coupons?.length)
    if (!!`${data?.coupon_code}`.trim()) {
      const DATA_SUBMIT = {
        coupon_code: `${data?.coupon_code}`,
      }
      if (!!dataCart?.cart?.applied_coupons?.length) {
        mutateRemoveCouponToCart()
      } else {
        mutateApplyCouponToCart(DATA_SUBMIT)
      }
    } else {
      toast.error('Vui lòng nhập mã giảm giá')
    }
  }

  return (
    <div className={clsx(styles.rightSection, styles.cartRightSection)}>
      <div className={clsx(styles.summaryContainer)}>
        <h3>Tóm tắt đơn hàng</h3>
        <div className={clsx(styles.summaryInfo, styles.cartPrice, 'flex mt-10')}>
          <p>Thành tiền</p>
          <p>
            {_format.getVND(dataCart?.cart?.prices?.subtotal_excluding_tax?.value || 0)}
            {dataCart?.cart?.prices?.subtotal_excluding_tax?.currency || ' VNĐ'}
          </p>
        </div>
        <div className={clsx(styles.summaryInfo, styles.shipping, 'flex')}>
          <p>Vận chuyển</p>
          <p>Liên hệ phí vận chuyển sau</p>
        </div>
        <div className={clsx(styles.summaryInfo)}>
          <div className="flex items-start">
            <Tooltip title="Xóa mã giảm giá">
              <ButtonMain
                background="transparent"
                loading={isLoadingDataRemoveCouponToCart}
                className="-translate-y-2/4 mr-2 !bg-[transparent] !p-0"
                type="button"
                onClick={() => mutateRemoveCouponToCart()}
              ></ButtonMain>
            </Tooltip>

            <p>Mã giảm giá</p>
          </div>

          {dataCart?.cart?.prices?.discounts?.map((dicount) => {
            return (
              <>
                <p className="mr-4">{dicount?.label}</p>
                <p className={'text-[#C82127] font-bold'}>
                  {_format.getVND(dicount?.amount?.value || 0)}
                  {dicount?.amount?.currency || ' VNĐ'}
                </p>
              </>
            )
          })}
        </div>
        <div className={clsx(styles.underline)}></div>
        <div className={clsx(styles.voucher)}>
          <Controller
            name="coupon_code"
            control={control}
            render={({ field }) => <input {...field} type="text" placeholder="Mã giảm giá" />}
          />
          <ButtonMain
            background="green"
            type="button"
            onClick={handleSubmit(onSubmit)}
            loading={isLoadingDataApplyCouponToCart}
          >
            Sử dụng
          </ButtonMain>
        </div>
        <div className={clsx(styles.summaryInfo, styles.total)}>
          <p>Tổng cộng</p>
          <p>
            {_format.getVND(dataCart?.cart?.prices?.grand_total?.value || 0)}
            {dataCart?.cart?.prices?.grand_total?.currency || ' VNĐ'}
          </p>
        </div>
        <button
          className={clsx(styles.orderButton)}
          onClick={() => setCurrentStep((prev) => prev + 1)}
        >
          <img src={'/icons/cart-icon.svg'} alt="" />
          <p>Mua ngay</p>
        </button>
      </div>
    </div>
  )
}

export default SumaryOrder
