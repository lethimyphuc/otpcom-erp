import router from "next/router";
import React, { useEffect } from "react";
// import { IconButton, FormCard } from "~src/components";
import Modal from "~src/components/global/Modal";
import { LoadingSuccess } from "~src/components/screen/status/LoadingSuccess";
import { _format } from "~src/util";

type TPropsModal = {
  visible: boolean;
  onCancel: () => void;
  content: string;
};

export const ModalShowNotification: React.FC<TPropsModal> = ({
  visible,
  onCancel,
  content,
}) => {
  return (
    <Modal
      visible={visible}
      onCancel={onCancel}
      width={400}
      classModal="!top-[30%]"
    >
      <LoadingSuccess />
      <div className="text-center w-full text-green">
        <p>{content}</p>
      </div>
    </Modal>
  );
};
