/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
// import { FormCard } from "~src/components";
import Modal from "~src/components/global/Modal";
import { _format } from "~src/util";
import * as loading from "../../../../assets/json/loading-modal.json";
import Lottie from "react-lottie";

export const ModalLoading: React.FC<{
  visible: boolean;
  onCancel: () => void;
}> = ({ visible, onCancel }) => {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: loading,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };
  return (
    <Modal visible={visible} onCancel={onCancel} width={400}>
      {/* <FormCard>
        <FormCard.Body>
          <div className="w-full grid text-center justify-center py-2 h-[300px] items-center">
            <Lottie options={defaultOptions} height={200} width={200} />
            <span className="text-[#8f8e8e]">
              Đang tải vui lòng đợi trong giây lát ...
            </span>
          </div>
        </FormCard.Body>
      </FormCard> */}
    </Modal>
  );
};
