/* eslint-disable @next/next/no-img-element */
import React from 'react'
import clsx from 'clsx'
import { BreadcrumbBanner, BreadcrumbProps } from '~src/components/global/Layout/BreadcrumbBanner'
import styles from './BreadcrumbHead.module.scss'

type BreadcrumbHeadProps = Partial<BreadcrumbProps> & {}

export const BreadcrumbHead = ({ ...props }: BreadcrumbHeadProps) => {
  return (
    <div className={clsx(styles.breadcrumbContainer)}>
      <div className={clsx(styles.breadcrumbHead)}>
        <div className={clsx(styles.iconHead)}>
          <img src={'/icons/breadcrumb-head.svg'} alt="" />
          <img src={'/icons/breadcrumb-head.svg'} alt="" />
        </div>
        <div className={clsx(styles.breadcrumb)}>
          <BreadcrumbBanner {...props} breadcrumbHead={true} />
        </div>
      </div>
    </div>
  )
}
