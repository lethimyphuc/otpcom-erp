/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @next/next/no-img-element */
import clsx from 'clsx'
import { useRouter } from 'next/router'
import { ReactNode, useEffect } from 'react'
import { useQuery } from 'react-query'
import { INFO_CUSTOMER } from '~src/api'
import { graphQLClient } from '~src/api/graphql'
import { BreadcrumbHead } from '~src/components/global/Layout/BreadcrumHead'
import styles from './AccountInfo.module.scss'
import { useCookies } from 'react-cookie'

export const AccountLayout = ({ children }: { children: ReactNode }) => {
  const router = useRouter()
  const [cookies, setCookie, removeCookie] = useCookies(['token'])

  const { data: dataInfoCustomer, refetch } = useQuery({
    queryKey: ['getInfoCustomerOrder'],
    queryFn: async () => {
      const data = (await graphQLClient.request(INFO_CUSTOMER, {
        customer: {
          orders: {
            // filter: {
            //   number: {
            //     eq: !!router.query?.search ? router.query?.search : ''
            //   }
            // },
            currentPage: !!router.query?.currentPage ? parseInt(`${router.query?.currentPage}`) : 1,
          },
        },
      })) as {
        customer: any
      }
      return data?.customer
    },
    cacheTime: Infinity,
  })

  useEffect(() => {
    if (typeof window !== 'undefined') {
      if (!!localStorage?.getItem('token')) {
        refetch()
      }
    }
  }, [])

  const tabs = [
    {
      name: 'Thông tin tài khoản',
      icon: '/icons/user-info.svg',
      route: '/account',
    },
    {
      name: 'Sổ địa chỉ',
      icon: '/icons/history-step.svg',
      route: '/account/address',
    },
    {
      name: 'Thay đổi mật khẩu',
      icon: '/icons/padlock.svg',
      route: '/account/change-password',
    },
    {
      name: 'Lịch sử đơn hàng',
      icon: '/icons/order-history.svg',
      route: '/account/orders',
    },
  ]

  return (
    <div className={clsx(styles.accountContainer)}>
      <BreadcrumbHead />
      <div className={clsx(styles.accountBg)}>
        <div className={clsx(styles.accountBox)}>
          <div className={clsx(styles.accountBoxLeft)}>
            <div className={clsx(styles.boxLeftHead)}>
              <img src={dataInfoCustomer?.profile_picture || '/image/user-avatar.png'} alt="" />
              <div>
                <p>
                  {dataInfoCustomer?.lastname} {dataInfoCustomer?.firstname}
                </p>
                <span
                  className="cursor-pointer"
                  onClick={() => {
                    localStorage.removeItem('token')
                    removeCookie('token', { path: '/' })
                    localStorage.removeItem('loginSocial')
                    localStorage.removeItem('cartId')
                    window.location.replace(`${window.location.origin}/login`)
                  }}
                >
                  Đăng xuất
                </span>
              </div>
            </div>
            <ul>
              {tabs.map((item) => (
                <li
                  key={item.route}
                  className={clsx(
                    `${
                      item.route === router.pathname ||
                      (item.route === '/account/orders' && router.pathname?.includes(item.route))
                        ? styles.active
                        : ''
                    }`
                  )}
                  onClick={() => router.push(item.route)}
                >
                  <img src={item.icon} alt="" />
                  <p>{item.name}</p>
                </li>
              ))}
            </ul>
          </div>

          {children}
        </div>
      </div>
    </div>
  )
}
