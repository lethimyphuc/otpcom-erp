/* eslint-disable @next/next/no-img-element */
import Link from 'next/link'
import { useRouter } from 'next/router'
import React, { useEffect, useMemo, useState } from 'react'
import { useQuery } from 'react-query'
import { GET_CONTENT_HOME_PAGE, GET_MENU_FOOTER } from '~src/api'
import { graphQLClientGET } from '~src/api/graphql'
import { GET_LIST_POLICY } from '~src/api/post'

type TProps = {
  hover: boolean
}

declare global {
  interface Window {
    FB: {
      XFBML: {
        parse(): void
      }
    }
  }
}

const Footer: React.FC<TProps> = ({ hover }) => {
  const router = useRouter()
  const [shouldLoadFBPlugin, setShouldLoadFBPlugin] = useState(false)

  const { data: contentHomePage } = useQuery<any>({
    queryKey: ['getFooterContact'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_CONTENT_HOME_PAGE, {
        identifiers: ['footer-link-social'],
      })) as { snowdogMenus: { items: any } }
      return data.snowdogMenus.items[0].nodes.items
    },
  })
  const { data: footerContact } = useQuery<any>({
    queryKey: ['footerContact'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_CONTENT_HOME_PAGE, {
        identifiers: ['footer-contact'],
      })) as { snowdogMenus: { items: any } }
      return data.snowdogMenus.items[0].nodes.items
    },
  })

  const { data: listPolicy } = useQuery({
    queryKey: ['listPolicy'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_LIST_POLICY, {
        action: 'get_post_by_categoryId',
        categoryId: 4,
      })) as {
        mpBlogPosts: {
          items: TPost[]
          page_info: TPageInfo
          total_count: number
        }
      }
      return data.mpBlogPosts.items
    },
    cacheTime: Infinity,
  })

  const { zalo, facebook, youtube } = useMemo(() => {
    let zalo = ''
    let facebook = ''
    let youtube = ''

    contentHomePage?.forEach?.((item: any) => {
      if (item?.classes == 'zalo-link') {
        zalo = item?.content
      }
      if (item?.classes == 'youtube') {
        youtube = item?.content
      }
      if (item?.classes == 'facebook') {
        facebook = item?.content
      }
    })

    return {
      zalo,
      facebook,
      youtube,
    }
  }, [contentHomePage])

  useEffect(() => {
    // Đặt thời gian chờ trước khi hiển thị Facebook plugin (ví dụ: 2000ms = 2 giây)
    const delayTime = 1000

    // Sau khoảng thời gian chờ, sẽ hiển thị Facebook plugin
    const timer = setTimeout(() => {
      setShouldLoadFBPlugin(true)
    }, delayTime)

    return () => clearTimeout(timer)
  }, [])

  useEffect(() => {
    // Nếu shouldLoadFBPlugin là true (đã hết thời gian chờ), thì tải và parse Facebook plugin
    if (shouldLoadFBPlugin) {
      if (window && typeof window !== 'undefined') {
        window.FB?.XFBML?.parse()
      }
    }
  }, [shouldLoadFBPlugin])

  const { data } = useQuery({
    queryKey: ['getFooterMenu'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_MENU_FOOTER, {
        identifier: 'footer-link-menu',
      })) as { snowdogMenuNodes: { items: TMenuHeader[] } }
      return data.snowdogMenuNodes.items
    },
    cacheTime: Infinity,
  })
  return (
    <footer className="app-footer">
      <div className="img-position">
        <img src="/image/124.png" alt="" />
      </div>
      <div className="bg-footer">
        <div className="img">
          <img src="/image/Union.png" alt="" />
        </div>
      </div>
      <div className="!mb-20 md:!m-0">
        <div className="inner">
          <div className="logo-footer">
            <Link href="/">
              <a className="logo-link">
                <img src="/image/logo/logo-footer.png" alt="" />
              </a>
            </Link>
          </div>
          <div className="content-inners containers">
            <div className="info">
              <div className="info-inner">
                <h1 className="text-[1.6rem] text-white font-bold">Thông tin công ty</h1>
                {footerContact
                  ?.filter((item: any) => item?.classes !== 'dkkd')
                  ?.map((item: any, index: number) => {
                    return (
                      <div className="info-item" key={index}>
                        <div className="icon">
                          <img src={item?.image} alt="" />
                        </div>
                        <div className="text-group">
                          <p className="text">{item?.title}</p>
                          {item?.classes === 'phone_number' ? (
                            <Link href={`tel:${item?.content}`}>
                              <a className="subtext" href={`tel:${item?.content}`}>
                                {item?.content}
                              </a>
                            </Link>
                          ) : item?.classes === 'email' ? (
                            <Link href={`mailto:${item?.content}`}>
                              <a className="subtext" href={`mailto:${item?.content}`}>
                                {item?.content}
                              </a>
                            </Link>
                          ) : (
                            <p className="subtext">{item?.content}</p>
                          )}
                        </div>
                      </div>
                    )
                  })}
              </div>
              {/* <div className="tags">
                <img src="/image/tags.png" alt="" />
              </div> */}
            </div>
            <div className="catalog">
              <div className="catalog-inner">
                <h1 className="text-[1.6rem] text-white font-bold text-center md:text-left">
                  Danh mục
                </h1>
                {data?.map((item: any, index: number) => {
                  const pathname = router?.pathname === '/' ? undefined : router?.pathname
                  const isActive = item?.content?.includes(pathname?.split('/')?.[1])
                  const isActiveChild = item?.childrens?.find(
                    (child: any) => child?.title === pathname
                  )
                  if (item?.level !== 0) {
                    return <React.Fragment key={index}></React.Fragment>
                  }
                  return (
                    <div
                      className={`${isActive || isActiveChild ? 'active' : ''} item-cata `}
                      key={index}
                    >
                      <Link href={item?.content || '/'}>
                        <a>
                          <div className="text !w-fit md:!w-80 !text-left ">
                            <span>{item?.title}</span>
                          </div>
                        </a>
                      </Link>
                    </div>
                  )
                })}
              </div>
            </div>
            <div className="catalog !py-10 md:!py-0 sm:!w-fit  !w-full">
              <div className="catalog-inner">
                <h1 className="text-[1.6rem] text-white font-bold text-center md:text-left">
                  Chính sách chung
                </h1>
                {listPolicy?.map((item: any, index: number) => {
                  const pathname = router?.pathname === '/' ? undefined : router?.pathname
                  const isActive = item?.content?.includes(pathname?.split('/')?.[1])
                  const isActiveChild = item?.childrens?.find(
                    (child: any) => child?.title === pathname
                  )
                  return (
                    <div
                      className={`${
                        isActive || isActiveChild ? 'active' : ''
                      } item-cata !justify-center md:!justify-start `}
                      key={index}
                    >
                      <Link href={'/policy/' + item?.url_key || '/'}>
                        <a>
                          <div className="text !w-fit md:!w-80 !text-left  ">
                            <span className="whitespace-nowrap">{item?.name}</span>
                          </div>
                        </a>
                      </Link>
                    </div>
                  )
                })}
                <div className={`item-cata !justify-center md:!justify-start`}>
                  <Link href="http://online.gov.vn/Home/WebDetails/111471">
                    <a className="w-48 md:w-64 mt-4">
                      <img alt="" title="" src="/image/logoSaleNoti.png" />
                    </a>
                  </Link>
                </div>
              </div>
            </div>
            <div className="about !pt-10 lg:!pt-0 !w-full sm:!w-fit md:!w-full lg:!w-fit">
              <div className="about-inner">
                <div className="item-about">
                  <div className="info-top">
                    <div className="text">
                      <span>Theo dõi chúng tôi</span>
                    </div>
                    <div className="social">
                      <div className="img">
                        <a target="_blank" href={facebook} rel="noopener noreferrer">
                          <img src="/icons/facebook.svg" alt="" />
                        </a>
                      </div>
                      <div className="img">
                        <a target="_blank" href={zalo} rel="noopener noreferrer">
                          <img src="/icons/zalo.svg" alt="" />
                        </a>
                      </div>
                      <div className="img">
                        <a target="_blank" href={youtube} rel="noopener noreferrer">
                          <img src="/icons/youtube.svg" alt="" />
                        </a>
                      </div>
                    </div>
                  </div>
                  <div className="info-bottom">
                    {shouldLoadFBPlugin && (
                      <div
                        className="fb-page"
                        data-href="https://www.facebook.com/otpcom"
                        data-width=""
                        data-height=""
                        data-small-header="false"
                        data-adapt-container-width="true"
                        data-hide-cover="false"
                        data-show-facepile="true"
                      >
                        <blockquote
                          cite="https://www.facebook.com/otpcom"
                          className="fb-xfbml-parse-ignore"
                        ></blockquote>
                      </div>
                    )}
                  </div>
                  <div className="copy-right">
                    <div className="flex items-center justify-center">
                      <span>© All rights reserved </span>
                      <img
                        className="ml-3 w-auto h-10 object-contain"
                        src="/image/logo/logo-footer.png"
                        alt=""
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className=" bg-label !py-4">
            <div className=" containers !block">
              <span>{footerContact?.find((item: any) => item?.classes == 'dkkd')?.content}</span>
            </div>
          </div>
        </div>
      </div>
    </footer>
  )
}

export default Footer
