/* eslint-disable @next/next/no-img-element */
import React from "react";
import { BreadcrumbBanner } from "../../BreadcrumbBanner";

type BannerProps = {
  title: string;
};
const Banner = (props: BannerProps) => {
  const { title } = props;
  return (
    <div className="banner-promotion">
      <div className="img">
        <img src="/image/bg-product.png" alt="" />
      </div>
      <div className="img-position">
        <div className="img">
          <img src="/image/21.png" alt="" />
        </div>
      </div>
      <div className="content-inner !translate-x-[-50%] [&_ol]:justify-start container">
        <div className="breadcrum">
          <BreadcrumbBanner breadcrumbHead={false} />
        </div>
        <div className="title">{title}</div>
      </div>
    </div>
  );
};

export default Banner;
