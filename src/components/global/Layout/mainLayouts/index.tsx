/* eslint-disable @next/next/no-img-element */
/**
 * /* eslint-disable @next/next/no-img-element
 *
 * @format
 */

/* eslint-disable react-hooks/exhaustive-deps */
import React, { ReactElement, useCallback, useEffect, useRef, useState } from 'react'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { useMediaQuery } from 'react-responsive'
import {
  CUSTOMER_CART,
  GET_CONTENT_HOME_PAGE,
  INFO_CUSTOMER,
  LOGIN_SOCIAL_GOOGLE,
  MERGE_CART,
} from '~src/api'
import { CREATE_EMPTY_CART } from '~src/api/cart'
import { graphQLClient, graphQLClientGET } from '~src/api/graphql'
import { TAuthenticateGoogle } from '~src/types/authenticate'
import { TlayoutWithChild } from '~src/types/layout'
import useClickOutside from '~src/util/useClickOutside'
import Footer from './Footer'
import Header from './Header'
import MiniCart from './MiniCart'
import Sidebar from './SideBar'
import { AnimatePresence } from 'framer-motion'
import { toast } from 'react-toastify'
import cookie, { useCookies } from 'react-cookie'
import Link from 'next/link'

type TProps = {
  breadcrumb?: string
  userPage?: boolean
  children?: React.ReactNode | any
}

export const MainLayout: TlayoutWithChild & React.FC<TProps> = ({ children }) => {
  let isTabletOrMobile = useMediaQuery({ query: '(max-width: 1200px)' })
  const [hover, setHover] = useState(false)
  const isBreadcrumb: any = children?.props?.isBreadcrumb
  const [isScroll, setIsScroll] = useState(false)
  // const [miniCartOpen, setMiniCartOpen] = useState(false)
  const queryClient = useQueryClient()
  const cartRef = useRef(null)
  const headerRef = useRef(null)
  const [cookies, setCookie] = useCookies(['token'])

  const handleOpenCart = (visible: boolean) => {
    queryClient.setQueryData<boolean>('miniCartVisible', visible)
  }

  const { data: contentHomePage } = useQuery<any>({
    queryKey: ['getContentHomePage'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_CONTENT_HOME_PAGE, {
        identifiers: ['info-contact'],
      })) as {
        snowdogMenus: any
      }
      return data?.snowdogMenus
    },
    cacheTime: Infinity,
  })

  const redirectToZalo = () => {
    const zaloLink = contentHomePage?.items
      ?.find((item: any) => item.identifier === 'info-contact')
      ?.nodes?.items?.find((item: any) => item.classes == 'zalo')?.content
    if (zaloLink) {
      window.open(zaloLink)
    }
  }
  const getZaloInfo = contentHomePage?.items
    ?.find((item: any) => item.identifier === 'info-contact')
    ?.nodes?.items?.find((item: any) => item.classes == 'zalo')
  const getPhoneInfo = contentHomePage?.items
    ?.find((item: any) => item.identifier === 'info-contact')
    ?.nodes?.items?.find((item: any) => item.classes == 'phone-number')

  const getPhoneNumber = () => {
    const phone = contentHomePage?.items
      ?.find((item: any) => item.identifier === 'info-contact')
      ?.nodes?.items?.find((item: any) => item.classes == 'phone-number')?.content
    return phone
  }

  const loginWithGoogle = useMutation({
    mutationKey: 'loginWithGoogle',
    mutationFn: async (data: TAuthenticateGoogle) => {
      return await graphQLClient.request(LOGIN_SOCIAL_GOOGLE, {
        authuser: data?.authuser,
        code: data?.code,
        handle: data?.handle,
        prompt: data?.prompt,
        scope: data?.scope,
        state: data?.state,
      })
    },
    onSuccess: async (response: any) => {
      const token = response?.generateCustomerTokenSocialLogin?.token
      localStorage.setItem('token', token)
      setCookie('token', token, { path: '/' })

      graphQLClient.setHeaders({
        Authorization: `Bearer ${response?.generateCustomerTokenSocialLogin?.token}`,
        'Content-Type': 'application/json',
        accept: 'application/json',
      })
      graphQLClientGET.setHeaders({
        Authorization: `Bearer ${response?.generateCustomerTokenSocialLogin?.token}`,
        'Content-Type': 'application/json',
        accept: 'application/json',
      })
      const cartEmptyResponse: any = await graphQLClient.request(CREATE_EMPTY_CART, {
        input: {},
      })
      const source_cart_id = localStorage.getItem('cartId')
      const destination_cart_id = cartEmptyResponse?.createEmptyCart

      if (destination_cart_id && source_cart_id) {
        try {
          mergeCart.mutate({
            source_cart_id: source_cart_id,
            destination_cart_id: destination_cart_id,
          })
        } catch (error) {
          // console.log('Merge cart error: ', error)
        }
      }

      localStorage.setItem('cartId', destination_cart_id)
    },
    onError: (error: any) => {
      toast.error(error?.response.errors[0]?.message)
    },
  })

  const createEmptyCart = useMutation({
    mutationKey: 'createEmptyCart',
    mutationFn: async () => {
      return await graphQLClient.request(CREATE_EMPTY_CART, {
        input: {},
      })
    },
    onSuccess: (response: any) => {
      localStorage.setItem('cartId', response?.createEmptyCart)
    },
  })

  const mergeCart = useMutation({
    mutationKey: 'mergeCart',
    mutationFn: async (data: any) => {
      return await graphQLClient.request(MERGE_CART, {
        source_cart_id: data?.source_cart_id,
        destination_cart_id: data?.destination_cart_id,
      })
    },
    onSuccess: (response: any) => {
      queryClient.setQueryData(['getCartId'], (oldData: any) => {
        return {
          ...oldData,
          cart: response?.mergeCart?.cart,
        }
      })
    },
    onError: () => {
      createEmptyCart.mutate()
    },
  })

  const { data: dataInfoCustomer, isLoading: isLoadingDataInfoCustomer } = useQuery({
    queryKey: ['getInfoCustomer'],
    queryFn: async () => {
      const data = (await graphQLClient.request(INFO_CUSTOMER)) as {
        customer: any
      }
      return data
    },
    cacheTime: Infinity,
    enabled: typeof window == 'object' && !!localStorage?.token,
  })

  const {
    data: dataCustomerCart,
    isLoading: isLoadingDataCustomerCart,
    refetch,
  } = useQuery({
    queryKey: ['getCustomerCart'],
    queryFn: async () => {
      const data = (await graphQLClient.request(CUSTOMER_CART)) as TDataUpdateToCartCustomer
      return data
    },
    cacheTime: Infinity,
    onSuccess: (response) => {
      if (localStorage.getItem('cartId') !== `${response?.customerCart?.id}`) {
        mergeCart.mutate({
          source_cart_id: localStorage.getItem('cartId'),
          destination_cart_id: `${response?.customerCart?.id}`,
        })
      }
    },
    enabled: false,
  })

  useEffect(() => {
    if (!localStorage.getItem('cartId')) {
      createEmptyCart.mutate()
    } else {
      if (!!localStorage.getItem('token')) {
        refetch()
      }
    }
  }, [])

  useEffect(() => {
    if (window.location.href.includes('handle=google')) {
      let convertString = `${window.location.search}`.substring(1)
      const dataSearch = JSON.parse(
        '{"' +
          decodeURI(convertString).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g, '":"') +
          '"}'
      )
      loginWithGoogle.mutateAsync(dataSearch).then(() => {
        localStorage.setItem('loginSocial', dataSearch?.handle)
      })
    }
  }, [])
  const handleHover = useCallback((bool: boolean) => setHover(bool), [])

  // const handleOpenCart = useCallback((bool: boolean) => setMiniCartOpen(bool), [])

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    })
  }

  useEffect(() => {
    const handleScroll = () => {
      const scrollTop = window.pageYOffset
      if (scrollTop > 80) {
        setIsScroll(true)
      } else {
        setIsScroll(false)
      }
    }

    window.addEventListener('scroll', handleScroll)

    return () => {
      window.removeEventListener('scroll', handleScroll)
    }
  }, [])

  useClickOutside([headerRef, cartRef], () => handleOpenCart(false))

  return (
    <div className="app">
      <div className="relative">
        <Header
          {...{
            hover,
            handleHover,
            isBreadcrumb,
            isScroll,
            headerRef,
            dataInfoCustomer,
          }}
        />
        <AnimatePresence mode="wait">
          {isTabletOrMobile ? <Sidebar {...{ hover, handleHover, dataInfoCustomer }} /> : <></>}
        </AnimatePresence>
      </div>
      <main className={`app-main ${isScroll ? 'isScroll' : ''}`}>
        <div className="app-content"> {children}</div>
      </main>
      <Footer {...{ hover }} />

      {!!getPhoneNumber() && (
        <div className="social-fixed">
          <Link href={`tel:+${getPhoneNumber()}`}>
            <div className="a-tag" onClick={getPhoneNumber}>
              <div className="phone-button">
                <div className="contact-desktop">
                  <div className="icon trin-trin xs:!flex">
                    <img src={getPhoneInfo?.image || '/icons/PhoneCall.svg'} alt="" className="" />
                  </div>
                  <a href={`tel:+${getPhoneNumber()}`} className="text">
                    {getPhoneNumber() || '028 2269 1234'}
                  </a>
                </div>
                <div className="contact-mobile">
                  <p className="">Liên hệ: {getPhoneNumber() || '028 2269 1234'} </p>
                </div>
              </div>
            </div>
          </Link>
          <button onClick={redirectToZalo} className="zalo-button">
            <img src={getZaloInfo?.image || '/images/image3.png'} alt="" className="icon-zalo" />
            <p className="text-zalo flex gap-3 items-center">
              Gửi tin nhắn <i className="fab fa-facebook-messenger"></i>
            </p>
          </button>
          {/* <button
            onClick={() => {
              window.open('https://www.messenger.com/t/otpcom')
            }}
            className="zalo-button"
          >
            <img src="/image/messenger-ico.webp" alt="" className="icon-zalo" />
            <p className="text-zalo flex gap-3 items-center">
              Gửi tin nhắn <i className="fab fa-facebook-messenger"></i>
            </p>
          </button> */}
        </div>
      )}

      <MiniCart {...{ hover, handleHover }} isScroll={isScroll} cartRef={cartRef} />
      {isScroll && (
        <button onClick={scrollToTop} className={`back-to-top-button`}>
          <div className="img">
            <img src="/icons/icon-btt.svg" alt="" />
          </div>
          <div className="car-focus">
            <img src="/image/car-focus.png" alt="" />
          </div>
          <div className="anima">
            <div className="triangle">
              <img src="/icons/back.svg" alt="" />
            </div>
            <div className="triangle">
              <img src="/icons/back2.svg" alt="" />
            </div>
          </div>
        </button>
      )}
    </div>
  )
}

MainLayout.getLayout = (page: ReactElement) => <MainLayout>{page}</MainLayout>
