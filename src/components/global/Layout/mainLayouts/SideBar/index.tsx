/**
 * /* eslint-disable @next/next/no-html-link-for-pages
 *
 * @format
 */

/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @next/next/no-img-element */
import { Drawer, Collapse, Menu, Dropdown, Space } from 'antd'
import clsx from 'clsx'
import _ from 'lodash'
import Link from 'next/link'
import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import { useQuery } from 'react-query'
import { SNOW_DOG_MENU_NODES } from '~src/api'
import {graphQLClient, graphQLClientGET} from '~src/api/graphql'
import { AnimatePresence, Variants, motion } from 'framer-motion'
import { removeEmptyValueFromObject } from '~src/util'
import { useCookies } from 'react-cookie'

type TProps = {
  hover: boolean
  handleHover: (bool: boolean) => void
  dataInfoCustomer?: any
}

const Sidebar: React.FC<TProps> = ({ hover = false, handleHover, dataInfoCustomer = null }) => {
  const router = useRouter()
  const [activeChildIdx, setActiveChildIdx] = useState(-1)
  const [cookies, setCookie, removeCookie] = useCookies(['token'])

  const { data: dataHeaderMenu } = useQuery({
    queryKey: ['getHeaderMenu'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(SNOW_DOG_MENU_NODES, {
        identifier: 'mainmenu',
      })) as { snowdogMenuNodes: { items: TMenuHeader[] } }
      return data
    },
    cacheTime: Infinity,
  })

  const variants: Variants = {
    init: {
      x: '-100%',
    },
    show: {
      x: '0%',
    },
    hide: {
      x: '-100%',
    },
  }
  const menu = (
    <Menu className="mt-8 p-4">
      <Menu.Item>
        <Link href="/account">
          <span>Thông tin tài khoản</span>
        </Link>
      </Menu.Item>
      <Menu.Item
        onClick={() => {
          localStorage.removeItem('token')
          removeCookie('token', { path: '/' })
          localStorage.removeItem('loginSocial')
          localStorage.removeItem('cartId')
          window.location.replace(`${window.location.origin}/login`)
        }}
      >
        Đăng xuất
      </Menu.Item>
    </Menu>
  )

  useEffect(() => {
    handleHover(false)
  }, [router])

  useEffect(() => {
    if (!hover) {
      setActiveChildIdx(-1)
    }
  }, [hover])

  return (
    <React.Fragment>
      <motion.div
        variants={variants}
        initial={'init'}
        animate={hover ? 'show' : 'hide'}
        transition={{ duration: 0.5, ease: [0.33, 1, 0.68, 1] }}
        className="fixed z-[999] left-0 top-0 bg-white h-screen w-full mt-[7rem]"
      >
        {!dataInfoCustomer ? (
          <div className="flex items-center justify-between bg-[#F7F7F7] gap-5 border-b-[1px] border-b-[#CCCCCC]">
            <div className="p-5 text-center  w-full">
              <Link href={'/register'}>
                <a className="hover:text-[#239f40]"> Đăng ký</a>
              </Link>
            </div>
            <div className="w-[2px] h-8 bg-[#CCCCCC]" />
            <div className="p-5 text-center w-full ">
              <Link href="/login">
                <a className="hover:text-[#239f40]"> Đăng nhập</a>
              </Link>
            </div>
          </div>
        ) : (
          <div className=" flex justify-between px-6 text-[18px] py-4 bg-[#f7f7f7]">
            <Link href="/account">
              <span className="font-semibold">
                {dataInfoCustomer?.customer?.firstname} {dataInfoCustomer?.customer?.lastname}
              </span>
            </Link>
            <div
              className=" text-red"
              onClick={() => {
                localStorage.removeItem('token')
                removeCookie('token', { path: '/' })
                localStorage.removeItem('loginSocial')
                localStorage.removeItem('cartId')
                window.location.replace(`${window.location.origin}/login`)
              }}
            >
              Đăng xuất
            </div>
          </div>
        )}

        <div className="menu-bar-small ">
          <nav className="menu ">
            <ul>
              {dataHeaderMenu?.snowdogMenuNodes?.items?.map((item: any, index: number) => {
                // const MenuChild =
                //   dataHeaderMenu?.snowdogMenuNodes?.items?.filter(
                //     (val: any) => val?.parent_id == item?.node_id
                //   );
                const MenuChild =
                  item.classes == 'brand'
                    ? item.brand
                    : dataHeaderMenu?.snowdogMenuNodes?.items?.filter(
                        (val: any) => val?.parent_id == item?.node_id
                      )
                const pathName = `${router?.pathname}`

                const isActiveChild = item?.childrens?.find(
                  (child: any) => child?.title === pathName
                )

                const isActive = item?.url_key?.includes(pathName?.split('/')?.[1])

                if (item?.level !== 0) {
                  return <React.Fragment key={index}></React.Fragment>
                }

                return (
                  <li key={index}>
                    {MenuChild.length === 0 && (
                      <Link
                        href={{
                          pathname: item?.url_key?.includes('/category')
                            ? '/category'
                            : item.url_key,
                          query: getQuery(item),
                        }}
                      >
                        <a onClick={() => handleHover(false)} className="hover:text-[#239f40]">
                          <span
                            className={`!flex justify-between items-center w-full leading-none text-[20px] hover:text-[#239f40] abs ${
                              isActive || isActiveChild ? 'menuActive' : ''
                            } `}
                          >
                            {item?.title}
                          </span>
                        </a>
                      </Link>
                    )}
                    {MenuChild.length > 0 && (
                      <div className="flex justify-between w-full items-center !h-16 ">
                        <Link href={item.url_key}>
                          <a
                            className={clsx({
                              menuActive: activeChildIdx === index,
                            })}
                          >
                            <span
                              className={`!flex justify-between items-center w-full leading-none text-[20px] hover:text-[#239f40] abs ${
                                isActive || isActiveChild ? 'menuActive' : ''
                              } `}
                            >
                              {item?.title}
                            </span>
                          </a>
                        </Link>
                        {MenuChild?.length > 0 && (
                          <button
                            className="w-2/5 text-end leading-none"
                            onClick={() => {
                              activeChildIdx === index
                                ? setActiveChildIdx(-1)
                                : setActiveChildIdx(index)
                            }}
                          >
                            <i className={clsx(' fas fa-caret-down -rotate-90')}></i>
                          </button>
                        )}
                      </div>
                    )}
                    <AnimatePresence mode="sync">
                      {activeChildIdx === index && (
                        <SubMenu
                          item={item}
                          data={dataHeaderMenu?.snowdogMenuNodes?.items}
                          pathName={pathName}
                          handleHover={handleHover}
                          hover={hover}
                        />
                      )}
                    </AnimatePresence>
                  </li>
                )
              })}
            </ul>
          </nav>
        </div>
      </motion.div>
    </React.Fragment>
  )
}

type TMenuHeader = {
  item: any
  data: any
  pathName: any
  hover: boolean
  handleHover: React.Dispatch<boolean>
}

const SubMenu: React.FC<TMenuHeader> = ({ item, data, pathName, hover, handleHover }) => {
  const router = useRouter()
  const [hoverMySeft, setHoverMySeft] = useState(hover)
  const [hoverChild, setHoverChild] = useState(false)
  const [activeChildIdx, setActiveChildIdx] = useState(-1)
  useEffect(() => {
    setHoverChild(true)
  }, [router])
  useEffect(() => {
    if (!hoverChild) {
      setActiveChildIdx(-1)
    }
  }, [hover])
  const variants: Variants = {
    init: {
      x: '-100%',
    },
    show: {
      x: '0%',
    },
    hide: {
      x: '-100%',
    },
  }

  let MenuChild = []
  if (item.classes == 'brand') {
    MenuChild = item.brand
  } else if (item.children !== undefined) {
    MenuChild = item.children
  } else {
    MenuChild = data?.filter((val: any) => val?.parent_id == item?.node_id)
  }

  return (
    <React.Fragment>
      <motion.div
        variants={variants}
        initial={'init'}
        animate={hoverMySeft ? 'show' : 'hide'}
        transition={{ duration: 0.5, ease: [0.33, 1, 0.68, 1] }}
        className="fixed z-[999] left-0 top-0 bg-white h-screen w-full overflow-scroll no-scrollbar"
      >
        <div className="menu-bar-small h-screen">
          <nav className="menu">
            <ul>
              <li className="px-5 !h-20 flex gap-5 bg-gray">
                <div className="img w-[16px]" onClick={() => setHoverMySeft(false)}>
                  <img src={'/image/svg/arrow-left.svg'} alt="" />
                </div>
                <div>{item.title}</div>
              </li>
              {MenuChild.map((item: any, index: number) => {
                return (
                  <li key={index} className="!h-auto ">
                    <div className="flex justify-between w-full items-center !h-16 ">
                      <Link
                        href={{
                          pathname: item?.url_key?.includes('/category')
                            ? '/category'
                            : item.url_key,
                          query: getQuery(item),
                        }}
                      >
                        <a
                          onClick={() => handleHover(false)}
                          className={clsx({
                            menuActive: activeChildIdx === index,
                          })}
                        >
                          <span
                            className={`!flex justify-between items-center w-full leading-none hover:text-[#239f40]`}
                          >
                            {item?.title}
                          </span>
                        </a>
                      </Link>
                      {(item.children !== undefined
                        ? item.children
                        : data.filter((val: any) => val?.parent_id == item?.node_id)
                      ).length > 0 && (
                        <button
                          className="arrow w-2/5 leading-none"
                          onClick={() => {
                            activeChildIdx === index
                              ? setActiveChildIdx(-1)
                              : setActiveChildIdx(index)
                          }}
                        >
                          <i className="float-right">
                            {activeChildIdx === index ? (
                              <i className={clsx(' fas fa-caret-down ')}></i>
                            ) : (
                              <i className={clsx(' fas fa-caret-down -rotate-90')}></i>
                            )}
                          </i>
                        </button>
                      )}
                    </div>
                    {activeChildIdx === index && (
                      <AnimatePresence mode="sync">
                        <SubMenuItem
                          item={item}
                          data={data}
                          pathName={pathName}
                          handleHover={handleHover}
                          hover={hoverChild}
                        />
                      </AnimatePresence>
                    )}
                  </li>
                )
              })}
            </ul>
          </nav>
        </div>
      </motion.div>
    </React.Fragment>
  )
}
type TSubMenu = {
  item: any
  data: any
  pathName: any
  handleHover: React.Dispatch<boolean>
  hover?: boolean
}

export const SubMenuItem: React.FC<TSubMenu> = ({ item, data, pathName, handleHover, hover }) => {
  const router = useRouter()
  const [hoverChild, setHoverChild] = useState(false)
  const [activeChildIdx, setActiveChildIdx] = useState(-1)

  useEffect(() => {
    setHoverChild(false)
  }, [router])

  useEffect(() => {
    if (!hoverChild) {
      setActiveChildIdx(-1)
    }
  }, [hover])

  const MenuChild =
    item.children !== undefined
      ? item.children
      : data.filter((val: any) => val?.parent_id == item?.node_id)
  return (
    <motion.div
      initial={{ height: 0 }}
      animate={hover && { height: 'auto' }}
      exit={{ height: 0 }}
      transition={{ duration: 0.3, ease: [0.33, 1, 0.68, 1] }}
      className="overflow-hidden"
    >
      {MenuChild?.length > 0 ? (
        <>
          <div className="inner-sub">
            {MenuChild?.map((menu: any, idx: number) => {
              const activeChild = pathName === menu?.content

              return (
                <div className="relative px-[3rem] wrapper-menu" key={`${idx}`}>
                  <div className="flex justify-between w-full items-center !h-16 border-b border-[#e5e5e5]">
                    <Link
                      key={menu?.content}
                      href={{
                        pathname: menu?.url_key?.includes('/category') ? '/category' : menu.url_key,
                        query: getQuery(menu),
                      }}
                    >
                      <a
                        className={`item w-full ${clsx({
                          menuActive: activeChildIdx === idx,
                        })} ${activeChild && 'active'} flex items-center justify-between`}
                        onClick={() => {
                          setHoverChild(true)
                        }}
                      >
                        <div className="icons flex items-center gap-2 h">
                          <div className="icon">
                            <img src={menu?.image} alt="" width={24} />
                          </div>
                          <div className="name leading-none h-full w-full hover:text-[#239f40]">
                            {menu?.title}
                          </div>
                        </div>
                      </a>
                    </Link>
                    {(item.classes == 'brand'
                      ? item.brand
                      : data?.filter((val: any) => val?.parent_id == menu?.node_id)
                    ).length > 0 && (
                      <div
                        className="arrow leading-none"
                        onClick={() => {
                          activeChildIdx === idx ? setActiveChildIdx(-1) : setActiveChildIdx(idx)
                        }}
                      >
                        {activeChildIdx === idx ? (
                          <i className={clsx(' fas fa-caret-down ')}></i>
                        ) : (
                          <i className={clsx(' fas fa-caret-down -rotate-90')}></i>
                        )}
                      </div>
                    )}
                  </div>
                  {activeChildIdx === idx && (
                    <SubMenuItem
                      pathName={item}
                      data={data}
                      item={menu}
                      handleHover={handleHover}
                      hover={!hoverChild}
                    />
                  )}
                </div>
              )
            })}
          </div>
        </>
      ) : (
        <></>
      )}
    </motion.div>
  )
}

export default Sidebar

const getQuery = (menu: any) => {
  let query: ProductFilterReq = {}
  const urls = menu.categories_uids
  if (urls?.length) {
    query = removeEmptyValueFromObject({
      company: urls?.[0],
      type: urls?.[1],
      end: urls?.[2],
      tag: urls?.[3],
      submitted: true,
    })
  }
  return query
}
