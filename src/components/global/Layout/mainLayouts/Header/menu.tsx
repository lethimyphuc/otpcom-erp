/* eslint-disable @next/next/no-img-element */
import clsx from 'clsx'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useState } from 'react'
import { removeEmptyValueFromObject } from '~src/util'

export type MenuData = any

type MenuItemProps = {
  data: MenuData[]
}

type MenuProps = MenuItemProps & {
  initData: MenuData[]
  renderChildren?: (data: { item: MenuData }) => JSX.Element | null
}

export const Menu: React.FC<MenuProps> = ({ data, initData, renderChildren }) => {
  const router = useRouter()
  const pathname = router?.pathname === '/' ? undefined : router?.pathname
  const [currentItem, setCurrentItem] = useState<MenuData | undefined>()
  if (!data?.length) return null
  return (
    <div className="relative">
      <ul className="no-scrollbar w-[220px] max-h-[70vh] overflow-y-auto">
        {data?.map((menu, index) => {
          const active = pathname === menu?.content
          const hasChildren =
            menu?.children?.length ||
            [...initData].filter((val: any) => val?.parent_id === menu?.node_id)?.length
          const showChild =
            currentItem &&
            (currentItem?.entity_id || currentItem?.node_id || currentItem?.id) ===
              (menu?.entity_id || menu?.node_id || menu?.id)

          let query: ProductFilterReq = {}
          const urls = menu.categories_uids
          if (urls?.length) {
            query = removeEmptyValueFromObject({
              company: urls?.[0],
              type: urls?.[1],
              end: urls?.[2],
              tag: urls?.[3],
              submitted: true,
            })
          }

          return (
            <li
              key={index}
              className="border-b border-gray10 border-solid last:border-b-0"
              onMouseEnter={() => setCurrentItem(menu)}
              onMouseLeave={() => setCurrentItem(undefined)}
            >
              <Link
                href={{
                  pathname: menu?.url_key?.includes('/category') ? '/category' : '/' + menu.url_key,
                  query,
                }}
                key={menu?.content}
              >
                <a className="flex items-center px-[12px] py-[12px] cursor-pointer group">
                  {menu?.image || menu?.image_icon_menu || menu?.logo ? (
                    <div className="mr-[8px]">
                      <img
                        className="w-[32px] h-[32px] object-contain"
                        src={menu?.image || menu?.image_icon_menu || menu?.logo}
                        alt=""
                        width={24}
                      />
                    </div>
                  ) : null}

                  <div className="flex-1 flex items-center overflow-hidden">
                    <p
                      className={clsx(
                        'in-1-line text-[15px] flex flex-1 leading-[25px] font-normal select-none hover:text-green',
                        showChild && 'text-green'
                      )}
                    >
                      {menu?.title}
                    </p>
                    {hasChildren ? (
                      <span className="ml-[8px]">
                        {/* {active ? (
                          <img src="/icons/arrow-up.svg" alt="" />
                        ) : (
                          <img src="/icons/arrow-right.svg" alt="" />
                        )} */}
                        <img src="/icons/arrow-right.svg" alt="" />
                      </span>
                    ) : null}
                  </div>
                </a>
              </Link>

              {showChild ? renderChildren?.({ item: currentItem }) : null}
            </li>
          )
        })}
      </ul>
    </div>
  )
}
