/* eslint-disable @next/next/no-html-link-for-pages */
/* eslint-disable @next/next/no-img-element */
import { Breadcrumb } from 'antd'
import clsx from 'clsx'
import { motion } from 'framer-motion'
import { Fade as Hamburger } from 'hamburger-react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import React, { Fragment } from 'react'
import { useQuery } from 'react-query'
import { SNOW_DOG_MENU_NODES } from '~src/api'
import { graphQLClientGET } from '~src/api/graphql'
import { SearchBox } from '~src/components'
import { CartCount } from '~src/components/common/CartCount'
import LanguageDropdown from '~src/components/global/Language'
import UserInformation from '~src/components/global/UserInformation'
import { Menu } from './menu'

type TProps = {
  isBreadcrumb: boolean
  isScroll?: boolean
  headerRef: any
  hover: boolean
  handleHover: (bol: boolean) => void
  dataInfoCustomer: any
}

const Header: React.FC<TProps> = ({
  isBreadcrumb = false,
  isScroll,
  headerRef,
  hover,
  handleHover,
  dataInfoCustomer,
}) => {
  const router = useRouter()
  const breadcrumbs: any = [{ name: 'Trang chủ', key: null }]

  const defaultData = [
    'Hãng xe',
    'Thương hiệu',
    'Nhóm phụ tùng',
    'Khuyến mãi',
    'Tra vận đơn',
    'Nhật ký',
  ]

  const { data: dataHeaderMenu } = useQuery({
    queryKey: ['getHeaderMenu'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(SNOW_DOG_MENU_NODES, {
        identifier: 'mainmenu',
      })) as { snowdogMenuNodes: { items: TMenuHeader[] } }
      return data
    },
    cacheTime: Infinity,
  })

  return (
    <header ref={headerRef} className={`app-header ${isScroll ? 'sticky-header' : ''}`}>
      <div className="containers">
        <div className="content-header">
          <div className="logo">
            <Link href="/">
              <a>
                <img src="/image/logo/logo-header.png" alt="" />
              </a>
            </Link>
          </div>
          <div className="visible-menu">
            <div className="flex items-center justify-start gap-0 xs:gap-5 max-h-[70px]">
              <button onClick={() => handleHover(!hover)}>
                <Hamburger toggled={hover} direction="right" size={22} />
              </button>
              <Link href="/" className="block m-8">
                <img
                  src="/image/logo/logo-header.png"
                  alt=""
                  className="object-cover w-full max-h-[40px]"
                />
              </Link>
            </div>
          </div>

          <nav className="menu">
            <ul className="whitespace-nowrap menu-wrapper">
              {!!dataHeaderMenu?.snowdogMenuNodes?.items ? (
                dataHeaderMenu?.snowdogMenuNodes?.items.map((itemParent: any, index: number) => {
                  const pathname = router?.pathname === '/' ? undefined : router?.pathname
                  const isActive = itemParent?.url_key?.includes(pathname?.split('/')?.[1])
                  const isActiveChild = itemParent?.childrens?.find(
                    (child: any) => child?.title === pathname
                  )
                  const initData = dataHeaderMenu?.snowdogMenuNodes?.items as any
                  const data =
                    itemParent.classes == 'brand'
                      ? itemParent.brand
                      : [...initData]?.filter((val: any) => val?.parent_id == itemParent?.node_id)

                  if (itemParent?.level !== 0) return null
                  const className =
                    'absolute hidden group-hover:block bg-white z-[1000] rounded-b-[5px] shadow-md'
                  return (
                    <li className="relative group" key={index}>
                      <Link
                        href={
                          itemParent?.content?.startsWith('/')
                            ? itemParent?.content
                            : '/' + itemParent?.content
                        }
                      >
                        <a className={clsx((isActive || isActiveChild) && 'menuActive')}>
                          {itemParent?.title}
                        </a>
                      </Link>
                      <div className={clsx(className, 'top-[100%] left-0 header-menu-item')}>
                        <Menu
                          data={data}
                          initData={initData}
                          renderChildren={({ item }) => {
                            return (
                              <div
                                className={clsx(
                                  className,
                                  'min-h-full left-full top-0 no-scrollbar header-menu-child'
                                )}
                              >
                                <Menu
                                  initData={initData}
                                  data={
                                    item?.children ||
                                    [...initData]?.filter((val) => val?.parent_id == item?.node_id)
                                  }
                                  renderChildren={({ item }) => (
                                    <div
                                      className={clsx(
                                        className,
                                        'min-h-full left-full top-0 overflow-auto header-menu-child'
                                      )}
                                    >
                                      <Menu
                                        initData={initData}
                                        data={
                                          item?.children ||
                                          [...initData]?.filter(
                                            (val) => val?.parent_id == item?.node_id
                                          )
                                        }
                                      />
                                    </div>
                                  )}
                                />
                              </div>
                            )
                          }}
                        />
                      </div>
                    </li>
                  )
                })
              ) : (
                <>
                  {defaultData.map((item, index: number) => {
                    return (
                      <li key={index}>
                        <Link href={'/'}>
                          <a>
                            <span> {item}</span>
                          </a>
                        </Link>
                      </li>
                    )
                  })}
                </>
              )}
            </ul>
          </nav>
          <div className="sticky">
            <SearchBox router={router} />
            <UserInformation dataInfoCustomer={dataInfoCustomer} />
            <div className={router.pathname === '/shopping-cart' ? 'hidden' : ''}>
              <CartCount />
            </div>
            <LanguageDropdown />
          </div>
        </div>
      </div>
      {isBreadcrumb ? (
        <div className="breadcrumb font-bold m-0">
          {breadcrumbs.length > 0 && (
            <Breadcrumb>
              {breadcrumbs?.map((item: any, index: number) => (
                <Breadcrumb.Item key={index}>
                  {item.key ? (
                    <Link href={item.key} className="font-medium">
                      {item.name}
                    </Link>
                  ) : (
                    item.name
                  )}
                </Breadcrumb.Item>
              ))}
              {router.query && router.query?.Name && router.query?.Name?.length > 0 && (
                <Breadcrumb.Item>{router.query.Name}</Breadcrumb.Item>
              )}
            </Breadcrumb>
          )}
        </div>
      ) : (
        ''
      )}
    </header>
  )
}

export default Header

type TMenuDropdown = {
  item: any
  data: any
  pathname: any
}

export const MenuDropdown: React.FC<TMenuDropdown> = ({ item, pathname, data }) => {
  const MenuChild =
    item.classes == 'brand'
      ? item.brand
      : data?.filter((val: any) => val?.parent_id == item?.node_id)
  return (
    <Fragment>
      <div>
        {!!MenuChild?.length && (
          <div className={`sub-menu ${!!MenuChild?.length ? '!rounded-br-none' : ''}`}>
            <div className="inner-sub">
              {MenuChild?.map((menu: any, index: number) => {
                const activeChild = pathname === menu?.content
                return (
                  <div className="relative px-[2rem] wrapper-menu " key={`${index}`}>
                    <div className="border-b border-gray10">
                      <a className={`item ${activeChild && 'active'}`}>
                        <Link href={menu?.url_key ?? ''}>
                          <div className="icons">
                            <div className="icon">
                              <a>
                                <img src={menu?.image ? menu.image : menu.logo} alt="" />
                              </a>
                            </div>
                            <div className="name ">{menu?.title}</div>
                          </div>
                        </Link>
                        {(item.classes == 'brand'
                          ? item.brand
                          : data?.filter((val: any) => val?.parent_id == menu?.node_id)
                        ).length > 0 && (
                          <div className="arrow">
                            {activeChild ? (
                              <img src="/icons/arrow-up.svg" alt="" />
                            ) : (
                              <img src="/icons/arrow-right.svg" alt="" />
                            )}
                          </div>
                        )}
                      </a>
                    </div>
                    <SubMenuItemChild pathName={item} data={data} item={menu} />
                  </div>
                )
              })}
            </div>
          </div>
        )}
      </div>
    </Fragment>
  )
}

type TSubMenu = {
  data: any
  pathName: any
  item: any
  // handleHover: React.Dispatch<boolean>;
}

export const SubMenuItemChild: React.FC<TSubMenu> = ({
  data,
  pathName,
  item,
  // handleHover,
}) => {
  const MenuChild =
    item.children !== undefined
      ? item.children
      : data.filter((val: any) => val?.parent_id == item?.node_id)
  return (
    <motion.div
      initial={{ height: 0 }}
      animate={{ height: 'auto' }}
      exit={{ height: 0 }}
      transition={{ duration: 0.3, ease: [0.33, 1, 0.68, 1] }}
      className="wrapper-child-menu"
    >
      {MenuChild?.length > 0 ? (
        <>
          <div className="inner-sub menu-child overflow-y-scroll max-h-[70vh] no-scrollbar">
            {MenuChild?.map((menu: any, idx: number) => {
              const activeChild = pathName === menu?.content
              return (
                <div className="relative px-[1rem] wrapper-menu" key={`${idx}`}>
                  <Link href={menu?.url_key} key={menu?.content}>
                    <a
                      className={`item !py-4 ${
                        activeChild && 'active'
                      } flex items-center justify-between`}
                      // onClick={() => handleHover(false)}
                    >
                      <div className="icons flex items-center gap-2">
                        {menu?.image
                          ? menu.image
                          : menu.logo && (
                              <div className="icon">
                                <img src={menu?.image} alt="" width={24} />
                              </div>
                            )}

                        <div className="name !text-[14px] !font-normal">{menu?.title}</div>
                      </div>
                      {data.filter((val: any) => val?.parent_id == menu?.node_id).length > 0 && (
                        <div className="arrow">
                          {activeChild ? (
                            <img src="/icons/arrow-up.svg" alt="" />
                          ) : (
                            <img src="/icons/arrow-right.svg" alt="" />
                          )}
                        </div>
                      )}
                    </a>
                  </Link>
                  <SubMenuItemChild pathName={item} data={data} item={menu} />
                </div>
              )
            })}
          </div>
        </>
      ) : (
        <></>
      )}
    </motion.div>
  )
}
