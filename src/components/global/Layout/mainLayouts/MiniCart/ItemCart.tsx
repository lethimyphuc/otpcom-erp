/* eslint-disable @next/next/no-img-element */
import clsx from 'clsx'
import React from 'react'
import { useMutation, useQueryClient } from 'react-query'
import { toast } from 'react-toastify'
import { graphQLClient } from '~src/api/graphql'
import { REMOVE_PRODUCT_TO_CART } from '~src/api/product'
import { _format } from '~src/util'
import styles from './MiniCart.module.scss'
import { useRouter } from 'next/router'
import Link from 'next/link'

type ItemCartProps = {
  item: TDataCart
  miniCartOpen?: boolean
  index?: number
  update?: any
  setSubmitCart?: React.Dispatch<React.SetStateAction<boolean>>
}
const ItemCart = (props: ItemCartProps) => {
  const router = useRouter()
  const { item, miniCartOpen, index, update, setSubmitCart } = props

  const queryClient = useQueryClient()

  const { isLoading: isLoadingDataRemoveToCart, mutate: mutateRemoveToCart } = useMutation({
    mutationFn: async (data: TRemoveToCart) => {
      return (await graphQLClient.request(REMOVE_PRODUCT_TO_CART, {
        input: {
          cart_id: localStorage?.getItem('cartId'),
          cart_item_uid: data.cart_item_uid,
        },
      })) as { removeItemFromCart: TDataUpdateToCart }
    },
    onSuccess: (response: { removeItemFromCart: TDataUpdateToCart }) => {
      if (!!response?.removeItemFromCart?.user_errors?.length) {
        toast.error(response?.removeItemFromCart?.user_errors[0].message)
        return
      }
      queryClient.setQueryData(['getCartId'], (oldData: any) => {
        if (!!oldData) {
          return {
            ...oldData,
            cart: {
              ...oldData?.cart,
              total_quantity:
                response?.removeItemFromCart?.cart?.total_quantity ?? oldData?.cart?.total_quantity,
              items: response?.removeItemFromCart?.cart?.items,
              prices: response?.removeItemFromCart?.cart?.prices,
            },
          }
        } else {
          return oldData
        }
      })
    },
  })

  const handleRemoveItemToCart = (cart_item_uid: string) => {
    const DATA_SUBMIT = {
      cart_item_uid: cart_item_uid,
    }
    mutateRemoveToCart(DATA_SUBMIT)
  }

  return (
    <div className={clsx(styles.cartItem)}>
      <div className={clsx(styles.wrapperInfo)}>
        <button
          className="text-[#e2e0e0] focus:!text-[#C82127]"
          onClick={() => handleRemoveItemToCart(item?.uid)}
        >
          <i className="fas fa-times-circle "></i>
        </button>
        <Link
          passHref
          href={{
            pathname: `/product-detail/Hãng xe/${item.product?.name}`,
            query: {
              sku: item.product?.sku,
            },
          }}
        >
          <img className={clsx(styles.cartItemImage)} src={`${item?.product?.image?.url}`} alt="" />
        </Link>
        <div className={clsx(styles.cartItemInfo)}>
          <Link
            passHref
            href={{
              pathname: `/product-detail/Hãng xe/${item.product?.name}`,
              query: {
                sku: item.product?.sku,
              },
            }}
          >
            <p>
              <a>{item?.product?.name}</a>
            </p>
          </Link>

          <div className={clsx(styles.quantity)}>
            <button
              className="flex items-center justify-center h-full w-full"
              onClick={() => {
                if (item?.quantity > 1) {
                  update(index, {
                    ...item,
                    quantity: item?.quantity - 1,
                  })
                  !!setSubmitCart && setSubmitCart(true)
                }
              }}
            >
              <img src={'/icons/decrease.svg'} alt="" />
            </button>
            <p className="!flex items-center justify-center px-3 mt-[4px] !w-full mx-auto">
              {item?.quantity}
            </p>
            <button
              className="flex items-center justify-center h-full w-full"
              onClick={() => {
                update(index, {
                  ...item,
                  quantity: item?.quantity + 1,
                })
                !!setSubmitCart && setSubmitCart(true)
              }}
            >
              <img src={'/icons/increase.svg'} alt="" />
            </button>
          </div>
        </div>
      </div>

      <p className={clsx(styles.price, 'whitespace-nowrap')}>
        {!!item?.prices?.price?.value ? _format.getVND(item?.prices?.price?.value) : '0'}
        {!!item?.prices?.price?.value ? item?.prices?.price?.currency : ' VNĐ'}
      </p>
    </div>
  )
}

export default ItemCart
