/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @next/next/no-img-element */
import clsx from 'clsx'
import { AnimatePresence, motion } from 'framer-motion'
import { useRouter } from 'next/router'
import React, { useEffect, useRef, useState } from 'react'
import { useFieldArray, useForm } from 'react-hook-form'
import { MdClose } from 'react-icons/md'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { toast } from 'react-toastify'
import { CREATE_EMPTY_CART, GET_CART } from '~src/api/cart'
import { graphQLClient } from '~src/api/graphql'
import { UPDATE_PRODUCT_TO_CART } from '~src/api/product'
import { LoadingCP } from '~src/components/screen/status/LoadingCP'
import { useWindowSize } from '~src/hook'
import useDebounce from '~src/hook/useDebounce'
import { _format } from '~src/util'
import EmptyCart from './EmptyCart'
import ItemCart from './ItemCart'
import styles from './MiniCart.module.scss'
import { error } from 'console'

type TProps = {
  isScroll?: boolean
  miniCartVisible?: boolean
  cartRef: any
}

const MiniCart: React.FC<TProps> = ({ isScroll, cartRef }) => {
  const elCartListWrapperRef = useRef<HTMLDivElement>(null)
  const [isTopCart, setIsTopCart] = useState(true)
  const [isBottomCart, setisBottomCart] = useState(false)
  const { isMobile } = useWindowSize()
  const queryClient = useQueryClient()
  const router = useRouter()

  const { data: miniCartVisible } = useQuery<boolean>({
    queryKey: ['miniCartVisible'],
    queryFn: () => {
      return false
    },
    cacheTime: Infinity,
    enabled: true,
  })

  const handleOpenCart = (visible: boolean) => {
    queryClient.setQueryData<boolean>('miniCartVisible', visible)
  }

  const { handleSubmit, control, setValue, getValues } = useForm<{
    items: TDataCart[]
  }>({
    defaultValues: {
      items: [],
    },
  })
  const { fields, update } = useFieldArray({
    control, // control props comes from useForm (optional: if you are using FormContext)
    name: 'items', // unique name for your Field Array
  })
  const [submitCart, setSubmitCart] = useState(false)
  const debouncedSearch = useDebounce(fields, 500)

  const {
    data: dataCart,
    isLoading: isLoadingDataCart,
    isFetching: isFetchingDataCart,
  } = useQuery({
    queryKey: ['getCartId'],
    queryFn: async () => {
      if (!localStorage.getItem('cartId')) return
      const data: TDataUpdateToCart = await graphQLClient
        .request(GET_CART, {
          cart_id: localStorage.getItem('cartId'),
        })
        .then(async (res: any) => {
          if (res?.cart == null) {
            await graphQLClient
              .request(CREATE_EMPTY_CART, {
                input: {},
              })
              .then((res: any) => {
                localStorage.setItem('cartId', res?.createEmptyCart)
                toast.error('Đã có lỗi xảy ra, vui lòng thử lại')
              })
          }
          return res
        })

      setValue('items', data?.cart?.items, {
        shouldDirty: false,
        shouldTouch: false,
      })
      setSubmitCart(false)
      return data
    },

    cacheTime: Infinity,
    enabled: !!miniCartVisible,
  })

  const { isLoading: isLoadingDataUpdateToCart, mutate: mutateUpdateToCart } = useMutation({
    mutationFn: async (data: TUpdateToCart[]) => {
      return (await graphQLClient.request(UPDATE_PRODUCT_TO_CART, {
        input: {
          cart_id: localStorage?.getItem('cartId'),
          cart_items: data,
        },
      })) as { updateCartItems: TDataUpdateToCart }
    },
    onSuccess: (response: { updateCartItems: TDataUpdateToCart }) => {
      if (!!response?.updateCartItems?.user_errors?.length) {
        toast.error(response?.updateCartItems?.user_errors[0].message)
      }
      queryClient.setQueryData(['getCartId'], (oldData: any) => {
        if (!!oldData) {
          return {
            ...oldData,
            cart: {
              ...oldData?.cart,
              total_quantity:
                response?.updateCartItems?.cart?.total_quantity ?? oldData?.cart?.total_quantity,
              items: response?.updateCartItems?.cart?.items,
              prices: response?.updateCartItems?.cart?.prices,
            },
          }
        } else {
          return oldData
        }
      })
    },
  })

  useEffect(() => {
    handleOpenCart(false)
  }, [router])

  useEffect(() => {
    if (!!miniCartVisible && !!dataCart) {
      setValue('items', !!dataCart ? dataCart?.cart?.items : [])
      setSubmitCart(false)
    }
  }, [miniCartVisible, dataCart])

  const navigateCart = () => {
    if (router.route !== '/shopping-cart') {
      router.push('/shopping-cart/')
    } else {
    }
    handleOpenCart(false)
  }

  const onSubmit = (data: { items: TDataCart[] }) => {
    const DATA_SUBMIT = data?.items?.map((field) => ({
      cart_item_uid: field?.uid,
      quantity: field?.quantity,
    }))
    mutateUpdateToCart(DATA_SUBMIT)
  }

  const handleCartListScroll = (e: any) => {
    if (!elCartListWrapperRef.current) return
    const rectWrapper = elCartListWrapperRef.current!.getBoundingClientRect()
    const triggerOffset = 20
    const currentScroll = (e.target as HTMLDivElement).scrollTop

    if (currentScroll < triggerOffset) {
      setIsTopCart(true)
    } else if (currentScroll > rectWrapper!.height - triggerOffset) {
      setisBottomCart(true)
    } else {
      setIsTopCart(false)
      setisBottomCart(false)
    }
  }

  useEffect(() => {
    if (fields && fields.length < 4) {
      setIsTopCart(true)
      setisBottomCart(false)
    }
  }, [fields])

  useEffect(() => {
    if (submitCart && debouncedSearch === fields) {
      handleSubmit(onSubmit)()
    }
  }, [debouncedSearch, submitCart])
  return (
    <React.Fragment>
      <div
        className={clsx(
          styles.cartPopup,
          `${isScroll ? styles.stickyCart : ''}`,
          `${!miniCartVisible ? styles.cartPopupHidden : ''}`
        )}
      >
        <div
          ref={cartRef}
          className={clsx(
            styles.container,
            // 'h-[calc(100%-7rem)]',
            `${!isScroll ? styles.heightStiker : 'h-[calc(100%-7rem)]'}`,
            // `${!isScroll ? 'h-[calc(100%-10rem)]' : 'h-[calc(100%-7rem)]'}`,
            `${!miniCartVisible ? styles.cartHidden : ''}`
          )}
        >
          {isLoadingDataCart ? (
            <LoadingCP />
          ) : (
            <>
              {!dataCart?.cart?.items?.length ? (
                <EmptyCart router={router} />
              ) : (
                <div className={clsx(styles.miniCartContainer)}>
                  {isMobile && (
                    <div className={styles.cartClose} onClick={() => handleOpenCart(false)}>
                      <MdClose size={30} />
                    </div>
                  )}
                  <h1 className="mb-[24px]">Giỏ hàng</h1>
                  <div className={clsx(styles.cartListWrapper)}>
                    <div
                      ref={elCartListWrapperRef}
                      className={clsx(styles.cartList)}
                      onScroll={handleCartListScroll}
                    >
                      {fields?.map((item: TDataCart, index: number) => (
                        <ItemCart
                          update={update}
                          key={`${item?.uid}`}
                          index={index}
                          setSubmitCart={setSubmitCart}
                          item={item}
                          miniCartOpen={miniCartVisible}
                        />
                      ))}
                    </div>

                    <AnimatePresence mode="wait">
                      {!isTopCart && (
                        <motion.div
                          initial={{ opacity: 0 }}
                          animate={{ opacity: 1 }}
                          exit={{ opacity: 0 }}
                          transition={{ duration: 0.2, ease: 'easeInOut' }}
                          className={clsx(styles.cartOverlayTop)}
                        />
                      )}
                    </AnimatePresence>
                    <AnimatePresence mode="wait">
                      {!isBottomCart && (
                        <motion.div
                          initial={{ opacity: 0 }}
                          animate={{ opacity: 1 }}
                          exit={{ opacity: 0 }}
                          transition={{ duration: 0.2, ease: 'easeInOut' }}
                          className={clsx(styles.cartOverlayBottom)}
                        />
                      )}
                    </AnimatePresence>
                  </div>

                  <div className="px-[16px] sm:px-[24px]">
                    <div className={clsx(styles.total)}>
                      <p>Tổng cộng</p>
                      <p className={clsx(styles.totalPrice)}>
                        {_format.getVND(dataCart?.cart?.prices?.subtotal_excluding_tax?.value || 0)}
                        {dataCart?.cart?.prices?.subtotal_excluding_tax?.currency || ' VNĐ'}
                      </p>
                    </div>
                    <div className={clsx(styles.cartButton)}>
                      <button onClick={() => navigateCart()}>
                        <p>Xem giỏ hàng</p>
                      </button>
                      <button onClick={() => navigateCart()} className={clsx(styles.buttonOrder)}>
                        <img src={'/icons/cart-icon.svg'} alt="" />
                        <p>Đặt mua</p>
                      </button>
                    </div>
                    {/* <img
                      className={clsx(styles.gearCart)}
                      src="/image/gear-cart.png"
                      alt=""
                    /> */}
                  </div>
                </div>
              )}
            </>
          )}
        </div>
      </div>
    </React.Fragment>
  )
}

export default MiniCart
