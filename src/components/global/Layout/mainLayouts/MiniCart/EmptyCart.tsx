/* eslint-disable @next/next/no-img-element */
import React from 'react'
import styles from './MiniCart.module.scss'
import clsx from 'clsx'
import { NextRouter } from 'next/router'

type EmptyCartProps = {
  router: NextRouter
}
export default function EmptyCart(props: EmptyCartProps) {
  const { router } = props

  const handleBackHome = () => {
    router.push('/')
  }

  return (
    <div className={clsx(styles.emptyCartContainer)}>
      <div className={clsx(styles.mainContent, 'relative z-20')}>
        <h1>Giỏ hàng</h1>
        <p>Hiện tại giỏ hàng của bạn đang trống</p>
        <button onClick={handleBackHome}>Về trang chủ</button>
      </div>
      <img className={clsx(styles.gearEmptyCart)} src="/image/gear-cart.png" alt="" />
      <img className={clsx(styles.emptyCart)} src="/image/empty-cart.png" alt="" />
      <img className={clsx(styles.emptyCartFooter)} src="/image/empty-cart-footer.png" alt="" />
    </div>
  )
}
