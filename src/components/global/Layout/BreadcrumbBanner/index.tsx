import { Breadcrumb } from 'antd'
import { Route } from 'antd/lib/breadcrumb/Breadcrumb'
import clsx from 'clsx'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { adminRouter } from '~src/config/router/adminRouter'

export type BreadcrumbProps = {
  breadcrumbHead: boolean
  customsBreadcrumb?: { label: string; href: string }[]
  breadcrumbs?: Route[]
}

export const BreadcrumbBanner = ({
  breadcrumbHead,
  breadcrumbs: externalBreadcrumbs,
}: BreadcrumbProps) => {
  const router = useRouter()
  const [breadcrumbs, setBreadcrumbs] = useState<Route[]>(
    !!externalBreadcrumbs?.length
      ? [
          {
            path: '/',
            breadcrumbName: 'Trang chủ',
          },
          ...externalBreadcrumbs,
        ]
      : () => {
          const data: Route[] = [
            {
              path: '/',
              breadcrumbName: 'Trang chủ',
              children: [],
            },
          ]
          adminRouter.forEach((item) => {
            const { pathname } = router
            if (item.Link.includes(pathname) || pathname.includes(item.Link)) {
              data.push({
                path: item.Link,
                breadcrumbName: item.Name,
                children: [],
              })

              const child = item.childrens?.find?.((item) => item.Link === pathname)
              if (child) {
                data.push({
                  path: child.Link,
                  breadcrumbName: child.Name,
                  children: [],
                })
              }
            }
          })
          return data
        }
  )

  useEffect(() => {
    if (externalBreadcrumbs?.length) {
      setBreadcrumbs([
        {
          path: '/',
          breadcrumbName: 'Trang chủ',
        },
        ...externalBreadcrumbs,
      ])
    }
  }, [externalBreadcrumbs])

  if (breadcrumbs?.length <= 1) return null
  return (
    <div className={`${breadcrumbHead ? 'breadcrumb-head' : 'breadcrumb-banner'}`}>
      <Breadcrumb separator=">">
        {breadcrumbs?.map((item, index: number) => {
          return (
            <Breadcrumb.Item
              key={index}
              separator=">"
              className={clsx('cursor-pointer ant-breadcrumb-text h-full')}
            >
              {item?.path ? (
                <Link href={item.path}>
                  <a>{item.breadcrumbName}</a>
                </Link>
              ) : (
                item.breadcrumbName
              )}
            </Breadcrumb.Item>
          )
        })}
      </Breadcrumb>
    </div>
  )
}
