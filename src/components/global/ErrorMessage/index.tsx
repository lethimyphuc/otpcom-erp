import React from "react";

type ErrorMessageProps = {
  children: any;
};
const ErrorMessage = (props: ErrorMessageProps) => {
  const { children } = props;
  return (
    <div>
      <p className="!text-lg !mt-1 !text-important-pending !font-normal">
        {children}
      </p>
    </div>
  );
};

export default ErrorMessage;
