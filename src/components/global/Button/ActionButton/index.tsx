import { Tooltip } from "antd";
import clsx from "clsx";
import React, { FC, useState } from "react";
import { useIsMutating } from "react-query";
import styles from "./index.module.css";

type TProps = {
  icon: string;
  title: string;
  onClick?: () => void;
  iconContainerClassName?: string;
  btnGreen?: boolean;
  btnRed?: boolean;
  btnYellow?: boolean;
  btnViolet?: boolean;
  btnBlue?: boolean;
  disabale?: boolean;
};

const btnStyleGreen = "text-[#1f8f2b]";
const btnStyleRed = "text-[#f02b02] ";
const btnStyleYellow = "text-[#edb90e]  ";
const btnStyleViolet = "text-[#7410b3]";
const btnStyleBlue = "text-[#119ff5]";

export const ActionButton: FC<TProps> = ({
  icon,
  iconContainerClassName,
  title,
  onClick,
  btnGreen,
  btnRed,
  btnYellow,
  btnViolet,
  btnBlue,
  disabale = false,
  ...props
}) => {
  const [loading, setLoading] = useState(false);
  const handleClick = async () => {
    setLoading(true);
    await onClick?.();
    setLoading(false);
  };

  return (
    <Tooltip title={title} placement="topLeft">
      <div {...props} className="group inline-block  cursor-pointer">
        <button
          className="cursor-pointer"
          onClick={handleClick}
          disabled={disabale || loading}
        >
          <div
            className={clsx(
              " transition duration-300 text-center hover:shadow rounded-md text-[#a1a1a1] bg-[#efecec] flex justify-center items-center",
              iconContainerClassName,
              btnGreen && btnStyleGreen,
              btnRed && btnStyleRed,
              btnYellow && btnStyleYellow,
              btnViolet && btnStyleViolet,
              btnBlue && btnStyleBlue
            )}
          >
            <i
              className={clsx(
                disabale == false && styles.text,
                icon,
                " transition duration-300 !w-[40px] h-[40px] !p-3 text-[16px] rounded"
              )}
            ></i>
          </div>
        </button>
      </div>
    </Tooltip>
  );
};
