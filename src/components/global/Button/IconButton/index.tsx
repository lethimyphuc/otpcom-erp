import { Tooltip } from 'antd'
import clsx from 'clsx'
import { resolve } from 'path'
import React, { useState } from 'react'
import styles from './index.module.css'

type TProps = {
  title: string
  icon?: string
  toolip?: string
  onClick?: (data?: any) => void | Promise<any>
  btnClass?: string
  btnIconClass?: string
  showLoading?: boolean
  disabled?: boolean
  red?: boolean
  yellow?: boolean
  green?: boolean
  blue?: boolean
  text?: boolean
  btnRef?: any
}

const styleBtnGreen = '!text-[#fff] !bg-[#239F40]'
const styleBtnRed = '!text-[#fff] !bg-[#E54C36]'
const styleBtnBlue = '!text-[#fff] !bg-[#2196F3]'
const styleBtnYellow = '!text-[#fff] !bg-[#daaa42]'
const styleDisable = '!text-[#fff] !bg-[#7b7b7b] !h-[38px]'

export const IconButton: React.FC<TProps> = ({
  icon,
  title,
  toolip,
  disabled = false,
  onClick,
  btnClass,
  btnIconClass,
  showLoading,
  red,
  yellow,
  green,
  blue,
  text,
  btnRef,
}) => {
  const [loading, setLoading] = React.useState(false)

  const _onPress = async (e: any) => {
    e.stopPropagation()
    // DISABLE BASE ON FLAGS: DISABLED, LOADING
    if (disabled || loading) return

    // DONT SHOW LOADING EFFECT
    if (!showLoading) {
      onClick?.()

      return
    }

    // TOGGLE LOADING FLAG
    try {
      setLoading(true)

      await onClick?.()
    } finally {
      setLoading(false)
    }
  }

  return (
    <Tooltip placement="topLeft" title={toolip}>
      <button
        onClick={disabled ? undefined : _onPress}
        // onClick={!disabled && !loading ? _onPress : undefined}
        className={clsx(
          'bg-blue text-white h-[40px] min-w-[80px] mx-0 py-[6px] px-[12px] rounded !font-semibold tracking-wide hover:shadow !flex !items-center justify-center !gap-2 focus:shadow-md',
          btnClass,
          disabled && '!bg-unactive !text-darkLight',
          green && styleBtnGreen,
          red && styleBtnRed,
          blue && styleBtnBlue,
          yellow && styleBtnYellow
        )}
        disabled={disabled}
        ref={btnRef}
      >
        {loading ? (
          <div>
            <i
              className={clsx(
                loading ? 'fad fa-spinner-third fa-spin' : icon,
                '!text-sm',
                btnIconClass
              )}
            ></i>
          </div>
        ) : null}
        <div>
          <span>{title}</span>
        </div>
      </button>
    </Tooltip>
  )
}
