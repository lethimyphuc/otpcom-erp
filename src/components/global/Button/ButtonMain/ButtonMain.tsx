import React from 'react'
import Lottie from 'react-lottie'
import * as loadingIcon from '../../../../assets/json/loading-KT.json'

type I = {
  background:
    | 'green'
    | 'yellow'
    | 'blue'
    | 'red'
    | 'black'
    | 'disabled'
    | 'primary'
    | 'orange'
    | 'transparent'
    | 'something'
  type: 'button' | 'submit'
  onClick?: Function
  children?: React.ReactNode
  className?: string
  disable?: boolean
  loading?: boolean
}
export const ButtonMain: React.FC<I> = (props) => {
  const { children, className, type, disable = false, loading, onClick, background } = props

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: loadingIcon,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice',
    },
  }

  const _onClick = (event: any) => {
    if (!onClick && disable) {
      return
    }
    onClick && onClick(event)
  }

  function getBG() {
    if (!!disable || !!loading) {
      return 'bg-[#cacaca] hover:bg-[#bababa] focus:bg-[#acacac] cursor-not-allowed'
    } else {
      if (background === 'green') {
        return 'bg-[#007134] hover:bg-[#007134] focus:bg-[#007134]'
      }
      if (background === 'blue') {
        return 'bg-[#0068ac] hover:bg-[#0068ac] focus:bg-[#0068ac]'
      }
      if (background === 'red') {
        return 'bg-[#e92327] hover:bg-[#e92327] focus:bg-[#e92327]'
      }
      if (background === 'yellow') {
        return 'bg-[#FFBA0A] hover:bg-[#e7ab11] focus:bg-[#d19b10]'
      }
      if (background === 'black') {
        return 'bg-[#000] hover:bg-[#191919] focus:bg-[#313131]'
      }
      if (background === 'primary') {
        return 'bg-[#ab1d38] hover:bg-[#9a1b33] focus:bg-[#85172c]'
      }
      if (background === 'disabled') {
        return 'bg-[#cacaca] hover:bg-[#bababa] focus:bg-[#acacac] cursor-not-allowed'
      }
      if (background === 'orange') {
        return 'bg-[#FF9800] hover:bg-[#bababa] focus:bg-[#acacac] cursor-not-allowed'
      }
      if (background === 'transparent') {
        return 'bg-[] hover:bg-[] focus:bg-[]'
      }
      if (background === 'something') {
        return 'bg-[] focus:bg-[] border cursor-not-allowed border-solid border-[#007134]'
      }
    }
  }
  function getColor() {
    if (!!disable || !!loading) {
      return 'text-white'
    } else {
      if (background === 'green') {
        return 'text-white '
      }
      if (background === 'blue') {
        return 'text-white '
      }
      if (background === 'red') {
        return 'text-white '
      }
      if (background === 'yellow') {
        return 'text-black'
      }
      if (background === 'black') {
        return 'text-white'
      }
      if (background === 'primary') {
        return 'text-white'
      }
      if (background === 'disabled') {
        return 'text-white'
      }
    }
  }
  return (
    <button
      disabled={!!disable || !!loading}
      type={type}
      onClick={_onClick}
      className={`main-button ${getBG()} ${getColor()} ${className} relative transition-all`}
    >
      <div className={`flex items-center justify-center gap-1`}>
        {!!loading && (
          <div className="spinner left-1/2 top-1/2 absolute">
            <i className="spinner-icon fad fa-spinner"></i>
          </div>
        )}
        <div
          className={`flex-shrink-0 flex items-center justify-center ${loading ? 'opacity-0' : ''}`}
        >
          {children}
        </div>
      </div>
    </button>
  )
}
