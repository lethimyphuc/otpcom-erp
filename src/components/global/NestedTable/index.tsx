import { Table, TablePaginationConfig } from "antd";
import { TableProps } from "antd/es/table";
import { SorterResult } from "antd/lib/table/interface";
import { useMediaQuery } from "react-responsive";
import { TColumnsType } from "~src/types/table";
import style from "./index.module.css";

type TProps<TRecord extends object> = {
  columns: TColumnsType<TRecord>;
  data: TRecord[];
  tableProps?: TableProps<TRecord>;
  pagination?: TablePaginationConfig | false;
  onChange?: (
    pagination: TablePaginationConfig,
    filter: any,
    sorter: SorterResult<TRecord> | SorterResult<TRecord>[]
  ) => void;
  loading?: boolean;
};

export const NestedTable = <TRecord extends object>({
  columns,
  data,
  tableProps,
  pagination = false,
  onChange,
  loading = false,
}: TProps<TRecord>) => {
  return (
    <Table
      rowKey={"Id"}
      dataSource={data}
      columns={columns}
      className={style["nested-table"]}
      bordered={false}
      // tableLayout="fixed"
      loading={loading}
      pagination={pagination}
      onChange={onChange}
      // scroll={{
      //   x: "1200px",
      // }}
      {...tableProps}
    ></Table>
  );
};
