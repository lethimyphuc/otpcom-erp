import clsx from 'clsx'
import { InputHTMLAttributes, useState } from 'react'
import { Control, useController } from 'react-hook-form'
import { AiOutlineEye, AiOutlineEyeInvisible } from 'react-icons/ai'
import ErrorMessage from '~src/components/global/ErrorMessage'

type TextFieldProps = InputHTMLAttributes<any> & {
  control: Control<any>
  name: string
  label?: string
}

export const TextField = ({ control, type, name, label, required, ...field }: TextFieldProps) => {
  const [showPassword, setShowPassword] = useState<boolean>(false)
  const {
    field: { onChange, onBlur, value, ref },
    fieldState: { error },
  } = useController({ name, control })

  return (
    <div>
      <div className="relative">
        <p className="text-[14px] leading-4 font-bold">
          {label} {required ? <span className="text-red">*</span> : null}
        </p>
        <input
          type={type === 'password' ? (showPassword ? 'text' : 'password') : 'text'}
          {...field}
          ref={ref}
          onBlur={onBlur}
          required={required}
          className="peer focus:outline-none focus:placeholder:text-[#000] focus:placeholder:text-[14px] w-full border-0 border-b-[1px] border-[#CCCCCC] p-[8px] mt-4 placeholder:text-[#cccccc] text-[14px]"
          value={value || ''}
          onChange={onChange}
        />
        <div className="peer-focus:w-full absolute bottom-0 left-0 w-0 h-[2px] bg-[#239f40] transition-all duration-500" />

        {type === 'password' ? (
          <div
            className="absolute top-[2.5rem] lg:top-[2.8rem] right-2 cursor-pointer"
            onClick={() => setShowPassword(!showPassword)}
          >
            {!showPassword ? <AiOutlineEyeInvisible /> : <AiOutlineEye />}
          </div>
        ) : null}
      </div>
      {!!error ? (
        <div className="mt-[8px]">
          <div className={clsx(error ? 'overflow-visible' : 'hidden')}>
            <ErrorMessage>{error?.message || 'Vui lòng nhập trường này'}</ErrorMessage>
          </div>
        </div>
      ) : null}
    </div>
  )
}
