/* eslint-disable react-hooks/exhaustive-deps */
import { Html5QrcodeScanner } from "html5-qrcode";
import { useEffect, useRef, useState } from "react";

const qrcodeRegionId = "html5qr-code-full-region";

// Creates the configuration object for Html5QrcodeScanner.
const createConfig = (props: any) => {
  let config: any = {};
  if (props.fps) {
    config.fps = props.fps;
  }
  if (props.qrbox) {
    config.qrbox = props.qrbox;
  }
  if (props.aspectRatio) {
    config.aspectRatio = props.aspectRatio;
  }
  if (props.disableFlip !== undefined) {
    config.disableFlip = props.disableFlip;
  }
  return config;
};

const Html5QrcodePlugin = (props: any) => {
  const html5QrcodeScannerRef = useRef<any>(null);
  const [isWebcamOn, setWebcamOn] = useState(false);

  useEffect(() => {
    // when component mounts
    const config = createConfig(props);
    const verbose = props.verbose === true;
    // Suceess callback is required.
    if (!props.qrCodeSuccessCallback) {
      throw "qrCodeSuccessCallback is required callback.";
    }
    const html5QrcodeScanner = new Html5QrcodeScanner(
      qrcodeRegionId,
      config,
      verbose
    );
    html5QrcodeScannerRef.current = html5QrcodeScanner;
    html5QrcodeScanner.render(
      props.qrCodeSuccessCallback,
      props.qrCodeErrorCallback
    );

    // cleanup function when component will unmount
    return () => {
      if (!!html5QrcodeScannerRef.current) {
        html5QrcodeScannerRef.current.clear().catch((error: any) => {
          props.Cancel();
          console.error("Failed to clear html5QrcodeScanner. ", error);
        });
      }
    };
  }, []);

  const startWebcam = () => {
    setWebcamOn(true);
  };

  const stopWebcam = () => {
    setWebcamOn(false);
    if (html5QrcodeScannerRef.current) {
      html5QrcodeScannerRef.current.stop().catch((error: any) => {
        console.error("Failed to stop html5QrcodeScanner. ", error);
      });
    }
  };

  return (
    <div>
      <div id={qrcodeRegionId} />

      {isWebcamOn ? (
        <button onClick={stopWebcam}>Dừng webcam</button>
      ) : (
        <button onClick={startWebcam}>Bắt đầu webcam</button>
      )}
    </div>
  );
};

export default Html5QrcodePlugin;
