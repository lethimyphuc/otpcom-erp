import { ErrorMessage } from '@hookform/error-message'
import clsx from 'clsx'
import _ from 'lodash'
import React from 'react'
import { Control, Controller, FieldValues, Path, RegisterOptions } from 'react-hook-form'

import { _format } from '../../../util'
import { Select, Spin } from 'antd'
import { DefaultOptionType } from 'antd/lib/select'

type TProps = {
  className?: string
  label?: string
  required?: boolean
  name: any
  control: any
  rules?: any
  defaultValue?: any
  mode?: 'multiple' | 'tags'
  isLoading?: boolean
  handleScroll?: any
  style?: object
  placeholder?: string
  disabled?: boolean
  optionList: any
  onSearch?: any
  onFocus?: any
  showSearch?: boolean
  allowClear?: boolean
  onChange?: (value: any, option?: DefaultOptionType | DefaultOptionType[]) => void
}

export const FormSelect: React.FC<TProps> = ({
  className,
  label,
  required = false,
  name,
  control,
  rules,
  defaultValue,
  mode,
  isLoading,
  handleScroll,
  style,
  showSearch = true,
  placeholder,
  disabled = false,
  onSearch,
  onFocus,
  optionList,
  allowClear = false,
  onChange = undefined,
}) => {
  const handlePopupScroll = (e: any) => {
    const { target } = e
    if (target.scrollHeight - target.scrollTop === target.clientHeight) {
      handleScroll && handleScroll()
    }
  }
  return (
    <div className={clsx('w-full', className)}>
      {label && (
        <div className="label-form">
          <label className="" htmlFor={name}>
            {label} {required === true && <span className="text-red">*</span>}
          </label>
        </div>
      )}
      <Controller
        control={control}
        name={name}
        rules={rules}
        render={({
          field: { value, onChange: onChangeForm, ...newField },
          fieldState: { error },
          formState: { errors },
        }) => (
          <React.Fragment key={defaultValue as any}>
            <Select
              defaultValue={defaultValue && defaultValue}
              mode={mode}
              className={`${className}`}
              showSearch={showSearch}
              allowClear={allowClear}
              placement="bottomLeft"
              value={value && value}
              loading={isLoading}
              onPopupScroll={handlePopupScroll}
              style={style}
              placeholder={placeholder}
              optionFilterProp="children"
              disabled={disabled}
              maxTagCount="responsive"
              onChange={(value, option) => {
                onChangeForm(value)
                !!onChange ? onChange(value, option) : undefined
              }}
              filterOption={(input, option) => {
                return (optionList?.label?.toString().toLowerCase() ?? '').includes(
                  input.toLowerCase()
                )
              }}
              notFoundContent={isLoading ? <Spin size="small" /> : null}
              onSearch={onSearch && _.debounce(onSearch, 500)}
              getPopupContainer={(trigger) => trigger.parentNode} // cái này để cái dropdown ko chạy tùm lum khi scroll
              options={optionList}
              onFocus={() => onFocus && onFocus()}
              {...newField}
            />
            {!value && (
              <ErrorMessage
                errors={errors}
                name={name as any}
                render={({ message }) => (
                  <p className="text-warning text-xs font-medium mt-1">{message}</p>
                )}
              />
            )}
          </React.Fragment>
        )}
      />
    </div>
  )
}
