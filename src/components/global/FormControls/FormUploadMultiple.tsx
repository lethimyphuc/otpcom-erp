/* eslint-disable @next/next/no-img-element */
import { PlusOutlined } from '@ant-design/icons'
import { Modal, Upload } from 'antd'
import type { RcFile, UploadProps } from 'antd/es/upload'
import type { UploadFile } from 'antd/es/upload/interface'
import React, { useState } from 'react'
import { UseFormSetValue, UseFormWatch } from 'react-hook-form'

const getBase64 = (file: RcFile | any): Promise<string> =>
  new Promise((resolve, reject) => {
    const reader = new FileReader()
    // console.log("FILE: ", file, reader);
    reader.readAsDataURL(file)
    reader.onload = () => resolve(reader.result as string)
    reader.onerror = (error) => reject(error)
  })

type FormUploadProps = {
  setValue: UseFormSetValue<{
    nickname: string
    email: string
    ratings: number
    text: string
    anonymous: boolean
    images: any
    sku: string
  }>
  watch: UseFormWatch<{
    nickname: string
    email: string
    ratings: number
    text: string
    anonymous: boolean
    images: any
    sku: string
  }>
}
const FormUpload = (props: FormUploadProps) => {
  const { setValue, watch } = props
  const [previewOpen, setPreviewOpen] = useState(false)
  const [previewImage, setPreviewImage] = useState('')
  const [previewTitle, setPreviewTitle] = useState('')

  const handleCancel = () => setPreviewOpen(false)

  const handlePreview = async (file: UploadFile) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj as RcFile)
    }

    setPreviewImage(file.url || (file.preview as string))
    setPreviewOpen(true)
    setPreviewTitle(file.name || file.url!.substring(file.url!.lastIndexOf('/') + 1))
  }

  const handleChange: UploadProps['onChange'] = ({ fileList: newFileList }) => {
    // console.log("newFileList: ", newFileList);
    const createObjectURL = newFileList.map((file: any) => {
      if (file?.url) {
        return file
      } else {
        const createUrl = URL.createObjectURL(file?.originFileObj)
        // console.log("createUrl: ", createUrl);
        return {
          ...file,
          status: 'done',
          url: createUrl,
        }
      }
    })
    setValue('images', createObjectURL)
    // setFileList(createObjectURL);
  }

  const uploadButton = (
    // <div>
    //   <PlusOutlined />
    //   <div style={{ marginTop: 8 }}>Upload</div>
    // </div>

    <Upload
      action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
      // listType="picture-card"
      fileList={watch('images')}
      onPreview={handlePreview}
      onChange={handleChange}
    >
      <button className={'upload'}>
        <img src="/icons/upload-image.svg" alt="" />
        <p>Thêm hình ảnh/video</p>
      </button>
    </Upload>
  )
  return (
    <>
      {uploadButton}
      <Modal visible={previewOpen} title={previewTitle} footer={null} onCancel={handleCancel}>
        <img alt="example" style={{ width: '100%' }} src={previewImage} />
      </Modal>
    </>
  )
}

export default FormUpload
