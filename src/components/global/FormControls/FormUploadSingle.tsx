/* eslint-disable @next/next/no-img-element */
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons'
import { message, Upload } from 'antd'
import type { UploadChangeParam } from 'antd/es/upload'
import type { RcFile, UploadFile, UploadProps } from 'antd/es/upload/interface'
import React, { useState } from 'react'
import { UseFormSetValue, UseFormWatch } from 'react-hook-form'
import { TChangeInfoCustomer } from '~src/types/authenticate'

const getBase64 = (img: RcFile, callback: (url: string) => void) => {
  const reader = new FileReader()
  reader.addEventListener('load', () => callback(reader.result as string))
  reader.readAsDataURL(img)
}

const beforeUpload = (file: RcFile) => {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png'
  if (!isJpgOrPng) {
    message.error('You can only upload JPG/PNG file!')
  }
  const isLt2M = file.size / 1024 / 1024 < 2
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!')
  }
  return isJpgOrPng && isLt2M
}

type FormUploadSingleProps = {
  title: string
  setValue: UseFormSetValue<TChangeInfoCustomer>
  watch: UseFormWatch<TChangeInfoCustomer>
}

const FormUploadSingle = (props: FormUploadSingleProps) => {
  const { title, setValue, watch } = props
  const [loading, setLoading] = useState(false)
  // const [imageUrl, setImageUrl] = useState<string>();

  const handleChange: UploadProps['onChange'] = (info: UploadChangeParam<UploadFile>) => {
    getBase64(info.file.originFileObj as RcFile, (url) => {
      setLoading(false)
      setValue('avatar', { ...info?.file, url: url })
    })
  }

  const uploadButton = (
    <div className="flex items-center justify-center gap-2">
      {loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div className="!mt-0">{title}</div>
    </div>
  )
  return (
    <Upload
      name="avatar"
      listType="picture-card"
      className="avatar-uploader avatar-form-single"
      showUploadList={false}
      beforeUpload={beforeUpload}
      onChange={handleChange}
    >
      {watch('avatar')?.url ? (
        <img src={watch('avatar')?.url} alt="avatar" className="!w-full !h-full" />
      ) : (
        uploadButton
      )}
    </Upload>
  )
}

export default FormUploadSingle
