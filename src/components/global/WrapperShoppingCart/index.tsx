/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from 'react'
import clsx from 'clsx'
import styles from '../../pages/shopping-cart/ShoppingCart.module.scss'
import { useMutation, useQueryClient } from 'react-query'
import ItemProduct from '~src/components/pages/shopping-cart/components/ItemProduct'
import { useFieldArray, useForm } from 'react-hook-form'
import useDebounce from '~src/hook/useDebounce'
import { graphQLClient } from '~src/api/graphql'
import { UPDATE_PRODUCT_TO_CART } from '~src/api/product'
import { toast } from 'react-toastify'

type WrapperShoppingCartProps = {
  dataCart?: TDataUpdateToCart
  isFetchingDataCart?: boolean
}
const WrapperShoppingCart = (props: WrapperShoppingCartProps) => {
  const { dataCart, isFetchingDataCart } = props
  const queryClient = useQueryClient()
  const { handleSubmit, control, setValue, getValues } = useForm<{
    items: TDataCart[]
  }>({
    defaultValues: {
      items: [],
    },
  })
  const { fields, update } = useFieldArray({
    control, // control props comes from useForm (optional: if you are using FormContext)
    name: 'items', // unique name for your Field Array
  })
  const [submitCart, setSubmitCart] = useState(false)
  const debouncedSearch = useDebounce(fields, 500)

  useEffect(() => {
    if (!!dataCart && !submitCart) {
      setValue('items', dataCart?.cart?.items)
    }
  }, [dataCart, submitCart])

  const { isLoading: isLoadingDataUpdateToCart, mutate: mutateUpdateToCart } = useMutation({
    mutationFn: async (data: TUpdateToCart[]) => {
      return (await graphQLClient.request(UPDATE_PRODUCT_TO_CART, {
        input: {
          cart_id: localStorage?.getItem('cartId'),
          cart_items: data,
        },
      })) as { updateCartItems: TDataUpdateToCart }
    },
    onSuccess: (response: { updateCartItems: TDataUpdateToCart }) => {
      if (!!response?.updateCartItems?.user_errors?.length) {
        toast.error(response?.updateCartItems?.user_errors[0].message)
      }
      queryClient.setQueryData(['getCartId'], (oldData: any) => {
        if (!!oldData) {
          return {
            ...oldData,
            cart: {
              ...oldData?.cart,
              total_quantity:
                response?.updateCartItems?.cart?.total_quantity ?? oldData?.cart?.total_quantity,
              items: response?.updateCartItems?.cart?.items,
              prices: response?.updateCartItems?.cart?.prices,
            },
          }
        } else {
          return oldData
        }
      })
    },
  })

  const onSubmit = (data: { items: TDataCart[] }) => {
    const DATA_SUBMIT = data?.items?.map((field) => ({
      cart_item_uid: field?.uid,
      quantity: field?.quantity,
    }))
    console.log('FIELD SUBMIT: ', DATA_SUBMIT)
    mutateUpdateToCart(DATA_SUBMIT)
  }

  useEffect(() => {
    console.log('debouncedSearch === fields: ', debouncedSearch === fields)
    if (submitCart && debouncedSearch === fields) {
      handleSubmit(onSubmit)()
    }
  }, [debouncedSearch, submitCart])

  return (
    <div className={clsx(styles.leftSection, styles.cartLeftSection)}>
      <div className={`${clsx(styles.leftSectionTitle)} flex-col xs:flex-row mb-16 xs:mb-0 `}>
        <div>
          <h1 className="!text-[24px] lg:!text-[48px]">Giỏ hàng</h1>
          <span>{dataCart?.cart?.total_quantity}</span>
        </div>
        <button
          className="justify-center items-center"
          onClick={() => {
            queryClient.invalidateQueries(['getCartId'])
          }}
        >
          <div>
            <img
              className={isFetchingDataCart ? 'keyframe-rotate' : ''}
              src="/icons/refresh-cart.svg"
              alt=""
            />
            <p className="!text-[12px] lg:!text-[16px]">Cập nhật giỏ hàng</p>
          </div>
        </button>
      </div>
      <div className={clsx(styles.cartInfo, 'gap-16 grid grid-flow-row xs:gap-0')}>
        <div className={clsx(styles.cartInfoHead, '!hidden xs:!flex')}>
          <div className=""></div>
          <div className="">
            <p>Sản phẩm</p>
          </div>
          <div className="text-right sm:text-left">
            <p>Số lượng</p>
          </div>
          <div className="justify-end sm:!justify-start lg:!justify-end">
            <p>Giá tiền</p>
          </div>
        </div>
        {fields?.map((item, index) => (
          <ItemProduct
            item={item}
            key={`${item?.uid}`}
            update={update}
            index={index}
            setSubmitCart={setSubmitCart}
          />
        ))}
      </div>
    </div>
  )
}

export default WrapperShoppingCart
