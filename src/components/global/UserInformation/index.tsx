/* eslint-disable @next/next/no-img-element */
/**
 * /* eslint-disable @next/next/no-img-element
 *
 * @format
 */

import { Dropdown, Menu, Space } from 'antd'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useCookies } from 'react-cookie'

type UserInformationProps = {
  dataInfoCustomer?: Record<string, any>
}
const UserInformation = (props: UserInformationProps) => {
  const { dataInfoCustomer } = props
  const router = useRouter()
  const [cookies, setCookie, removeCookie] = useCookies(['token'])

  const items = [
    {
      key: '1',
      label: '',
      icon: (
        <div key={1} className="px-4 py-2 !bg-white hover:!bg-[#f5f5f5] duration-200">
          <Link href="/account">
            <span className="!text-[14px]">Thông tin tài khoản</span>
          </Link>
        </div>
      ),
    },
    {
      key: '2',
      label: '',
      icon: (
        <div
          className="px-4 py-2 !bg-white hover:!bg-[#f5f5f5] duration-200 !text-[14px]"
          key={2}
          onClick={() => {
            localStorage.removeItem('token')
            removeCookie('token', { path: '/' })
            localStorage.removeItem('loginSocial')
            localStorage.removeItem('cartId')
            window.location.replace(`${window.location.origin}/login`)
          }}
        >
          Đăng xuất
        </div>
      ),
    },
  ]

  const menu = <Menu className="mt-6 mx-2" items={items}></Menu>

  return (
    <>
      {!!dataInfoCustomer?.customer ? (
        <div
          className="flex"
          onClick={() => {
            if (window.innerWidth > 1200) {
              router.push('/account')
            }
          }}
        >
          <Dropdown
            className="cursor-pointer justify-between w-full !gap-0 sm:!gap-6"
            overlay={menu}
            trigger={['hover']}
            placement="bottom"
          >
            <Space>
              <div className="icon-user min-w-[12px]">
                <div className="img">
                  <img src={'/image/svg/user.svg'} alt="" />
                </div>
              </div>
              <span className="font-semibold hidden sm:flex overflow-hidden whitespace-nowrap">
                {dataInfoCustomer?.customer?.firstname} {dataInfoCustomer?.customer?.lastname}
              </span>
            </Space>
          </Dropdown>
        </div>
      ) : (
        <Link href="/login">
          <button className="icon-user min-w-[12px]">
            <div className="img max-w-xs">
              <img src={'/image/svg/user.svg'} alt="" />
            </div>
          </button>
        </Link>
      )}
    </>
  )
}

export default UserInformation
