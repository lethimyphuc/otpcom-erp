import React, { FC } from "react";

type TProps = {
  title: string;
};

export const TitleInfoTag: FC<TProps> = ({ title = "" }) => {
  return (
    <div>
      <div className="col-span-3 bg-[#eef3f7] text-[#41a6ef] w-fit h-fit px-3 py-1 rounded font-semibold">
        <span>{title}</span>
      </div>
    </div>
  );
};
