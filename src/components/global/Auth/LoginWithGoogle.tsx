/* eslint-disable @next/next/no-img-element */
import React from "react";
import { GoogleLogin } from "@react-oauth/google";

const LoginWithGoogle = () => {
  const loginGoogleSuccess = (response: any) => {
    try {
      console.log("response: ", response);
    } catch (err) {
      console.log("Err: ", err);
    }
  };
  return (
    <div className="relative">
      <div className=" absolute top-0 left-0 opacity-0">
        <GoogleLogin
          size="large"
          width="100%"
          onSuccess={(credentialResponse) =>
            loginGoogleSuccess(credentialResponse)
          }
          onError={() => console.log("lỗi rồi")}
          useOneTap={false}
          auto_select={false}
          text="continue_with"
          locale="vi_VN"
        />
      </div>

      <div className="w-[32px] h-[32px]">
        <img src={"/icons/svg/google.svg"} alt="" />
      </div>
    </div>
  );
};

export default LoginWithGoogle;
