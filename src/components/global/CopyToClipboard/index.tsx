import { Tooltip } from "antd";
import clsx from "clsx";
import React, { useEffect } from "react";
import { useCopyToClipboard } from "react-use";
import { toast } from "~src/components";

export const CopyToClipboard: React.FC<{ content: string }> = ({ content }) => {
  const [copied, copyToClipboard] = useCopyToClipboard();
  useEffect(() => {
    if (!copied.value) {
      return;
    }
  }, [copied]);
  return (
    <div>
      {(content && (
        <Tooltip title="Sao chép" placement="topLeft">
          <button
            onClick={() => {
              if (content) {
                if (copied?.noUserInteraction) {
                  toast.success("Đã sao chép");
                }
                copyToClipboard(content);
              }
            }}
            className={clsx("cursor-pointer focus:bg-[#d2e8ff]")}
          >
            {content && <i className="far fa-copy text-xs mr-2"></i>}
            <span>{content}</span>
          </button>
        </Tooltip>
      )) || <>__</>}
    </div>
  );
};
