import * as React from "react";
import { usePagination, DOTS } from "../../../hook/usePagination";
import clsx from "clsx";
import styles from "./pagination.module.scss";

export interface IPaginationProps {
  totalCount: number;
  siblingCount?: number;
  currentPage: number;
  pageSize: number;
  className: string;
  onPageChange: (currentPage: number) => void;
}

export default function Pagination(props: IPaginationProps) {
  const {
    onPageChange,
    totalCount,
    siblingCount = 1,
    currentPage,
    pageSize,
    className,
  } = props;

  const paginationRange: any = usePagination({
    currentPage,
    totalCount,
    siblingCount,
    pageSize,
  });

  if (currentPage === 0 || paginationRange.length < 2) {
    return null;
  }

  const onNext = () => {
    onPageChange(currentPage + 1);
  };

  const onPrevious = () => {
    onPageChange(currentPage - 1);
  };

  let lastPage = paginationRange[paginationRange.length - 1];

  return (
    <ul
      className={clsx(styles.paginationContainer, { [className]: className })}
    >
      <li
        className={clsx(styles.paginationItem, {
          disabled: currentPage === 1,
        })}
        onClick={onPrevious}
      >
        <div className={clsx(styles.arrow, styles.left)} />
      </li>
      {paginationRange.map((pageNumber: number, index: number) => {
        if (pageNumber === Number(DOTS)) {
          return (
            <li
              key={index}
              className={clsx(styles.paginationItem, styles.dots)}
            >
              &#8230;
            </li>
          );
        }

        return (
          <li
            key={index}
            className={clsx(styles.paginationItem, {
              selected: pageNumber === currentPage,
            })}
            onClick={() => onPageChange(pageNumber)}
          >
            {pageNumber}
          </li>
        );
      })}
      <li
        className={clsx(styles.paginationItem, {
          disabled: currentPage === lastPage,
        })}
        onClick={onNext}
      >
        <div className={clsx(styles.arrow, styles.right)} />
      </li>
    </ul>
  );
}
