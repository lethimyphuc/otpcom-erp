/* eslint-disable @next/next/no-img-element */

export const TitleProduct = ({ title }: { title?: string | null }) => {
  return (
    <div className="list-search">
      <div className="title-search">
        <div className="icon-center">
          <img src="/icons/search.svg" alt="" />
        </div>
        <div className="content-text">
          <span className="text">{title ?? 'tất cả sản phẩm'}</span>
        </div>
      </div>
    </div>
  )
}
