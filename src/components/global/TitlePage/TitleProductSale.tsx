/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @next/next/no-img-element */
import React, { useEffect } from 'react'
import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger'

type TitleProductSaleProps = {
  dataDailySales?: TDailySales[]
  title: string | null
}
export const TitleProductSale = (props: TitleProductSaleProps) => {
  const { dataDailySales, title } = props
  // useEffect(() => {
  //   if (!!dataDailySales?.length && !!dataDailySales[0]?.end_date) {
  //     const zeroFill = (n: any) => {
  //       return ("0" + n).slice(-2);
  //     };

  //     let timeId = setInterval(() => {
  //       const now = new Date().getTime();
  //       const dateEnd = !!dataDailySales[0]?.end_date
  //         ? new Date(dataDailySales[0]?.end_date).getTime()
  //         : 0;
  //       let timeLeft = dateEnd - now;
  //       if (timeLeft > 0) {
  //         const hours = Math.floor(
  //           (timeLeft % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
  //         );
  //         const minutes = Math.floor(
  //           (timeLeft % (1000 * 60 * 60)) / (1000 * 60)
  //         );
  //         const seconds = Math.floor((timeLeft % (1000 * 60)) / 1000);
  //         const getElementHours = document.querySelector("#hours");
  //         const getElementMinutes = document.querySelector("#minutes");
  //         const getElementSeconds = document.querySelector("#seconds");
  //         if (!!getElementHours) {
  //           getElementHours.innerHTML = zeroFill(`${hours}`);
  //         }
  //         if (!!getElementMinutes) {
  //           getElementMinutes.innerHTML = zeroFill(`${minutes}`);
  //         }
  //         if (!!getElementSeconds) {
  //           getElementSeconds.innerHTML = zeroFill(`${seconds}`);
  //         }
  //       }
  //     }, 1000);
  //     return () => {
  //       clearInterval(timeId);
  //     };
  //   }
  // }, [!!dataDailySales?.length]);

  // useEffect(() => {
  //   gsap.registerPlugin(ScrollTrigger);

  //   const title = document.querySelector(".title-product-sale");

  //   gsap.set(title, { opacity: 0, y: 100 });

  //   ScrollTrigger.create({
  //     trigger: title,
  //     start: "top 80%",
  //     end: "top 30%",
  //     onEnter: () => {
  //       gsap.to(title, {
  //         opacity: 1,
  //         y: 0,
  //         duration: 1,
  //         ease: "power2.out",
  //       });
  //     },
  //     onLeaveBack: () => {
  //       gsap.to(title, {
  //         opacity: 0,
  //         y: 100,
  //         duration: 1,
  //         ease: "power2.out",
  //       });
  //     },
  //   });
  // }, []);

  return (
    <div className="title-product-sale">
      <div className="list-search">
        <div className="title-search">
          <div className="icon-center">
            <img src="/icons/search.svg" alt="" />
          </div>
          <div className="content-text">
            <span className="text">{title ?? 'sản phẩm khuyến mãi'}</span>
          </div>
        </div>
      </div>
      {/* <div className="title-right">
        <div className="text">
          <span>Kết thúc trong</span>
        </div>
        <div className="times">
          <div className="time hours">
            <div id="hours" className="num">
              00
            </div>
            <div className="line"></div>
            <div className="txt">giờ</div>
          </div>
          <div className="time minute">
            <div id="minutes" className="num">
              00
            </div>
            <div className="line"></div>
            <div className="txt">phút</div>
          </div>
          <div className="time second">
            <div id="seconds" className="num">
              00
            </div>
            <div className="line"></div>
            <div className="txt">giây</div>
          </div>
        </div>
      </div> */}
    </div>
  )
}
