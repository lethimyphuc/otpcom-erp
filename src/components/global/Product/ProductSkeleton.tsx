import React from "react";

const ProductSkeleton = () => {
  return (
    <div className="fx-skeleton">
      <div className="overlay z-[2]" />
      <div className="inner-item">
        <div className="image">
          <div className="skeholder w-full h-full" />
        </div>
        <div className="text-inner space-y-2">
          <div className="skeholder w-full rounded-[4px]">_</div>
          <div className="skeholder w-[60%] mx-auto rounded-[4px]">_</div>
          <div className="my-4 leading-[25px] skeholder w-[80%] mx-auto rounded-[4px]">
            _
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductSkeleton;
