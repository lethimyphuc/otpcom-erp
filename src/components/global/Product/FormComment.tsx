/* eslint-disable @next/next/no-img-element */
/**
 * /* eslint-disable @next/next/no-img-element
 *
 * @format
 */

import { Switch } from 'antd'
import React from 'react'
import {
  Control,
  Controller,
  FieldErrorsImpl,
  UseFormSetValue,
  UseFormWatch,
} from 'react-hook-form'
import ErrorMessage from '../ErrorMessage'

type FormCommentProps = {
  control: Control<
    {
      nickname: string
      email: string
      text: string
      anonymous: boolean
      sku: string
    },
    any
  >
  setValue: UseFormSetValue<{
    nickname: string
    email: string
    text: string
    anonymous: boolean
    sku: string
  }>
  watch: UseFormWatch<{
    nickname: string
    email: string
    text: string
    anonymous: boolean
    sku: string
  }>
  setOpen: any
  errors: FieldErrorsImpl<{
    nickname: string
    email: string
    text: string
    anonymous: boolean
    sku: string
  }>
  dataProductDetail: TProduct
}
const FormComment = (props: FormCommentProps) => {
  const { control, setValue, watch, errors, dataProductDetail, setOpen } = props
  const onChange = (checked: boolean) => {
    console.log(`switch to ${checked}`)
    setValue('anonymous', checked)
  }

  return (
    <div className="wrapper-form">
      <div className={'emptyContainer'}>
        {dataProductDetail?.reviews?.items?.length == 0 && (
          <div className={'empty'}>
            <img src="/icons/empty-review.svg" alt="empty" />
            <h3>Chưa có bình luận</h3>
            <p>Hiện chưa có bình luận nào cho sản phẩm này.</p>
          </div>
        )}

        <div className={'flexItem'}>
          <h3>Để lại bình luận</h3>
          <p>Hãy chia sẻ những điều bạn nghĩ về sản phẩm này với những người mua khác nhé.</p>
        </div>
        <div className={'reviewForm'}>
          <div className="flex-1">
            <div className={'formItem'}>
              <p>
                Họ và tên <span>*</span>
              </p>
              <Controller
                name="nickname"
                control={control}
                rules={{
                  required: true,
                }}
                render={({ field }) => (
                  <input
                    {...field}
                    className={'formInput'}
                    type="text"
                    name=""
                    id=""
                    placeholder="Nhập tên của bạn"
                  />
                )}
              />

              <div className={'formUnderline'}></div>
            </div>
            {errors?.nickname ? <ErrorMessage>Vui lòng không để trống</ErrorMessage> : <></>}
          </div>

          <div className="flex-1">
            <div className={'formItem'}>
              <p>
                Email <span>*</span>
              </p>
              <Controller
                name="email"
                control={control}
                render={({ field }) => (
                  <input
                    {...field}
                    className={'formInput'}
                    type="text"
                    name=""
                    id=""
                    placeholder="Nhập email của bạn"
                  />
                )}
              />

              <div className={'formUnderline'}></div>
            </div>
            {errors?.email ? <ErrorMessage>Vui lòng không để trống</ErrorMessage> : <></>}
          </div>
        </div>
        <div className={'writeReview'}>
          <div className={'formItem'}>
            <p>
              Bình luận của bạn <span>*</span>
            </p>
            <Controller
              name="text"
              control={control}
              render={({ field }) => (
                <textarea {...field} name="" id="" placeholder="Viết bình luận"></textarea>
              )}
            />

            <div className={'formUnderline'}></div>
          </div>
          {errors?.text ? <ErrorMessage>Vui lòng không để trống</ErrorMessage> : <></>}
        </div>
        <div className={'actionReview flex justify-between'}>
          <div className="flex items-center gap-[10px] h-[44px]">
            <Switch onChange={onChange} className="switch-anonymous" />
            <span>Ẩn danh</span>
          </div>

          <div className="flex sm:gap-8 gap-3 justify-between">
            <div
              className="bg-warning sm:px-12 px-8 rounded-full text-white font-bold flex items-center cursor-pointer hover:bg-[#c1342a]"
              onClick={() => setOpen(false)}
            >
              Huỷ
            </div>
            <button className={'sendReview'} type="submit">
              <img src="/icons/send-btn.svg" alt="" />
              <p>Gửi ngay</p>
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default FormComment
