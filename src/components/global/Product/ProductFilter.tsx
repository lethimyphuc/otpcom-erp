/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @next/next/no-img-element */
import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { FormSelect } from "../FormControls/FormSelect";
import { ButtonMain } from "../Button/ButtonMain/ButtonMain";
import { useQuery } from "react-query";
import {graphQLClient, graphQLClientGET} from "~src/api/graphql";
import { GET_CATEGORIES } from "~src/api/categories";
import { DefaultOptionType } from "antd/lib/select";
import { NextRouter } from "next/router";

type ProductFilterProps = {
  router: NextRouter;
};
export const ProductFilter = (props: ProductFilterProps) => {
  const { router } = props;
  const { control, handleSubmit, setValue, watch } = useForm<BoxSearchForm>({
    defaultValues: {
      type: null,
      company: null,
      end: null,
      groupAccessary: null,
      sort: null,
    },
  });

  useEffect(() => {
    setValue(
      "company",
      !!router.query?.company ? `${router.query?.company}` : null
    );
    setValue("type", !!router.query?.type ? `${router.query?.type}` : null);
    setValue("end", router.query?.end ? `${router.query?.end}` : null);
    setValue(
      "groupAccessary",
      !!router.query?.groupAccessary ? `${router.query?.groupAccessary}` : null
    );
    setValue(
      "sort",
      !!router.query?.sort ? `${router.query?.sort}` : "ascName"
    );
  }, [
    router.query?.company,
    router.query?.type,
    router.query?.end,
    router.query?.groupAccessary,
  ]);

  console.log("watch: ", watch());

  // Danh sách Hãng xe
  const {
    data: dataCategoriesByCompany,
    isLoading: isLoadingDataCategoriesByCompany,
  }: {
    data?: TChildrenCategories[];
    isLoading?: boolean;
  } = useQuery({
    queryKey: ["getCategoriesByCompany"],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_CATEGORIES, {
        eq: "Ng==",
      })) as TCategories;
      return data.categories?.items[0]?.children;
    },
  });

  // Danh sách Dòng xe
  const {
    data: dataCategoriesByType,
    isLoading: isLoadingDataCategoriesByType,
  }: {
    data?: TChildrenCategories[];
    isLoading?: boolean;
  } = useQuery({
    queryKey: ["getCategoriesByType", { eq: watch("company") }],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_CATEGORIES, {
        eq: `${watch("company")}`,
      })) as TCategories;
      return data.categories?.items[0]?.children;
    },
    enabled: !!watch("company"),
  });

  // Danh sách Đời xe
  const {
    data: dataCategoriesByEnd,
    isLoading: isLoadingDataCategoriesByEnd,
  }: {
    data?: TChildrenCategories[];
    isLoading?: boolean;
  } = useQuery({
    queryKey: ["getCategoriesByEnd", { eq: watch("type") }],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_CATEGORIES, {
        eq: `${watch("type")}`,
      })) as TCategories;
      return data.categories?.items[0]?.children;
    },
    enabled: !!watch("type"),
  });

  // Danh sách nhóm phụ tùng
  const {
    data: dataSpareParts,
    isLoading: isLoadingDataSpareParts,
  }: {
    data?: TChildrenCategories[];
    isLoading?: boolean;
  } = useQuery({
    queryKey: ["getSpareParts"],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_CATEGORIES, {
        eq: "Nw==",
      })) as TCategories;
      return data.categories?.items[0]?.children;
    },
  });

  const handleSelectCategoryCompany = (
    value: any,
    option?: DefaultOptionType | DefaultOptionType[] | undefined
  ) => {
    setValue("type", null);
    setValue("end", null);
  };

  const handleSelectCategoryType = (
    value: any,
    option?: DefaultOptionType | DefaultOptionType[] | undefined
  ) => {
    setValue("end", null);
  };

  const onSubmit = (data: BoxSearchForm) => {
    console.log("Data: ", data);
    const DATA_SUBMIT: Record<string, any> = {
      company: !!data?.company ? `${data?.company}`.trim() : null,
      type: !!data?.type ? `${data?.type}`.trim() : null,
      end: !!data?.end ? `${data?.end}`.trim() : null,
      groupAccessary: !!data?.groupAccessary
        ? `${data?.groupAccessary}`.trim()
        : null,
      sort: !!data?.sort ? `${data?.sort}`.trim() : null,
    };
    Object.keys(DATA_SUBMIT).forEach((key: string) => {
      if (!DATA_SUBMIT[key]) {
        delete DATA_SUBMIT[key];
      }
    });
    console.log("DATA_SUBMIT: ", DATA_SUBMIT);
    router.push({
      query: {
        ...DATA_SUBMIT,
      },
    });
  };

  return (
    <div className="form-filter-product">
      <div className="label-filter">
        <div className="item">Sắp xếp theo:</div>
        <div className="item">Lọc theo:</div>
        <div className="item"></div>
        <div className="item"></div>
        <div className="item"></div>
      </div>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="box">
          <div className="items">
            <div className="item">
              <FormSelect
                control={control}
                name={"sort"}
                optionList={[
                  { label: "Tên A -> Z", value: "ascName" },
                  { label: "Tên Z -> A", value: "descName" },
                  { label: "Giá tăng dần", value: "ascPrice" },
                  { label: "Giá giảm dần", value: "descPrice" },
                  { label: "Vị trí tăng dần", value: "ascPosition" },
                  { label: "Vị trí giảm dần", value: "descPosition" },
                  { label: "Kết quả tìm kiếm tăng dần", value: "ascRelevance" },
                  {
                    label: "Kết quả tìm kiếm giảm dần",
                    value: "descRelevance",
                  },
                ]}
                label=""
                placeholder="Sắp xếp theo"
                allowClear
              />
            </div>
            <div className="item">
              <FormSelect
                control={control}
                name={"company"}
                optionList={dataCategoriesByCompany?.map(
                  (company: TChildrenCategories) => {
                    return {
                      label: company?.name,
                      value: company?.uid,
                    };
                  }
                )}
                onChange={handleSelectCategoryCompany}
                label=""
                placeholder="Hãng xe"
                allowClear
              />
            </div>
            <div className="item">
              <FormSelect
                control={control}
                name={"type"}
                optionList={dataCategoriesByType?.map(
                  (type: TChildrenCategories) => {
                    return {
                      label: type?.name,
                      value: type?.uid,
                    };
                  }
                )}
                onChange={handleSelectCategoryType}
                label=""
                placeholder="Dòng xe"
                allowClear
              />
            </div>
            <div className="item">
              <FormSelect
                control={control}
                name={"end"}
                optionList={dataCategoriesByEnd?.map(
                  (end: TChildrenCategories) => {
                    return {
                      label: end?.name,
                      value: end?.uid,
                    };
                  }
                )}
                label=""
                placeholder="Đời xe"
                allowClear
              />
            </div>
            <div className="item">
              <FormSelect
                control={control}
                name={"groupAccessary"}
                optionList={dataSpareParts?.map(
                  (sparePart: TChildrenCategories) => {
                    return {
                      label: sparePart?.name,
                      value: sparePart?.uid,
                    };
                  }
                )}
                label=""
                placeholder="Nhóm phụ tùng"
                allowClear
              />
            </div>
          </div>

          <div className="button-search">
            <ButtonMain
              background="green"
              type="button"
              onClick={handleSubmit(onSubmit)}
            >
              <div className="flex items-center gap-[0.4rem]">
                <div className="icon">
                  <img src="/icons/search-icon.svg" alt="" />
                </div>
                <div className="text">Tìm kiếm</div>
              </div>
            </ButtonMain>
          </div>
        </div>
      </form>
    </div>
  );
};
