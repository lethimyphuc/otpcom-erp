/* eslint-disable @next/next/no-img-element */
import { Rate, Switch, Upload, UploadFile, UploadProps } from 'antd'
import React, { Fragment, useEffect, useState } from 'react'
import {
  Control,
  Controller,
  FieldErrorsImpl,
  UseFormSetValue,
  UseFormWatch,
  useForm,
} from 'react-hook-form'
import ErrorMessage from '../ErrorMessage'
import { ButtonMain } from '../Button/ButtonMain/ButtonMain'
import { RcFile } from 'antd/lib/upload'

type FormReviewProps = {
  control: Control<
    {
      nickname: string
      email: string
      ratings: any
      text: string
      anonymous: boolean
      images: never[]
      sku: string
      summary: string
    },
    any
  >
  setValue: UseFormSetValue<{
    nickname: string
    email: string
    ratings: any
    text: string
    anonymous: boolean
    images: never[]
    sku: string
    summary: string
  }>
  watch: UseFormWatch<{
    nickname: string
    email: string
    ratings: any
    text: string
    anonymous: boolean
    images: never[]
    sku: string
    summary: string
  }>
  errors: FieldErrorsImpl<{
    nickname: string
    email: string
    ratings: number
    text: string
    anonymous: boolean
    images: never[]
    sku: string
    summary: string
  }>
  dataRatingProduct?: TReviewRating[]
  isLoadingCreateProductReview?: boolean
  fileList: UploadFile[]
  setFileList: (fileList: UploadFile[]) => void
  setIsHideName: (bool: boolean) => void
  dataReviews?: any
}
const FormReview = (props: FormReviewProps) => {
  const {
    control,
    watch,
    errors,
    dataRatingProduct,
    isLoadingCreateProductReview,
    fileList,
    setFileList,
    setIsHideName,
    dataReviews,
  } = props
  const handleBeforeUpload = async (file: any) => {
    const reader = new FileReader()
    // Đọc hình ảnh thành Base64
    reader.readAsDataURL(file)
    reader.onload = () => {
      const base64Data: any = reader.result
      const newFile: any = {
        uid: file?.uid,
        name: file?.name,
        thumbUrl: base64Data,
      }
      setFileList([...fileList, newFile])
    }

    return false // Không tải lên mặc định của Ant Design
  }

  return (
    <div className="wrapper-form">
      <div className={'emptyContainer'}>
        {dataReviews?.length == 0 && (
          <div className={'empty'}>
            <img src="/icons/empty-review.svg" alt="empty" />
            <h3>Chưa có đánh giá</h3>
            <p>Hiện chưa có đánh giá nào cho sản phẩm này.</p>
          </div>
        )}

        <div className={'flexItem'}>
          <h3>Đánh giá sản phẩm</h3>
          <p>Hãy chia sẻ những điều bạn nghĩ về sản phẩm này với những người mua khác nhé.</p>
        </div>
        <div className="wrapper-rating">
          {dataRatingProduct?.map((item: TReviewRating) => {
            return (
              <div className={'qualify'} key={`${item?.id}`}>
                <p className="min-w-[80px]">{item?.name}</p>
                <Controller
                  name={`ratings.${item?.id}`}
                  control={control}
                  render={({ field }) => (
                    <Rate {...field} defaultValue={watch('ratings')} className={'rating_star'} />
                  )}
                />
              </div>
            )
          })}
        </div>

        <div className={'reviewForm'}>
          <div className="flex-1">
            <div className={'formItem'}>
              <p>
                Họ và tên <span>*</span>
              </p>
              <Controller
                name="nickname"
                control={control}
                rules={{
                  required: true,
                }}
                render={({ field }) => (
                  <input
                    {...field}
                    className={'formInput'}
                    type="text"
                    name=""
                    id=""
                    placeholder="Nhập tên của bạn"
                  />
                )}
              />

              <div className={'formUnderline'}></div>
            </div>
            {errors?.nickname ? <ErrorMessage>Vui lòng không để trống</ErrorMessage> : <></>}
          </div>
        </div>

        <div className={'reviewForm'}>
          <div className="flex-1">
            <div className={'formItem'}>
              <p>
                Tiêu đề <span>*</span>
              </p>
              <Controller
                name="summary"
                control={control}
                rules={{
                  required: true,
                }}
                render={({ field }) => (
                  <input
                    {...field}
                    className={'formInput'}
                    type="text"
                    name=""
                    id=""
                    placeholder="Nhập tên của bạn"
                  />
                )}
              />

              <div className={'formUnderline'}></div>
            </div>
            {errors?.summary ? <ErrorMessage>Vui lòng không để trống</ErrorMessage> : <></>}
          </div>
        </div>
        <div className={'writeReview'}>
          <div className={'formItem'}>
            <p>
              Đánh giá của bạn <span>*</span>
            </p>
            <Controller
              name="text"
              control={control}
              render={({ field }) => (
                <textarea {...field} name="" id="" placeholder="Viết nội dung đánh giá"></textarea>
              )}
            />

            <div className={'formUnderline'}></div>
          </div>
          {errors?.text ? <ErrorMessage>Vui lòng không để trống</ErrorMessage> : <></>}
        </div>
        <div className="my-3">
          <div className="w-full flex flex-wrap gap-3 items-center">
            {fileList?.map((picture, index) => {
              const handleDelete = (e: any) => {
                e.stopPropagation()
                const newFileList = fileList?.filter((item) => item?.uid !== picture?.uid)
                setFileList(newFileList)
              }
              return (
                <Fragment key={index}>
                  <div className="item-picture relative">
                    <button onClick={() => !!window && window.open(`${picture?.thumbUrl}`)}>
                      <img
                        src={picture?.thumbUrl}
                        alt="Ảnh"
                        width={100}
                        height={100}
                        className="rounded-3xl w-[100px] h-[100px]"
                      />
                    </button>
                    <button
                      onClick={handleDelete}
                      className="absolute right-2 hover:opacity-90 opacity-30 text-red"
                    >
                      <i className="fas fa-times-circle"></i>
                    </button>
                  </div>
                </Fragment>
              )
            })}
          </div>
        </div>
        <div className={'actionReview'}>
          <div className={'btnContainer'}>
            <Upload beforeUpload={handleBeforeUpload} showUploadList={false} maxCount={5}>
              <button className={'upload'} type="button">
                <img src="/icons/upload-image.svg" alt="" />
                <p>Thêm hình ảnh/video</p>
              </button>
            </Upload>
          </div>

          <div className="sm:ml-[50px] flex items-center gap-[10px] h-[44px]">
            <Switch onChange={(data) => setIsHideName(data)} className="switch-anonymous" />
            <span>Ẩn danh</span>
          </div>

          <ButtonMain
            background="green"
            loading={isLoadingCreateProductReview}
            className={'sendReview'}
            type="submit"
          >
            <img src="/icons/send-btn.svg" alt="" />
            <p>Gửi ngay</p>
          </ButtonMain>
        </div>
      </div>
    </div>
  )
}

export default FormReview
