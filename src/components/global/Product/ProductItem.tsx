/* eslint-disable @next/next/no-img-element */
import Link from 'next/link'
import { NextRouter } from 'next/router'
import { useMutation, useQueryClient } from 'react-query'
import { toast } from 'react-toastify'
import { graphQLClient } from '~src/api/graphql'
import { ADD_PRODUCT_TO_CART } from '~src/api/product'
import { _format } from '~src/util'
import { ButtonMain } from '../Button/ButtonMain/ButtonMain'
import { useEffect, useState } from 'react'

type ProductItemProps = {
  product?: TProduct & { sale_price: number }
  router: NextRouter
  position?: any
  isPermission?: boolean
}
const ProductItem = (props: ProductItemProps) => {
  const { product } = props
  const queryClient = useQueryClient()

  const { isLoading: isLoadingDataAddToCart, mutate: mutateAddToCart } = useMutation({
    mutationFn: async (data: TAddToCart[]) => {
      return (await graphQLClient.request(ADD_PRODUCT_TO_CART, {
        cartId: localStorage?.getItem('cartId'),
        cartItems: data,
      })) as { addProductsToCart: TDataAddToCart }
    },
    onSuccess: async (response: { addProductsToCart: TDataAddToCart }) => {
      await queryClient.refetchQueries(['getCartId'])
      if (!!response?.addProductsToCart?.user_errors?.length) {
        toast.error('Sản phẩm đã hết hàng')
      } else {
        queryClient.setQueryData<boolean>('miniCartVisible', true)
      }
    },
  })

  const handleAddToCart = (event: any) => {
    event?.stopPropagation()
    const DATA_SUBMIT: TAddToCart[] = [
      {
        sku: product?.sku || '',
        quantity: 1,
      },
    ]
    mutateAddToCart(DATA_SUBMIT)
  }

  return (
    <div className="product-item">
      <Link
        href={{
          pathname: `/product-detail/Hãng xe/${product?.name}`,
          query: {
            sku: product?.sku,
          },
        }}

        // onClick={handleDetailProduct}
      >
        <div className="inner-item relative">
          <div className={`image ${product?.stock_status === 'IN_STOCK' ? '' : 'out-of-stock'}`}>
            {!!product?.daily_sale &&
            !!product?.daily_sale?.end_date &&
            !!product?.daily_sale?.sale_price &&
            product?.daily_sale?.sold_qty !== undefined &&
            product?.daily_sale?.sale_qty !== undefined &&
            new Date(product?.daily_sale?.end_date) >= new Date() &&
            product?.daily_sale?.sold_qty < product?.daily_sale?.sale_qty ? (
              <div className="startTenPoint">
                <p className="flex justify-center text-white z-50">
                  -
                  {Math.floor(
                    100 -
                      ((product?.daily_sale?.sale_price || 0) /
                        (product?.price_range?.maximum_price?.regular_price?.value || 0)) *
                        100
                  )}
                  %
                </p>
              </div>
            ) : (
              ''
            )}
            {props.position && <div className="product-position">{props.position}</div>}

            <img src={product?.image?.url || product?.img} alt="" />
            {!!product?.price_tiers?.discount?.percent_off ? (
              <div className="hot">
                <div className="bg">
                  <img src="/icons/Star-red.svg" alt="" />
                </div>
                <div className="text">-{product?.price_tiers?.discount?.percent_off}%</div>
              </div>
            ) : (
              <></>
            )}
            {!props.isPermission && (
              <ButtonMain
                type="button"
                background="green"
                className="cart-icon"
                onClick={handleAddToCart}
                loading={isLoadingDataAddToCart}
              >
                <img src="/icons/shopping-cart.svg" alt="" />
              </ButtonMain>
            )}
          </div>
          <div className="text-inner">
            <div className="name">{product?.name}</div>
            <div className="price">
              <div className="price-new">
                {!!product?.daily_sale &&
                !!product?.daily_sale?.end_date &&
                !!product?.daily_sale?.sale_price &&
                product?.daily_sale?.sold_qty !== undefined &&
                product?.daily_sale?.sale_qty !== undefined &&
                new Date(product?.daily_sale?.end_date) >= new Date() &&
                product?.daily_sale?.sold_qty < product?.daily_sale?.sale_qty
                  ? _format.getVND(product?.daily_sale?.sale_price)
                  : !!product?.sale_price
                  ? _format.getVND(product?.sale_price)
                  : _format.getVND(
                      product?.price_range?.minimum_price?.final_price?.value || 0
                    )}{' '}
                {product?.price_range?.minimum_price?.final_price?.currency || 'VND'}
              </div>
              {product?.daily_sale && (
                <div className="price-old">
                  {_format.getVND(product?.price_range?.maximum_price?.regular_price?.value || 0)}{' '}
                  {product?.price_range?.minimum_price?.final_price?.currency || 'VND'}
                </div>
              )}
            </div>
          </div>
        </div>
      </Link>
    </div>
  )
}

export default ProductItem
