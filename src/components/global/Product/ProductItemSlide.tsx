/* eslint-disable @next/next/no-img-element */
import Link from 'next/link'
import { NextRouter } from 'next/router'
import React from 'react'
import { useMutation, useQueryClient } from 'react-query'
import { toast } from 'react-toastify'
import { graphQLClient } from '~src/api/graphql'
import { ADD_PRODUCT_TO_CART } from '~src/api/product'
import { _format } from '~src/util'
import { ButtonMain } from '../Button/ButtonMain/ButtonMain'

type ProductItemSlideProps = {
  product?: TProduct
  router: NextRouter
}
const ProductItemSlide = (props: ProductItemSlideProps) => {
  const { product, router } = props
  const queryClient = useQueryClient()

  const { isLoading: isLoadingDataAddToCart, mutate: mutateAddToCart } = useMutation({
    mutationFn: async (data: TAddToCart[]) => {
      return (await graphQLClient.request(ADD_PRODUCT_TO_CART, {
        cartId: localStorage?.getItem('cartId'),
        cartItems: data,
      })) as { addProductsToCart: TDataAddToCart }
    },
    onSuccess: async (response: { addProductsToCart: TDataAddToCart }) => {
      await queryClient.refetchQueries(['getCartId'])
      if (!!response?.addProductsToCart?.user_errors?.length) {
        toast.error('Sản phẩm đã hết hàng')
      } else {
        queryClient.setQueryData<boolean>('miniCartVisible', true)
      }
    },
  })

  const handleAddToCart = (event: any) => {
    event?.stopPropagation()
    const DATA_SUBMIT: TAddToCart[] = [
      {
        sku: product?.sku || '',
        quantity: 1,
      },
    ]
    mutateAddToCart(DATA_SUBMIT)
  }

  return (
    <div>
      <Link
        href={{
          pathname: `/product-detail/Hãng xe/${product?.name}`,
          query: {
            sku: product?.sku,
          },
        }}
      >
        <div className="inner-item">
          <div className={`image ${product?.stock_status === 'IN_STOCK' ? '' : 'out-of-stock'}`}>
            <img src={product?.image?.url} alt="" />
            {!!product?.daily_sale && (
              <div className="hot">
                <div className="bg">
                  <img src="/icons/Star-red.svg" alt="" />
                </div>
                <div className="text">
                  {
                    -parseInt(
                      ((((product?.price_range?.maximum_price?.regular_price?.value as number) -
                        (product?.daily_sale?.sale_price as number)) *
                        100) /
                        (product?.price_range?.maximum_price?.regular_price
                          ?.value as number)) as any
                    )
                  }
                  %
                </div>
              </div>
            )}

            {/* <div className="cart-icon" onClick={handleAddToCart}>
              <img src="/icons/shopping-cart.svg" alt="" />
            </div> */}

            <ButtonMain
              type="button"
              background="green"
              className="cart-icon"
              onClick={handleAddToCart}
              loading={isLoadingDataAddToCart}
            >
              <img src="/icons/shopping-cart.svg" alt="" />
            </ButtonMain>
          </div>
          <div className="text-inner">
            <div className="name">{product?.name}</div>
            <div className="price">
              <div className="price-new">
                {!!product?.daily_sale &&
                !!product?.daily_sale?.end_date &&
                !!product?.daily_sale?.sale_price &&
                product?.daily_sale?.sold_qty != undefined &&
                product?.daily_sale?.sale_qty != undefined &&
                new Date(product?.daily_sale?.end_date) >= new Date() &&
                product?.daily_sale?.sold_qty < product?.daily_sale?.sale_qty
                  ? _format.getVND(
                      product?.daily_sale?.sale_price,
                      product?.daily_sale?.currency || 'VNĐ'
                    )
                  : _format.getVND(
                      product?.price_range?.minimum_price?.final_price?.value || 0,
                      product?.price_range?.minimum_price?.final_price?.currency || ' VNĐ'
                    )}
              </div>
              <div className="price-old">
                {_format.getVND(product?.price_range?.maximum_price?.regular_price?.value || 0)}{' '}
                {product?.price_range?.maximum_price?.regular_price?.currency}
              </div>
            </div>
          </div>
        </div>
      </Link>
    </div>
  )
}

export default ProductItemSlide
