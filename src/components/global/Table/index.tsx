/* eslint-disable react/no-children-prop */
import { Table, TablePaginationConfig, TableProps } from "antd";
import { SorterResult, TableRowSelection } from "antd/lib/table/interface";
import clsx from "clsx";
import React, { ReactNode } from "react";
import { useMediaQuery } from "react-responsive";
import { TColumnsType } from "~src/types/table";
import styles from "./index.module.css";

type TProps<T extends object> = {
  rowKey?: keyof T | "id";
  style?: "main" | "secondary";
  title?: string;
  columns: TColumnsType<T>;
  data: T[];
  bordered?: boolean;
  pagination?: TablePaginationConfig | false;
  onChange?: (
    pagination: TablePaginationConfig,
    filter: any,
    sorter: SorterResult<T> | SorterResult<T>[]
  ) => void;
  summary?: (data: readonly T[]) => React.ReactNode | null;
  rowSelection?: TableRowSelection<T>;
  scroll?: TableProps<T>["scroll"];
  loading?: boolean;
  expandable?: any;
  onRow?: (data?: any, e?: any) => void;
  ScrollX?: any;
  footer?: any;
  expandRowByClick?: boolean;
  defaultExpandAllRows?: boolean;
  ScrollY?: any;
  children?: ReactNode;
};

export const DataTable = <T extends object = object>({
  style = "main",
  title = "",
  columns,
  data,
  bordered = undefined,
  pagination = false,
  onChange,
  rowSelection,
  summary = undefined,
  scroll = { x: true },
  rowKey = "id",
  loading = false,
  expandable,
  onRow,
  ScrollX = 0,
  ScrollY = 0,
  footer,
  expandRowByClick = false,
  defaultExpandAllRows = false,
  children,
}: TProps<T>) => {
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 1824px)" });

  return (
    <React.Fragment>
      {!!title.length && (
        <p className={clsx("titleTable", style === "secondary" && "")}>
          {title}
        </p>
      )}
      <Table
        loading={loading}
        rowKey={rowKey as string}
        bordered={bordered}
        columns={columns}
        dataSource={data}
        className={clsx(style !== "main" ? styles.table : styles.maintable)}
        pagination={pagination}
        summary={summary}
        onChange={onChange}
        rowSelection={rowSelection}
        footer={footer}
        scroll={{
          x: ScrollX == 0 ? true : ScrollX,
          y:
            ScrollY == 0
              ? `${!!isTabletOrMobile ? "500px" : "720px"}`
              : ScrollY,
        }}
        expandable={expandable}
        expandRowByClick={expandRowByClick}
        defaultExpandAllRows={defaultExpandAllRows}
        onRow={(record, index) => {
          return {
            onClick: (e) => {
              e.preventDefault();
              onRow?.(record, e);
            },
          };
        }}
      ></Table>

      {pagination && data?.length > 0 && (
        <div className="h-0">
          <div className="cursor-pointer relative sm:bottom-12 bg-[#edf0f3] text-[#294f81] font-semibold flex justify-center items-center gap-2 w-fit h-[32px] px-3 mx-3 text-sm rounded">
            <span>Tổng: </span>
            <span>{pagination?.total}</span>
          </div>
        </div>
      )}
    </React.Fragment>
  );
};
