/* eslint-disable @next/next/no-img-element */
import React from 'react'
import { useMutation, useQueryClient } from 'react-query'
import { toast } from 'react-toastify'
import { graphQLClient } from '~src/api/graphql'
import { ADD_PRODUCT_TO_CART } from '~src/api/product'

type Props = {
  product?: TProduct & { sale_price: number }
}

export const ButtonAddCartSecond: React.FC<Props> = ({ product }) => {
  const queryClient = useQueryClient()

  const { isLoading: isLoadingDataAddToCart, mutate: mutateAddToCart } = useMutation({
    mutationFn: async (data: TAddToCart[]) => {
      return (await graphQLClient.request(ADD_PRODUCT_TO_CART, {
        cartId: localStorage?.getItem('cartId'),
        cartItems: data,
      })) as { addProductsToCart: TDataAddToCart }
    },
    onSuccess: async (response: { addProductsToCart: TDataAddToCart }) => {
      await queryClient.refetchQueries(['getCartId'])
      if (!!response?.addProductsToCart?.user_errors?.length) {
        toast.error('Sản phẩm đã hết hàng')
      } else {
        queryClient.setQueryData<boolean>('miniCartVisible', true)
      }
    },
  })

  const handleAddToCart = (event: any) => {
    event?.stopPropagation()
    const DATA_SUBMIT: TAddToCart[] = [
      {
        sku: product?.sku || '',
        quantity: 1,
      },
    ]
    mutateAddToCart(DATA_SUBMIT)
  }
  return isLoadingDataAddToCart ? (
    <div className="btn-add-cart-second">
      <div className="spinner left-1/2 top-1/2 absolute">
        <i className="spinner-icon fad fa-spinner"></i>
      </div>
    </div>
  ) : (
    <div className="btn-add-cart-second" onClick={handleAddToCart}>
      <img src="/icons/shopping-cart.svg" alt="" />
    </div>
  )
}
