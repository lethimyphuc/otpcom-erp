/* eslint-disable @next/next/no-img-element */
import { Badge } from 'antd'
import { useQuery, useQueryClient } from 'react-query'
import { GET_CART } from '~src/api/cart'
import { graphQLClient } from '~src/api/graphql'

export const CartCount = () => {
  const queryClient = useQueryClient()
  const { data: dataCart } = useQuery({
    queryKey: ['getCartId'],
    queryFn: async () => {
      if (!localStorage.getItem('cartId')) return
      const data = (await graphQLClient.request(GET_CART, {
        cart_id: localStorage.getItem('cartId'),
      })) as TDataUpdateToCart
      return data
    },
    cacheTime: Infinity,
    enabled: true,
  })

  return (
    <button
      className="flex"
      onClick={() => queryClient.setQueryData<boolean>('miniCartVisible', (visible) => !visible)}
    >
      <Badge
        color="#c82127"
        count={dataCart?.cart?.total_quantity || 0}
        size="small"
        overflowCount={9}
      >
        <div className="img min-w-[12px]">
          <img src={'/image/svg/shopping-cart.svg'} alt="" />
        </div>
      </Badge>
    </button>
  )
}
