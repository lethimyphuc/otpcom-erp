/* eslint-disable react-hooks/exhaustive-deps */
/**
 * /* eslint-disable react-hooks/exhaustive-deps
 *
 * @format
 */

/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useMemo } from 'react'
import { useForm } from 'react-hook-form'
import { useQuery } from 'react-query'
import {graphQLClient, graphQLClientGET} from '~src/api/graphql'
import { GET_CATEGORIES, GET_CATEGORIE_DETAIL } from '~src/api/categories'
import { DefaultOptionType } from 'antd/lib/select'
import { NextRouter } from 'next/router'
import { FormSelect } from '../global/FormControls/FormSelect'
import { ButtonMain } from '../global/Button/ButtonMain/ButtonMain'

type ProductFilterProps = {
  router: NextRouter
  filterData: any
  banner?: any
}
export const FormFilterProduct = (props: ProductFilterProps) => {
  const { router, banner } = props
  const { control, handleSubmit, setValue, watch } = useForm<BoxSearchForm>({
    defaultValues: {
      type: null,
      company: null,
      end: null,
      groupAccessary: null,
      sort: null,
      tag: null,
    },
  })
  const slugs = useMemo(() => router.query.slug!, [router])
  const carCompanySlug = useMemo<string | null>(() => {
    return (slugs || [])[0] ?? null
  }, [slugs])

  const carTypeSlug = useMemo<string | null>(() => {
    return (slugs || [])[1] ?? null
  }, [slugs])

  const carEndSlug = useMemo<string | null>(() => {
    return (slugs || [])[2] ?? null
  }, [slugs])
  const carTagSlug = useMemo<string | null>(() => {
    return (slugs || [])[3] ?? null
  }, [slugs])

  // Danh sách Hãng xe
  const {
    data: dataCategoriesByCompany,
    isLoading: isLoadingDataCategoriesByCompany,
  }: {
    data?: TChildrenCategories[]
    isLoading?: boolean
  } = useQuery({
    queryKey: ['getCategoriesByCompany'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_CATEGORIES, {
        eq: 'Ng==',
      })) as TCategories
      return data.categories?.items[0]?.children
    },
  })

  // Danh sách Dòng xe
  const {
    data: dataCategoriesByType,
    isLoading: isLoadingDataCategoriesByType,
  }: {
    data?: TChildrenCategories[]
    isLoading?: boolean
  } = useQuery({
    queryKey: ['getCategoriesByType', { eq: watch('company') }],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_CATEGORIES, {
        eq: `${watch('company')}`,
      })) as TCategories
      return data.categories?.items[0]?.children
    },
    enabled: !!watch('company'),
  })

  // Danh sách Đời xe
  const {
    data: dataCategoriesByEnd,
    isLoading: isLoadingDataCategoriesByEnd,
  }: {
    data?: TChildrenCategories[]
    isLoading?: boolean
  } = useQuery({
    queryKey: ['getCategoriesByEnd', { eq: watch('type') }],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_CATEGORIES, {
        eq: `${watch('type')}`,
      })) as TCategories
      return data.categories?.items[0]?.children
    },
    enabled: !!watch('type'),
  })

  // Danh sách Tag
  const {
    data: dataSpareTags,
    isLoading: isLoadingDataSpareTags,
  }: {
    data?: TChildrenCategories[]
    isLoading?: boolean
  } = useQuery({
    queryKey: ['getCategoriesByTags', { eq: watch('end') }],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_CATEGORIE_DETAIL, {
        eq: `${watch('end')}`,
      })) as TCategories
      return data.categories?.items[0]?.tags
    },
    enabled: !!watch('end'),
  })
  const handleSelectCategoryCompany = (
    value: any,
    option?: DefaultOptionType | DefaultOptionType[] | undefined
  ) => {
    setValue('company', value)
    setValue('type', null)
    setValue('end', null)
    setValue('tag', null)
  }

  const handleSelectCategoryType = (
    value: any,
    option?: DefaultOptionType | DefaultOptionType[] | undefined
  ) => {
    setValue('type', value)
    setValue('end', null)
    setValue('tag', null)
  }
  const handleSelectCategoryEnd = (
    value: any,
    option?: DefaultOptionType | DefaultOptionType[] | undefined
  ) => {
    setValue('end', value)
    setValue('tag', null)
  }
  //get uid of category by name
  const carCompanyPickedDataBySlug = useMemo<TChildrenCategories | null>(() => {
    return (dataCategoriesByCompany || []).find((v) => v.url_key === carCompanySlug) ?? null
  }, [dataCategoriesByCompany, carCompanySlug])

  const carTypePickedDataBySlug = useMemo<TChildrenCategories | null>(() => {
    return (dataCategoriesByType || []).find((v) => v.url_key === carTypeSlug) ?? null
  }, [dataCategoriesByType, carTypeSlug])

  const carEndPickedDataBySlug = useMemo<TChildrenCategories | null>(() => {
    return (dataCategoriesByEnd || []).find((v) => v.url_key === carEndSlug) ?? null
  }, [dataCategoriesByEnd, carEndSlug])

  const carTagsPickedDataBySlug = useMemo<any>(() => {
    return (dataSpareTags || [])?.find((v: any) => v.id === carTagSlug) ?? null
  }, [dataSpareTags, carTagSlug])

  const setBanerImage = useMemo<any>(() => {
    if (carEndPickedDataBySlug?.image_banner) {
      return carEndPickedDataBySlug?.image_banner
    }
    if (carTypePickedDataBySlug?.image_banner) {
      return carTypePickedDataBySlug?.image_banner
    }
    if (carCompanyPickedDataBySlug?.image_banner) {
      return carCompanyPickedDataBySlug?.image_banner
    }
  }, [carCompanyPickedDataBySlug, carEndPickedDataBySlug, carTypePickedDataBySlug])
  useEffect(() => {
    banner(setBanerImage)
  }, [setBanerImage])

  // get Name of category
  const carCompanyPickedDataByUID = useMemo<TChildrenCategories | null>(() => {
    return (dataCategoriesByCompany || []).find((v) => v.uid === watch('company')) ?? null
  }, [dataCategoriesByCompany, watch('company')])

  const carTypePickedDataByUID = useMemo<TChildrenCategories | null>(() => {
    return (dataCategoriesByType || []).find((v) => v.uid === watch('type')) ?? null
  }, [dataCategoriesByType, watch('type')])

  const carEndPickedDataByUID = useMemo<TChildrenCategories | null>(() => {
    return (dataCategoriesByEnd || []).find((v) => v.uid === watch('end')) ?? null
  }, [dataCategoriesByEnd, watch('end')])
  const carTagPickedDataByUID = useMemo<any>(() => {
    return (dataSpareTags || []).find((v: any) => v.id === watch('tag')) ?? null
  }, [dataSpareTags, watch('tag')])

  useEffect(() => {
    carCompanyPickedDataBySlug && setValue('company', carCompanyPickedDataBySlug?.uid)
    props.filterData(watch())
  }, [carCompanyPickedDataBySlug])

  useEffect(() => {
    carTypePickedDataBySlug && setValue('type', carTypePickedDataBySlug?.uid)
    props.filterData(watch())
  }, [carTypePickedDataBySlug])
  useEffect(() => {
    carEndPickedDataBySlug && setValue('end', carEndPickedDataBySlug?.uid)
    props.filterData(watch())
  }, [carEndPickedDataBySlug])

  useEffect(() => {
    carTagsPickedDataBySlug && setValue('tag', carTagsPickedDataBySlug?.id)
    props.filterData(watch())
  }, [carTagsPickedDataBySlug])

  const onSubmit = (data: BoxSearchForm) => {
    const DATA_SUBMIT: Record<string, any> = {
      company: !!data?.company ? `${data?.company}`.trim() : null,
      type: !!data?.type ? `${data?.type}`.trim() : null,
      end: !!data?.end ? `${data?.end}`.trim() : null,
      groupAccessary: !!data?.groupAccessary ? `${data?.groupAccessary}`.trim() : null,
      sort: !!data?.sort ? `${data?.sort}`.trim() : null,
      tag: !!data?.tag ? `${data?.tag}`.trim() : null,
    }

    Object.keys(DATA_SUBMIT).forEach((key: string) => {
      if (!DATA_SUBMIT[key]) {
        delete DATA_SUBMIT[key]
      }
    })

    const carCompanyPath = `/${carCompanyPickedDataByUID?.url_key ?? ''}`
    const carTypePath = `/${carTypePickedDataByUID?.url_key ?? ''}`
    const carEndPath = `/${carEndPickedDataByUID?.url_key ?? ''}`
    const carTagPath = `/${carTagPickedDataByUID?.id ?? ''}`
    let finalQuery
    if (!!carTagPickedDataByUID) {
      finalQuery = `${carCompanyPath}/${carTypePath}/${carEndPath}/${carTagPath}`
    } else if (!!carEndPickedDataByUID) {
      finalQuery = `${carCompanyPath}/${carTypePath}/${carEndPath}`
    } else if (!!carTypePickedDataByUID) {
      finalQuery = `${carCompanyPath}/${carTypePath}`
    } else if (!!carCompanyPickedDataByUID) {
      finalQuery = `${carCompanyPath}`
    } else {
      finalQuery = ''
    }
    // console.log(DATA_SUBMIT)
    // props.filterData(DATA_SUBMIT)
    // router.replace(
    //   {
    //     pathname: finalQuery == '' ? '/category' : '/category' + finalQuery,
    //   },
    //   undefined,
    //   { scroll: false }
    // )
  }

  return (
    <div className="form-filter-product">
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="box">
          <div className="items">
            <div className="item">
              <div className="item-label">Sắp xếp theo:</div>
              <FormSelect
                control={control}
                name={'sort'}
                optionList={[
                  { label: 'Tên A -> Z', value: '{"name":"ASC"}' },
                  { label: 'Tên Z -> A', value: '{"name":"DESC"}' },
                  { label: 'Giá tăng dần', value: '{"price":"ASC"}' },
                  { label: 'Giá giảm dần', value: '{"price":"DESC"}' },
                  // { label: "Vị trí tăng dần", value: "ascPosition" },
                  // { label: "Vị trí giảm dần", value: "descPosition" },
                  // { label: "Kết quả tìm kiếm tăng dần", value: "ascRelevance" },
                  // {
                  //   label: "Kết quả tìm kiếm giảm dần",
                  //   value: "descRelevance",
                  // },
                ]}
                label=""
                placeholder="Sắp xếp theo"
                allowClear
              />
            </div>
            <div className="item">
              <div className="item-label">Lọc theo:</div>
              <FormSelect
                control={control}
                name={'company'}
                optionList={dataCategoriesByCompany?.map((company: TChildrenCategories) => {
                  return {
                    label: company?.name,
                    value: company?.uid,
                    name: company?.name,
                  }
                })}
                onChange={handleSelectCategoryCompany}
                label=""
                placeholder="Hãng xe"
                allowClear
              />
            </div>
            <div className="item">
              <FormSelect
                control={control}
                name={'type'}
                optionList={dataCategoriesByType?.map((type: TChildrenCategories) => {
                  return {
                    label: type?.name,
                    value: type?.uid,
                    name: type?.name,
                  }
                })}
                onChange={handleSelectCategoryType}
                label=""
                placeholder="Dòng xe"
                allowClear
              />
            </div>
            <div className="item">
              <div className="item-label"></div>
              <FormSelect
                control={control}
                name={'end'}
                optionList={dataCategoriesByEnd?.map((end: TChildrenCategories) => {
                  return {
                    label: end?.name,
                    value: end?.uid,
                    name: end?.name,
                  }
                })}
                label=""
                onChange={handleSelectCategoryEnd}
                placeholder="Đời xe"
                allowClear
              />
            </div>
          </div>
          <div className="button-search h-[45px]">
            <ButtonMain background="green" type="button" onClick={handleSubmit(onSubmit)}>
              <div className="flex items-center gap-[0.4rem]">
                <div className="icon">
                  <img src="/icons/search-icon.svg" alt="" />
                </div>
                <div className="text">Tìm kiếm</div>
              </div>
            </ButtonMain>
          </div>
        </div>
        {dataSpareTags && (
          <div className="box-tag">
            {dataSpareTags?.map((item: any) => {
              return (
                <div
                  className={`items-tag ${watch('tag') === item.id && 'active-tag'}`}
                  key={item?.id}
                  onClick={() => {
                    setValue('tag', item.id)
                    handleSubmit(onSubmit)()
                  }}
                >
                  <div className="item-tag">
                    <p>{item.name}</p>
                  </div>
                </div>
              )
            })}
          </div>
        )}
      </form>
    </div>
  )
}
