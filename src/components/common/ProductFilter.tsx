/* eslint-disable @next/next/no-img-element */
import { yupResolver } from '@hookform/resolvers/yup'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useQuery } from 'react-query'
import * as Yup from 'yup'
import { GET_CATEGORIES, GET_CATEGORIE_DETAIL } from '~src/api/categories'
import { graphQLClientGET } from '~src/api/graphql'
import { removeEmptyValueFromObject } from '~src/util'
import { ButtonMain } from '../global/Button/ButtonMain/ButtonMain'
import { FormSelect } from '../global/FormControls/FormSelect'

type ProductFilterProps = {
  value?: ProductFilterReq
  defaultValues?: ProductFilterReq
  onChangeBanner?: (url: string | undefined) => void
  onChangeImageCar?: (url: string | undefined) => void
  onChange?: (value: ProductFilterReq) => void
}

export const ProductFilter = ({
  value,
  onChange,
  onChangeBanner,
  onChangeImageCar,
  defaultValues,
}: ProductFilterProps) => {
  const router = useRouter()
  const [submitted, setSubmitted] = useState<boolean>(false)
  const { watch, handleSubmit, setValue, control } = useForm<ProductFilterReq>({
    mode: 'all',
    resolver: yupResolver(schema),
    defaultValues: defaultValues || {
      type: null,
      company: null,
      end: null,
      groupAccessary: null,
      sort: null,
      tag: null,
    },
  })

  // Danh sách Hãng xe
  const {
    data: dataCategoriesByCompany,
    isLoading: isLoadingDataCategoriesByCompany,
  }: {
    data?: TChildrenCategories[]
    isLoading?: boolean
  } = useQuery({
    queryKey: ['getCategoriesByCompany'],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_CATEGORIES, {
        eq: 'Ng==',
      })) as TCategories
      return data.categories?.items[0]?.children
    },
  })
  // Danh sách Dòng xe
  const {
    data: dataCategoriesByType,
    isLoading: isLoadingDataCategoriesByType,
  }: {
    data?: TChildrenCategories[]
    isLoading?: boolean
  } = useQuery({
    queryKey: ['getCategoriesByType', { eq: watch('company') }],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_CATEGORIES, {
        eq: `${watch('company')}`,
      })) as TCategories
      return data.categories?.items[0]?.children
    },
    enabled: !!watch('company'),
  })

  // Danh sách Đời xe
  const {
    data: dataCategoriesByEnd,
    isLoading: isLoadingDataCategoriesByEnd,
  }: {
    data?: TChildrenCategories[]
    isLoading?: boolean
  } = useQuery({
    queryKey: ['getCategoriesByEnd', { eq: watch('type') }],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_CATEGORIES, {
        eq: `${watch('type')}`,
      })) as TCategories
      return data.categories?.items[0]?.children
    },
    enabled: !!watch('type'),
  })

  // Danh sách Tag
  const {
    data: tags,
    isLoading: tagsLoading,
  }: {
    data?: TagRes[]
    isLoading?: boolean
  } = useQuery({
    queryKey: ['getCategoriesByTags', { eq: watch('end') }],
    queryFn: async () => {
      const data = (await graphQLClientGET.request(GET_CATEGORIE_DETAIL, {
        eq: `${watch('end')}`,
      })) as TCategories
      return data.categories?.items[0]?.tags
    },
    enabled: submitted && !!watch('end'),
  })

  useEffect(() => {
    if (router.query?.submitted) {
      setSubmitted(true)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [router.query?.submitted])

  useEffect(() => {
    const {
      company = null,
      end = null,
      groupAccessary = null,
      sort = null,
      tag = null,
      type = null,
    } = value || {}
    setValue('company', company)
    setValue('end', end)
    setValue('sort', sort)
    setValue('tag', tag)
    setValue('type', type)
    setValue('groupAccessary', groupAccessary)
    if (tag) {
      setSubmitted(true)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value])

  useEffect(() => {
    const { type = null, end = null, company = null } = value || {}
    if (
      !isLoadingDataCategoriesByEnd &&
      !isLoadingDataCategoriesByType &&
      !isLoadingDataCategoriesByCompany
    ) {
      //kiểm tra xem có banner của end không rồi mới kiểm tra của type
      if (dataCategoriesByEnd?.find?.((item) => item.uid === end)?.image_banner) {
        onChangeBanner?.(dataCategoriesByEnd?.find?.((item) => item.uid === end)?.image_banner)
      } else if (dataCategoriesByType?.find?.((item) => item.uid === type)?.image_banner) {
        onChangeBanner?.(dataCategoriesByType?.find?.((item) => item.uid === type)?.image_banner)
      } else if (dataCategoriesByCompany?.find?.((item) => item.uid === company)?.image_banner) {
        onChangeBanner?.(
          dataCategoriesByCompany?.find?.((item) => item.uid === company)?.image_banner
        )
      } else {
        onChangeBanner?.(undefined)
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dataCategoriesByType, dataCategoriesByCompany, dataCategoriesByEnd, value, onChangeBanner])

  useEffect(() => {
    const { type = null, end = null, company = null } = value || {}
    if (dataCategoriesByEnd?.find?.((item) => item.uid === end)?.image_car) {
      return onChangeImageCar?.(dataCategoriesByEnd?.find?.((item) => item.uid === end)?.image_car)
    }
    if (dataCategoriesByType?.find?.((item) => item.uid === type)?.image_car) {
      return onChangeImageCar?.(
        dataCategoriesByType?.find?.((item) => item.uid === type)?.image_car
      )
    }
    if (dataCategoriesByCompany?.find?.((item) => item.uid === company)?.image_car) {
      return onChangeImageCar?.(
        dataCategoriesByCompany?.find?.((item) => item.uid === company)?.image_car
      )
    }
    onChangeImageCar?.(undefined)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dataCategoriesByEnd, value])

  const onSubmitHandler = handleSubmit((data) => {
    setSubmitted(true)
    onChange?.(removeEmptyValueFromObject(data))
  })
  return (
    <div className="form-filter-product">
      <form onSubmit={onSubmitHandler}>
        <div
          className={`box flex justify-center ${
            submitted && tags ? '!items-center' : '!items-end'
          }`}
        >
          <div className="w-full">
            <div className="items">
              <div className="item">
                <div className="item-label">Sắp xếp theo:</div>
                <FormSelect
                  allowClear
                  name={'sort'}
                  control={control}
                  placeholder="Sắp xếp theo"
                  onChange={() => setSubmitted(false)}
                  optionList={[
                    { label: 'Tên A -> Z', value: '{"name":"ASC"}' },
                    { label: 'Tên Z -> A', value: '{"name":"DESC"}' },
                    { label: 'Giá tăng dần', value: '{"price":"ASC"}' },
                    { label: 'Giá giảm dần', value: '{"price":"DESC"}' },
                  ]}
                />
              </div>
              <div className="item">
                <div className="item-label">Lọc theo:</div>
                <FormSelect
                  allowClear
                  control={control}
                  name={'company'}
                  placeholder="Hãng xe"
                  onChange={() => {
                    setValue('type', null)
                    setValue('end', null)
                    setValue('tag', null)
                    setSubmitted(false)
                  }}
                  optionList={dataCategoriesByCompany?.map((company) => ({
                    label: company?.name,
                    value: company?.uid,
                    name: company?.name,
                  }))}
                />
              </div>
              <div className="item">
                <FormSelect
                  label=""
                  allowClear
                  name={'type'}
                  control={control}
                  placeholder="Dòng xe"
                  onChange={() => {
                    setValue('end', null)
                    setValue('tag', null)
                    setSubmitted(false)
                  }}
                  optionList={dataCategoriesByType?.map((type) => ({
                    label: type?.name,
                    value: type?.uid,
                    name: type?.name,
                  }))}
                />
              </div>
              <div className="item">
                <div className="item-label"></div>
                <FormSelect
                  allowClear
                  name={'end'}
                  control={control}
                  placeholder="Đời xe"
                  onChange={() => {
                    setValue('tag', null)
                    setSubmitted(false)
                  }}
                  optionList={dataCategoriesByEnd?.map((end) => ({
                    label: end?.name,
                    value: end?.uid,
                    name: end?.name,
                  }))}
                />
              </div>
            </div>
            {submitted && tags?.length ? (
              <div className="box-tag">
                {tags?.map((item) => {
                  return (
                    <Controller
                      key={item.id}
                      control={control}
                      name="tag"
                      render={({ field: { onChange, value } }) => (
                        <div
                          className={`items-tag select-none ${value === item.id && 'active-tag'}`}
                          onClick={() => {
                            onChange(item?.id === value ? null : item.id)
                            onSubmitHandler()
                          }}
                        >
                          <div className="item-tag">
                            <p className="!mb-0">
                              {item.display_name ? item.display_name : item.name}
                            </p>
                          </div>
                        </div>
                      )}
                    />
                  )
                })}
              </div>
            ) : null}
          </div>
          <div
            className={`h-full flex w-full xl:w-fit ${
              submitted && tags?.length ? ' !mb-20' : 'lg:!-mb-0 md:!items-end'
            }`}
          >
            <div className="button-search h-[45px]">
              <ButtonMain background="green" type="button" onClick={onSubmitHandler}>
                <div className="flex items-center gap-[0.4rem]">
                  <div className="icon">
                    <img src="/icons/search-icon.svg" alt="" />
                  </div>
                  <div className="text">Tìm kiếm</div>
                </div>
              </ButtonMain>
            </div>
          </div>

          {/* 
            <div className="box-tag">
              {tags?.map((item) => {
                return (
                  <Controller
                    key={item.id}
                    control={control}
                    name="tag"
                    render={({ field: { onChange, value } }) => (
                      <div
                        className={`items-tag select-none ${value === item.id && 'active-tag'}`}
                        onClick={() => {
                          onChange(item?.id === value ? null : item.id)
                          onSubmitHandler()
                        }}
                      >
                        <div className="item-tag">
                          <p>{item.name}</p>
                        </div>
                      </div>
                    )}
                  />
                )
              })}
            </div>
          */}
        </div>
      </form>
    </div>
  )
}

const schema: Yup.ObjectSchema<ProductFilterReq> = Yup.object().shape({
  type: Yup.string().nullable(),
  company: Yup.string().nullable(),
  end: Yup.string().nullable(),
  groupAccessary: Yup.string().nullable(),
  sort: Yup.string().nullable(),
  tag: Yup.string().nullable(),
  search: Yup.string().nullable(),
})
