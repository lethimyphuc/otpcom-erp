/* eslint-disable @next/next/no-img-element */
import { Pagination } from 'antd'
import React from 'react'

type I = {
  current: number
  total: number
  pageSize: number
  onChange: any
  totalPage?: number
}
export const PaginationCustom: React.FC<I> = ({
  current,
  total,
  pageSize,
  onChange,
  totalPage = 1,
}) => {
  if (totalPage <= 1) return null
  return (
    <div className="flex items-center">
      <button className="button-pagination first-page" type="button" onClick={() => onChange(1)}>
        <img src={'/icons/first.svg'} alt="" />
      </button>
      <Pagination
        current={current}
        total={total}
        pageSize={pageSize}
        onChange={(val) => onChange(val)}
        showSizeChanger={false}
      />
      <button
        className="button-pagination last-page"
        type="button"
        onClick={() => onChange(totalPage)}
      >
        <img src={'/icons/last.svg'} alt="" />
      </button>
    </div>
  )
}
