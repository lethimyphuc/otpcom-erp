/* eslint-disable @next/next/no-img-element */
import { Skeleton } from 'antd'
import React from 'react'

type Props = {}

export const ProductSkeleton: React.FC<Props> = ({}) => {
  return (
    <div className={`pro-wr pro-skeleton`}>
      <div className="pro-img">
        <div className="inner">
          <Skeleton.Image active />
        </div>
      </div>
      <div className="pro-content">
        <Skeleton active />
      </div>
    </div>
  )
}
