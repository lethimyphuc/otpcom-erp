type TPost = {
  created_at: string
  enabled: number
  image: string
  name: string
  post_content: string
  author_name: string
  post_id: number
  publish_date: string
  short_description: string
  store_ids: string
  updated_at: string
  url_key: string
  categories?: {
    items: {
      category_id: number
      name: string
      url_key: string
      path: string
    }[]
  }
  posts?: {
    items?: {
      created_at: string
      enabled: number
      image: string
      name: string
      post_content: string
      author_name: string
      post_id: number
      publish_date: string
      short_description: string
      store_ids: string
      updated_at: string
      url_key: string
    }[]
  }
}

type TPageInfoSecond = {
  currentPage: number
  endPage: number
  hasNextPage: boolean
  hasPreviousPage: boolean
  pageSize: number
  startPage: number
}

type TCategoryPost = {
  category_id: number
  children_count: number
  created_at: string
  description: string
  enabled: number
  import_source: string
  level: number
  meta_description: string
  meta_keywords: string
  meta_robots: string
  meta_title: string
  name: string
  parent_id: string
  path: string
  position: number
  store_ids: string
  updated_at: string
  url_key: string
}
