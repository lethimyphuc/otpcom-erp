type TNotification = {
  user_id: number;
  role_id: number;
  content: number;
  post_id: number;
  title: number;
  created: number;
  status: number;
  type: number;
};

type TCreateNotification = {
  notification: TNotification;
};
