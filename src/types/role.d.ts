type TRole = {
  Id: string;
  Name: string;
  Permissions: Array;
  Active: boolean;
  Created: number;
  CreatedBy: string;
  Updated: number;
  UpdatedBy: string;
  MenuList: Array;
  Code: string;
};
