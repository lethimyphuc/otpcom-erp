type TProduct = {
  name?: string
  sku?: string
  uid?: string
  color?: any
  brand?: any
  package_size?: any
  country?: any
  unit?: any
  length?: number | string
  width?: number | string
  catalogues: string
  height?: number | string
  sort_name?: string
  short_description: {
    html: string
  }
  description: {
    html: string
  }
  rating_summary?: number
  review_count?: number
  reviews?: {
    items?: {
      link_video: string
      images: any[]
      average_rating: number
      customer_avatar: string
      average_rating?: number
      created_at?: string
      nickname?: string
      text?: string
      product?: {
        image?: {
          url?: string
        }
      }
    }[]
    page_info?: {
      current_page?: number
      page_size?: number
      total_pages?: number
    }
  }
  media_gallery?: {
    url?: string
  }[]
  daily_sale?: {
    end_date?: string
    entity_id?: number
    product_id?: number
    sale_price?: number
    sale_qty?: number
    sold_qty?: number
    start_date?: string
    currency?: string
  }
  stock_status?: string
  image?: {
    url?: string
  }
  price_tiers?: {
    discount?: {
      percent_off?: number
    }
    final_price?: TMoney
  }
  price_range?: {
    maximum_price?: {
      final_price?: TMoney
      regular_price?: TMoney
    }
    minimum_price?: {
      final_price?: TMoney
      regular_price?: TMoney
    }
  }
  img?: string
  price_new?: number
  price_old?: number
  reviews?: {
    items: {
      comments: ReviewOptionsFields
      created_at: string
      customer_avatar: string
      images: ReviewOptionsFields
      review_id: string
      link_video: string
      nickname: string
      ratings_breakdown: {
        name: string
        value: string
      }
      statistics: {
        comment: string
        dislike: string
        images: string
        like: string
        video: string
      }
    }
  }
  related_products: TProduct[]
}

type TReviewOptionsFields = {
  content: string
  created_at: string
  customer_avatar: string
  customer_id: string
  post_by: string
  review_id: string
  status: string
  type: string
}

type TReviewProduct = {
  average_rating: number
  created_at: string
  nickname: string
  ratings_breakdown: {
    name: string
    value: string
  }[]
  product: {
    reviews: {
      items: {
        average_rating: number
        nickname: string
        text: string
        created_at: string
        product: {
          image: {
            url: string
          }
        }
      }[]
      page_info: TPageInfo
    }
  }
  summary: string
  text: string
}

type TDailySales = {
  end_date?: string
  entity_id?: number
  items?: TItemDailySale[]
  priority?: string
  show_in_home?: string
  start_date?: string
  status: number
  title: string
}

type TAddToCart = {
  sku: string
  quantity: number
}

type TDataAddToCart = {
  cart: {
    email: string
    id: string
    is_virtual: boolean
    items: {
      prices: {
        price: {
          currentcy: string
          value: number
        }
      }
      product: {
        name: string
        sku: string
      }
      quantity: number
      uid: string
    }
  }
  user_errors: {
    code: string
    message: string
  }[]
}

type TUpdateToCart = {
  cart_item_uid: string
  quantity: number
}

type TRemoveToCart = {
  cart_item_uid: string
}

type TDataUpdateToCart = {
  cart: {
    total_quantity: number
    email: string
    id: string
    is_virtual: boolean
    items: TDataCart[]
    prices: {
      applied_taxes: {
        amount: TMoney
        label: string
      }
      discounts: {
        amount: TMoney
        label: string
      }[]
      grand_total: TMoney
      subtotal_excluding_tax: TMoney
      subtotal_including_tax: TMoney
      subtotal_with_discount_excluding_tax: TMoney
    }
    selected_payment_method: {
      code: string
      purchase_order_number: string
      title: string
    }
    total_quantity: number
    applied_coupons: {
      code: string
    }[]
    available_payment_methods: TMethodPayment[]
    shipping_addresses: {
      available_shipping_methods: TMethodShipping[]
      // cart_items_v2: {
      //   errors: {
      //     code: string
      //     message: string
      //   }
      //   prices: {
      //       discounts: any;
      //       price: TMoney;
      //       price_including_tax: TMoney;
      //       row_total: TMoney;
      //       row_total_including_tax: TMoney;
      //       total_item_discount: TMoney;
      //   }
      //   product: TProduct
      //   quantity: number
      //   uid: any
      // }
      city: string
      company: string
      country: {
        code: string
        label: string
      }
      customer_notes: string
      firstname: string
      items_weight: number
      lastname: string
      pickup_location_code: string
      postcode: string
      region: {
        code: string
        label: string
        region_id: string
      }
      selected_shipping_method: {
        amount: TMoney
        carrier_code: string
        carrier_title: string
        method_code: string
      }
      street: string[]
      telephone: string
      uid: string
      vat_id: string
    }[]
    billing_address: {
      city: string
      company: string
      country: {
        code: string
        label: string
      }
      customer_notes: string // Chỉ sử dụng cho field shipping address
      firstname: string
      lastname: string
      postcode: string
      region: {
        code: string
        label: string
        region_id: number
      }
      street: string
      telephone: string
      uid: string
      vat_id: string
    }
    gift_message: {
      from: string
      message: string
      to: string
    }
  }
  user_errors: {
    code: string
    message: string
  }[]
}

type TDataUpdateToCartCustomer = {
  customerCart: {
    total_quantity: number
    id?: string
    email: string
    id: string
    is_virtual: boolean
    items: TDataCart[]
    prices: {
      applied_taxes: {
        amount: TMoney
        label: string
      }
      discounts: {
        amount: TMoney
        label: string
      }[]
      grand_total: TMoney
      subtotal_excluding_tax: TMoney
      subtotal_including_tax: TMoney
      subtotal_with_discount_excluding_tax: TMoney
    }
    selected_payment_method: {
      code: string
      purchase_order_number: string
      title: string
    }
    total_quantity: number
    applied_coupons: {
      code: string
    }[]
    available_payment_methods: TMethodPayment[]
    shipping_addresses: {
      available_shipping_methods: TMethodShipping[]
      // cart_items_v2: {
      //   errors: {
      //     code: string
      //     message: string
      //   }
      //   prices: {
      //       discounts: any;
      //       price: TMoney;
      //       price_including_tax: TMoney;
      //       row_total: TMoney;
      //       row_total_including_tax: TMoney;
      //       total_item_discount: TMoney;
      //   }
      //   product: TProduct
      //   quantity: number
      //   uid: any
      // }
      city: string
      company: string
      country: {
        code: string
        label: string
      }
      customer_notes: string
      firstname: string
      items_weight: number
      lastname: string
      pickup_location_code: string
      postcode: string
      region: {
        code: string
        label: string
        region_id: string
      }
      selected_shipping_method: {
        amount: TMoney
        carrier_code: string
        carrier_title: string
        method_code: string
      }
      street: string[]
      telephone: string
      uid: string
      vat_id: string
    }[]
    billing_address: {
      city: string
      company: string
      country: {
        code: string
        label: string
      }
      customer_notes: string // Chỉ sử dụng cho field shipping address
      firstname: string
      lastname: string
      postcode: string
      region: {
        code: string
        label: string
        region_id: number
      }
      street: string
      telephone: string
      uid: string
      vat_id: string
    }
    gift_message: {
      from: string
      message: string
      to: string
    }
  }
  user_errors: {
    code: string
    message: string
  }[]
}

type TMethodPayment = {
  code: string
  is_deferred: boolean
  title: string
}

type TMethodShipping = {
  amount: TMoney
  method_title: string
  carrier_code: string
  carrier_title: string
  available: boolean
  method_code: string
  carrierName?: string
  shipFee: number
  logo?: string
  serviceName?: string
}

type TDataCart = {
  prices: {
    discounts: any
    price: TMoney
    price_including_tax: TMoney
    row_total: TMoney
    row_total_including_tax: TMoney
    total_item_discount: TMoney
  }
  product: {
    image: {
      url: string
    }
    name: string
    daily_sale: any
    price_range: {
      maximum_price: {
        final_price: TMoney
        regular_price: TMoney
      }
      minimum_price: {
        final_price: TMoney
        regular_price: TMoney
      }
    }
    sku: string
  }
  quantity: number
  uid: string
}

type TReviewRating = {
  id: string
  name: string
  values: TItemReviewRating[]
}

type TItemReviewRating = {
  value_id: string
  value: string
}

type TItemDailySale = {
  entity_id?: number
  product?: TProduct
  product_id?: number
  sale_price?: number
  sale_qty?: number
  sold_qty?: number
}

type TPageInfo = {
  current_page?: number
  page_size?: number
  total_pages?: number
}

type TMoney = {
  value: number
  currency: string
}

type TTierPrices = {
  customer_group_id: number
  qty: number
  value: number
}

type TCustomAttributes = {
  attribute_code: string
  value: string
}

type TCategoryLinks = {
  position: number
  category_id: string
}

type TExtensionAttributes = {
  website_ids: []
  category_links: TCategoryLinks[]
}

type TCreateProduct = {
  product: TProduct
  saveOptions: true
}

type ProductFilterReq = {
  type?: string | null
  company?: string | null
  end?: string | null
  groupAccessary?: string | null
  sort?: string | null
  tag?: string | null
  search?: string | null
}

type TagRes = {
  id: string
  name: string
  display_name: string
}
