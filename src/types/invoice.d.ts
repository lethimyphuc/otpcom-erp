type TItemInvoice = {
  order_item_id: number;
  qty: number;
};

type TCreateInvoice = {
  capture: boolean;
  items: TItemInvoice[];
  notify: boolean;
  appendComment: boolean;
  comment: {
    comment: string;
    is_visible_on_front: number;
  };
};

type TCreateRefund = {
  items: TItemInvoice[];
  notify: boolean;
  appendComment: boolean;
  comment: {
    comment: string;
    is_visible_on_front: number;
  };
  arguments: {
    shipping_amount: number;
  };
};

type TInvoice = {};
