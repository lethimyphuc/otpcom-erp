type TLocation = {
  sku: string;
  source_code: string;
  location: string;
  updated: string;
  created: string;
};
