type TUser = {
  Id: string
  Email: string
  FullName: string
  IsCheckOTP: boolean
  IsConfirmOTP: boolean
  Phone: string
  UserId: number
  UserGroupId: number
  UserName: string
  Active: boolean
  Created: number
  CreatedBy: string
  Id: string
  Name: string
  Permissions: string
  Updated: number
  UpdatedBy: string
  RoleIds: any
  Roles: any
  Branchs: any
}

export type Region = {
  region: string
  region_code: string
  region_id: number
}

export type UserAddress = {
  city: string
  company: string
  country_code: string
  country_id: string
  district: string
  custom_attributes: string
  default_billing: boolean
  default_shipping: boolean
  extension_attributes: null
  fax: string
  firstname: string
  id: number
  lastname: string
  middlename: null
  postcode: string
  prefix: null
  region: Region
  ward: string
  region_id: number
  street: string[]
  suffix: string
  telephone: string
  vat_id: string
}

export type BankAccount = {
  bank_id: number
  bank: string
  account_holder: string
  account_number: string
  payment_method: 'banktransfer' | string
  sort_order: number
  logo: string
  qr_code: string
}
