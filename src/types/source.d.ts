type TSource = {
  carrier_links: [];
  city: string;
  contact_name: string;
  country_id: string;
  email: string;
  enabled: true;
  extension_attributes: {
    is_pickup_location_active: boolean;
    frontend_name: string;
  };
  name: string;
  phone: string;
  postcode: string;
  region: string;
  source_code: string;
  street: string;
  use_default_carrier_config: boolean;
};
type TSourceCreate = {
  source_code: string;
  name: string;
  email: string;
  contact_name: string;
  enabled: true;
  description: string;
  country_id: string;
  region_id: 0;
  region: string;
  city: string;
  street: string;
  postcode: string;
  phone: string;
};

type TAddSourceItem = {
  sourceItems: [
    {
      sku: string;
      source_code: string;
      quantity: number;
      status: number;
      extension_attributes: any;
    }
  ];
};
