type TCreateLogs = {
  log: {
    created: string;
    staff_id: number;
    store_id: number;
    total: number;
    type: string;
    bank_id: number;
    reason: string;
    note: string;
    status: string;
  };
};

type TLogs = {
  created: string;
  staff_id: number;
  store_id: number;
  total: number;
  type: string;
  bank_id: number;
  reason: string;
  note: string;
  status: string;
};
