type TCondition = {
  condition_type: string;
  aggregator_type: string;
  operator: string;
  value: string;
};

type TPromotion = {
  rule_id: number;
  name: string;
  store_labels: [];
  description: string;
  website_ids: number[];
  customer_group_ids: number[];
  from_date: string;
  to_date: string;
  uses_per_customer: number;
  is_active: boolean;
  condition: TCondition;
  action_condition: TCondition;
  stop_rules_processing: boolean;
  is_advanced: boolean;
  sort_order: number;
  simple_action: string;
  discount_amount: number;
  discount_qty: number;
  discount_step: number;
  apply_to_shipping: boolean;
  times_used: number;
  is_rss: boolean;
  coupon_type: string;
  use_auto_generation: boolean;
  uses_per_coupon: number;
  simple_free_shipping: string;
};

type TCondition = {
  condition_type: string;
  conditions: any;
  aggregator_type: string;
  operator: string;
  attribute_name: string;
  value: string;
  extension_attributes: any;
};

type TCreatePromotion = {
  rule: {
    name: string;
    description: string;
    website_ids: number[];
    customer_group_ids: number[];
    from_date: string;
    to_date: string;
    uses_per_customer: number[];
    is_active: boolean;
    condition: TCondition;
    action_condition: TCondition;
    stop_rules_processing: boolean;
    is_advanced: boolean;
    product_ids: number[];
    sort_order: number;
    simple_action: string;
    discount_amount: number;
    discount_qty: number;
    discount_step: number;
    apply_to_shipping: boolean;
    times_used: number;
    is_rss: boolean;
    coupon_type: string;
    use_auto_generation: boolean;
    uses_per_coupon: number;
    simple_free_shipping: string;
  };
};
