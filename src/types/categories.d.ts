type TCategories = {
  categories: {
    items: {
      children?: TChildrenCategories[];
      level: number;
      tags:
        | {
            id: number;
            name: string;
          }[]
        | null;
      meta_description: any;
      meta_keywords: any;
      name: string;
      uid: string;
      url_key: string;
      image: string;
      image_car: string;
      image_banner: string;
      product_count: number;
    }[];
  };
};

type TChildrenCategories = {
  children: any;
  level: number;
  tags:
    | {
        id: number;
        name: string;
      }[]
    | null;
  meta_description: any;
  meta_keywords: any;
  name: string;
  uid: string;
  url_key: string;
  id?: number;
  image: string;
  image_car: string;
  image_banner: string;
  product_count: number;
};
