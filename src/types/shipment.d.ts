type TShipmentItem = {
  entity_id: number;
  name: string;
  parent_id: number;
  price: number;
  product_id: number;
  sku: string;
  weight: number;
  order_item_id: number;
  qty: number;
};

type TShipmentItemTrack = {
  order_id: number;
  created_at: string;
  entity_id: number;
  parent_id: number;
  updated_at: string;
  weight: null;
  qty: null;
  description: null;
  track_number: string;
  title: string;
  carrier_code: string;
};

type TShipment = {
  billing_address_id: number;
  created_at: string;
  customer_id: number;
  entity_id: number;
  increment_id: string;
  order_id: number;
  packages: any[];
  shipping_address_id: number;
  store_id: number;
  total_qty: number;
  updated_at: string;
  items: TShipmentItem[];
  tracks: TShipmentItemTrack[];
  comments: any[];
  extension_attributes: {
    source_code: string;
  };
};
