type BoxSearchForm = {
  type: string | null;
  company: string | null;
  end: string | null;
  groupAccessary: string | null;
  sort?: string | null;
  tag: string | null;
  search?: string | null;
};

type BoxSearchFormBrand = {
  type: string | null;
  brand: string | null;
  sort?: string | null;
}
type BoxSearchFormAccessories = {
  accessories: string | null;
  tag: string | null;
  sort?: string | null;
}