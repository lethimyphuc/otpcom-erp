type TMenuHeader = {
  additional_data: string[];
  classes: string;
  creation_time: string;
  level: number;
  menu_id: number;
  node_id: number;
  node_template: string;
  parent_id: number;
  position: number;
  submenu_template: string;
  title: string;
  type: string;
  update_time: string;
  url_key: string
}