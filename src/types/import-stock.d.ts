type TItemImportStock = {
  sku: string;
  qty: number;
  price: number;
  note: string;
  source_code: string;
};

type TImportStock = {
  import_stock: {
    staff_id: number;
    supplier_id: number;
    source_code: string;
    stock_id: number;
    reason: string;
    created: string;
    status: number;
    order_buy_id: number;
    items: TItemImportStock[];
  };
};
