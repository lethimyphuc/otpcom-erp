type TOrderItems = {
  sku: string;
  qty: number;
  price: number;
  source_code: string;
};

type TCreateOrderBuy = {
  order: {
    supplier_id: number;
    staff_id: number;
    source_code: string;
    stock_id: number;
    reason: string;
    created: string;
    status: string;
    items: TOrderItems[];
  };
};

type TSetShippingAddressNoSelectCustomerAddress = {
  shipping_addresses: TShippingAddress[];
};

type TSetBillingAddress = {
  billing_address: TBillingAddress;
};

type TShippingMethods = {
  carrier_code: string;
  method_code: string;
  amount?: TMoney;
  available?: boolean;
  carrier_title?: string;
  method_title?: string;
};

type TShippingAddress = {
  address?: {
    city: string;
    country_code?: string;
    company?: string;
    firstname: string;
    lastname: string;
    street: string[];
    telephone: string;
    postcode?: string;
    save_in_address_book: boolean;
    region: string;
  };
  customer_address_id?: number;
  customer_notes: string;
};

type TBillingAddress = {
  address?: {
    city: string;
    country_code?: string;
    company?: string;
    firstname: string;
    lastname: string;
    street: string[];
    telephone: string;
    postcode?: string;
    save_in_address_book: boolean;
    region: string;
  };
  customer_address_id?: number;
  same_as_shipping: boolean;
};

type TSetPaymentMethodOnCart = {
  payment_method: {
    code?: string;
  };
};

type TApplyCouponToCart = {
  coupon_code: string;
};
