type TCompany = {
  name: string;
  short_name: string;
  address: string;
  tax_code: string;
  website: string;
  representative: string;
  position: string;
};
type TSuppliers = {
  id: string;
  code: string;
  phone: string;
  address: string;
  birthday: string;
  gender: number;
  store_id: number;
  website_id: number;
  email: string;
  company: TCompany;
};

type TCreateSupplier = {
  supplier: {
    code: string;
    name: string;
    phone: string;
    address: string;
    gender: number;
    store_id: number;
    website_id: number;
    email: string;
    company_id: number;
    create_at: string;
    birthday: string;
    company: {
      id: number;
      name: string;
      short_name: string;
      address: string;
      tax_code: string;
      website: string;
      representative: string;
      position: string;
    };
  };
};
