type TLogin = {
  username: string
  password: string
}

type TForgot = {
  username: string
}

type TChangePassword = {
  currentPassword: string
  newPassword: string
  confirmPassword?: string
}

type TRegister = {}

type LoginForm = {
  cartId?: string | null
  email: string
  password: string
}

type TAuthenticate = {
  firstname?: string
  lastname?: string
  email: string
  password?: string
  repassword?: string
  term?: any
  phone_number?: string
  cartId?: string | null
}

type RegisterForm = {
  firstname: string
  lastname: string
  email: string
  password: string
  repassword: string
  phone_number: string
  term: boolean
}

type TSocialLoginUrl = {
  key: string
  label: string
  login_url: string
}

type TAuthenticateGoogle = {
  authuser: string
  code: string
  handle: string
  prompt: string
  scope: string
  state: string
}

type TChangeInfoCustomer = {
  lastname?: string
  firstname?: string
  date_of_birth?: string
  email?: string
  telephone?: string
  avatar?: any
  phone_number?: string
  city?: string | null
  region?: string | null
  ward?: string | null
  street?: string
}

export type CreateAddressForm = {
  lastname: string
  firstname: string
  telephone: string
  city: string
  district: string
  ward: string
  street: string
}

export type CreateAddressReq = {
  lastname: string
  firstname: string
  telephone: string
  city: string
  district: string
  ward: string
  street: string[]
  country_id: string
  postcode: string
}
