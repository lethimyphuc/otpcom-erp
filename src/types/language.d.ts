type TLanguage = {
  base_media_url: string;
  base_static_url: string;
  base_url: string;
  copyright: string;
  locale: string;
  secure_base_media_url: string;
  secure_base_static_url: string;
  secure_base_url: string;
  store_code: string;
  store_name: string;
  timezone: string;
  website_name: string;
  welcome: string;
};
