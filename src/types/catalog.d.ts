interface ICatalogData {
  position: number
  posX: string
  posY: string
  products: ICatalogDataITems[]
}
interface IOptionsCatalogProps {
  filter?: any
}
interface ICatalogDataITems {
  id: string
  link: string
  name: string
  sku: string
  src: string
  price: number
  status: string
}
interface ICatalogMenu {
  id: string
  path: string
  meta_description: string
  meta_keywords: string
  meta_title: string
  name: string
  url_key: string
  breadcrumbs: ICatalogBreadCrumb[]
  children: ICatalogMenu[]
  catalogues: ICatalogItem[]
  image: string
  image_icon_menu: string
  image_banner: string
  image_car: string
  tags: {
    id: string
    name: string
    display_name: string
  }[]
}
interface ICatalogBreadCrumb {
  category_id: number
  category_level: number
  category_name: string
  category_uid: string
  category_url_key: string
  category_url_path: string
}
interface ICatalogItem {
  catalogue_image: string
  category_id: string
  created_at: string
  id: number
  meta_description: string
  meta_keyword: string
  meta_title: string
  name: string
  status: boolean
  updated_at: string
  url_key: string
}
