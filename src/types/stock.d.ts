type TExtensionAttributes = {
  sales_channels: [];
};

type TStock = {
  name: string;
  stock_id: number;
  extension_attributes: TExtensionAttributes;
};
