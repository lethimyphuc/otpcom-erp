import Document, { Html, Head, NextScript, Main } from 'next/document'

// INJECT EXTERNAL SCRIPTs/CDN LINKs HERE
export default class MyDocument extends Document {
  render(): JSX.Element {
    return (
      <Html>
        <Head>
          <link
            rel="icon"
            type="image/png"
            href="/image/logo/otp-logo-small.png"
            className="w-[50px] h-[50px]"
          />
          <meta httpEquiv="Permissions-Policy" content="interest-cohort=()" />

          <link
            href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap"
            rel="stylesheet"
          ></link>
          <script
            async
            defer
            crossOrigin="anonymous"
            src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v17.0&appId=459474598851881&autoLogAppEvents=1"
            nonce="eWU4inD0"
          ></script>
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}
