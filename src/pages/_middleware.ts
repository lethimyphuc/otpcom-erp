// import { NextFetchEvent, NextRequest, NextResponse } from 'next/server'

import { NextFetchEvent, NextRequest, NextResponse } from 'next/server'

export function middleware(req: NextRequest, ev: NextFetchEvent) {
  const url = req.nextUrl.clone()
  url.pathname = '/maintaince'

  const baseUrl: string = process.env.NEXT_PUBLIC_DOMAIN_URL ?? 'http://localhost:3000'
  const unprotectedPaths = [
    `${baseUrl}/favicon.ico`,
    `${baseUrl}/image/logo/logo-header.png`,
    `${baseUrl}/image/svg/search.svg`,
    `${baseUrl}/image/svg/user.svg`,
    `${baseUrl}/image/svg/shopping-cart.svg`,
    `${baseUrl}/image/svg/vietnam.svg`,
    `${baseUrl}/image/Subtract.png`,
    `${baseUrl}/image/red-car.png`,
    `${baseUrl}/icons/svg/vertical-logo.svg`,
    `${baseUrl}/icons/svg/stripes.svg`,
    `${baseUrl}/image/gear.png`,
    `${baseUrl}/image/logo/logo-footer.png`,
    `${baseUrl}/image/svg/search.svg`,
    `${baseUrl}/image/Union.png`,
    `${baseUrl}/image/svg/vietnam.svg`,
    `${baseUrl}/image/svg/user.svg`,
    `${baseUrl}/icons/email.svg`,
    `${baseUrl}/icons/phone.svg`,
    `${baseUrl}/icons/maps.svg`,
    `${baseUrl}/image/svg/shopping-cart.svg`,
    `${baseUrl}/icons/svg/google.svg`,
    `${baseUrl}/image/124.png`,
    `${baseUrl}/image/tags.png`,
    `${baseUrl}/icons/facebook.svg`,
    `${baseUrl}/icons/zalo.svg`,
    `${baseUrl}/icons/youtube.svg`,
    `${baseUrl}/image/gear-cart.png`,
    `${baseUrl}/image/empty-cart.png`,
    `${baseUrl}/image/empty-cart-footer.png`,
    `${baseUrl}/image/Subtract.png`,
    `${baseUrl}/image/red-car.png`,
    `${baseUrl}/image/gear.png`,
    `${baseUrl}/icons/svg/vertical-logo.svg`,
    `${baseUrl}/icons/svg/stripes.svg`,
    `${baseUrl}/image/logo/otp-logo-small.png`,
    `${baseUrl}/icons/PhoneCall.svg`,
    `${baseUrl}/icons/PhoneCall.svg`,
    `${baseUrl}/image/image3.png`,
    `${baseUrl}/image/gear-cart.png`,
    `${baseUrl}/image/empty-cart.png`,
    `${baseUrl}/image/empty-cart-footer.png`,
    `${baseUrl}/icons/arrow-right.svg`,
  ]

  if (unprotectedPaths?.includes(req.url)) {
    return void 0
  } else if (
    process.env.NEXT_PUBLIC_MODE === 'maintaince' &&
    !req?.cookies?.token &&
    !req.nextUrl.pathname?.includes('/login')
  ) {
    return NextResponse.rewrite(url)
  } else {
    return NextResponse.next()
  }
}
