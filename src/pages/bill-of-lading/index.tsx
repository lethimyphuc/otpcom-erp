import React from "react";
import { MainLayout } from "~src/components";
import { TNextPageWithLayout } from "~src/types/layout";
import { PageName } from "~src/config/displayNameConfig";
import BillOfLading from "~src/components/pages/bill-of-lading";

const Index: TNextPageWithLayout = () => {
  return <BillOfLading />;
};

Index.Layout = MainLayout;
Index.displayName = PageName.billOfLading;

export default Index;
