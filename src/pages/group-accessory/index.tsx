import React from "react";
import { MainLayout } from "~src/components";
import { TNextPageWithLayout } from "~src/types/layout";
import { PageName } from "~src/config/displayNameConfig";
import { GroupAccessoryPage } from "~src/components/pages/group-accessory/GroupAccessoryPage";

const Index: TNextPageWithLayout = () => {
  return <GroupAccessoryPage />;
};

Index.Layout = MainLayout;
Index.displayName = PageName.groupAccessory;

export default Index;
