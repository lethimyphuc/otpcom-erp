import { MainLayout } from '~src/components'
import { CatalogCategoryPage } from '~src/components/pages/catalog/catalog-category/CatalogCategoryPage'
import { PageName } from '~src/config/displayNameConfig'
import { TNextPageWithLayout } from '~src/types/layout'

const Index: TNextPageWithLayout = () => {
  return <CatalogCategoryPage />
}

Index.Layout = MainLayout
Index.displayName = PageName.catalog

export default Index
