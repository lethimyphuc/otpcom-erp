import { useRouter } from "next/router";
import React from "react";
import { Loading, MainLayout } from "~src/components";
import ProductPageContent from "~src/components/pages/product";
import { PageName } from "~src/config/displayNameConfig";

const ProductPage = () => {
  const router = useRouter();

  if (!router.isReady) {
    return <Loading />;
  }
  return (
    <>
      <ProductPageContent router={router} />
    </>
  );
};

ProductPage.Layout = MainLayout;
ProductPage.displayName = PageName.product;

export default ProductPage;
