import { useRouter } from "next/router";
import React from "react";
import { Loading, MainLayout } from "~src/components";
import ProductDetailPage from "~src/components/pages/product-detail";
import { PageName } from "~src/config/displayNameConfig";

const ProductDetail = () => {
  const router = useRouter();

  return <ProductDetailPage router={router} />;
};

export default ProductDetail;
ProductDetail.Layout = MainLayout;
ProductDetail.displayName = PageName.product;
