import React from "react";
import { MainLayout } from "~src/components";
import ResetPasswordSection from "~src/components/pages/reset-password/ResetPasswordSection";
import ForgotPasswordPage from "../forgot-password";
import { TNextPageWithLayout } from "~src/types/layout";
import { PageName } from "~src/config/displayNameConfig";

const ResetPasswordPage: TNextPageWithLayout = () => {
  return <ResetPasswordSection />;
};

ResetPasswordPage.Layout = MainLayout;
ResetPasswordPage.displayName = PageName.resetPassword;

export default ResetPasswordPage;
