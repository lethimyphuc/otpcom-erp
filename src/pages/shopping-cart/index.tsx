import React from "react";
import { MainLayout } from "~src/components";
import { TNextPageWithLayout } from "~src/types/layout";
import { PageName } from "~src/config/displayNameConfig";
import ShoppingCart from "~src/components/pages/shopping-cart";

const Index: TNextPageWithLayout = () => {
  return <ShoppingCart />;
};

Index.Layout = MainLayout;
Index.displayName = PageName.shoppingCart;

export default Index;
