import { useRouter } from "next/router";
import React from "react";
import { Loading, MainLayout } from "~src/components";
import DiaryDetailPage from "~src/components/pages/diary-detail";
import { PageName } from "~src/config/displayNameConfig";

const DiaryPageDetail = () => {
  const router = useRouter();
  if (!router.isReady) {
    return <Loading />;
  }
  return <DiaryDetailPage router={router} />;
};
DiaryPageDetail.Layout = MainLayout;
DiaryPageDetail.displayName = PageName.diary;
export default DiaryPageDetail;
