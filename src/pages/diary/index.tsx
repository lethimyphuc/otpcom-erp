import React, { useEffect } from "react";
import { Loading, MainLayout } from "~src/components";
import { TNextPageWithLayout } from "~src/types/layout";
import { PageName } from "~src/config/displayNameConfig";
import { HomePage } from "~src/components/pages";
import DiaryPage from "~src/components/pages/diary/DiaryPage";
import { useRouter } from "next/router";

const Index: TNextPageWithLayout = () => {
  const router = useRouter();

  return <DiaryPage />;
};

Index.Layout = MainLayout;
Index.displayName = PageName.diary;

export default Index;
