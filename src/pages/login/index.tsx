import React from 'react'
import { MainLayout } from '~src/components'
import { TNextPageWithLayout } from '~src/types/layout'
import { PageName } from '~src/config/displayNameConfig'
import LoginPage from '~src/components/pages/login'

const Index: TNextPageWithLayout = () => {
  return <LoginPage tab={1} />
}

Index.Layout = MainLayout
Index.displayName = PageName.login

export default Index
