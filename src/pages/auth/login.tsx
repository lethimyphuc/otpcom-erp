import Head from "next/head";
import { NextPage } from "next/types";
import React, { Fragment } from "react";

const Login: NextPage = () => {
  return (
    <Fragment>
      <Head>
        <title>Login</title>
      </Head>
    </Fragment>
  );
};

export default Login;
