/* eslint-disable @next/next/no-img-element */
/* eslint-disable react/no-unescaped-entities */
import { TNextPageWithLayout } from '~src/types/layout'

const Index: TNextPageWithLayout = () => {
  return (
    <div className="bg-green flex-1 flex flex-col justify-center min-h-screen items-center relative overflow-hidden text-center">
      <div className="img mb-[24px]">
        <img className="w-[300px] h-auto" src="/image/logo/logo-header.png" alt="" />
      </div>

      <h1 className="text-white text md:text-[48px] text-[36px] font-bold mb-[16px]">
        We're Coming Soon
      </h1>
      <p className="text-white text text-[16px] font-normal mb-[16px] text-center px-[15px]">
        Perfect and awesome to present your future product or service. Hooking audience attention is
        all in the opener.
      </p>
    </div>
  )
}

Index.Layout = Index

export default Index
