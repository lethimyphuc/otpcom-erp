import React from "react";
import { MainLayout } from "~src/components";
import { TNextPageWithLayout } from "~src/types/layout";
import { PageName } from "~src/config/displayNameConfig";
import { HomePage } from "~src/components/pages";
import { BrandPage } from "~src/components/pages/brand/BrandPage";

const Index: TNextPageWithLayout = () => {
  return <BrandPage />;
};

Index.Layout = MainLayout;
Index.displayName = PageName.brand;

export default Index;
