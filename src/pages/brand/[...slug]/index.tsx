import { MainLayout } from '~src/components'
import { BrandPage } from '~src/components/pages/brand/BrandPage'
import { PageName } from '~src/config/displayNameConfig'
import { TNextPageWithLayout } from '~src/types/layout'

const Index: TNextPageWithLayout = () => {
  return <BrandPage />
}

Index.Layout = MainLayout
Index.displayName = PageName.brand

export default Index
