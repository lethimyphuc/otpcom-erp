/** @format */

import { GetStaticPaths, GetStaticProps } from 'next'
import { useRouter } from 'next/router'
import { MainLayout } from '~src/components'
import DynamicPageSection from '~src/components/pages/dynamic-page'
import { PageName } from '~src/config/displayNameConfig'
import { TNextPageWithLayout } from '~src/types/layout'

const DynamicPage: TNextPageWithLayout = () => {
  const router = useRouter()
  return <DynamicPageSection />
}

DynamicPage.Layout = MainLayout
DynamicPage.displayName = PageName.carCompany

export default DynamicPage

