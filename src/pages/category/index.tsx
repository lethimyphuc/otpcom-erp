/** @format */

import { GetStaticProps } from 'next'
import { MainLayout } from '~src/components'
import DynamicPageSection from '~src/components/pages/dynamic-page'
import { PageName } from '~src/config/displayNameConfig'
import { TNextPageWithLayout } from '~src/types/layout'

const DynamicPage: TNextPageWithLayout = () => {
  return <DynamicPageSection />
}

DynamicPage.Layout = MainLayout
DynamicPage.displayName = PageName.carCompany

export default DynamicPage
