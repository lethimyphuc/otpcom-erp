import { useRouter } from 'next/router'
import React from 'react'
import { MainLayout } from '~src/components'
import AccessoriesPage from '~src/components/pages/accessories'
import { PageName } from '~src/config/displayNameConfig'
import { TNextPageWithLayout } from '~src/types/layout'

const DynamicPage: TNextPageWithLayout = () => {
  const router = useRouter()
  return <AccessoriesPage />
}

DynamicPage.Layout = MainLayout
DynamicPage.displayName = PageName.carCompany

export default DynamicPage
