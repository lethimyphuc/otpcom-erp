import { MainLayout } from '~src/components'
import AccessoriesPage from '~src/components/pages/accessories'
import { PageName } from '~src/config/displayNameConfig'
import { TNextPageWithLayout } from '~src/types/layout'

const Index: TNextPageWithLayout = () => {
  return <AccessoriesPage />
}

Index.Layout = MainLayout
Index.displayName = PageName.groupAccessory

export default Index
