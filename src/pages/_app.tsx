/* eslint-disable @next/next/no-page-custom-font */
import '../../styles/globals.css'
import { TAppPropsWithLayout } from '~src/types/layout'
import { QueryClientProvider, QueryClient, QueryClientConfig } from 'react-query'
import Router from 'next/router'
import NProgress from 'nprogress'
// import { AppProps } from "next/app";
import React, { useEffect, useState } from 'react'
import ToastProvider from '../providers/ToastProvider'
import Head from 'next/head'
import 'react-toastify/dist/ReactToastify.min.css'
import 'nprogress/nprogress.css'
import 'moment/locale/vi'
import locale from 'antd/lib/locale/vi_VN'
import { ReactQueryDevtools } from 'react-query/devtools'

//Add style
import '../assets/css/main.scss'
import '../../styles/globals.css'
import '../../styles/styles.css'
import 'antd/dist/antd.css'
import { ConfigProvider } from 'antd'
import 'swiper/css'
//css
import '../styles/global.scss'
//add style icon
import '../assets/fontawesome/css/all.min.css'
import BlankLayout from '../components/global/Layout/blankLayouts'
import Store from '~src/providers/Store'

// Create a client
const queryClientConfig: QueryClientConfig = {
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
    },
  },
}

const queryClient = new QueryClient(queryClientConfig)

const App = ({ Component, pageProps: { session, ...pageProps } }: TAppPropsWithLayout) => {
  const Layout = Component.Layout || BlankLayout
  const [loading, setLoading] = useState(false)

  const [showing, setShowing] = useState(false)

  useEffect(() => {
    setShowing(true)
  }, [])

  React.useEffect(() => {
    const handleRouteStart = () => {
      setLoading(true)
      // NProgress.start();
    }
    const handleRouteDone = () => {
      setLoading(false)
      // NProgress.done();

      localStorage.setItem('callbackUrl', '/')
    }

    Router.events.on('routeChangeStart', handleRouteStart)
    Router.events.on('routeChangeComplete', handleRouteDone)
    Router.events.on('routeChangeError', handleRouteDone)

    return () => {
      // Make sure to remove the event handler on unmount
      Router.events.on('routeChangeStart', handleRouteStart)
      Router.events.on('routeChangeComplete', handleRouteDone)
      Router.events.on('routeChangeError', handleRouteDone)
    }
  }, [])

  if (!showing) {
    return null
  }
  if (typeof window === 'undefined') {
    return <></>
  }
  return (
    <ToastProvider>
      <QueryClientProvider client={queryClient}>
        <ConfigProvider locale={locale}>
          <Head>
            <title>{!loading ? Component?.displayName : 'Đang chuyển hướng...'}</title>
          </Head>

          {
            <Store>
              <Layout>
                <Component {...pageProps} isBreadcrumb={Component.isBreadcrumb} />
              </Layout>
            </Store>
          }
        </ConfigProvider>
        <ReactQueryDevtools initialIsOpen={false} />
      </QueryClientProvider>
    </ToastProvider>
  )
}

export default App
