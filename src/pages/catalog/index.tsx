import { MainLayout } from '~src/components'
import { CatalogPage } from '~src/components/pages/catalog/CatalogPage'
import { PageName } from '~src/config/displayNameConfig'
import { TNextPageWithLayout } from '~src/types/layout'

const Index: TNextPageWithLayout = () => {
  return <CatalogPage />
}

Index.Layout = MainLayout
Index.displayName = PageName.catalog

export default Index
