import { graphQLClient, graphQLClientGET } from '~src/api/graphql'
import { MainLayout } from '~src/components'
import { CatalogAdminWrapper } from '~src/components/pages/catalog/catalog-detail/CatalogAdminWrapper'
import { TNextPageWithLayout } from '~src/types/layout'

const Index: TNextPageWithLayout = ({ isPermission }: any) => {
  console.log('isPermission', isPermission)
  return <CatalogAdminWrapper isPermission={isPermission} />
}

Index.Layout = MainLayout
Index.displayName = 'Chi tiết catalog'

export default Index

export const getServerSideProps = async ({ query, res }: any) => {
  res.setHeader('Cache-Control', 'public, s-maxage=10, stale-while-revalidate=59')
  graphQLClient.setHeaders({ token: query?.token })
  const isPermission = await graphQLClient
    .request(
      ` mutation checkPermission {
          checkPermission
      }`,
      {},
      {
        Authorization: `Bearer ${query?.token}`,
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache',
      }
    )
    .then((res: any) => {
      return res?.checkPermission
    })
    .catch((error: any) => {
      console.log(error)
    })

  return {
    props: {
      isPermission,
    },
  }
}
