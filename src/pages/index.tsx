import React from "react";
import { MainLayout } from "~src/components";
import { TNextPageWithLayout } from "~src/types/layout";
import { PageName } from "~src/config/displayNameConfig";
import { HomePage } from "~src/components/pages";

const Index: TNextPageWithLayout = () => {
  return <HomePage />;
};

Index.Layout = MainLayout;
Index.displayName = PageName.home;

export default Index;
