import React from "react";
import { Loading, MainLayout } from "~src/components";
import { TNextPageWithLayout } from "~src/types/layout";
import { PageName } from "~src/config/displayNameConfig";
import { HomePage } from "~src/components/pages";
import { PromotionPage } from "~src/components/pages/promotion/PromotionPage";
import { useRouter } from "next/router";

const Index: TNextPageWithLayout = () => {
  const router = useRouter();
  return <PromotionPage router={router} />;
};

Index.Layout = MainLayout;
Index.displayName = PageName.promotion;

export default Index;
