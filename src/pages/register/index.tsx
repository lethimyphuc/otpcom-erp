import React from 'react'
import { MainLayout } from '~src/components'
import { TNextPageWithLayout } from '~src/types/layout'
import { PageName } from '~src/config/displayNameConfig'
import LoginPage from '~src/components/pages/login'

const Index: TNextPageWithLayout = () => {
  return <LoginPage tab={2} />
}

Index.Layout = MainLayout
Index.displayName = PageName.register

export default Index
