import React from "react";
import { MainLayout } from "~src/components";
import ForgotPasswordSection from "~src/components/pages/forgot-password/ForgotPasswordSection";
import { PageName } from "~src/config/displayNameConfig";
import { TNextPageWithLayout } from "~src/types/layout";

const ForgotPasswordPage: TNextPageWithLayout = () => {
  return <ForgotPasswordSection />;
};

ForgotPasswordPage.Layout = MainLayout;
ForgotPasswordPage.displayName = PageName.forgotPassword;

export default ForgotPasswordPage;
