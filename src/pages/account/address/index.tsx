import React from 'react'
import { AccountLayout, MainLayout } from '~src/components'
import { TNextPageWithLayout } from '~src/types/layout'
import { PageName } from '~src/config/displayNameConfig'
import { UserAddress } from '~src/components/pages/account-information/components/UserAddress'

const Index: TNextPageWithLayout = () => {
  return (
    <AccountLayout>
      <UserAddress />
    </AccountLayout>
  )
}

Index.Layout = MainLayout
Index.displayName = PageName.accountInfo

export default Index
