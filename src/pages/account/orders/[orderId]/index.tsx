import { AccountLayout, MainLayout } from '~src/components'
import OrderDetail from '~src/components/pages/account-information/components/OrderDetail'
import { PageName } from '~src/config/displayNameConfig'
import { TNextPageWithLayout } from '~src/types/layout'

const Index: TNextPageWithLayout = () => {
  return (
    <AccountLayout>
      <OrderDetail />
    </AccountLayout>
  )
}

Index.Layout = MainLayout
Index.displayName = PageName.accountInfo

export default Index
