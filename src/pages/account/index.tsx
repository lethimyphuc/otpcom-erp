import React from 'react'
import { AccountLayout, MainLayout } from '~src/components'
import { TNextPageWithLayout } from '~src/types/layout'
import { PageName } from '~src/config/displayNameConfig'
import AccountInfo from '~src/components/pages/account-information'

const Index: TNextPageWithLayout = () => {
  return (
    <AccountLayout>
      <AccountInfo />
    </AccountLayout>
  )
}

Index.Layout = MainLayout
Index.displayName = PageName.accountInfo

export default Index
