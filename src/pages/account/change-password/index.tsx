import { AccountLayout, MainLayout } from '~src/components'
import ChangePasswordTab from '~src/components/pages/account-information/components/ChangePasswordTab'
import { PageName } from '~src/config/displayNameConfig'
import { TNextPageWithLayout } from '~src/types/layout'

const Index: TNextPageWithLayout = () => {
  return (
    <AccountLayout>
      <ChangePasswordTab />
    </AccountLayout>
  )
}

Index.Layout = MainLayout
Index.displayName = PageName.accountInfo

export default Index
