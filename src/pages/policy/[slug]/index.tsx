import React from 'react'
import { MainLayout } from '~src/components'
import { TNextPageWithLayout } from '~src/types/layout'
import { PageName } from '~src/config/displayNameConfig'
import PolicyPage from '~src/components/pages/policy'

const Index: TNextPageWithLayout = () => {
  return <PolicyPage />
}

Index.Layout = MainLayout
Index.displayName = PageName.policy

export default Index
