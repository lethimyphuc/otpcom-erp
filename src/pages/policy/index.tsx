import { GetStaticProps } from 'next'
import { useRouter } from 'next/router'
import { graphQLClient } from '~src/api/graphql'
import { GET_LIST_POLICY } from '~src/api/post'

const Policy = (props: any) => {
  const router = useRouter()
  const policyList = props.dataPolicy.mpBlogPosts.items
  router.replace(`/policy/${policyList[0]?.url_key}`)
  return <></>
}

export default Policy

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const dataPolicy = (await graphQLClient.request(GET_LIST_POLICY, {
    action: 'get_post_by_categoryId',
    categoryId: 4,
  })) as {
    mpBlogPosts: {
      items: TPost[]
      page_info: TPageInfo
      total_count: number
    }
  }
  return {
    props: { dataPolicy },
    revalidate: 10,
  }
}
