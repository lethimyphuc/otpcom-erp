import React from "react";
import { MainLayout, NotFound } from "~src/components";
import { TNextPageWithLayout } from "~src/types/layout";

const Index: TNextPageWithLayout = () => {
  return <NotFound />;
};

Index.Layout = Index;

export default Index;
