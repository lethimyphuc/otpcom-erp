## OTPCOM.VN WEBSITE 🐶 🐹 🐰

<img src="https://img.shields.io/badge/react-18.2.0-%23EC407A"/> <img src="https://img.shields.io/badge/next-12.2.2-orange"/> <img src="https://img.shields.io/badge/antd-4.21.6-42A5F5"/>

### Người mới vui lòng đọc hết và xem kĩ các lưu ý 🐥

## Cài đặt ⚙️

Mình có cài thêm thư viện yup và @hookform/resolvers nha

Clone source:

```sh
git clone https://gitlab.com/lethimyphuc/otpcom-erp.git
```

Cài thư viện:

```sh
yarn install
```

Chạy dự án:

```sh
yarn dev
```

Xuất dự án ( static ):

```sh
yarn export
```

## Lưu ý 🗯

- Trước khi push thì chạy

```sh
yarn build
```

coi nó có lỗi ko.Không build trước khi push lên, deploy không được Mỹ Phúc chữi sấp mặt nha.

- Dự án này vẫn có thể build server bình thường.
- Khi tạo page mới thì nhớ "export default" do cơ chế của Nextjs nó bắt vậy.
- Tạo page mới có thể code trong page hoặc tạo components rồi import vào.
- Code xong phần nào nhớ test thật kĩ trước khi next ( lỗi ngu phạt 5 xị ).

## Lấy thông tin của tài khoản đang đăng nhập 🙎 :

```tsx
import { useSelector } from 'react-redux'
import { RootState } from '~/store'

// ...
const user = useSelector((state: RootState) => state.user.information)
```

## Tạo thông báo 🆘 :

```tsx
import { toast } from '~src/components/toast'

//...
toast.info('Bạn đã được tăng lương')

//...
toast.warning('Bạn sắp bị đuổi')

//...
toast.error('Bạn đã bị đuổi')
```

## Sử dụng [Dynamic Routes (slug)](https://nextjs.org/docs/routing/dynamic-routes) cho Static HTML Export

- Thay vì đặt tên file là "[slug].tsx" mà hãy đặt là "index.tsx"
- Thay vì viết như này

```tsx
router.push({
  pathname: `$/course/video-course/detail/${slug}`
})
```

- Thì đổi thành viết như vầy

```javascript
router.push({
  pathname: '/course/video-course/detail',
  query: { slug: Id }
})
```

- Static files không thể hiểu được [slug] nên nếu cố viết theo cách đó sẽ bị lỗi khi reload lại trang.

## Cấu trúc dự án 🪓

```markdown
├ public
│ ├─── icons ⇾ lưu các icon png, svg
│ └─── images ⇾ lưu các ảnh sử dụng trong hệ thống (logo, background...)
│
├ src
│ ├ api ⇾ lưu các api
│ │ ├─ example ⇾ mẫu cách gọi 1 api (GET, POST, PUT, DELETE)
│ │ ├─ types ⇾ các khai báo types cho api
│ │ └─ instance.ts ⇾ file config axios (coi thôi đừng sửa)
│ │
│ ├ common
│ │ ├─ components ⇾ các componets sử dụng trong dự án
│ │ │ ├─ MainLayout ⇾ components Layout của dự án
│ │ │ └─ Primary ⇾ các components tái sử dụng nhiều lần (Button, Table...)
│ │ │
│ │ ├─ libs ⇾ lưu các biến static trong này cho dễ tái sử dụng
│ │ │
│ │ ├─ types ⇾ thư mục khai báo types
│ │ │
│ │ └─ utils ⇾ thư mục lưu các hàm hay sử dụng (ShowNoti...)
│ │
│ ├ pages ⇾ lưu các page của hệ thống
│ │
│ ├ services ⇾ lưu các api được gọi trước khi đăng nhập (không có token)
│ │
│ ├ store ⇾ thư mục khởi tạo và lưu reducer của redux
│ │
│ ├ styles ⇾ thư mục lưu các custom css (sass)
│ │
│ ├ appConfig.js ⇾ file cấu hình hệ thống
│ │
├ .env ⇾ file chứa các biến môi trường (kêu người nắm source đưa)
│ ├ NEXTAUTH_URL ⇾ root link
│ ├ NEXT_PUBLIC_API_ENDPOINT ⇾ api link
│ └ NEXT_PUBLIC_ONE_SIGNAL ⇾ key thông báo (push notification)
│
└ tailwind.config.js ⇾ file config "Tailwind"
```

## Liên hệ 💰

- Mọi vấn đề liên hệ: [hothilan](https://www.facebook.com/htlan0101)

## Keyword

- monamedia
