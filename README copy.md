This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

<!-- install yarn : -->

npm i yarn

<!-- check version -->

yarn -v

<!-- install nodemodules -->

yarn

<!-- start -->

npm run dev

# or

yarn dev

<!-- port 3000 -->

http://localhost:3000/
