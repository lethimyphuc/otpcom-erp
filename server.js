const express = require("express");
const http = require("http");
const socketIO = require("socket.io");

const app = express();
const server = http.createServer(app);
const io = socketIO(server);

io.on("connection", (socket) => {
  console.log("A client connected.");

  socket.on("chat message", (message) => {
    io.emit("chat message", message); // Broadcast the message to all connected clients.
  });

  socket.on("disconnect", () => {
    console.log("A client disconnected.");
  });
});

const port = 4000;

server.listen(port, () => {
  console.log(`Socket.IO server is running on port ${port}`);
});
