const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx}',
    './src/forms/**/*.{js,ts,jsx,tsx}',
    './src/components/**/*.{js,ts,jsx,tsx}',
  ],
  // đổi màu ở đây thì vào globals đổi màu theo (nếu có)
  theme: {
    colors: {
      label: '#6B6F82',
      warning: '#F44336',
      warningBold: '#f14f04',
      brown: '#d17905',
      info: '#2196F3',
      success: '#388E3C',
      ['important-pending']: '#D32F2F',
      active: '#f14f04',
      orange: '#f14f04',
      pending: '#f57c00',
      unactive: '#ccc',
      pink: '#ff4081',
      gray: '#e6e6e6',
      purple: '#a0f',
      cyan: '#00e5ff',
      white: '#fff',
      gold: '#ff9100',
      blue: '#2196F3',
      green: '#388E3C',
      red: '#F44336',
      black: '#000',
      yellow: '#fbc02d',
      violet: '#4d5bd4',
      dark: '#242526',
      darkLight: '#3e4042',
      blueLight: '#0084ff',
      lightGray: '#E4E6EB',
      main: '#191f2f',
      remain: '#7790b6',
      lining: '#f7f1e6',
      key: '#d7d8d9',
      sideBar: '#0d3064',
      textMain: '#90622E',
      primary: '#C82127',
      green: '#239f40',
    },
    extend: {
      colors: {
        black70: 'rgba(0, 0, 0, 0.7)',
        black60: 'rgba(0, 0, 0, 0.6)',
        black50: 'rgba(0, 0, 0, 0.5)',
        black40: 'rgba(0, 0, 0, 0.4)',
        black30: 'rgba(0, 0, 0, 0.3)',
        black20: 'rgba(0, 0, 0, 0.2)',
        black10: 'rgba(0, 0, 0, 0.1)',
        gray10: '#F0F0F0',
        gray20: '#BFBFBF',
        gray30: '#595959',
        gray40: '#373737',
        gray50: '#858585',
        gray60: '#9D9D9D',
        gray70: '#767676',
        gray80: '#4E4E4E',
        gray90: '#595959',
      },
      boxShadow: {
        custom:
          '0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.12), 0 1px 5px 0 rgba(0,0,0,.20)',
        sidebar:
          '0 16px 16px 0 rgba(0,0,0,.04), 0 1px 5px 0 rgba(0,0,0,.02), 0 3px 1px -2px rgba(0,0,0,.02)',
        statistic:
          '0 4px 5px 0 rgba(0,0,0,.14), 0 1px 10px 0 rgba(0,0,0,.12), 0 2px 4px -1px rgba(0,0,0,.3)',
        input: '0 0 0 2px rgba(246,67,2,.2)',
      },
      backgroundImage: {
        custom:
          'repeating-linear-gradient(-45deg, transparent 0px, transparent 7px, rgba(0,0,0,0.1) 7px, rgba(0,0,0,0.1) 9px)',
      },
    },
    screens: {
      // => @media (min-width: 640px) { ... }
      xxs: '380px',
      xs: '480px',
      ...defaultTheme.screens,
    },
    // fontSize: {
    //   xs: "12px",
    // },
  },
  plugins: [],
}
